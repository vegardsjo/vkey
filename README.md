# vkey virtual key remapper

# Setup/Install
* Change `VIRTUAL_KEYBOARD_NAME` const in `vkey.c` to your keyboard device
* run `build.sh`
* Modify `ExecStart` parameter in `vkey.service` to absolute path of `vkey`
* Install the systemd service: `cp vkey.service /etc/systemd/system && systemctl enable vkey.service`
* Use keyboard.xkb as a keyboard "description":

### Using sway
```
input 0:0:vkey_keyboard {
  repeat_delay 220
  repeat_rate 20
  xkb_file PATH_TO_XKB_FILE
}
```

## Mappings:
* Caps Lock to Escape/Control (mod-tap)
* Escape to Left Control
* Left Control to Super/Windows_L
* Left Alt to Print/SysRq to Hyper_L
* Right Alt + _key_ to various accents and norwegian letters:
  * ; to æ
  * o to ø
  * a to å
  * e to é
  * w to è
  * i to ó
  * p to ô
  * u to ò
  * k to ö
  * F10 to Volume mute
  * F11 to Volume down
  * F12 to Volume up
  * Insert to restart vkey
  * Delete to 😖
