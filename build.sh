#!/bin/sh -x
#gcc -O3 vkey.c -o vkey $(pkg-config --libs --cflags libevdev)
gcc -O3 vkey-new-magic.c -o vkey-new-magic $(pkg-config --libs --cflags libevdev)
gcc -O3 vkey-numpad.c -o vkey-numpad $(pkg-config --libs --cflags libevdev)
ln -sf vkey-new-magic vkey
