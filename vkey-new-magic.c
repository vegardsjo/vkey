#include <time.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include <stdarg.h>
#include <unistd.h>
#include <errno.h>
#include <fcntl.h>
#include <assert.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <linux/input.h>
#include <libevdev/libevdev.h>
#include <libevdev/libevdev-uinput.h>

const bool DEBUG = false;
const char *VIRTUAL_KEYBOARD_NAME = "vkey keyboard";
const char *FILENAME = "/dev/input/by-id/usb-Apple_Inc._Magic_Keyboard_F0T1353007N18DTAN-if01-event-kbd";
//const char *FILENAME = "/dev/input/by-id/usb-Apple_Inc._Magic_Keyboard_F0T5407008DGD6LA4-if01-event-kbd";

const struct timespec *SLEEP_TIME = &(struct timespec){.tv_nsec = 3 * 100000}; // 0.3 ms

bool caps_modtap = true;
bool caps_pressed = false;
unsigned caps_repeat = 0;
bool non_caps_key = false;
bool ralt_pressed = false;
bool ctrl_key_sent = false;
bool custom_keydown_sent = false;

const int ralt_key = KEY_RIGHTMETA;

void debug(const char *format, ...) {
  if (!DEBUG) return;

  va_list ap;
  va_start(ap, format);
  vprintf(format, ap);
  va_end(ap);
}

struct libevdev_uinput *create_device(struct libevdev *orig_dev) {
  (void)orig_dev; // unused for now

  struct libevdev *dev = libevdev_new();
  libevdev_set_phys(dev, "vkey-uinput");
  libevdev_set_name(dev, VIRTUAL_KEYBOARD_NAME);

  /*
    libevdev_set_id_vendor(device, 0x1);
    libevdev_set_id_product(device, 0x1);
    libevdev_set_id_version(device, 0x1);
    libevdev_set_id_bustype(device, BUS_USB);
  */

  printf("Created device: \"%s\"\n", libevdev_get_name(dev));
  printf("Device ID: bus %#x vendor %#x product %#x\n",
         libevdev_get_id_bustype(dev),
         libevdev_get_id_vendor(dev),
         libevdev_get_id_product(dev));

  for (unsigned int code = 0; code < 0x230; code++) {
    if (libevdev_enable_event_code(dev, EV_KEY, code, NULL)) {
      fprintf(stderr, "Could not enable key %s\n", libevdev_event_code_get_name(EV_KEY, code));
    }
  }
  
  /*
  for (unsigned int code = 0; code < SW_CNT; code++) {
    if (code == SW_RFKILL_ALL) continue;

    if (libevdev_enable_event_code(dev, EV_SW, code, NULL)) {
      fprintf(stderr, "Could not enable key %s\n", libevdev_event_code_get_name(EV_SW, code));
    }
  }

  for (unsigned int code = 0; code < REL_CNT; code++) {
    if (libevdev_enable_event_code(dev, EV_REL, code, NULL)) {
      fprintf(stderr, "Could not enable key %s\n", libevdev_event_code_get_name(EV_REL, code));
    }
  }

  if (libevdev_enable_event_code(dev, EV_LED, LED_CAPSL, NULL)) {
    fprintf(stderr, "Could not enable key %s\n", libevdev_event_code_get_name(EV_LED, LED_CAPSL));
  }
  */

  struct libevdev_uinput *uidev;
  if (libevdev_uinput_create_from_device(dev, LIBEVDEV_UINPUT_OPEN_MANAGED, &uidev) != 0) {
    fprintf(stderr, "Could not create new device");
    return NULL;
  }

  return uidev;
}

void send_custom_key(struct libevdev_uinput *uidev, int code, int value) {
  libevdev_uinput_write_event(uidev, EV_KEY, code, value);
  libevdev_uinput_write_event(uidev, EV_SYN, SYN_REPORT, 0);
  custom_keydown_sent = value > 0;

  debug("[send_custom_key] sent %s %d, custom_keydown_sent=%d\n",
        libevdev_event_code_get_name(EV_KEY, code),
        value,
        custom_keydown_sent);
}

void send_key(struct libevdev_uinput *uidev, int code, int value) {
  libevdev_uinput_write_event(uidev, EV_KEY, code, value);
  libevdev_uinput_write_event(uidev, EV_SYN, SYN_REPORT, 0);
}

int main(void) {
  struct libevdev *dev = NULL;

  int fd = open(FILENAME, O_RDONLY|O_NONBLOCK);
  int rc = libevdev_new_from_fd(fd, &dev);

  if (rc < 0) {
    fprintf(stderr, "Failed to init libevdev (%s)\n", strerror(-rc));
    exit(1);
  }

  printf("Input device name: \"%s\"\n", libevdev_get_name(dev));
  printf("Input device ID: bus %#x vendor %#x product %#x\n",
         libevdev_get_id_bustype(dev),
         libevdev_get_id_vendor(dev),
         libevdev_get_id_product(dev));

  if (libevdev_grab(dev, LIBEVDEV_GRAB)) {
    fprintf(stderr, "Could not grab device %s\n", FILENAME);
    exit(1);
  }

  struct libevdev_uinput *uidev = create_device(dev);
  if (!uidev) {
    exit(1);
  }

  do {
    struct input_event ev;
    rc = libevdev_next_event(dev, LIBEVDEV_READ_FLAG_NORMAL, &ev);
    if (rc == 0) {
      int type = ev.type;
      int code = ev.code;
      int value = ev.value;

      if (type == EV_KEY) {
        if (code == KEY_CAPSLOCK) {
          if (caps_modtap) {
            if (value == 1) {
              caps_repeat = false;
              if (caps_pressed) {
                continue;
              }

              caps_pressed = true;
              debug("sending control down\n");
              libevdev_uinput_write_event(uidev, type, KEY_LEFTCTRL, 1);
              libevdev_uinput_write_event(uidev, EV_SYN, SYN_REPORT, 0);
              non_caps_key = false;
              continue;
            } else if (value == 2) {
              caps_repeat++;
            } else {
              debug("caps released\n");
              caps_pressed = false;
              debug("sending ctrl up\n");
              libevdev_uinput_write_event(uidev, type, KEY_LEFTCTRL, 0);
              libevdev_uinput_write_event(uidev, EV_SYN, SYN_REPORT, 0);

              if (!non_caps_key &&  caps_repeat < 10) {
                debug("sending esc down\n");
                libevdev_uinput_write_event(uidev, type, KEY_ESC, 1);
                libevdev_uinput_write_event(uidev, EV_SYN, SYN_REPORT, 0);
                debug("sending esc up\n");
                libevdev_uinput_write_event(uidev, type, KEY_ESC, 0);
                libevdev_uinput_write_event(uidev, EV_SYN, SYN_REPORT, 0);
              }

              caps_repeat = 0;
            }
          } else {
            send_key(uidev, KEY_LEFTCTRL, ev.value);
          }

          continue;
        }

        if (value > 0) {
          non_caps_key = true;
        }

        if (code == ralt_key) {
          ralt_pressed = value;
          continue;
        }
    
        if (code == KEY_ESC && caps_modtap && !ralt_pressed) {
          send_key(uidev, KEY_LEFTCTRL, ev.value);
          continue;
        }

        if (code == KEY_LEFTCTRL) {
          send_key(uidev, KEY_LEFTMETA, ev.value);
          continue;
        }

        if (code == KEY_LEFTALT) {
          send_key(uidev, KEY_SYSRQ, ev.value);
          continue;
        }

        if (code == KEY_LEFTMETA) {
          send_key(uidev, KEY_LEFTALT, ev.value);
          continue;
        }

        if (code == KEY_LEFTCTRL) {
          send_key(uidev, KEY_SYSRQ, ev.value);
          continue;
        }

        // Apple keyboard: section to grave/tilde
        /* if (code == KEY_102ND) { */
        /*   send_key(uidev, KEY_GRAVE, ev.value); */
        /*   continue; */
        /* } */
        
        
        // Apple keyboard: less/greater to sysrq (hyper)
        if (code == KEY_102ND) {
          send_key(uidev, KEY_SYSRQ, ev.value);
          continue;
        }

        if (ralt_pressed || custom_keydown_sent) {
          // æ
          if (code == KEY_SEMICOLON) {
            send_custom_key(uidev, KEY_KP0, ev.value);
            continue;
          }

          // ø
          if (code == KEY_O) {
            send_custom_key(uidev, KEY_KP1, ev.value);
            continue;
          }

          // å
          if (code == KEY_A) {
            send_custom_key(uidev, KEY_KP2, ev.value);
            continue;
          }

          // é
          if (code == KEY_E) {
            send_custom_key(uidev, KEY_KP3, ev.value);
            continue;
          }

          // è
          if (code == KEY_W) {
            send_custom_key(uidev, KEY_KP4, ev.value);
            continue;
          }

          // ó
          if (code == KEY_I) {
            send_custom_key(uidev, KEY_KP5, ev.value);
            continue;
          }

          // ô
          if (code == KEY_P) {
            send_custom_key(uidev, KEY_KP6, ev.value);
            continue;
          }

          // ò
          if (code == KEY_J) {
            send_custom_key(uidev, KEY_KP7, ev.value);
            continue;
          }

          // ö
          if (code == KEY_L) {
            send_custom_key(uidev, KEY_KP8, ev.value);
            continue;
          }

          // ü
          if (code == KEY_U) {
            send_custom_key(uidev, KEY_KP9, ev.value);
            continue;
          }
          
          // ä
          if (code == KEY_S) {
            send_custom_key(uidev, KEY_KPPLUS, ev.value);
            continue;
          }
        
          if (code == KEY_SCREENLOCK) {
            custom_keydown_sent = ev.value > 0;
            if (ev.value == 0) {
              caps_modtap = !caps_modtap;
              libevdev_uinput_write_event(uidev, EV_LED, LED_CAPSL, !caps_modtap);
              libevdev_uinput_write_event(uidev, EV_SYN, SYN_REPORT, 0);
            }

            continue;
          }
          
          if (code == KEY_F10) {
            send_custom_key(uidev, KEY_MUTE, ev.value);
            continue;
          }

          if (code == KEY_F11) {
            send_custom_key(uidev, KEY_VOLUMEDOWN, ev.value);
            continue;
          }

          if (code == KEY_F12) {
            send_custom_key(uidev, KEY_VOLUMEUP, ev.value);
            continue;
          }

          if (code == KEY_INSERT) {
            debug("User exit\n");
            exit(0);
          }
        }

        if (code == KEY_SCREENLOCK) {
          send_key(uidev, KEY_DELETE, ev.value);
          continue;
        }
      }
        
      debug("%s %s %d\n",
          libevdev_event_type_get_name(type),
          libevdev_event_code_get_name(type, code),
          value);

      libevdev_uinput_write_event(uidev, type, code, value);
    }

    nanosleep(SLEEP_TIME, NULL);

  } while(rc == 1 || rc == 0 || rc == -EAGAIN);

  libevdev_uinput_destroy(uidev);
  libevdev_free(dev);
}
