#include <time.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include <stdarg.h>
#include <unistd.h>
#include <errno.h>
#include <fcntl.h>
#include <assert.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <linux/input.h>
#include <libevdev/libevdev.h>
#include <libevdev/libevdev-uinput.h>

const bool DEBUG = true;
const char *VIRTUAL_KEYBOARD_NAME = "vkey numpad";
const char *FILENAME = "/dev/input/by-id/usb-04d9_1203-event-kbd";

const struct timespec *SLEEP_TIME = &(struct timespec){.tv_nsec = 3 * 100000}; // 0.3 ms

bool caps_pressed = false;
bool caps_repeat = false;
bool non_caps_key = false;
bool ralt_pressed = false;
bool ctrl_key_sent = false;
bool custom_keydown_sent = false;

const int ralt_key = KEY_RIGHTMETA;

void debug(const char *format, ...) {
  if (!DEBUG) return;

  va_list ap;
  va_start(ap, format);
  vprintf(format, ap);
  va_end(ap);
}

struct libevdev_uinput *create_device(struct libevdev *orig_dev) {
  (void)orig_dev; // unused for now

  int uifd = open("/dev/uinput", O_RDWR);
  if (uifd < 0) {
    fprintf(stderr, "Could not open uinput device for reading+writing\n");
    return NULL;
  }

  struct libevdev *dev = libevdev_new();
  libevdev_set_name(dev, VIRTUAL_KEYBOARD_NAME);

  printf("Created device: \"%s\"\n", libevdev_get_name(dev));
  printf("Device ID: bus %#x vendor %#x product %#x\n",
         libevdev_get_id_bustype(dev),
         libevdev_get_id_vendor(dev),
         libevdev_get_id_product(dev));

  for (unsigned int code = 0; code < KEY_MAX; code++) {
    if (libevdev_enable_event_code(dev, EV_KEY, code, NULL)) {
      fprintf(stderr, "Could not enable key %s\n", libevdev_event_code_get_name(EV_KEY, code));
    }
  }

  for (unsigned int code = 0; code < SW_MAX; code++) {
    if (code == SW_RFKILL_ALL) continue;

    if (libevdev_enable_event_code(dev, EV_SW, code, NULL)) {
      fprintf(stderr, "Could not enable key %s\n", libevdev_event_code_get_name(EV_SW, code));
    }
  }

  for (unsigned int code = 0; code < REL_MAX; code++) {
    if (libevdev_enable_event_code(dev, EV_REL, code, NULL)) {
      fprintf(stderr, "Could not enable key %s\n", libevdev_event_code_get_name(EV_REL, code));
    }
  }

  struct libevdev_uinput *uidev;
  if (libevdev_uinput_create_from_device(dev, uifd, &uidev) != 0) {
    fprintf(stderr, "Could not create new device");
    return NULL;
  }

  return uidev;
}

void send_custom_key(struct libevdev_uinput *uidev, int code, int value) {
  libevdev_uinput_write_event(uidev, EV_KEY, code, value);
  libevdev_uinput_write_event(uidev, EV_SYN, SYN_REPORT, 0);
  custom_keydown_sent = value > 0;

  debug("[send_custom_key] sent %s %d, custom_keydown_sent=%d\n",
        libevdev_event_code_get_name(EV_KEY, code),
        value,
        custom_keydown_sent);
}

void send_key(struct libevdev_uinput *uidev, int code, int value) {
  libevdev_uinput_write_event(uidev, EV_KEY, code, value);
  libevdev_uinput_write_event(uidev, EV_SYN, SYN_REPORT, 0);
}

int main(void) {
  struct libevdev *dev = NULL;

  int fd = open(FILENAME, O_RDONLY|O_NONBLOCK);
  int rc = libevdev_new_from_fd(fd, &dev);

  if (rc < 0) {
    fprintf(stderr, "Failed to init libevdev (%s)\n", strerror(-rc));
    exit(1);
  }

  printf("Input device name: \"%s\"\n", libevdev_get_name(dev));
  printf("Input device ID: bus %#x vendor %#x product %#x\n",
         libevdev_get_id_bustype(dev),
         libevdev_get_id_vendor(dev),
         libevdev_get_id_product(dev));

  if (libevdev_grab(dev, LIBEVDEV_GRAB)) {
    fprintf(stderr, "Could not grab device %s\n", FILENAME);
    exit(1);
  }

  struct libevdev_uinput *uidev = create_device(dev);
  if (!uidev) {
    exit(1);
  }

  do {
    struct input_event ev;
    rc = libevdev_next_event(dev, LIBEVDEV_READ_FLAG_NORMAL, &ev);

    if (rc == 0) {
      int type = ev.type;
      int code = ev.code;
      int value = ev.value;

      if (type == EV_KEY) {
        debug("%s %s %d\n",
              libevdev_event_type_get_name(type),
              libevdev_event_code_get_name(type, code),
              value);

        switch (code) {
          case KEY_BACKSPACE:
            send_custom_key(uidev, KEY_LEFTALT, ev.value);
            continue;
          default:
            break;
        }
      }

      libevdev_uinput_write_event(uidev, type, code, value);
    }

    nanosleep(SLEEP_TIME, NULL);

  } while(rc == 1 || rc == 0 || rc == -EAGAIN);

  libevdev_uinput_destroy(uidev);
  libevdev_free(dev);
}
