


        import core.stdc.config;
        import core.stdc.stdarg: va_list;
        static import core.simd;
        static import std.conv;

        struct Int128 { long lower; long upper; }
        struct UInt128 { ulong lower; ulong upper; }

        struct __locale_data { int dummy; }



alias _Bool = bool;
struct dpp {
    static struct Opaque(int N) {
        void[N] bytes;
    }

    static bool isEmpty(T)() {
        return T.tupleof.length == 0;
    }
    static struct Move(T) {
        T* ptr;
    }


    static auto move(T)(ref T value) {
        return Move!T(&value);
    }
    mixin template EnumD(string name, T, string prefix) if(is(T == enum)) {
        private static string _memberMixinStr(string member) {
            import std.conv: text;
            import std.array: replace;
            return text(` `, member.replace(prefix, ""), ` = `, T.stringof, `.`, member, `,`);
        }
        private static string _enumMixinStr() {
            import std.array: join;
            string[] ret;
            ret ~= "enum " ~ name ~ "{";
            static foreach(member; __traits(allMembers, T)) {
                ret ~= _memberMixinStr(member);
            }
            ret ~= "}";
            return ret.join("\n");
        }
        mixin(_enumMixinStr());
    }
}

extern(C)
{
    alias size_t = c_ulong;
    int getentropy(void*, c_ulong) @nogc nothrow;
    char* crypt(const(char)*, const(char)*) @nogc nothrow;
    int fdatasync(int) @nogc nothrow;
    int lockf(int, int, c_long) @nogc nothrow;
    c_long syscall(c_long, ...) @nogc nothrow;
    void* sbrk(c_long) @nogc nothrow;
    int brk(void*) @nogc nothrow;
    int ftruncate(int, c_long) @nogc nothrow;
    int truncate(const(char)*, c_long) @nogc nothrow;
    int getdtablesize() @nogc nothrow;
    int getpagesize() @nogc nothrow;
    void sync() @nogc nothrow;
    c_long gethostid() @nogc nothrow;
    int fsync(int) @nogc nothrow;
    char* getpass(const(char)*) @nogc nothrow;
    int chroot(const(char)*) @nogc nothrow;
    int daemon(int, int) @nogc nothrow;
    void setusershell() @nogc nothrow;
    void endusershell() @nogc nothrow;
    char* getusershell() @nogc nothrow;
    int acct(const(char)*) @nogc nothrow;
    int profil(ushort*, c_ulong, c_ulong, uint) @nogc nothrow;
    int revoke(const(char)*) @nogc nothrow;
    int vhangup() @nogc nothrow;
    int setdomainname(const(char)*, c_ulong) @nogc nothrow;
    int getdomainname(char*, c_ulong) @nogc nothrow;
    int sethostid(c_long) @nogc nothrow;
    int sethostname(const(char)*, c_ulong) @nogc nothrow;
    int gethostname(char*, c_ulong) @nogc nothrow;
    int setlogin(const(char)*) @nogc nothrow;
    int getlogin_r(char*, c_ulong) @nogc nothrow;
    char* getlogin() @nogc nothrow;
    int tcsetpgrp(int, int) @nogc nothrow;
    int tcgetpgrp(int) @nogc nothrow;
    int rmdir(const(char)*) @nogc nothrow;
    int unlinkat(int, const(char)*, int) @nogc nothrow;
    int unlink(const(char)*) @nogc nothrow;
    c_long readlinkat(int, const(char)*, char*, c_ulong) @nogc nothrow;
    int symlinkat(const(char)*, int, const(char)*) @nogc nothrow;
    c_long readlink(const(char)*, char*, c_ulong) @nogc nothrow;
    int symlink(const(char)*, const(char)*) @nogc nothrow;
    int linkat(int, const(char)*, int, const(char)*, int) @nogc nothrow;
    int link(const(char)*, const(char)*) @nogc nothrow;
    int ttyslot() @nogc nothrow;
    int isatty(int) @nogc nothrow;
    int ttyname_r(int, char*, c_ulong) @nogc nothrow;
    char* ttyname(int) @nogc nothrow;
    int vfork() @nogc nothrow;
    int fork() @nogc nothrow;
    int setegid(uint) @nogc nothrow;
    int setregid(uint, uint) @nogc nothrow;
    int setgid(uint) @nogc nothrow;
    int seteuid(uint) @nogc nothrow;
    int setreuid(uint, uint) @nogc nothrow;
    int setuid(uint) @nogc nothrow;
    int getgroups(int, uint*) @nogc nothrow;
    uint getegid() @nogc nothrow;
    uint getgid() @nogc nothrow;
    uint geteuid() @nogc nothrow;
    uint getuid() @nogc nothrow;
    int getsid(int) @nogc nothrow;
    int setsid() @nogc nothrow;
    int setpgrp() @nogc nothrow;
    int setpgid(int, int) @nogc nothrow;
    int getpgid(int) @nogc nothrow;
    int __getpgid(int) @nogc nothrow;
    int getpgrp() @nogc nothrow;
    int getppid() @nogc nothrow;
    int getpid() @nogc nothrow;
    c_ulong confstr(int, char*, c_ulong) @nogc nothrow;
    c_long sysconf(int) @nogc nothrow;
    c_long fpathconf(int, int) @nogc nothrow;
    c_long pathconf(const(char)*, int) @nogc nothrow;
    void _exit(int) @nogc nothrow;
    int nice(int) @nogc nothrow;
    int execlp(const(char)*, const(char)*, ...) @nogc nothrow;
    int execvp(const(char)*, char**) @nogc nothrow;
    int execl(const(char)*, const(char)*, ...) @nogc nothrow;
    int execle(const(char)*, const(char)*, ...) @nogc nothrow;
    int execv(const(char)*, char**) @nogc nothrow;
    int fexecve(int, char**, char**) @nogc nothrow;
    int execve(const(char)*, char**, char**) @nogc nothrow;
    extern __gshared char** __environ;
    int dup2(int, int) @nogc nothrow;
    int dup(int) @nogc nothrow;
    char* getwd(char*) @nogc nothrow;
    char* getcwd(char*, c_ulong) @nogc nothrow;
    int fchdir(int) @nogc nothrow;
    int chdir(const(char)*) @nogc nothrow;
    int fchownat(int, const(char)*, uint, uint, int) @nogc nothrow;
    int lchown(const(char)*, uint, uint) @nogc nothrow;
    int fchown(int, uint, uint) @nogc nothrow;
    int chown(const(char)*, uint, uint) @nogc nothrow;
    int pause() @nogc nothrow;
    int usleep(uint) @nogc nothrow;
    uint ualarm(uint, uint) @nogc nothrow;
    uint sleep(uint) @nogc nothrow;
    uint alarm(uint) @nogc nothrow;
    int pipe(int*) @nogc nothrow;
    c_long pwrite(int, const(void)*, c_ulong, c_long) @nogc nothrow;
    c_long pread(int, void*, c_ulong, c_long) @nogc nothrow;
    c_long write(int, const(void)*, c_ulong) @nogc nothrow;
    c_long read(int, void*, c_ulong) @nogc nothrow;
    int close(int) @nogc nothrow;
    c_long lseek(int, c_long, int) @nogc nothrow;
    int faccessat(int, const(char)*, int, int) @nogc nothrow;
    int access(const(char)*, int) @nogc nothrow;
    alias socklen_t = uint;
    alias intptr_t = c_long;
    alias pid_t = int;
    alias useconds_t = uint;
    alias off_t = c_long;
    alias uid_t = uint;
    alias gid_t = uint;
    alias ssize_t = c_long;
    alias fsfilcnt_t = c_ulong;
    alias fsblkcnt_t = c_ulong;
    alias blkcnt_t = c_long;
    alias blksize_t = c_long;
    alias register_t = c_long;
    alias u_int64_t = c_ulong;
    alias u_int32_t = uint;
    alias u_int16_t = ushort;
    alias u_int8_t = ubyte;
    alias key_t = int;
    alias caddr_t = char*;
    alias daddr_t = int;
    alias id_t = uint;
    alias nlink_t = c_ulong;
    alias dev_t = c_ulong;
    alias ino_t = c_ulong;
    alias loff_t = c_long;
    alias fsid_t = __fsid_t;
    alias u_quad_t = c_ulong;
    alias quad_t = c_long;
    alias u_long = c_ulong;
    alias u_int = uint;
    alias u_short = ushort;
    alias u_char = ubyte;
    int futimes(int, const(timeval)*) @nogc nothrow;
    int lutimes(const(char)*, const(timeval)*) @nogc nothrow;
    int utimes(const(char)*, const(timeval)*) @nogc nothrow;
    int setitimer(int, const(itimerval)*, itimerval*) @nogc nothrow;
    int getitimer(int, itimerval*) @nogc nothrow;
    alias __itimer_which_t = int;
    struct itimerval
    {
        timeval it_interval;
        timeval it_value;
    }
    enum __itimer_which
    {
        ITIMER_REAL = 0,
        ITIMER_VIRTUAL = 1,
        ITIMER_PROF = 2,
    }
    enum ITIMER_REAL = __itimer_which.ITIMER_REAL;
    enum ITIMER_VIRTUAL = __itimer_which.ITIMER_VIRTUAL;
    enum ITIMER_PROF = __itimer_which.ITIMER_PROF;
    int adjtime(const(timeval)*, timeval*) @nogc nothrow;
    int settimeofday(const(timeval)*, const(timezone)*) @nogc nothrow;
    alias __s8 = byte;
    alias __u8 = ubyte;
    alias __s16 = short;
    alias __u16 = ushort;
    alias __s32 = int;
    alias __u32 = uint;
    alias __s64 = long;
    alias __u64 = ulong;
    int gettimeofday(timeval*, void*) @nogc nothrow;
    struct timezone
    {
        int tz_minuteswest;
        int tz_dsttime;
    }
    alias suseconds_t = c_long;
    int pselect(int, fd_set*, fd_set*, fd_set*, const(timespec)*, const(__sigset_t)*) @nogc nothrow;
    int select(int, fd_set*, fd_set*, fd_set*, timeval*) @nogc nothrow;
    alias fd_mask = c_long;
    struct fd_set
    {
        c_long[16] __fds_bits;
    }
    alias __fd_mask = c_long;
    int ioctl(int, c_ulong, ...) @nogc nothrow;
    int strncasecmp_l(const(char)*, const(char)*, c_ulong, __locale_struct*) @nogc nothrow;
    int strcasecmp_l(const(char)*, const(char)*, __locale_struct*) @nogc nothrow;
    alias __kernel_long_t = c_long;
    alias __kernel_ulong_t = c_ulong;
    alias __kernel_ino_t = c_ulong;
    alias __kernel_mode_t = uint;
    alias __kernel_pid_t = int;
    alias __kernel_ipc_pid_t = int;
    alias __kernel_uid_t = uint;
    alias __kernel_gid_t = uint;
    alias __kernel_suseconds_t = c_long;
    alias __kernel_daddr_t = int;
    alias __kernel_uid32_t = uint;
    alias __kernel_gid32_t = uint;
    alias __kernel_size_t = c_ulong;
    alias __kernel_ssize_t = c_long;
    alias __kernel_ptrdiff_t = c_long;
    struct __kernel_fsid_t
    {
        int[2] val;
    }
    alias __kernel_off_t = c_long;
    alias __kernel_loff_t = long;
    alias __kernel_old_time_t = c_long;
    alias __kernel_time_t = c_long;
    alias __kernel_time64_t = long;
    alias __kernel_clock_t = c_long;
    alias __kernel_timer_t = int;
    alias __kernel_clockid_t = int;
    alias __kernel_caddr_t = char*;
    alias __kernel_uid16_t = ushort;
    alias __kernel_gid16_t = ushort;
    int strncasecmp(const(char)*, const(char)*, c_ulong) @nogc nothrow;
    alias __kernel_old_uid_t = ushort;
    alias __kernel_old_gid_t = ushort;
    int strcasecmp(const(char)*, const(char)*) @nogc nothrow;
    alias __kernel_old_dev_t = c_ulong;
    int ffsll(long) @nogc nothrow;
    static ushort __bswap_16(ushort) @nogc nothrow;
    static uint __bswap_32(uint) @nogc nothrow;
    static c_ulong __bswap_64(c_ulong) @nogc nothrow;
    enum _Anonymous_0
    {
        _PC_LINK_MAX = 0,
        _PC_MAX_CANON = 1,
        _PC_MAX_INPUT = 2,
        _PC_NAME_MAX = 3,
        _PC_PATH_MAX = 4,
        _PC_PIPE_BUF = 5,
        _PC_CHOWN_RESTRICTED = 6,
        _PC_NO_TRUNC = 7,
        _PC_VDISABLE = 8,
        _PC_SYNC_IO = 9,
        _PC_ASYNC_IO = 10,
        _PC_PRIO_IO = 11,
        _PC_SOCK_MAXBUF = 12,
        _PC_FILESIZEBITS = 13,
        _PC_REC_INCR_XFER_SIZE = 14,
        _PC_REC_MAX_XFER_SIZE = 15,
        _PC_REC_MIN_XFER_SIZE = 16,
        _PC_REC_XFER_ALIGN = 17,
        _PC_ALLOC_SIZE_MIN = 18,
        _PC_SYMLINK_MAX = 19,
        _PC_2_SYMLINKS = 20,
    }
    enum _PC_LINK_MAX = _Anonymous_0._PC_LINK_MAX;
    enum _PC_MAX_CANON = _Anonymous_0._PC_MAX_CANON;
    enum _PC_MAX_INPUT = _Anonymous_0._PC_MAX_INPUT;
    enum _PC_NAME_MAX = _Anonymous_0._PC_NAME_MAX;
    enum _PC_PATH_MAX = _Anonymous_0._PC_PATH_MAX;
    enum _PC_PIPE_BUF = _Anonymous_0._PC_PIPE_BUF;
    enum _PC_CHOWN_RESTRICTED = _Anonymous_0._PC_CHOWN_RESTRICTED;
    enum _PC_NO_TRUNC = _Anonymous_0._PC_NO_TRUNC;
    enum _PC_VDISABLE = _Anonymous_0._PC_VDISABLE;
    enum _PC_SYNC_IO = _Anonymous_0._PC_SYNC_IO;
    enum _PC_ASYNC_IO = _Anonymous_0._PC_ASYNC_IO;
    enum _PC_PRIO_IO = _Anonymous_0._PC_PRIO_IO;
    enum _PC_SOCK_MAXBUF = _Anonymous_0._PC_SOCK_MAXBUF;
    enum _PC_FILESIZEBITS = _Anonymous_0._PC_FILESIZEBITS;
    enum _PC_REC_INCR_XFER_SIZE = _Anonymous_0._PC_REC_INCR_XFER_SIZE;
    enum _PC_REC_MAX_XFER_SIZE = _Anonymous_0._PC_REC_MAX_XFER_SIZE;
    enum _PC_REC_MIN_XFER_SIZE = _Anonymous_0._PC_REC_MIN_XFER_SIZE;
    enum _PC_REC_XFER_ALIGN = _Anonymous_0._PC_REC_XFER_ALIGN;
    enum _PC_ALLOC_SIZE_MIN = _Anonymous_0._PC_ALLOC_SIZE_MIN;
    enum _PC_SYMLINK_MAX = _Anonymous_0._PC_SYMLINK_MAX;
    enum _PC_2_SYMLINKS = _Anonymous_0._PC_2_SYMLINKS;
    int ffsl(c_long) @nogc nothrow;
    int ffs(int) @nogc nothrow;
    char* rindex(const(char)*, int) @nogc nothrow;
    char* index(const(char)*, int) @nogc nothrow;
    void bzero(void*, c_ulong) @nogc nothrow;
    void bcopy(const(void)*, void*, c_ulong) @nogc nothrow;
    enum _Anonymous_1
    {
        _SC_ARG_MAX = 0,
        _SC_CHILD_MAX = 1,
        _SC_CLK_TCK = 2,
        _SC_NGROUPS_MAX = 3,
        _SC_OPEN_MAX = 4,
        _SC_STREAM_MAX = 5,
        _SC_TZNAME_MAX = 6,
        _SC_JOB_CONTROL = 7,
        _SC_SAVED_IDS = 8,
        _SC_REALTIME_SIGNALS = 9,
        _SC_PRIORITY_SCHEDULING = 10,
        _SC_TIMERS = 11,
        _SC_ASYNCHRONOUS_IO = 12,
        _SC_PRIORITIZED_IO = 13,
        _SC_SYNCHRONIZED_IO = 14,
        _SC_FSYNC = 15,
        _SC_MAPPED_FILES = 16,
        _SC_MEMLOCK = 17,
        _SC_MEMLOCK_RANGE = 18,
        _SC_MEMORY_PROTECTION = 19,
        _SC_MESSAGE_PASSING = 20,
        _SC_SEMAPHORES = 21,
        _SC_SHARED_MEMORY_OBJECTS = 22,
        _SC_AIO_LISTIO_MAX = 23,
        _SC_AIO_MAX = 24,
        _SC_AIO_PRIO_DELTA_MAX = 25,
        _SC_DELAYTIMER_MAX = 26,
        _SC_MQ_OPEN_MAX = 27,
        _SC_MQ_PRIO_MAX = 28,
        _SC_VERSION = 29,
        _SC_PAGESIZE = 30,
        _SC_RTSIG_MAX = 31,
        _SC_SEM_NSEMS_MAX = 32,
        _SC_SEM_VALUE_MAX = 33,
        _SC_SIGQUEUE_MAX = 34,
        _SC_TIMER_MAX = 35,
        _SC_BC_BASE_MAX = 36,
        _SC_BC_DIM_MAX = 37,
        _SC_BC_SCALE_MAX = 38,
        _SC_BC_STRING_MAX = 39,
        _SC_COLL_WEIGHTS_MAX = 40,
        _SC_EQUIV_CLASS_MAX = 41,
        _SC_EXPR_NEST_MAX = 42,
        _SC_LINE_MAX = 43,
        _SC_RE_DUP_MAX = 44,
        _SC_CHARCLASS_NAME_MAX = 45,
        _SC_2_VERSION = 46,
        _SC_2_C_BIND = 47,
        _SC_2_C_DEV = 48,
        _SC_2_FORT_DEV = 49,
        _SC_2_FORT_RUN = 50,
        _SC_2_SW_DEV = 51,
        _SC_2_LOCALEDEF = 52,
        _SC_PII = 53,
        _SC_PII_XTI = 54,
        _SC_PII_SOCKET = 55,
        _SC_PII_INTERNET = 56,
        _SC_PII_OSI = 57,
        _SC_POLL = 58,
        _SC_SELECT = 59,
        _SC_UIO_MAXIOV = 60,
        _SC_IOV_MAX = 60,
        _SC_PII_INTERNET_STREAM = 61,
        _SC_PII_INTERNET_DGRAM = 62,
        _SC_PII_OSI_COTS = 63,
        _SC_PII_OSI_CLTS = 64,
        _SC_PII_OSI_M = 65,
        _SC_T_IOV_MAX = 66,
        _SC_THREADS = 67,
        _SC_THREAD_SAFE_FUNCTIONS = 68,
        _SC_GETGR_R_SIZE_MAX = 69,
        _SC_GETPW_R_SIZE_MAX = 70,
        _SC_LOGIN_NAME_MAX = 71,
        _SC_TTY_NAME_MAX = 72,
        _SC_THREAD_DESTRUCTOR_ITERATIONS = 73,
        _SC_THREAD_KEYS_MAX = 74,
        _SC_THREAD_STACK_MIN = 75,
        _SC_THREAD_THREADS_MAX = 76,
        _SC_THREAD_ATTR_STACKADDR = 77,
        _SC_THREAD_ATTR_STACKSIZE = 78,
        _SC_THREAD_PRIORITY_SCHEDULING = 79,
        _SC_THREAD_PRIO_INHERIT = 80,
        _SC_THREAD_PRIO_PROTECT = 81,
        _SC_THREAD_PROCESS_SHARED = 82,
        _SC_NPROCESSORS_CONF = 83,
        _SC_NPROCESSORS_ONLN = 84,
        _SC_PHYS_PAGES = 85,
        _SC_AVPHYS_PAGES = 86,
        _SC_ATEXIT_MAX = 87,
        _SC_PASS_MAX = 88,
        _SC_XOPEN_VERSION = 89,
        _SC_XOPEN_XCU_VERSION = 90,
        _SC_XOPEN_UNIX = 91,
        _SC_XOPEN_CRYPT = 92,
        _SC_XOPEN_ENH_I18N = 93,
        _SC_XOPEN_SHM = 94,
        _SC_2_CHAR_TERM = 95,
        _SC_2_C_VERSION = 96,
        _SC_2_UPE = 97,
        _SC_XOPEN_XPG2 = 98,
        _SC_XOPEN_XPG3 = 99,
        _SC_XOPEN_XPG4 = 100,
        _SC_CHAR_BIT = 101,
        _SC_CHAR_MAX = 102,
        _SC_CHAR_MIN = 103,
        _SC_INT_MAX = 104,
        _SC_INT_MIN = 105,
        _SC_LONG_BIT = 106,
        _SC_WORD_BIT = 107,
        _SC_MB_LEN_MAX = 108,
        _SC_NZERO = 109,
        _SC_SSIZE_MAX = 110,
        _SC_SCHAR_MAX = 111,
        _SC_SCHAR_MIN = 112,
        _SC_SHRT_MAX = 113,
        _SC_SHRT_MIN = 114,
        _SC_UCHAR_MAX = 115,
        _SC_UINT_MAX = 116,
        _SC_ULONG_MAX = 117,
        _SC_USHRT_MAX = 118,
        _SC_NL_ARGMAX = 119,
        _SC_NL_LANGMAX = 120,
        _SC_NL_MSGMAX = 121,
        _SC_NL_NMAX = 122,
        _SC_NL_SETMAX = 123,
        _SC_NL_TEXTMAX = 124,
        _SC_XBS5_ILP32_OFF32 = 125,
        _SC_XBS5_ILP32_OFFBIG = 126,
        _SC_XBS5_LP64_OFF64 = 127,
        _SC_XBS5_LPBIG_OFFBIG = 128,
        _SC_XOPEN_LEGACY = 129,
        _SC_XOPEN_REALTIME = 130,
        _SC_XOPEN_REALTIME_THREADS = 131,
        _SC_ADVISORY_INFO = 132,
        _SC_BARRIERS = 133,
        _SC_BASE = 134,
        _SC_C_LANG_SUPPORT = 135,
        _SC_C_LANG_SUPPORT_R = 136,
        _SC_CLOCK_SELECTION = 137,
        _SC_CPUTIME = 138,
        _SC_THREAD_CPUTIME = 139,
        _SC_DEVICE_IO = 140,
        _SC_DEVICE_SPECIFIC = 141,
        _SC_DEVICE_SPECIFIC_R = 142,
        _SC_FD_MGMT = 143,
        _SC_FIFO = 144,
        _SC_PIPE = 145,
        _SC_FILE_ATTRIBUTES = 146,
        _SC_FILE_LOCKING = 147,
        _SC_FILE_SYSTEM = 148,
        _SC_MONOTONIC_CLOCK = 149,
        _SC_MULTI_PROCESS = 150,
        _SC_SINGLE_PROCESS = 151,
        _SC_NETWORKING = 152,
        _SC_READER_WRITER_LOCKS = 153,
        _SC_SPIN_LOCKS = 154,
        _SC_REGEXP = 155,
        _SC_REGEX_VERSION = 156,
        _SC_SHELL = 157,
        _SC_SIGNALS = 158,
        _SC_SPAWN = 159,
        _SC_SPORADIC_SERVER = 160,
        _SC_THREAD_SPORADIC_SERVER = 161,
        _SC_SYSTEM_DATABASE = 162,
        _SC_SYSTEM_DATABASE_R = 163,
        _SC_TIMEOUTS = 164,
        _SC_TYPED_MEMORY_OBJECTS = 165,
        _SC_USER_GROUPS = 166,
        _SC_USER_GROUPS_R = 167,
        _SC_2_PBS = 168,
        _SC_2_PBS_ACCOUNTING = 169,
        _SC_2_PBS_LOCATE = 170,
        _SC_2_PBS_MESSAGE = 171,
        _SC_2_PBS_TRACK = 172,
        _SC_SYMLOOP_MAX = 173,
        _SC_STREAMS = 174,
        _SC_2_PBS_CHECKPOINT = 175,
        _SC_V6_ILP32_OFF32 = 176,
        _SC_V6_ILP32_OFFBIG = 177,
        _SC_V6_LP64_OFF64 = 178,
        _SC_V6_LPBIG_OFFBIG = 179,
        _SC_HOST_NAME_MAX = 180,
        _SC_TRACE = 181,
        _SC_TRACE_EVENT_FILTER = 182,
        _SC_TRACE_INHERIT = 183,
        _SC_TRACE_LOG = 184,
        _SC_LEVEL1_ICACHE_SIZE = 185,
        _SC_LEVEL1_ICACHE_ASSOC = 186,
        _SC_LEVEL1_ICACHE_LINESIZE = 187,
        _SC_LEVEL1_DCACHE_SIZE = 188,
        _SC_LEVEL1_DCACHE_ASSOC = 189,
        _SC_LEVEL1_DCACHE_LINESIZE = 190,
        _SC_LEVEL2_CACHE_SIZE = 191,
        _SC_LEVEL2_CACHE_ASSOC = 192,
        _SC_LEVEL2_CACHE_LINESIZE = 193,
        _SC_LEVEL3_CACHE_SIZE = 194,
        _SC_LEVEL3_CACHE_ASSOC = 195,
        _SC_LEVEL3_CACHE_LINESIZE = 196,
        _SC_LEVEL4_CACHE_SIZE = 197,
        _SC_LEVEL4_CACHE_ASSOC = 198,
        _SC_LEVEL4_CACHE_LINESIZE = 199,
        _SC_IPV6 = 235,
        _SC_RAW_SOCKETS = 236,
        _SC_V7_ILP32_OFF32 = 237,
        _SC_V7_ILP32_OFFBIG = 238,
        _SC_V7_LP64_OFF64 = 239,
        _SC_V7_LPBIG_OFFBIG = 240,
        _SC_SS_REPL_MAX = 241,
        _SC_TRACE_EVENT_NAME_MAX = 242,
        _SC_TRACE_NAME_MAX = 243,
        _SC_TRACE_SYS_MAX = 244,
        _SC_TRACE_USER_EVENT_MAX = 245,
        _SC_XOPEN_STREAMS = 246,
        _SC_THREAD_ROBUST_PRIO_INHERIT = 247,
        _SC_THREAD_ROBUST_PRIO_PROTECT = 248,
    }
    enum _SC_ARG_MAX = _Anonymous_1._SC_ARG_MAX;
    enum _SC_CHILD_MAX = _Anonymous_1._SC_CHILD_MAX;
    enum _SC_CLK_TCK = _Anonymous_1._SC_CLK_TCK;
    enum _SC_NGROUPS_MAX = _Anonymous_1._SC_NGROUPS_MAX;
    enum _SC_OPEN_MAX = _Anonymous_1._SC_OPEN_MAX;
    enum _SC_STREAM_MAX = _Anonymous_1._SC_STREAM_MAX;
    enum _SC_TZNAME_MAX = _Anonymous_1._SC_TZNAME_MAX;
    enum _SC_JOB_CONTROL = _Anonymous_1._SC_JOB_CONTROL;
    enum _SC_SAVED_IDS = _Anonymous_1._SC_SAVED_IDS;
    enum _SC_REALTIME_SIGNALS = _Anonymous_1._SC_REALTIME_SIGNALS;
    enum _SC_PRIORITY_SCHEDULING = _Anonymous_1._SC_PRIORITY_SCHEDULING;
    enum _SC_TIMERS = _Anonymous_1._SC_TIMERS;
    enum _SC_ASYNCHRONOUS_IO = _Anonymous_1._SC_ASYNCHRONOUS_IO;
    enum _SC_PRIORITIZED_IO = _Anonymous_1._SC_PRIORITIZED_IO;
    enum _SC_SYNCHRONIZED_IO = _Anonymous_1._SC_SYNCHRONIZED_IO;
    enum _SC_FSYNC = _Anonymous_1._SC_FSYNC;
    enum _SC_MAPPED_FILES = _Anonymous_1._SC_MAPPED_FILES;
    enum _SC_MEMLOCK = _Anonymous_1._SC_MEMLOCK;
    enum _SC_MEMLOCK_RANGE = _Anonymous_1._SC_MEMLOCK_RANGE;
    enum _SC_MEMORY_PROTECTION = _Anonymous_1._SC_MEMORY_PROTECTION;
    enum _SC_MESSAGE_PASSING = _Anonymous_1._SC_MESSAGE_PASSING;
    enum _SC_SEMAPHORES = _Anonymous_1._SC_SEMAPHORES;
    enum _SC_SHARED_MEMORY_OBJECTS = _Anonymous_1._SC_SHARED_MEMORY_OBJECTS;
    enum _SC_AIO_LISTIO_MAX = _Anonymous_1._SC_AIO_LISTIO_MAX;
    enum _SC_AIO_MAX = _Anonymous_1._SC_AIO_MAX;
    enum _SC_AIO_PRIO_DELTA_MAX = _Anonymous_1._SC_AIO_PRIO_DELTA_MAX;
    enum _SC_DELAYTIMER_MAX = _Anonymous_1._SC_DELAYTIMER_MAX;
    enum _SC_MQ_OPEN_MAX = _Anonymous_1._SC_MQ_OPEN_MAX;
    enum _SC_MQ_PRIO_MAX = _Anonymous_1._SC_MQ_PRIO_MAX;
    enum _SC_VERSION = _Anonymous_1._SC_VERSION;
    enum _SC_PAGESIZE = _Anonymous_1._SC_PAGESIZE;
    enum _SC_RTSIG_MAX = _Anonymous_1._SC_RTSIG_MAX;
    enum _SC_SEM_NSEMS_MAX = _Anonymous_1._SC_SEM_NSEMS_MAX;
    enum _SC_SEM_VALUE_MAX = _Anonymous_1._SC_SEM_VALUE_MAX;
    enum _SC_SIGQUEUE_MAX = _Anonymous_1._SC_SIGQUEUE_MAX;
    enum _SC_TIMER_MAX = _Anonymous_1._SC_TIMER_MAX;
    enum _SC_BC_BASE_MAX = _Anonymous_1._SC_BC_BASE_MAX;
    enum _SC_BC_DIM_MAX = _Anonymous_1._SC_BC_DIM_MAX;
    enum _SC_BC_SCALE_MAX = _Anonymous_1._SC_BC_SCALE_MAX;
    enum _SC_BC_STRING_MAX = _Anonymous_1._SC_BC_STRING_MAX;
    enum _SC_COLL_WEIGHTS_MAX = _Anonymous_1._SC_COLL_WEIGHTS_MAX;
    enum _SC_EQUIV_CLASS_MAX = _Anonymous_1._SC_EQUIV_CLASS_MAX;
    enum _SC_EXPR_NEST_MAX = _Anonymous_1._SC_EXPR_NEST_MAX;
    enum _SC_LINE_MAX = _Anonymous_1._SC_LINE_MAX;
    enum _SC_RE_DUP_MAX = _Anonymous_1._SC_RE_DUP_MAX;
    enum _SC_CHARCLASS_NAME_MAX = _Anonymous_1._SC_CHARCLASS_NAME_MAX;
    enum _SC_2_VERSION = _Anonymous_1._SC_2_VERSION;
    enum _SC_2_C_BIND = _Anonymous_1._SC_2_C_BIND;
    enum _SC_2_C_DEV = _Anonymous_1._SC_2_C_DEV;
    enum _SC_2_FORT_DEV = _Anonymous_1._SC_2_FORT_DEV;
    enum _SC_2_FORT_RUN = _Anonymous_1._SC_2_FORT_RUN;
    enum _SC_2_SW_DEV = _Anonymous_1._SC_2_SW_DEV;
    enum _SC_2_LOCALEDEF = _Anonymous_1._SC_2_LOCALEDEF;
    enum _SC_PII = _Anonymous_1._SC_PII;
    enum _SC_PII_XTI = _Anonymous_1._SC_PII_XTI;
    enum _SC_PII_SOCKET = _Anonymous_1._SC_PII_SOCKET;
    enum _SC_PII_INTERNET = _Anonymous_1._SC_PII_INTERNET;
    enum _SC_PII_OSI = _Anonymous_1._SC_PII_OSI;
    enum _SC_POLL = _Anonymous_1._SC_POLL;
    enum _SC_SELECT = _Anonymous_1._SC_SELECT;
    enum _SC_UIO_MAXIOV = _Anonymous_1._SC_UIO_MAXIOV;
    enum _SC_IOV_MAX = _Anonymous_1._SC_IOV_MAX;
    enum _SC_PII_INTERNET_STREAM = _Anonymous_1._SC_PII_INTERNET_STREAM;
    enum _SC_PII_INTERNET_DGRAM = _Anonymous_1._SC_PII_INTERNET_DGRAM;
    enum _SC_PII_OSI_COTS = _Anonymous_1._SC_PII_OSI_COTS;
    enum _SC_PII_OSI_CLTS = _Anonymous_1._SC_PII_OSI_CLTS;
    enum _SC_PII_OSI_M = _Anonymous_1._SC_PII_OSI_M;
    enum _SC_T_IOV_MAX = _Anonymous_1._SC_T_IOV_MAX;
    enum _SC_THREADS = _Anonymous_1._SC_THREADS;
    enum _SC_THREAD_SAFE_FUNCTIONS = _Anonymous_1._SC_THREAD_SAFE_FUNCTIONS;
    enum _SC_GETGR_R_SIZE_MAX = _Anonymous_1._SC_GETGR_R_SIZE_MAX;
    enum _SC_GETPW_R_SIZE_MAX = _Anonymous_1._SC_GETPW_R_SIZE_MAX;
    enum _SC_LOGIN_NAME_MAX = _Anonymous_1._SC_LOGIN_NAME_MAX;
    enum _SC_TTY_NAME_MAX = _Anonymous_1._SC_TTY_NAME_MAX;
    enum _SC_THREAD_DESTRUCTOR_ITERATIONS = _Anonymous_1._SC_THREAD_DESTRUCTOR_ITERATIONS;
    enum _SC_THREAD_KEYS_MAX = _Anonymous_1._SC_THREAD_KEYS_MAX;
    enum _SC_THREAD_STACK_MIN = _Anonymous_1._SC_THREAD_STACK_MIN;
    enum _SC_THREAD_THREADS_MAX = _Anonymous_1._SC_THREAD_THREADS_MAX;
    enum _SC_THREAD_ATTR_STACKADDR = _Anonymous_1._SC_THREAD_ATTR_STACKADDR;
    enum _SC_THREAD_ATTR_STACKSIZE = _Anonymous_1._SC_THREAD_ATTR_STACKSIZE;
    enum _SC_THREAD_PRIORITY_SCHEDULING = _Anonymous_1._SC_THREAD_PRIORITY_SCHEDULING;
    enum _SC_THREAD_PRIO_INHERIT = _Anonymous_1._SC_THREAD_PRIO_INHERIT;
    enum _SC_THREAD_PRIO_PROTECT = _Anonymous_1._SC_THREAD_PRIO_PROTECT;
    enum _SC_THREAD_PROCESS_SHARED = _Anonymous_1._SC_THREAD_PROCESS_SHARED;
    enum _SC_NPROCESSORS_CONF = _Anonymous_1._SC_NPROCESSORS_CONF;
    enum _SC_NPROCESSORS_ONLN = _Anonymous_1._SC_NPROCESSORS_ONLN;
    enum _SC_PHYS_PAGES = _Anonymous_1._SC_PHYS_PAGES;
    enum _SC_AVPHYS_PAGES = _Anonymous_1._SC_AVPHYS_PAGES;
    enum _SC_ATEXIT_MAX = _Anonymous_1._SC_ATEXIT_MAX;
    enum _SC_PASS_MAX = _Anonymous_1._SC_PASS_MAX;
    enum _SC_XOPEN_VERSION = _Anonymous_1._SC_XOPEN_VERSION;
    enum _SC_XOPEN_XCU_VERSION = _Anonymous_1._SC_XOPEN_XCU_VERSION;
    enum _SC_XOPEN_UNIX = _Anonymous_1._SC_XOPEN_UNIX;
    enum _SC_XOPEN_CRYPT = _Anonymous_1._SC_XOPEN_CRYPT;
    enum _SC_XOPEN_ENH_I18N = _Anonymous_1._SC_XOPEN_ENH_I18N;
    enum _SC_XOPEN_SHM = _Anonymous_1._SC_XOPEN_SHM;
    enum _SC_2_CHAR_TERM = _Anonymous_1._SC_2_CHAR_TERM;
    enum _SC_2_C_VERSION = _Anonymous_1._SC_2_C_VERSION;
    enum _SC_2_UPE = _Anonymous_1._SC_2_UPE;
    enum _SC_XOPEN_XPG2 = _Anonymous_1._SC_XOPEN_XPG2;
    enum _SC_XOPEN_XPG3 = _Anonymous_1._SC_XOPEN_XPG3;
    enum _SC_XOPEN_XPG4 = _Anonymous_1._SC_XOPEN_XPG4;
    enum _SC_CHAR_BIT = _Anonymous_1._SC_CHAR_BIT;
    enum _SC_CHAR_MAX = _Anonymous_1._SC_CHAR_MAX;
    enum _SC_CHAR_MIN = _Anonymous_1._SC_CHAR_MIN;
    enum _SC_INT_MAX = _Anonymous_1._SC_INT_MAX;
    enum _SC_INT_MIN = _Anonymous_1._SC_INT_MIN;
    enum _SC_LONG_BIT = _Anonymous_1._SC_LONG_BIT;
    enum _SC_WORD_BIT = _Anonymous_1._SC_WORD_BIT;
    enum _SC_MB_LEN_MAX = _Anonymous_1._SC_MB_LEN_MAX;
    enum _SC_NZERO = _Anonymous_1._SC_NZERO;
    enum _SC_SSIZE_MAX = _Anonymous_1._SC_SSIZE_MAX;
    enum _SC_SCHAR_MAX = _Anonymous_1._SC_SCHAR_MAX;
    enum _SC_SCHAR_MIN = _Anonymous_1._SC_SCHAR_MIN;
    enum _SC_SHRT_MAX = _Anonymous_1._SC_SHRT_MAX;
    enum _SC_SHRT_MIN = _Anonymous_1._SC_SHRT_MIN;
    enum _SC_UCHAR_MAX = _Anonymous_1._SC_UCHAR_MAX;
    enum _SC_UINT_MAX = _Anonymous_1._SC_UINT_MAX;
    enum _SC_ULONG_MAX = _Anonymous_1._SC_ULONG_MAX;
    enum _SC_USHRT_MAX = _Anonymous_1._SC_USHRT_MAX;
    enum _SC_NL_ARGMAX = _Anonymous_1._SC_NL_ARGMAX;
    enum _SC_NL_LANGMAX = _Anonymous_1._SC_NL_LANGMAX;
    enum _SC_NL_MSGMAX = _Anonymous_1._SC_NL_MSGMAX;
    enum _SC_NL_NMAX = _Anonymous_1._SC_NL_NMAX;
    enum _SC_NL_SETMAX = _Anonymous_1._SC_NL_SETMAX;
    enum _SC_NL_TEXTMAX = _Anonymous_1._SC_NL_TEXTMAX;
    enum _SC_XBS5_ILP32_OFF32 = _Anonymous_1._SC_XBS5_ILP32_OFF32;
    enum _SC_XBS5_ILP32_OFFBIG = _Anonymous_1._SC_XBS5_ILP32_OFFBIG;
    enum _SC_XBS5_LP64_OFF64 = _Anonymous_1._SC_XBS5_LP64_OFF64;
    enum _SC_XBS5_LPBIG_OFFBIG = _Anonymous_1._SC_XBS5_LPBIG_OFFBIG;
    enum _SC_XOPEN_LEGACY = _Anonymous_1._SC_XOPEN_LEGACY;
    enum _SC_XOPEN_REALTIME = _Anonymous_1._SC_XOPEN_REALTIME;
    enum _SC_XOPEN_REALTIME_THREADS = _Anonymous_1._SC_XOPEN_REALTIME_THREADS;
    enum _SC_ADVISORY_INFO = _Anonymous_1._SC_ADVISORY_INFO;
    enum _SC_BARRIERS = _Anonymous_1._SC_BARRIERS;
    enum _SC_BASE = _Anonymous_1._SC_BASE;
    enum _SC_C_LANG_SUPPORT = _Anonymous_1._SC_C_LANG_SUPPORT;
    enum _SC_C_LANG_SUPPORT_R = _Anonymous_1._SC_C_LANG_SUPPORT_R;
    enum _SC_CLOCK_SELECTION = _Anonymous_1._SC_CLOCK_SELECTION;
    enum _SC_CPUTIME = _Anonymous_1._SC_CPUTIME;
    enum _SC_THREAD_CPUTIME = _Anonymous_1._SC_THREAD_CPUTIME;
    enum _SC_DEVICE_IO = _Anonymous_1._SC_DEVICE_IO;
    enum _SC_DEVICE_SPECIFIC = _Anonymous_1._SC_DEVICE_SPECIFIC;
    enum _SC_DEVICE_SPECIFIC_R = _Anonymous_1._SC_DEVICE_SPECIFIC_R;
    enum _SC_FD_MGMT = _Anonymous_1._SC_FD_MGMT;
    enum _SC_FIFO = _Anonymous_1._SC_FIFO;
    enum _SC_PIPE = _Anonymous_1._SC_PIPE;
    enum _SC_FILE_ATTRIBUTES = _Anonymous_1._SC_FILE_ATTRIBUTES;
    enum _SC_FILE_LOCKING = _Anonymous_1._SC_FILE_LOCKING;
    enum _SC_FILE_SYSTEM = _Anonymous_1._SC_FILE_SYSTEM;
    enum _SC_MONOTONIC_CLOCK = _Anonymous_1._SC_MONOTONIC_CLOCK;
    enum _SC_MULTI_PROCESS = _Anonymous_1._SC_MULTI_PROCESS;
    enum _SC_SINGLE_PROCESS = _Anonymous_1._SC_SINGLE_PROCESS;
    enum _SC_NETWORKING = _Anonymous_1._SC_NETWORKING;
    enum _SC_READER_WRITER_LOCKS = _Anonymous_1._SC_READER_WRITER_LOCKS;
    enum _SC_SPIN_LOCKS = _Anonymous_1._SC_SPIN_LOCKS;
    enum _SC_REGEXP = _Anonymous_1._SC_REGEXP;
    enum _SC_REGEX_VERSION = _Anonymous_1._SC_REGEX_VERSION;
    enum _SC_SHELL = _Anonymous_1._SC_SHELL;
    enum _SC_SIGNALS = _Anonymous_1._SC_SIGNALS;
    enum _SC_SPAWN = _Anonymous_1._SC_SPAWN;
    enum _SC_SPORADIC_SERVER = _Anonymous_1._SC_SPORADIC_SERVER;
    enum _SC_THREAD_SPORADIC_SERVER = _Anonymous_1._SC_THREAD_SPORADIC_SERVER;
    enum _SC_SYSTEM_DATABASE = _Anonymous_1._SC_SYSTEM_DATABASE;
    enum _SC_SYSTEM_DATABASE_R = _Anonymous_1._SC_SYSTEM_DATABASE_R;
    enum _SC_TIMEOUTS = _Anonymous_1._SC_TIMEOUTS;
    enum _SC_TYPED_MEMORY_OBJECTS = _Anonymous_1._SC_TYPED_MEMORY_OBJECTS;
    enum _SC_USER_GROUPS = _Anonymous_1._SC_USER_GROUPS;
    enum _SC_USER_GROUPS_R = _Anonymous_1._SC_USER_GROUPS_R;
    enum _SC_2_PBS = _Anonymous_1._SC_2_PBS;
    enum _SC_2_PBS_ACCOUNTING = _Anonymous_1._SC_2_PBS_ACCOUNTING;
    enum _SC_2_PBS_LOCATE = _Anonymous_1._SC_2_PBS_LOCATE;
    enum _SC_2_PBS_MESSAGE = _Anonymous_1._SC_2_PBS_MESSAGE;
    enum _SC_2_PBS_TRACK = _Anonymous_1._SC_2_PBS_TRACK;
    enum _SC_SYMLOOP_MAX = _Anonymous_1._SC_SYMLOOP_MAX;
    enum _SC_STREAMS = _Anonymous_1._SC_STREAMS;
    enum _SC_2_PBS_CHECKPOINT = _Anonymous_1._SC_2_PBS_CHECKPOINT;
    enum _SC_V6_ILP32_OFF32 = _Anonymous_1._SC_V6_ILP32_OFF32;
    enum _SC_V6_ILP32_OFFBIG = _Anonymous_1._SC_V6_ILP32_OFFBIG;
    enum _SC_V6_LP64_OFF64 = _Anonymous_1._SC_V6_LP64_OFF64;
    enum _SC_V6_LPBIG_OFFBIG = _Anonymous_1._SC_V6_LPBIG_OFFBIG;
    enum _SC_HOST_NAME_MAX = _Anonymous_1._SC_HOST_NAME_MAX;
    enum _SC_TRACE = _Anonymous_1._SC_TRACE;
    enum _SC_TRACE_EVENT_FILTER = _Anonymous_1._SC_TRACE_EVENT_FILTER;
    enum _SC_TRACE_INHERIT = _Anonymous_1._SC_TRACE_INHERIT;
    enum _SC_TRACE_LOG = _Anonymous_1._SC_TRACE_LOG;
    enum _SC_LEVEL1_ICACHE_SIZE = _Anonymous_1._SC_LEVEL1_ICACHE_SIZE;
    enum _SC_LEVEL1_ICACHE_ASSOC = _Anonymous_1._SC_LEVEL1_ICACHE_ASSOC;
    enum _SC_LEVEL1_ICACHE_LINESIZE = _Anonymous_1._SC_LEVEL1_ICACHE_LINESIZE;
    enum _SC_LEVEL1_DCACHE_SIZE = _Anonymous_1._SC_LEVEL1_DCACHE_SIZE;
    enum _SC_LEVEL1_DCACHE_ASSOC = _Anonymous_1._SC_LEVEL1_DCACHE_ASSOC;
    enum _SC_LEVEL1_DCACHE_LINESIZE = _Anonymous_1._SC_LEVEL1_DCACHE_LINESIZE;
    enum _SC_LEVEL2_CACHE_SIZE = _Anonymous_1._SC_LEVEL2_CACHE_SIZE;
    enum _SC_LEVEL2_CACHE_ASSOC = _Anonymous_1._SC_LEVEL2_CACHE_ASSOC;
    enum _SC_LEVEL2_CACHE_LINESIZE = _Anonymous_1._SC_LEVEL2_CACHE_LINESIZE;
    enum _SC_LEVEL3_CACHE_SIZE = _Anonymous_1._SC_LEVEL3_CACHE_SIZE;
    enum _SC_LEVEL3_CACHE_ASSOC = _Anonymous_1._SC_LEVEL3_CACHE_ASSOC;
    enum _SC_LEVEL3_CACHE_LINESIZE = _Anonymous_1._SC_LEVEL3_CACHE_LINESIZE;
    enum _SC_LEVEL4_CACHE_SIZE = _Anonymous_1._SC_LEVEL4_CACHE_SIZE;
    enum _SC_LEVEL4_CACHE_ASSOC = _Anonymous_1._SC_LEVEL4_CACHE_ASSOC;
    enum _SC_LEVEL4_CACHE_LINESIZE = _Anonymous_1._SC_LEVEL4_CACHE_LINESIZE;
    enum _SC_IPV6 = _Anonymous_1._SC_IPV6;
    enum _SC_RAW_SOCKETS = _Anonymous_1._SC_RAW_SOCKETS;
    enum _SC_V7_ILP32_OFF32 = _Anonymous_1._SC_V7_ILP32_OFF32;
    enum _SC_V7_ILP32_OFFBIG = _Anonymous_1._SC_V7_ILP32_OFFBIG;
    enum _SC_V7_LP64_OFF64 = _Anonymous_1._SC_V7_LP64_OFF64;
    enum _SC_V7_LPBIG_OFFBIG = _Anonymous_1._SC_V7_LPBIG_OFFBIG;
    enum _SC_SS_REPL_MAX = _Anonymous_1._SC_SS_REPL_MAX;
    enum _SC_TRACE_EVENT_NAME_MAX = _Anonymous_1._SC_TRACE_EVENT_NAME_MAX;
    enum _SC_TRACE_NAME_MAX = _Anonymous_1._SC_TRACE_NAME_MAX;
    enum _SC_TRACE_SYS_MAX = _Anonymous_1._SC_TRACE_SYS_MAX;
    enum _SC_TRACE_USER_EVENT_MAX = _Anonymous_1._SC_TRACE_USER_EVENT_MAX;
    enum _SC_XOPEN_STREAMS = _Anonymous_1._SC_XOPEN_STREAMS;
    enum _SC_THREAD_ROBUST_PRIO_INHERIT = _Anonymous_1._SC_THREAD_ROBUST_PRIO_INHERIT;
    enum _SC_THREAD_ROBUST_PRIO_PROTECT = _Anonymous_1._SC_THREAD_ROBUST_PRIO_PROTECT;
    int bcmp(const(void)*, const(void)*, c_ulong) @nogc nothrow;
    char* stpncpy(char*, const(char)*, c_ulong) @nogc nothrow;
    char* __stpncpy(char*, const(char)*, c_ulong) @nogc nothrow;
    char* stpcpy(char*, const(char)*) @nogc nothrow;
    char* __stpcpy(char*, const(char)*) @nogc nothrow;
    char* strsignal(int) @nogc nothrow;
    char* strsep(char**, const(char)*) @nogc nothrow;
    void explicit_bzero(void*, c_ulong) @nogc nothrow;
    char* strerror_l(int, __locale_struct*) @nogc nothrow;
    int strerror_r(int, char*, c_ulong) @nogc nothrow;
    char* strerror(int) @nogc nothrow;
    c_ulong strnlen(const(char)*, c_ulong) @nogc nothrow;
    c_ulong strlen(const(char)*) @nogc nothrow;
    char* strtok_r(char*, const(char)*, char**) @nogc nothrow;
    char* __strtok_r(char*, const(char)*, char**) @nogc nothrow;
    char* strtok(char*, const(char)*) @nogc nothrow;
    char* strstr(const(char)*, const(char)*) @nogc nothrow;
    char* strpbrk(const(char)*, const(char)*) @nogc nothrow;
    c_ulong strspn(const(char)*, const(char)*) @nogc nothrow;
    c_ulong strcspn(const(char)*, const(char)*) @nogc nothrow;
    char* strrchr(const(char)*, int) @nogc nothrow;
    char* strchr(const(char)*, int) @nogc nothrow;
    char* strndup(const(char)*, c_ulong) @nogc nothrow;
    char* strdup(const(char)*) @nogc nothrow;
    c_ulong strxfrm_l(char*, const(char)*, c_ulong, __locale_struct*) @nogc nothrow;
    int strcoll_l(const(char)*, const(char)*, __locale_struct*) @nogc nothrow;
    c_ulong strxfrm(char*, const(char)*, c_ulong) @nogc nothrow;
    int strcoll(const(char)*, const(char)*) @nogc nothrow;
    int strncmp(const(char)*, const(char)*, c_ulong) @nogc nothrow;
    int strcmp(const(char)*, const(char)*) @nogc nothrow;
    char* strncat(char*, const(char)*, c_ulong) @nogc nothrow;
    char* strcat(char*, const(char)*) @nogc nothrow;
    char* strncpy(char*, const(char)*, c_ulong) @nogc nothrow;
    char* strcpy(char*, const(char)*) @nogc nothrow;
    void* memchr(const(void)*, int, c_ulong) @nogc nothrow;
    int memcmp(const(void)*, const(void)*, c_ulong) @nogc nothrow;
    void* memset(void*, int, c_ulong) @nogc nothrow;
    void* memccpy(void*, const(void)*, int, c_ulong) @nogc nothrow;
    void* memmove(void*, const(void)*, c_ulong) @nogc nothrow;
    void* memcpy(void*, const(void)*, c_ulong) @nogc nothrow;
    alias __poll_t = uint;
    alias __wsum = uint;
    alias __sum16 = ushort;
    alias __be64 = ulong;
    alias __le64 = ulong;
    alias __be32 = uint;
    alias __le32 = uint;
    alias __be16 = ushort;
    alias __le16 = ushort;
    alias __kernel_mqd_t = int;
    alias __kernel_key_t = int;
    alias __kernel_sighandler_t = void function(int);
    struct __kernel_fd_set
    {
        c_ulong[16] fds_bits;
    }
    struct ff_effect
    {
        ushort type;
        short id;
        ushort direction;
        ff_trigger trigger;
        ff_replay replay;
        static union _Anonymous_2
        {
            ff_constant_effect constant;
            ff_ramp_effect ramp;
            ff_periodic_effect periodic;
            ff_condition_effect[2] condition;
            ff_rumble_effect rumble;
        }
        _Anonymous_2 u;
    }
    struct ff_rumble_effect
    {
        ushort strong_magnitude;
        ushort weak_magnitude;
    }
    struct ff_periodic_effect
    {
        ushort waveform;
        ushort period;
        short magnitude;
        short offset;
        ushort phase;
        ff_envelope envelope;
        uint custom_len;
        short* custom_data;
    }
    struct ff_condition_effect
    {
        ushort right_saturation;
        ushort left_saturation;
        short right_coeff;
        short left_coeff;
        ushort deadband;
        short center;
    }
    struct ff_ramp_effect
    {
        short start_level;
        short end_level;
        ff_envelope envelope;
    }
    struct ff_constant_effect
    {
        short level;
        ff_envelope envelope;
    }
    struct ff_envelope
    {
        ushort attack_length;
        ushort attack_level;
        ushort fade_length;
        ushort fade_level;
    }
    struct ff_trigger
    {
        ushort button;
        ushort interval;
    }
    struct ff_replay
    {
        ushort length;
        ushort delay;
    }
    struct input_mask
    {
        uint type;
        uint codes_size;
        ulong codes_ptr;
    }
    struct input_keymap_entry
    {
        ubyte flags;
        ubyte len;
        ushort index;
        uint keycode;
        ubyte[32] scancode;
    }
    struct input_absinfo
    {
        int value;
        int minimum;
        int maximum;
        int fuzz;
        int flat;
        int resolution;
    }
    enum _Anonymous_3
    {
        _CS_PATH = 0,
        _CS_V6_WIDTH_RESTRICTED_ENVS = 1,
        _CS_GNU_LIBC_VERSION = 2,
        _CS_GNU_LIBPTHREAD_VERSION = 3,
        _CS_V5_WIDTH_RESTRICTED_ENVS = 4,
        _CS_V7_WIDTH_RESTRICTED_ENVS = 5,
        _CS_LFS_CFLAGS = 1000,
        _CS_LFS_LDFLAGS = 1001,
        _CS_LFS_LIBS = 1002,
        _CS_LFS_LINTFLAGS = 1003,
        _CS_LFS64_CFLAGS = 1004,
        _CS_LFS64_LDFLAGS = 1005,
        _CS_LFS64_LIBS = 1006,
        _CS_LFS64_LINTFLAGS = 1007,
        _CS_XBS5_ILP32_OFF32_CFLAGS = 1100,
        _CS_XBS5_ILP32_OFF32_LDFLAGS = 1101,
        _CS_XBS5_ILP32_OFF32_LIBS = 1102,
        _CS_XBS5_ILP32_OFF32_LINTFLAGS = 1103,
        _CS_XBS5_ILP32_OFFBIG_CFLAGS = 1104,
        _CS_XBS5_ILP32_OFFBIG_LDFLAGS = 1105,
        _CS_XBS5_ILP32_OFFBIG_LIBS = 1106,
        _CS_XBS5_ILP32_OFFBIG_LINTFLAGS = 1107,
        _CS_XBS5_LP64_OFF64_CFLAGS = 1108,
        _CS_XBS5_LP64_OFF64_LDFLAGS = 1109,
        _CS_XBS5_LP64_OFF64_LIBS = 1110,
        _CS_XBS5_LP64_OFF64_LINTFLAGS = 1111,
        _CS_XBS5_LPBIG_OFFBIG_CFLAGS = 1112,
        _CS_XBS5_LPBIG_OFFBIG_LDFLAGS = 1113,
        _CS_XBS5_LPBIG_OFFBIG_LIBS = 1114,
        _CS_XBS5_LPBIG_OFFBIG_LINTFLAGS = 1115,
        _CS_POSIX_V6_ILP32_OFF32_CFLAGS = 1116,
        _CS_POSIX_V6_ILP32_OFF32_LDFLAGS = 1117,
        _CS_POSIX_V6_ILP32_OFF32_LIBS = 1118,
        _CS_POSIX_V6_ILP32_OFF32_LINTFLAGS = 1119,
        _CS_POSIX_V6_ILP32_OFFBIG_CFLAGS = 1120,
        _CS_POSIX_V6_ILP32_OFFBIG_LDFLAGS = 1121,
        _CS_POSIX_V6_ILP32_OFFBIG_LIBS = 1122,
        _CS_POSIX_V6_ILP32_OFFBIG_LINTFLAGS = 1123,
        _CS_POSIX_V6_LP64_OFF64_CFLAGS = 1124,
        _CS_POSIX_V6_LP64_OFF64_LDFLAGS = 1125,
        _CS_POSIX_V6_LP64_OFF64_LIBS = 1126,
        _CS_POSIX_V6_LP64_OFF64_LINTFLAGS = 1127,
        _CS_POSIX_V6_LPBIG_OFFBIG_CFLAGS = 1128,
        _CS_POSIX_V6_LPBIG_OFFBIG_LDFLAGS = 1129,
        _CS_POSIX_V6_LPBIG_OFFBIG_LIBS = 1130,
        _CS_POSIX_V6_LPBIG_OFFBIG_LINTFLAGS = 1131,
        _CS_POSIX_V7_ILP32_OFF32_CFLAGS = 1132,
        _CS_POSIX_V7_ILP32_OFF32_LDFLAGS = 1133,
        _CS_POSIX_V7_ILP32_OFF32_LIBS = 1134,
        _CS_POSIX_V7_ILP32_OFF32_LINTFLAGS = 1135,
        _CS_POSIX_V7_ILP32_OFFBIG_CFLAGS = 1136,
        _CS_POSIX_V7_ILP32_OFFBIG_LDFLAGS = 1137,
        _CS_POSIX_V7_ILP32_OFFBIG_LIBS = 1138,
        _CS_POSIX_V7_ILP32_OFFBIG_LINTFLAGS = 1139,
        _CS_POSIX_V7_LP64_OFF64_CFLAGS = 1140,
        _CS_POSIX_V7_LP64_OFF64_LDFLAGS = 1141,
        _CS_POSIX_V7_LP64_OFF64_LIBS = 1142,
        _CS_POSIX_V7_LP64_OFF64_LINTFLAGS = 1143,
        _CS_POSIX_V7_LPBIG_OFFBIG_CFLAGS = 1144,
        _CS_POSIX_V7_LPBIG_OFFBIG_LDFLAGS = 1145,
        _CS_POSIX_V7_LPBIG_OFFBIG_LIBS = 1146,
        _CS_POSIX_V7_LPBIG_OFFBIG_LINTFLAGS = 1147,
        _CS_V6_ENV = 1148,
        _CS_V7_ENV = 1149,
    }
    enum _CS_PATH = _Anonymous_3._CS_PATH;
    enum _CS_V6_WIDTH_RESTRICTED_ENVS = _Anonymous_3._CS_V6_WIDTH_RESTRICTED_ENVS;
    enum _CS_GNU_LIBC_VERSION = _Anonymous_3._CS_GNU_LIBC_VERSION;
    enum _CS_GNU_LIBPTHREAD_VERSION = _Anonymous_3._CS_GNU_LIBPTHREAD_VERSION;
    enum _CS_V5_WIDTH_RESTRICTED_ENVS = _Anonymous_3._CS_V5_WIDTH_RESTRICTED_ENVS;
    enum _CS_V7_WIDTH_RESTRICTED_ENVS = _Anonymous_3._CS_V7_WIDTH_RESTRICTED_ENVS;
    enum _CS_LFS_CFLAGS = _Anonymous_3._CS_LFS_CFLAGS;
    enum _CS_LFS_LDFLAGS = _Anonymous_3._CS_LFS_LDFLAGS;
    enum _CS_LFS_LIBS = _Anonymous_3._CS_LFS_LIBS;
    enum _CS_LFS_LINTFLAGS = _Anonymous_3._CS_LFS_LINTFLAGS;
    enum _CS_LFS64_CFLAGS = _Anonymous_3._CS_LFS64_CFLAGS;
    enum _CS_LFS64_LDFLAGS = _Anonymous_3._CS_LFS64_LDFLAGS;
    enum _CS_LFS64_LIBS = _Anonymous_3._CS_LFS64_LIBS;
    enum _CS_LFS64_LINTFLAGS = _Anonymous_3._CS_LFS64_LINTFLAGS;
    enum _CS_XBS5_ILP32_OFF32_CFLAGS = _Anonymous_3._CS_XBS5_ILP32_OFF32_CFLAGS;
    enum _CS_XBS5_ILP32_OFF32_LDFLAGS = _Anonymous_3._CS_XBS5_ILP32_OFF32_LDFLAGS;
    enum _CS_XBS5_ILP32_OFF32_LIBS = _Anonymous_3._CS_XBS5_ILP32_OFF32_LIBS;
    enum _CS_XBS5_ILP32_OFF32_LINTFLAGS = _Anonymous_3._CS_XBS5_ILP32_OFF32_LINTFLAGS;
    enum _CS_XBS5_ILP32_OFFBIG_CFLAGS = _Anonymous_3._CS_XBS5_ILP32_OFFBIG_CFLAGS;
    enum _CS_XBS5_ILP32_OFFBIG_LDFLAGS = _Anonymous_3._CS_XBS5_ILP32_OFFBIG_LDFLAGS;
    enum _CS_XBS5_ILP32_OFFBIG_LIBS = _Anonymous_3._CS_XBS5_ILP32_OFFBIG_LIBS;
    enum _CS_XBS5_ILP32_OFFBIG_LINTFLAGS = _Anonymous_3._CS_XBS5_ILP32_OFFBIG_LINTFLAGS;
    enum _CS_XBS5_LP64_OFF64_CFLAGS = _Anonymous_3._CS_XBS5_LP64_OFF64_CFLAGS;
    enum _CS_XBS5_LP64_OFF64_LDFLAGS = _Anonymous_3._CS_XBS5_LP64_OFF64_LDFLAGS;
    enum _CS_XBS5_LP64_OFF64_LIBS = _Anonymous_3._CS_XBS5_LP64_OFF64_LIBS;
    enum _CS_XBS5_LP64_OFF64_LINTFLAGS = _Anonymous_3._CS_XBS5_LP64_OFF64_LINTFLAGS;
    enum _CS_XBS5_LPBIG_OFFBIG_CFLAGS = _Anonymous_3._CS_XBS5_LPBIG_OFFBIG_CFLAGS;
    enum _CS_XBS5_LPBIG_OFFBIG_LDFLAGS = _Anonymous_3._CS_XBS5_LPBIG_OFFBIG_LDFLAGS;
    enum _CS_XBS5_LPBIG_OFFBIG_LIBS = _Anonymous_3._CS_XBS5_LPBIG_OFFBIG_LIBS;
    enum _CS_XBS5_LPBIG_OFFBIG_LINTFLAGS = _Anonymous_3._CS_XBS5_LPBIG_OFFBIG_LINTFLAGS;
    enum _CS_POSIX_V6_ILP32_OFF32_CFLAGS = _Anonymous_3._CS_POSIX_V6_ILP32_OFF32_CFLAGS;
    enum _CS_POSIX_V6_ILP32_OFF32_LDFLAGS = _Anonymous_3._CS_POSIX_V6_ILP32_OFF32_LDFLAGS;
    enum _CS_POSIX_V6_ILP32_OFF32_LIBS = _Anonymous_3._CS_POSIX_V6_ILP32_OFF32_LIBS;
    enum _CS_POSIX_V6_ILP32_OFF32_LINTFLAGS = _Anonymous_3._CS_POSIX_V6_ILP32_OFF32_LINTFLAGS;
    enum _CS_POSIX_V6_ILP32_OFFBIG_CFLAGS = _Anonymous_3._CS_POSIX_V6_ILP32_OFFBIG_CFLAGS;
    enum _CS_POSIX_V6_ILP32_OFFBIG_LDFLAGS = _Anonymous_3._CS_POSIX_V6_ILP32_OFFBIG_LDFLAGS;
    enum _CS_POSIX_V6_ILP32_OFFBIG_LIBS = _Anonymous_3._CS_POSIX_V6_ILP32_OFFBIG_LIBS;
    enum _CS_POSIX_V6_ILP32_OFFBIG_LINTFLAGS = _Anonymous_3._CS_POSIX_V6_ILP32_OFFBIG_LINTFLAGS;
    enum _CS_POSIX_V6_LP64_OFF64_CFLAGS = _Anonymous_3._CS_POSIX_V6_LP64_OFF64_CFLAGS;
    enum _CS_POSIX_V6_LP64_OFF64_LDFLAGS = _Anonymous_3._CS_POSIX_V6_LP64_OFF64_LDFLAGS;
    enum _CS_POSIX_V6_LP64_OFF64_LIBS = _Anonymous_3._CS_POSIX_V6_LP64_OFF64_LIBS;
    enum _CS_POSIX_V6_LP64_OFF64_LINTFLAGS = _Anonymous_3._CS_POSIX_V6_LP64_OFF64_LINTFLAGS;
    enum _CS_POSIX_V6_LPBIG_OFFBIG_CFLAGS = _Anonymous_3._CS_POSIX_V6_LPBIG_OFFBIG_CFLAGS;
    enum _CS_POSIX_V6_LPBIG_OFFBIG_LDFLAGS = _Anonymous_3._CS_POSIX_V6_LPBIG_OFFBIG_LDFLAGS;
    enum _CS_POSIX_V6_LPBIG_OFFBIG_LIBS = _Anonymous_3._CS_POSIX_V6_LPBIG_OFFBIG_LIBS;
    enum _CS_POSIX_V6_LPBIG_OFFBIG_LINTFLAGS = _Anonymous_3._CS_POSIX_V6_LPBIG_OFFBIG_LINTFLAGS;
    enum _CS_POSIX_V7_ILP32_OFF32_CFLAGS = _Anonymous_3._CS_POSIX_V7_ILP32_OFF32_CFLAGS;
    enum _CS_POSIX_V7_ILP32_OFF32_LDFLAGS = _Anonymous_3._CS_POSIX_V7_ILP32_OFF32_LDFLAGS;
    enum _CS_POSIX_V7_ILP32_OFF32_LIBS = _Anonymous_3._CS_POSIX_V7_ILP32_OFF32_LIBS;
    enum _CS_POSIX_V7_ILP32_OFF32_LINTFLAGS = _Anonymous_3._CS_POSIX_V7_ILP32_OFF32_LINTFLAGS;
    enum _CS_POSIX_V7_ILP32_OFFBIG_CFLAGS = _Anonymous_3._CS_POSIX_V7_ILP32_OFFBIG_CFLAGS;
    enum _CS_POSIX_V7_ILP32_OFFBIG_LDFLAGS = _Anonymous_3._CS_POSIX_V7_ILP32_OFFBIG_LDFLAGS;
    enum _CS_POSIX_V7_ILP32_OFFBIG_LIBS = _Anonymous_3._CS_POSIX_V7_ILP32_OFFBIG_LIBS;
    enum _CS_POSIX_V7_ILP32_OFFBIG_LINTFLAGS = _Anonymous_3._CS_POSIX_V7_ILP32_OFFBIG_LINTFLAGS;
    enum _CS_POSIX_V7_LP64_OFF64_CFLAGS = _Anonymous_3._CS_POSIX_V7_LP64_OFF64_CFLAGS;
    enum _CS_POSIX_V7_LP64_OFF64_LDFLAGS = _Anonymous_3._CS_POSIX_V7_LP64_OFF64_LDFLAGS;
    enum _CS_POSIX_V7_LP64_OFF64_LIBS = _Anonymous_3._CS_POSIX_V7_LP64_OFF64_LIBS;
    enum _CS_POSIX_V7_LP64_OFF64_LINTFLAGS = _Anonymous_3._CS_POSIX_V7_LP64_OFF64_LINTFLAGS;
    enum _CS_POSIX_V7_LPBIG_OFFBIG_CFLAGS = _Anonymous_3._CS_POSIX_V7_LPBIG_OFFBIG_CFLAGS;
    enum _CS_POSIX_V7_LPBIG_OFFBIG_LDFLAGS = _Anonymous_3._CS_POSIX_V7_LPBIG_OFFBIG_LDFLAGS;
    enum _CS_POSIX_V7_LPBIG_OFFBIG_LIBS = _Anonymous_3._CS_POSIX_V7_LPBIG_OFFBIG_LIBS;
    enum _CS_POSIX_V7_LPBIG_OFFBIG_LINTFLAGS = _Anonymous_3._CS_POSIX_V7_LPBIG_OFFBIG_LINTFLAGS;
    enum _CS_V6_ENV = _Anonymous_3._CS_V6_ENV;
    enum _CS_V7_ENV = _Anonymous_3._CS_V7_ENV;
    struct input_id
    {
        ushort bustype;
        ushort vendor;
        ushort product;
        ushort version_;
    }
    struct input_event
    {
        timeval time;
        ushort type;
        ushort code;
        int value;
    }
    int libevdev_get_repeat(const(libevdev)*, int*, int*) @nogc nothrow;
    int libevdev_property_from_name_n(const(char)*, c_ulong) @nogc nothrow;
    int libevdev_property_from_name(const(char)*) @nogc nothrow;
    int libevdev_event_value_from_name_n(uint, uint, const(char)*, c_ulong) @nogc nothrow;
    int libevdev_event_code_from_code_name_n(const(char)*, c_ulong) @nogc nothrow;
    int libevdev_event_code_from_code_name(const(char)*) @nogc nothrow;
    int libevdev_event_type_from_code_name_n(const(char)*, c_ulong) @nogc nothrow;
    int libevdev_event_type_from_code_name(const(char)*) @nogc nothrow;
    int libevdev_event_value_from_name(uint, uint, const(char)*) @nogc nothrow;
    int libevdev_event_code_from_name_n(uint, const(char)*, c_ulong) @nogc nothrow;
    int libevdev_event_code_from_name(uint, const(char)*) @nogc nothrow;
    int libevdev_event_type_from_name_n(const(char)*, c_ulong) @nogc nothrow;
    int libevdev_event_type_from_name(const(char)*) @nogc nothrow;
    int libevdev_event_type_get_max(uint) @nogc nothrow;
    const(char)* libevdev_property_get_name(uint) @nogc nothrow;
    const(char)* libevdev_event_value_get_name(uint, uint, int) @nogc nothrow;
    const(char)* libevdev_event_code_get_name(uint, uint) @nogc nothrow;
    const(char)* libevdev_event_type_get_name(uint) @nogc nothrow;
    int libevdev_event_is_code(const(input_event)*, uint, uint) @nogc nothrow;
    int libevdev_event_is_type(const(input_event)*, uint) @nogc nothrow;
    int libevdev_set_clock_id(libevdev*, int) @nogc nothrow;
    int libevdev_kernel_set_led_values(libevdev*, ...) @nogc nothrow;
    int libevdev_kernel_set_led_value(libevdev*, uint, libevdev_led_value) @nogc nothrow;
    enum libevdev_led_value
    {
        LIBEVDEV_LED_ON = 3,
        LIBEVDEV_LED_OFF = 4,
    }
    enum LIBEVDEV_LED_ON = libevdev_led_value.LIBEVDEV_LED_ON;
    enum LIBEVDEV_LED_OFF = libevdev_led_value.LIBEVDEV_LED_OFF;
    int libevdev_kernel_set_abs_info(libevdev*, uint, const(input_absinfo)*) @nogc nothrow;
    int libevdev_disable_event_code(libevdev*, uint, uint) @nogc nothrow;
    int libevdev_enable_event_code(libevdev*, uint, uint, const(void)*) @nogc nothrow;
    int libevdev_disable_event_type(libevdev*, uint) @nogc nothrow;
    int libevdev_enable_event_type(libevdev*, uint) @nogc nothrow;
    void libevdev_set_abs_info(libevdev*, uint, const(input_absinfo)*) @nogc nothrow;
    void libevdev_set_abs_resolution(libevdev*, uint, int) @nogc nothrow;
    void libevdev_set_abs_flat(libevdev*, uint, int) @nogc nothrow;
    void libevdev_set_abs_fuzz(libevdev*, uint, int) @nogc nothrow;
    void libevdev_set_abs_maximum(libevdev*, uint, int) @nogc nothrow;
    void libevdev_set_abs_minimum(libevdev*, uint, int) @nogc nothrow;
    int libevdev_get_current_slot(const(libevdev)*) @nogc nothrow;
    int libevdev_get_num_slots(const(libevdev)*) @nogc nothrow;
    int libevdev_fetch_slot_value(const(libevdev)*, uint, uint, int*) @nogc nothrow;
    int libevdev_set_slot_value(libevdev*, uint, uint, int) @nogc nothrow;
    int libevdev_get_slot_value(const(libevdev)*, uint, uint) @nogc nothrow;
    int libevdev_fetch_event_value(const(libevdev)*, uint, uint, int*) @nogc nothrow;
    int libevdev_set_event_value(libevdev*, uint, uint, int) @nogc nothrow;
    int libevdev_get_event_value(const(libevdev)*, uint, uint) @nogc nothrow;
    const(input_absinfo)* libevdev_get_abs_info(const(libevdev)*, uint) @nogc nothrow;
    int libevdev_get_abs_resolution(const(libevdev)*, uint) @nogc nothrow;
    int libevdev_get_abs_flat(const(libevdev)*, uint) @nogc nothrow;
    int libevdev_get_abs_fuzz(const(libevdev)*, uint) @nogc nothrow;
    int libevdev_get_abs_maximum(const(libevdev)*, uint) @nogc nothrow;
    int libevdev_get_abs_minimum(const(libevdev)*, uint) @nogc nothrow;
    int libevdev_has_event_code(const(libevdev)*, uint, uint) @nogc nothrow;
    int libevdev_has_event_type(const(libevdev)*, uint) @nogc nothrow;
    int libevdev_disable_property(libevdev*, uint) @nogc nothrow;
    int libevdev_enable_property(libevdev*, uint) @nogc nothrow;
    int libevdev_has_property(const(libevdev)*, uint) @nogc nothrow;
    int libevdev_get_driver_version(const(libevdev)*) @nogc nothrow;
    void libevdev_set_id_version(libevdev*, int) @nogc nothrow;
    int libevdev_get_id_version(const(libevdev)*) @nogc nothrow;
    void libevdev_set_id_bustype(libevdev*, int) @nogc nothrow;
    int libevdev_get_id_bustype(const(libevdev)*) @nogc nothrow;
    void libevdev_set_id_vendor(libevdev*, int) @nogc nothrow;
    int libevdev_get_id_vendor(const(libevdev)*) @nogc nothrow;
    void libevdev_set_id_product(libevdev*, int) @nogc nothrow;
    int libevdev_get_id_product(const(libevdev)*) @nogc nothrow;
    void libevdev_set_uniq(libevdev*, const(char)*) @nogc nothrow;
    const(char)* libevdev_get_uniq(const(libevdev)*) @nogc nothrow;
    void libevdev_set_phys(libevdev*, const(char)*) @nogc nothrow;
    const(char)* libevdev_get_phys(const(libevdev)*) @nogc nothrow;
    void libevdev_set_name(libevdev*, const(char)*) @nogc nothrow;
    const(char)* libevdev_get_name(const(libevdev)*) @nogc nothrow;
    int libevdev_has_event_pending(libevdev*) @nogc nothrow;
    int libevdev_next_event(libevdev*, uint, input_event*) @nogc nothrow;
    enum libevdev_read_status
    {
        LIBEVDEV_READ_STATUS_SUCCESS = 0,
        LIBEVDEV_READ_STATUS_SYNC = 1,
    }
    enum LIBEVDEV_READ_STATUS_SUCCESS = libevdev_read_status.LIBEVDEV_READ_STATUS_SUCCESS;
    enum LIBEVDEV_READ_STATUS_SYNC = libevdev_read_status.LIBEVDEV_READ_STATUS_SYNC;
    int libevdev_get_fd(const(libevdev)*) @nogc nothrow;
    int libevdev_change_fd(libevdev*, int) @nogc nothrow;
    int libevdev_set_fd(libevdev*, int) @nogc nothrow;
    int libevdev_grab(libevdev*, libevdev_grab_mode) @nogc nothrow;
    enum libevdev_grab_mode
    {
        LIBEVDEV_GRAB = 3,
        LIBEVDEV_UNGRAB = 4,
    }
    enum LIBEVDEV_GRAB = libevdev_grab_mode.LIBEVDEV_GRAB;
    enum LIBEVDEV_UNGRAB = libevdev_grab_mode.LIBEVDEV_UNGRAB;
    void libevdev_set_device_log_function(libevdev*, void function(const(libevdev)*, libevdev_log_priority, void*, const(char)*, int, const(char)*, const(char)*, va_list[1]), libevdev_log_priority, void*) @nogc nothrow;
    alias libevdev_device_log_func_t = void function(const(libevdev)*, libevdev_log_priority, void*, const(char)*, int, const(char)*, const(char)*, va_list*);
    libevdev_log_priority libevdev_get_log_priority() @nogc nothrow;
    void libevdev_set_log_priority(libevdev_log_priority) @nogc nothrow;
    void libevdev_set_log_function(void function(libevdev_log_priority, void*, const(char)*, int, const(char)*, const(char)*, va_list[1]), void*) @nogc nothrow;
    alias libevdev_log_func_t = void function(libevdev_log_priority, void*, const(char)*, int, const(char)*, const(char)*, va_list*);
    enum libevdev_log_priority
    {
        LIBEVDEV_LOG_ERROR = 10,
        LIBEVDEV_LOG_INFO = 20,
        LIBEVDEV_LOG_DEBUG = 30,
    }
    enum LIBEVDEV_LOG_ERROR = libevdev_log_priority.LIBEVDEV_LOG_ERROR;
    enum LIBEVDEV_LOG_INFO = libevdev_log_priority.LIBEVDEV_LOG_INFO;
    enum LIBEVDEV_LOG_DEBUG = libevdev_log_priority.LIBEVDEV_LOG_DEBUG;
    void libevdev_free(libevdev*) @nogc nothrow;
    int libevdev_new_from_fd(int, libevdev**) @nogc nothrow;
    libevdev* libevdev_new() @nogc nothrow;
    enum libevdev_read_flag
    {
        LIBEVDEV_READ_FLAG_SYNC = 1,
        LIBEVDEV_READ_FLAG_NORMAL = 2,
        LIBEVDEV_READ_FLAG_FORCE_SYNC = 4,
        LIBEVDEV_READ_FLAG_BLOCKING = 8,
    }
    enum LIBEVDEV_READ_FLAG_SYNC = libevdev_read_flag.LIBEVDEV_READ_FLAG_SYNC;
    enum LIBEVDEV_READ_FLAG_NORMAL = libevdev_read_flag.LIBEVDEV_READ_FLAG_NORMAL;
    enum LIBEVDEV_READ_FLAG_FORCE_SYNC = libevdev_read_flag.LIBEVDEV_READ_FLAG_FORCE_SYNC;
    enum LIBEVDEV_READ_FLAG_BLOCKING = libevdev_read_flag.LIBEVDEV_READ_FLAG_BLOCKING;
    struct libevdev;
    int libevdev_uinput_write_event(const(libevdev_uinput)*, uint, uint, int) @nogc nothrow;
    const(char)* libevdev_uinput_get_devnode(libevdev_uinput*) @nogc nothrow;
    const(char)* libevdev_uinput_get_syspath(libevdev_uinput*) @nogc nothrow;
    int libevdev_uinput_get_fd(const(libevdev_uinput)*) @nogc nothrow;
    void libevdev_uinput_destroy(libevdev_uinput*) @nogc nothrow;
    int libevdev_uinput_create_from_device(const(libevdev)*, int, libevdev_uinput**) @nogc nothrow;
    enum libevdev_uinput_open_mode
    {
        LIBEVDEV_UINPUT_OPEN_MANAGED = -2,
    }
    enum LIBEVDEV_UINPUT_OPEN_MANAGED = libevdev_uinput_open_mode.LIBEVDEV_UINPUT_OPEN_MANAGED;
    struct libevdev_uinput;
    struct flock
    {
        short l_type;
        short l_whence;
        c_long l_start;
        c_long l_len;
        int l_pid;
    }
    extern __gshared char* optarg;
    extern __gshared int optind;
    extern __gshared int opterr;
    extern __gshared int optopt;
    int getopt(int, char**, const(char)*) @nogc nothrow;
    struct winsize
    {
        ushort ws_row;
        ushort ws_col;
        ushort ws_xpixel;
        ushort ws_ypixel;
    }
    struct termio
    {
        ushort c_iflag;
        ushort c_oflag;
        ushort c_cflag;
        ushort c_lflag;
        ubyte c_line;
        ubyte[8] c_cc;
    }
    int posix_fallocate(int, c_long, c_long) @nogc nothrow;
    int posix_fadvise(int, c_long, c_long, int) @nogc nothrow;
    int creat(const(char)*, uint) @nogc nothrow;
    int openat(int, const(char)*, int, ...) @nogc nothrow;
    int open(const(char)*, int, ...) @nogc nothrow;
    int fcntl(int, int, ...) @nogc nothrow;
    alias mode_t = uint;
    int* __errno_location() @nogc nothrow;
    static c_ulong __uint64_identity(c_ulong) @nogc nothrow;
    static uint __uint32_identity(uint) @nogc nothrow;
    static ushort __uint16_identity(ushort) @nogc nothrow;
    alias timer_t = void*;
    alias time_t = c_long;
    struct timeval
    {
        c_long tv_sec;
        c_long tv_usec;
    }
    struct timespec
    {
        c_long tv_sec;
        c_long tv_nsec;
    }
    alias sigset_t = __sigset_t;
    alias locale_t = __locale_struct*;
    alias clockid_t = int;
    alias clock_t = c_long;
    struct __sigset_t
    {
        c_ulong[16] __val;
    }
    alias __locale_t = __locale_struct*;
    struct __locale_struct
    {
        __locale_data*[13] __locales;
        const(ushort)* __ctype_b;
        const(int)* __ctype_tolower;
        const(int)* __ctype_toupper;
        const(char)*[13] __names;
    }
    alias __sig_atomic_t = int;
    alias __socklen_t = uint;
    alias __intptr_t = c_long;
    alias __caddr_t = char*;
    alias __loff_t = c_long;
    alias __syscall_ulong_t = c_ulong;
    alias __syscall_slong_t = c_long;
    alias __ssize_t = c_long;
    alias __fsword_t = c_long;
    alias __fsfilcnt64_t = c_ulong;
    alias __fsfilcnt_t = c_ulong;
    alias __fsblkcnt64_t = c_ulong;
    alias __fsblkcnt_t = c_ulong;
    alias __blkcnt64_t = c_long;
    alias __blkcnt_t = c_long;
    alias __blksize_t = c_long;
    alias __timer_t = void*;
    alias __clockid_t = int;
    alias __key_t = int;
    alias __daddr_t = int;
    alias __suseconds64_t = c_long;
    alias __suseconds_t = c_long;
    alias __useconds_t = uint;
    alias __time_t = c_long;
    alias pthread_t = c_ulong;
    union pthread_mutexattr_t
    {
        char[4] __size;
        int __align;
    }
    union pthread_condattr_t
    {
        char[4] __size;
        int __align;
    }
    alias pthread_key_t = uint;
    alias pthread_once_t = int;
    union pthread_attr_t
    {
        char[56] __size;
        c_long __align;
    }
    union pthread_mutex_t
    {
        __pthread_mutex_s __data;
        char[40] __size;
        c_long __align;
    }
    union pthread_cond_t
    {
        __pthread_cond_s __data;
        char[48] __size;
        long __align;
    }
    union pthread_rwlock_t
    {
        __pthread_rwlock_arch_t __data;
        char[56] __size;
        c_long __align;
    }
    union pthread_rwlockattr_t
    {
        char[8] __size;
        c_long __align;
    }
    alias pthread_spinlock_t = int;
    union pthread_barrier_t
    {
        char[32] __size;
        c_long __align;
    }
    union pthread_barrierattr_t
    {
        char[4] __size;
        int __align;
    }
    alias __id_t = uint;
    alias __rlim64_t = c_ulong;
    alias __rlim_t = c_ulong;
    struct stat
    {
        c_ulong st_dev;
        c_ulong st_ino;
        c_ulong st_nlink;
        uint st_mode;
        uint st_uid;
        uint st_gid;
        int __pad0;
        c_ulong st_rdev;
        c_long st_size;
        c_long st_blksize;
        c_long st_blocks;
        timespec st_atim;
        timespec st_mtim;
        timespec st_ctim;
        c_long[3] __glibc_reserved;
    }
    alias __clock_t = c_long;
    struct __fsid_t
    {
        int[2] __val;
    }
    alias __pid_t = int;
    alias __off64_t = c_long;
    alias __off_t = c_long;
    alias __nlink_t = c_ulong;
    alias __mode_t = uint;
    alias __ino64_t = c_ulong;
    alias __ino_t = c_ulong;
    alias int8_t = byte;
    alias int16_t = short;
    alias int32_t = int;
    alias int64_t = c_long;
    struct __pthread_mutex_s
    {
        int __lock;
        uint __count;
        int __owner;
        uint __nusers;
        int __kind;
        short __spins;
        short __elision;
        __pthread_internal_list __list;
    }
    alias __gid_t = uint;
    struct __pthread_rwlock_arch_t
    {
        uint __readers;
        uint __writers;
        uint __wrphase_futex;
        uint __writers_futex;
        uint __pad3;
        uint __pad4;
        int __cur_writer;
        int __shared;
        byte __rwelision;
        ubyte[7] __pad1;
        c_ulong __pad2;
        uint __flags;
    }
    alias __uid_t = uint;
    alias __pthread_list_t = __pthread_internal_list;
    struct __pthread_internal_list
    {
        __pthread_internal_list* __prev;
        __pthread_internal_list* __next;
    }
    alias __pthread_slist_t = __pthread_internal_slist;
    struct __pthread_internal_slist
    {
        __pthread_internal_slist* __next;
    }
    struct __pthread_cond_s
    {
        static union _Anonymous_4
        {
            ulong __wseq;
            static struct _Anonymous_5
            {
                uint __low;
                uint __high;
            }
            _Anonymous_5 __wseq32;
        }
        _Anonymous_4 _anonymous_6;
        auto __wseq() @property @nogc pure nothrow { return _anonymous_6.__wseq; }
        void __wseq(_T_)(auto ref _T_ val) @property @nogc pure nothrow { _anonymous_6.__wseq = val; }
        auto __wseq32() @property @nogc pure nothrow { return _anonymous_6.__wseq32; }
        void __wseq32(_T_)(auto ref _T_ val) @property @nogc pure nothrow { _anonymous_6.__wseq32 = val; }
        static union _Anonymous_7
        {
            ulong __g1_start;
            static struct _Anonymous_8
            {
                uint __low;
                uint __high;
            }
            _Anonymous_8 __g1_start32;
        }
        _Anonymous_7 _anonymous_9;
        auto __g1_start() @property @nogc pure nothrow { return _anonymous_9.__g1_start; }
        void __g1_start(_T_)(auto ref _T_ val) @property @nogc pure nothrow { _anonymous_9.__g1_start = val; }
        auto __g1_start32() @property @nogc pure nothrow { return _anonymous_9.__g1_start32; }
        void __g1_start32(_T_)(auto ref _T_ val) @property @nogc pure nothrow { _anonymous_9.__g1_start32 = val; }
        uint[2] __g_refs;
        uint[2] __g_size;
        uint __g1_orig_size;
        uint __wrefs;
        uint[2] __g_signals;
    }
    alias __tss_t = uint;
    alias __thrd_t = c_ulong;
    struct __once_flag
    {
        int __data;
    }
    alias __dev_t = c_ulong;
    alias __u_char = ubyte;
    alias __u_short = ushort;
    alias __u_int = uint;
    alias __u_long = c_ulong;
    alias __int8_t = byte;
    alias __uint8_t = ubyte;
    alias __int16_t = short;
    alias __uint16_t = ushort;
    alias __int32_t = int;
    alias __uint32_t = uint;
    alias __int64_t = c_long;
    alias __uint64_t = c_ulong;
    alias __int_least8_t = byte;
    alias __uint_least8_t = ubyte;
    alias __int_least16_t = short;
    alias __uint_least16_t = ushort;
    alias __int_least32_t = int;
    alias __uint_least32_t = uint;
    alias __int_least64_t = c_long;
    alias __uint_least64_t = c_ulong;
    alias __quad_t = c_long;
    alias __u_quad_t = c_ulong;
    alias __intmax_t = c_long;
    alias __uintmax_t = c_ulong;



    static if(!is(typeof(__S32_TYPE))) {
        private enum enumMixinStr___S32_TYPE = `enum __S32_TYPE = int;`;
        static if(is(typeof({ mixin(enumMixinStr___S32_TYPE); }))) {
            mixin(enumMixinStr___S32_TYPE);
        }
    }




    static if(!is(typeof(__U32_TYPE))) {
        private enum enumMixinStr___U32_TYPE = `enum __U32_TYPE = unsigned int;`;
        static if(is(typeof({ mixin(enumMixinStr___U32_TYPE); }))) {
            mixin(enumMixinStr___U32_TYPE);
        }
    }




    static if(!is(typeof(__SLONGWORD_TYPE))) {
        private enum enumMixinStr___SLONGWORD_TYPE = `enum __SLONGWORD_TYPE = long int;`;
        static if(is(typeof({ mixin(enumMixinStr___SLONGWORD_TYPE); }))) {
            mixin(enumMixinStr___SLONGWORD_TYPE);
        }
    }




    static if(!is(typeof(__ULONGWORD_TYPE))) {
        private enum enumMixinStr___ULONGWORD_TYPE = `enum __ULONGWORD_TYPE = unsigned long int;`;
        static if(is(typeof({ mixin(enumMixinStr___ULONGWORD_TYPE); }))) {
            mixin(enumMixinStr___ULONGWORD_TYPE);
        }
    }




    static if(!is(typeof(__U16_TYPE))) {
        private enum enumMixinStr___U16_TYPE = `enum __U16_TYPE = unsigned short int;`;
        static if(is(typeof({ mixin(enumMixinStr___U16_TYPE); }))) {
            mixin(enumMixinStr___U16_TYPE);
        }
    }




    static if(!is(typeof(__S16_TYPE))) {
        private enum enumMixinStr___S16_TYPE = `enum __S16_TYPE = short int;`;
        static if(is(typeof({ mixin(enumMixinStr___S16_TYPE); }))) {
            mixin(enumMixinStr___S16_TYPE);
        }
    }




    static if(!is(typeof(__SQUAD_TYPE))) {
        private enum enumMixinStr___SQUAD_TYPE = `enum __SQUAD_TYPE = long int;`;
        static if(is(typeof({ mixin(enumMixinStr___SQUAD_TYPE); }))) {
            mixin(enumMixinStr___SQUAD_TYPE);
        }
    }




    static if(!is(typeof(__UQUAD_TYPE))) {
        private enum enumMixinStr___UQUAD_TYPE = `enum __UQUAD_TYPE = unsigned long int;`;
        static if(is(typeof({ mixin(enumMixinStr___UQUAD_TYPE); }))) {
            mixin(enumMixinStr___UQUAD_TYPE);
        }
    }




    static if(!is(typeof(__SWORD_TYPE))) {
        private enum enumMixinStr___SWORD_TYPE = `enum __SWORD_TYPE = long int;`;
        static if(is(typeof({ mixin(enumMixinStr___SWORD_TYPE); }))) {
            mixin(enumMixinStr___SWORD_TYPE);
        }
    }




    static if(!is(typeof(__UWORD_TYPE))) {
        private enum enumMixinStr___UWORD_TYPE = `enum __UWORD_TYPE = unsigned long int;`;
        static if(is(typeof({ mixin(enumMixinStr___UWORD_TYPE); }))) {
            mixin(enumMixinStr___UWORD_TYPE);
        }
    }




    static if(!is(typeof(__SLONG32_TYPE))) {
        private enum enumMixinStr___SLONG32_TYPE = `enum __SLONG32_TYPE = int;`;
        static if(is(typeof({ mixin(enumMixinStr___SLONG32_TYPE); }))) {
            mixin(enumMixinStr___SLONG32_TYPE);
        }
    }




    static if(!is(typeof(__ULONG32_TYPE))) {
        private enum enumMixinStr___ULONG32_TYPE = `enum __ULONG32_TYPE = unsigned int;`;
        static if(is(typeof({ mixin(enumMixinStr___ULONG32_TYPE); }))) {
            mixin(enumMixinStr___ULONG32_TYPE);
        }
    }




    static if(!is(typeof(__S64_TYPE))) {
        private enum enumMixinStr___S64_TYPE = `enum __S64_TYPE = long int;`;
        static if(is(typeof({ mixin(enumMixinStr___S64_TYPE); }))) {
            mixin(enumMixinStr___S64_TYPE);
        }
    }




    static if(!is(typeof(__U64_TYPE))) {
        private enum enumMixinStr___U64_TYPE = `enum __U64_TYPE = unsigned long int;`;
        static if(is(typeof({ mixin(enumMixinStr___U64_TYPE); }))) {
            mixin(enumMixinStr___U64_TYPE);
        }
    }




    static if(!is(typeof(__STD_TYPE))) {
        private enum enumMixinStr___STD_TYPE = `enum __STD_TYPE = typedef;`;
        static if(is(typeof({ mixin(enumMixinStr___STD_TYPE); }))) {
            mixin(enumMixinStr___STD_TYPE);
        }
    }




    static if(!is(typeof(_BITS_TYPES_H))) {
        private enum enumMixinStr__BITS_TYPES_H = `enum _BITS_TYPES_H = 1;`;
        static if(is(typeof({ mixin(enumMixinStr__BITS_TYPES_H); }))) {
            mixin(enumMixinStr__BITS_TYPES_H);
        }
    }




    static if(!is(typeof(__TIMESIZE))) {
        private enum enumMixinStr___TIMESIZE = `enum __TIMESIZE = __WORDSIZE;`;
        static if(is(typeof({ mixin(enumMixinStr___TIMESIZE); }))) {
            mixin(enumMixinStr___TIMESIZE);
        }
    }




    static if(!is(typeof(__TIME64_T_TYPE))) {
        private enum enumMixinStr___TIME64_T_TYPE = `enum __TIME64_T_TYPE = __TIME_T_TYPE;`;
        static if(is(typeof({ mixin(enumMixinStr___TIME64_T_TYPE); }))) {
            mixin(enumMixinStr___TIME64_T_TYPE);
        }
    }




    static if(!is(typeof(_BITS_TIME64_H))) {
        private enum enumMixinStr__BITS_TIME64_H = `enum _BITS_TIME64_H = 1;`;
        static if(is(typeof({ mixin(enumMixinStr__BITS_TIME64_H); }))) {
            mixin(enumMixinStr__BITS_TIME64_H);
        }
    }




    static if(!is(typeof(__ONCE_FLAG_INIT))) {
        private enum enumMixinStr___ONCE_FLAG_INIT = `enum __ONCE_FLAG_INIT = { 0 };`;
        static if(is(typeof({ mixin(enumMixinStr___ONCE_FLAG_INIT); }))) {
            mixin(enumMixinStr___ONCE_FLAG_INIT);
        }
    }




    static if(!is(typeof(_THREAD_SHARED_TYPES_H))) {
        private enum enumMixinStr__THREAD_SHARED_TYPES_H = `enum _THREAD_SHARED_TYPES_H = 1;`;
        static if(is(typeof({ mixin(enumMixinStr__THREAD_SHARED_TYPES_H); }))) {
            mixin(enumMixinStr__THREAD_SHARED_TYPES_H);
        }
    }






    static if(!is(typeof(__PTHREAD_RWLOCK_ELISION_EXTRA))) {
        private enum enumMixinStr___PTHREAD_RWLOCK_ELISION_EXTRA = `enum __PTHREAD_RWLOCK_ELISION_EXTRA = 0 , { 0 , 0 , 0 , 0 , 0 , 0 , 0 };`;
        static if(is(typeof({ mixin(enumMixinStr___PTHREAD_RWLOCK_ELISION_EXTRA); }))) {
            mixin(enumMixinStr___PTHREAD_RWLOCK_ELISION_EXTRA);
        }
    }
    static if(!is(typeof(__PTHREAD_MUTEX_HAVE_PREV))) {
        private enum enumMixinStr___PTHREAD_MUTEX_HAVE_PREV = `enum __PTHREAD_MUTEX_HAVE_PREV = 1;`;
        static if(is(typeof({ mixin(enumMixinStr___PTHREAD_MUTEX_HAVE_PREV); }))) {
            mixin(enumMixinStr___PTHREAD_MUTEX_HAVE_PREV);
        }
    }




    static if(!is(typeof(_THREAD_MUTEX_INTERNAL_H))) {
        private enum enumMixinStr__THREAD_MUTEX_INTERNAL_H = `enum _THREAD_MUTEX_INTERNAL_H = 1;`;
        static if(is(typeof({ mixin(enumMixinStr__THREAD_MUTEX_INTERNAL_H); }))) {
            mixin(enumMixinStr__THREAD_MUTEX_INTERNAL_H);
        }
    }




    static if(!is(typeof(_BITS_STDINT_INTN_H))) {
        private enum enumMixinStr__BITS_STDINT_INTN_H = `enum _BITS_STDINT_INTN_H = 1;`;
        static if(is(typeof({ mixin(enumMixinStr__BITS_STDINT_INTN_H); }))) {
            mixin(enumMixinStr__BITS_STDINT_INTN_H);
        }
    }




    static if(!is(typeof(UTIME_OMIT))) {
        private enum enumMixinStr_UTIME_OMIT = `enum UTIME_OMIT = ( ( 1l << 30 ) - 2l );`;
        static if(is(typeof({ mixin(enumMixinStr_UTIME_OMIT); }))) {
            mixin(enumMixinStr_UTIME_OMIT);
        }
    }




    static if(!is(typeof(UTIME_NOW))) {
        private enum enumMixinStr_UTIME_NOW = `enum UTIME_NOW = ( ( 1l << 30 ) - 1l );`;
        static if(is(typeof({ mixin(enumMixinStr_UTIME_NOW); }))) {
            mixin(enumMixinStr_UTIME_NOW);
        }
    }




    static if(!is(typeof(__S_IEXEC))) {
        private enum enumMixinStr___S_IEXEC = `enum __S_IEXEC = std.conv.octal!100;`;
        static if(is(typeof({ mixin(enumMixinStr___S_IEXEC); }))) {
            mixin(enumMixinStr___S_IEXEC);
        }
    }




    static if(!is(typeof(__S_IWRITE))) {
        private enum enumMixinStr___S_IWRITE = `enum __S_IWRITE = std.conv.octal!200;`;
        static if(is(typeof({ mixin(enumMixinStr___S_IWRITE); }))) {
            mixin(enumMixinStr___S_IWRITE);
        }
    }




    static if(!is(typeof(__S_IREAD))) {
        private enum enumMixinStr___S_IREAD = `enum __S_IREAD = std.conv.octal!400;`;
        static if(is(typeof({ mixin(enumMixinStr___S_IREAD); }))) {
            mixin(enumMixinStr___S_IREAD);
        }
    }




    static if(!is(typeof(__S_ISVTX))) {
        private enum enumMixinStr___S_ISVTX = `enum __S_ISVTX = std.conv.octal!1000;`;
        static if(is(typeof({ mixin(enumMixinStr___S_ISVTX); }))) {
            mixin(enumMixinStr___S_ISVTX);
        }
    }




    static if(!is(typeof(__S_ISGID))) {
        private enum enumMixinStr___S_ISGID = `enum __S_ISGID = std.conv.octal!2000;`;
        static if(is(typeof({ mixin(enumMixinStr___S_ISGID); }))) {
            mixin(enumMixinStr___S_ISGID);
        }
    }




    static if(!is(typeof(__S_ISUID))) {
        private enum enumMixinStr___S_ISUID = `enum __S_ISUID = std.conv.octal!4000;`;
        static if(is(typeof({ mixin(enumMixinStr___S_ISUID); }))) {
            mixin(enumMixinStr___S_ISUID);
        }
    }
    static if(!is(typeof(__S_IFSOCK))) {
        private enum enumMixinStr___S_IFSOCK = `enum __S_IFSOCK = std.conv.octal!140000;`;
        static if(is(typeof({ mixin(enumMixinStr___S_IFSOCK); }))) {
            mixin(enumMixinStr___S_IFSOCK);
        }
    }




    static if(!is(typeof(__S_IFLNK))) {
        private enum enumMixinStr___S_IFLNK = `enum __S_IFLNK = std.conv.octal!120000;`;
        static if(is(typeof({ mixin(enumMixinStr___S_IFLNK); }))) {
            mixin(enumMixinStr___S_IFLNK);
        }
    }




    static if(!is(typeof(__S_IFIFO))) {
        private enum enumMixinStr___S_IFIFO = `enum __S_IFIFO = std.conv.octal!10000;`;
        static if(is(typeof({ mixin(enumMixinStr___S_IFIFO); }))) {
            mixin(enumMixinStr___S_IFIFO);
        }
    }




    static if(!is(typeof(__S_IFREG))) {
        private enum enumMixinStr___S_IFREG = `enum __S_IFREG = std.conv.octal!100000;`;
        static if(is(typeof({ mixin(enumMixinStr___S_IFREG); }))) {
            mixin(enumMixinStr___S_IFREG);
        }
    }




    static if(!is(typeof(__S_IFBLK))) {
        private enum enumMixinStr___S_IFBLK = `enum __S_IFBLK = std.conv.octal!60000;`;
        static if(is(typeof({ mixin(enumMixinStr___S_IFBLK); }))) {
            mixin(enumMixinStr___S_IFBLK);
        }
    }




    static if(!is(typeof(__S_IFCHR))) {
        private enum enumMixinStr___S_IFCHR = `enum __S_IFCHR = std.conv.octal!20000;`;
        static if(is(typeof({ mixin(enumMixinStr___S_IFCHR); }))) {
            mixin(enumMixinStr___S_IFCHR);
        }
    }




    static if(!is(typeof(__S_IFDIR))) {
        private enum enumMixinStr___S_IFDIR = `enum __S_IFDIR = std.conv.octal!40000;`;
        static if(is(typeof({ mixin(enumMixinStr___S_IFDIR); }))) {
            mixin(enumMixinStr___S_IFDIR);
        }
    }




    static if(!is(typeof(__S_IFMT))) {
        private enum enumMixinStr___S_IFMT = `enum __S_IFMT = std.conv.octal!170000;`;
        static if(is(typeof({ mixin(enumMixinStr___S_IFMT); }))) {
            mixin(enumMixinStr___S_IFMT);
        }
    }
    static if(!is(typeof(st_ctime))) {
        private enum enumMixinStr_st_ctime = `enum st_ctime = st_ctim . tv_sec;`;
        static if(is(typeof({ mixin(enumMixinStr_st_ctime); }))) {
            mixin(enumMixinStr_st_ctime);
        }
    }




    static if(!is(typeof(st_mtime))) {
        private enum enumMixinStr_st_mtime = `enum st_mtime = st_mtim . tv_sec;`;
        static if(is(typeof({ mixin(enumMixinStr_st_mtime); }))) {
            mixin(enumMixinStr_st_mtime);
        }
    }




    static if(!is(typeof(st_atime))) {
        private enum enumMixinStr_st_atime = `enum st_atime = st_atim . tv_sec;`;
        static if(is(typeof({ mixin(enumMixinStr_st_atime); }))) {
            mixin(enumMixinStr_st_atime);
        }
    }




    static if(!is(typeof(_STAT_VER))) {
        private enum enumMixinStr__STAT_VER = `enum _STAT_VER = _STAT_VER_LINUX;`;
        static if(is(typeof({ mixin(enumMixinStr__STAT_VER); }))) {
            mixin(enumMixinStr__STAT_VER);
        }
    }




    static if(!is(typeof(_MKNOD_VER_LINUX))) {
        private enum enumMixinStr__MKNOD_VER_LINUX = `enum _MKNOD_VER_LINUX = 0;`;
        static if(is(typeof({ mixin(enumMixinStr__MKNOD_VER_LINUX); }))) {
            mixin(enumMixinStr__MKNOD_VER_LINUX);
        }
    }




    static if(!is(typeof(_STAT_VER_LINUX))) {
        private enum enumMixinStr__STAT_VER_LINUX = `enum _STAT_VER_LINUX = 1;`;
        static if(is(typeof({ mixin(enumMixinStr__STAT_VER_LINUX); }))) {
            mixin(enumMixinStr__STAT_VER_LINUX);
        }
    }




    static if(!is(typeof(_STAT_VER_KERNEL))) {
        private enum enumMixinStr__STAT_VER_KERNEL = `enum _STAT_VER_KERNEL = 0;`;
        static if(is(typeof({ mixin(enumMixinStr__STAT_VER_KERNEL); }))) {
            mixin(enumMixinStr__STAT_VER_KERNEL);
        }
    }




    static if(!is(typeof(_BITS_STAT_H))) {
        private enum enumMixinStr__BITS_STAT_H = `enum _BITS_STAT_H = 1;`;
        static if(is(typeof({ mixin(enumMixinStr__BITS_STAT_H); }))) {
            mixin(enumMixinStr__BITS_STAT_H);
        }
    }
    static if(!is(typeof(__have_pthread_attr_t))) {
        private enum enumMixinStr___have_pthread_attr_t = `enum __have_pthread_attr_t = 1;`;
        static if(is(typeof({ mixin(enumMixinStr___have_pthread_attr_t); }))) {
            mixin(enumMixinStr___have_pthread_attr_t);
        }
    }




    static if(!is(typeof(_BITS_PTHREADTYPES_COMMON_H))) {
        private enum enumMixinStr__BITS_PTHREADTYPES_COMMON_H = `enum _BITS_PTHREADTYPES_COMMON_H = 1;`;
        static if(is(typeof({ mixin(enumMixinStr__BITS_PTHREADTYPES_COMMON_H); }))) {
            mixin(enumMixinStr__BITS_PTHREADTYPES_COMMON_H);
        }
    }
    static if(!is(typeof(__SIZEOF_PTHREAD_BARRIERATTR_T))) {
        private enum enumMixinStr___SIZEOF_PTHREAD_BARRIERATTR_T = `enum __SIZEOF_PTHREAD_BARRIERATTR_T = 4;`;
        static if(is(typeof({ mixin(enumMixinStr___SIZEOF_PTHREAD_BARRIERATTR_T); }))) {
            mixin(enumMixinStr___SIZEOF_PTHREAD_BARRIERATTR_T);
        }
    }




    static if(!is(typeof(__SIZEOF_PTHREAD_RWLOCKATTR_T))) {
        private enum enumMixinStr___SIZEOF_PTHREAD_RWLOCKATTR_T = `enum __SIZEOF_PTHREAD_RWLOCKATTR_T = 8;`;
        static if(is(typeof({ mixin(enumMixinStr___SIZEOF_PTHREAD_RWLOCKATTR_T); }))) {
            mixin(enumMixinStr___SIZEOF_PTHREAD_RWLOCKATTR_T);
        }
    }




    static if(!is(typeof(__SIZEOF_PTHREAD_CONDATTR_T))) {
        private enum enumMixinStr___SIZEOF_PTHREAD_CONDATTR_T = `enum __SIZEOF_PTHREAD_CONDATTR_T = 4;`;
        static if(is(typeof({ mixin(enumMixinStr___SIZEOF_PTHREAD_CONDATTR_T); }))) {
            mixin(enumMixinStr___SIZEOF_PTHREAD_CONDATTR_T);
        }
    }




    static if(!is(typeof(__SIZEOF_PTHREAD_COND_T))) {
        private enum enumMixinStr___SIZEOF_PTHREAD_COND_T = `enum __SIZEOF_PTHREAD_COND_T = 48;`;
        static if(is(typeof({ mixin(enumMixinStr___SIZEOF_PTHREAD_COND_T); }))) {
            mixin(enumMixinStr___SIZEOF_PTHREAD_COND_T);
        }
    }




    static if(!is(typeof(__SIZEOF_PTHREAD_MUTEXATTR_T))) {
        private enum enumMixinStr___SIZEOF_PTHREAD_MUTEXATTR_T = `enum __SIZEOF_PTHREAD_MUTEXATTR_T = 4;`;
        static if(is(typeof({ mixin(enumMixinStr___SIZEOF_PTHREAD_MUTEXATTR_T); }))) {
            mixin(enumMixinStr___SIZEOF_PTHREAD_MUTEXATTR_T);
        }
    }




    static if(!is(typeof(__SIZEOF_PTHREAD_BARRIER_T))) {
        private enum enumMixinStr___SIZEOF_PTHREAD_BARRIER_T = `enum __SIZEOF_PTHREAD_BARRIER_T = 32;`;
        static if(is(typeof({ mixin(enumMixinStr___SIZEOF_PTHREAD_BARRIER_T); }))) {
            mixin(enumMixinStr___SIZEOF_PTHREAD_BARRIER_T);
        }
    }




    static if(!is(typeof(__SIZEOF_PTHREAD_RWLOCK_T))) {
        private enum enumMixinStr___SIZEOF_PTHREAD_RWLOCK_T = `enum __SIZEOF_PTHREAD_RWLOCK_T = 56;`;
        static if(is(typeof({ mixin(enumMixinStr___SIZEOF_PTHREAD_RWLOCK_T); }))) {
            mixin(enumMixinStr___SIZEOF_PTHREAD_RWLOCK_T);
        }
    }




    static if(!is(typeof(__SIZEOF_PTHREAD_ATTR_T))) {
        private enum enumMixinStr___SIZEOF_PTHREAD_ATTR_T = `enum __SIZEOF_PTHREAD_ATTR_T = 56;`;
        static if(is(typeof({ mixin(enumMixinStr___SIZEOF_PTHREAD_ATTR_T); }))) {
            mixin(enumMixinStr___SIZEOF_PTHREAD_ATTR_T);
        }
    }




    static if(!is(typeof(__SIZEOF_PTHREAD_MUTEX_T))) {
        private enum enumMixinStr___SIZEOF_PTHREAD_MUTEX_T = `enum __SIZEOF_PTHREAD_MUTEX_T = 40;`;
        static if(is(typeof({ mixin(enumMixinStr___SIZEOF_PTHREAD_MUTEX_T); }))) {
            mixin(enumMixinStr___SIZEOF_PTHREAD_MUTEX_T);
        }
    }




    static if(!is(typeof(_BITS_PTHREADTYPES_ARCH_H))) {
        private enum enumMixinStr__BITS_PTHREADTYPES_ARCH_H = `enum _BITS_PTHREADTYPES_ARCH_H = 1;`;
        static if(is(typeof({ mixin(enumMixinStr__BITS_PTHREADTYPES_ARCH_H); }))) {
            mixin(enumMixinStr__BITS_PTHREADTYPES_ARCH_H);
        }
    }




    static if(!is(typeof(_POSIX_TYPED_MEMORY_OBJECTS))) {
        private enum enumMixinStr__POSIX_TYPED_MEMORY_OBJECTS = `enum _POSIX_TYPED_MEMORY_OBJECTS = - 1;`;
        static if(is(typeof({ mixin(enumMixinStr__POSIX_TYPED_MEMORY_OBJECTS); }))) {
            mixin(enumMixinStr__POSIX_TYPED_MEMORY_OBJECTS);
        }
    }




    static if(!is(typeof(_POSIX_TRACE_LOG))) {
        private enum enumMixinStr__POSIX_TRACE_LOG = `enum _POSIX_TRACE_LOG = - 1;`;
        static if(is(typeof({ mixin(enumMixinStr__POSIX_TRACE_LOG); }))) {
            mixin(enumMixinStr__POSIX_TRACE_LOG);
        }
    }




    static if(!is(typeof(_POSIX_TRACE_INHERIT))) {
        private enum enumMixinStr__POSIX_TRACE_INHERIT = `enum _POSIX_TRACE_INHERIT = - 1;`;
        static if(is(typeof({ mixin(enumMixinStr__POSIX_TRACE_INHERIT); }))) {
            mixin(enumMixinStr__POSIX_TRACE_INHERIT);
        }
    }




    static if(!is(typeof(_POSIX_TRACE_EVENT_FILTER))) {
        private enum enumMixinStr__POSIX_TRACE_EVENT_FILTER = `enum _POSIX_TRACE_EVENT_FILTER = - 1;`;
        static if(is(typeof({ mixin(enumMixinStr__POSIX_TRACE_EVENT_FILTER); }))) {
            mixin(enumMixinStr__POSIX_TRACE_EVENT_FILTER);
        }
    }




    static if(!is(typeof(_POSIX_TRACE))) {
        private enum enumMixinStr__POSIX_TRACE = `enum _POSIX_TRACE = - 1;`;
        static if(is(typeof({ mixin(enumMixinStr__POSIX_TRACE); }))) {
            mixin(enumMixinStr__POSIX_TRACE);
        }
    }




    static if(!is(typeof(_POSIX_THREAD_SPORADIC_SERVER))) {
        private enum enumMixinStr__POSIX_THREAD_SPORADIC_SERVER = `enum _POSIX_THREAD_SPORADIC_SERVER = - 1;`;
        static if(is(typeof({ mixin(enumMixinStr__POSIX_THREAD_SPORADIC_SERVER); }))) {
            mixin(enumMixinStr__POSIX_THREAD_SPORADIC_SERVER);
        }
    }




    static if(!is(typeof(_POSIX_SPORADIC_SERVER))) {
        private enum enumMixinStr__POSIX_SPORADIC_SERVER = `enum _POSIX_SPORADIC_SERVER = - 1;`;
        static if(is(typeof({ mixin(enumMixinStr__POSIX_SPORADIC_SERVER); }))) {
            mixin(enumMixinStr__POSIX_SPORADIC_SERVER);
        }
    }




    static if(!is(typeof(_POSIX2_CHAR_TERM))) {
        private enum enumMixinStr__POSIX2_CHAR_TERM = `enum _POSIX2_CHAR_TERM = 200809L;`;
        static if(is(typeof({ mixin(enumMixinStr__POSIX2_CHAR_TERM); }))) {
            mixin(enumMixinStr__POSIX2_CHAR_TERM);
        }
    }




    static if(!is(typeof(_POSIX_RAW_SOCKETS))) {
        private enum enumMixinStr__POSIX_RAW_SOCKETS = `enum _POSIX_RAW_SOCKETS = 200809L;`;
        static if(is(typeof({ mixin(enumMixinStr__POSIX_RAW_SOCKETS); }))) {
            mixin(enumMixinStr__POSIX_RAW_SOCKETS);
        }
    }




    static if(!is(typeof(_POSIX_IPV6))) {
        private enum enumMixinStr__POSIX_IPV6 = `enum _POSIX_IPV6 = 200809L;`;
        static if(is(typeof({ mixin(enumMixinStr__POSIX_IPV6); }))) {
            mixin(enumMixinStr__POSIX_IPV6);
        }
    }




    static if(!is(typeof(_POSIX_ADVISORY_INFO))) {
        private enum enumMixinStr__POSIX_ADVISORY_INFO = `enum _POSIX_ADVISORY_INFO = 200809L;`;
        static if(is(typeof({ mixin(enumMixinStr__POSIX_ADVISORY_INFO); }))) {
            mixin(enumMixinStr__POSIX_ADVISORY_INFO);
        }
    }




    static if(!is(typeof(_POSIX_CLOCK_SELECTION))) {
        private enum enumMixinStr__POSIX_CLOCK_SELECTION = `enum _POSIX_CLOCK_SELECTION = 200809L;`;
        static if(is(typeof({ mixin(enumMixinStr__POSIX_CLOCK_SELECTION); }))) {
            mixin(enumMixinStr__POSIX_CLOCK_SELECTION);
        }
    }




    static if(!is(typeof(_POSIX_MONOTONIC_CLOCK))) {
        private enum enumMixinStr__POSIX_MONOTONIC_CLOCK = `enum _POSIX_MONOTONIC_CLOCK = 0;`;
        static if(is(typeof({ mixin(enumMixinStr__POSIX_MONOTONIC_CLOCK); }))) {
            mixin(enumMixinStr__POSIX_MONOTONIC_CLOCK);
        }
    }




    static if(!is(typeof(_POSIX_THREAD_PROCESS_SHARED))) {
        private enum enumMixinStr__POSIX_THREAD_PROCESS_SHARED = `enum _POSIX_THREAD_PROCESS_SHARED = 200809L;`;
        static if(is(typeof({ mixin(enumMixinStr__POSIX_THREAD_PROCESS_SHARED); }))) {
            mixin(enumMixinStr__POSIX_THREAD_PROCESS_SHARED);
        }
    }




    static if(!is(typeof(_POSIX_MESSAGE_PASSING))) {
        private enum enumMixinStr__POSIX_MESSAGE_PASSING = `enum _POSIX_MESSAGE_PASSING = 200809L;`;
        static if(is(typeof({ mixin(enumMixinStr__POSIX_MESSAGE_PASSING); }))) {
            mixin(enumMixinStr__POSIX_MESSAGE_PASSING);
        }
    }




    static if(!is(typeof(_POSIX_BARRIERS))) {
        private enum enumMixinStr__POSIX_BARRIERS = `enum _POSIX_BARRIERS = 200809L;`;
        static if(is(typeof({ mixin(enumMixinStr__POSIX_BARRIERS); }))) {
            mixin(enumMixinStr__POSIX_BARRIERS);
        }
    }




    static if(!is(typeof(_POSIX_TIMERS))) {
        private enum enumMixinStr__POSIX_TIMERS = `enum _POSIX_TIMERS = 200809L;`;
        static if(is(typeof({ mixin(enumMixinStr__POSIX_TIMERS); }))) {
            mixin(enumMixinStr__POSIX_TIMERS);
        }
    }




    static if(!is(typeof(_POSIX_SPAWN))) {
        private enum enumMixinStr__POSIX_SPAWN = `enum _POSIX_SPAWN = 200809L;`;
        static if(is(typeof({ mixin(enumMixinStr__POSIX_SPAWN); }))) {
            mixin(enumMixinStr__POSIX_SPAWN);
        }
    }




    static if(!is(typeof(_POSIX_SPIN_LOCKS))) {
        private enum enumMixinStr__POSIX_SPIN_LOCKS = `enum _POSIX_SPIN_LOCKS = 200809L;`;
        static if(is(typeof({ mixin(enumMixinStr__POSIX_SPIN_LOCKS); }))) {
            mixin(enumMixinStr__POSIX_SPIN_LOCKS);
        }
    }




    static if(!is(typeof(_POSIX_TIMEOUTS))) {
        private enum enumMixinStr__POSIX_TIMEOUTS = `enum _POSIX_TIMEOUTS = 200809L;`;
        static if(is(typeof({ mixin(enumMixinStr__POSIX_TIMEOUTS); }))) {
            mixin(enumMixinStr__POSIX_TIMEOUTS);
        }
    }




    static if(!is(typeof(_POSIX_SHELL))) {
        private enum enumMixinStr__POSIX_SHELL = `enum _POSIX_SHELL = 1;`;
        static if(is(typeof({ mixin(enumMixinStr__POSIX_SHELL); }))) {
            mixin(enumMixinStr__POSIX_SHELL);
        }
    }




    static if(!is(typeof(_POSIX_READER_WRITER_LOCKS))) {
        private enum enumMixinStr__POSIX_READER_WRITER_LOCKS = `enum _POSIX_READER_WRITER_LOCKS = 200809L;`;
        static if(is(typeof({ mixin(enumMixinStr__POSIX_READER_WRITER_LOCKS); }))) {
            mixin(enumMixinStr__POSIX_READER_WRITER_LOCKS);
        }
    }




    static if(!is(typeof(_POSIX_REGEXP))) {
        private enum enumMixinStr__POSIX_REGEXP = `enum _POSIX_REGEXP = 1;`;
        static if(is(typeof({ mixin(enumMixinStr__POSIX_REGEXP); }))) {
            mixin(enumMixinStr__POSIX_REGEXP);
        }
    }




    static if(!is(typeof(_POSIX_THREAD_CPUTIME))) {
        private enum enumMixinStr__POSIX_THREAD_CPUTIME = `enum _POSIX_THREAD_CPUTIME = 0;`;
        static if(is(typeof({ mixin(enumMixinStr__POSIX_THREAD_CPUTIME); }))) {
            mixin(enumMixinStr__POSIX_THREAD_CPUTIME);
        }
    }




    static if(!is(typeof(_POSIX_CPUTIME))) {
        private enum enumMixinStr__POSIX_CPUTIME = `enum _POSIX_CPUTIME = 0;`;
        static if(is(typeof({ mixin(enumMixinStr__POSIX_CPUTIME); }))) {
            mixin(enumMixinStr__POSIX_CPUTIME);
        }
    }




    static if(!is(typeof(_POSIX_SHARED_MEMORY_OBJECTS))) {
        private enum enumMixinStr__POSIX_SHARED_MEMORY_OBJECTS = `enum _POSIX_SHARED_MEMORY_OBJECTS = 200809L;`;
        static if(is(typeof({ mixin(enumMixinStr__POSIX_SHARED_MEMORY_OBJECTS); }))) {
            mixin(enumMixinStr__POSIX_SHARED_MEMORY_OBJECTS);
        }
    }




    static if(!is(typeof(_LFS64_STDIO))) {
        private enum enumMixinStr__LFS64_STDIO = `enum _LFS64_STDIO = 1;`;
        static if(is(typeof({ mixin(enumMixinStr__LFS64_STDIO); }))) {
            mixin(enumMixinStr__LFS64_STDIO);
        }
    }




    static if(!is(typeof(_LFS64_LARGEFILE))) {
        private enum enumMixinStr__LFS64_LARGEFILE = `enum _LFS64_LARGEFILE = 1;`;
        static if(is(typeof({ mixin(enumMixinStr__LFS64_LARGEFILE); }))) {
            mixin(enumMixinStr__LFS64_LARGEFILE);
        }
    }




    static if(!is(typeof(_LFS_LARGEFILE))) {
        private enum enumMixinStr__LFS_LARGEFILE = `enum _LFS_LARGEFILE = 1;`;
        static if(is(typeof({ mixin(enumMixinStr__LFS_LARGEFILE); }))) {
            mixin(enumMixinStr__LFS_LARGEFILE);
        }
    }




    static if(!is(typeof(_LFS64_ASYNCHRONOUS_IO))) {
        private enum enumMixinStr__LFS64_ASYNCHRONOUS_IO = `enum _LFS64_ASYNCHRONOUS_IO = 1;`;
        static if(is(typeof({ mixin(enumMixinStr__LFS64_ASYNCHRONOUS_IO); }))) {
            mixin(enumMixinStr__LFS64_ASYNCHRONOUS_IO);
        }
    }




    static if(!is(typeof(_POSIX_PRIORITIZED_IO))) {
        private enum enumMixinStr__POSIX_PRIORITIZED_IO = `enum _POSIX_PRIORITIZED_IO = 200809L;`;
        static if(is(typeof({ mixin(enumMixinStr__POSIX_PRIORITIZED_IO); }))) {
            mixin(enumMixinStr__POSIX_PRIORITIZED_IO);
        }
    }




    static if(!is(typeof(_LFS_ASYNCHRONOUS_IO))) {
        private enum enumMixinStr__LFS_ASYNCHRONOUS_IO = `enum _LFS_ASYNCHRONOUS_IO = 1;`;
        static if(is(typeof({ mixin(enumMixinStr__LFS_ASYNCHRONOUS_IO); }))) {
            mixin(enumMixinStr__LFS_ASYNCHRONOUS_IO);
        }
    }




    static if(!is(typeof(_POSIX_ASYNC_IO))) {
        private enum enumMixinStr__POSIX_ASYNC_IO = `enum _POSIX_ASYNC_IO = 1;`;
        static if(is(typeof({ mixin(enumMixinStr__POSIX_ASYNC_IO); }))) {
            mixin(enumMixinStr__POSIX_ASYNC_IO);
        }
    }




    static if(!is(typeof(_POSIX_ASYNCHRONOUS_IO))) {
        private enum enumMixinStr__POSIX_ASYNCHRONOUS_IO = `enum _POSIX_ASYNCHRONOUS_IO = 200809L;`;
        static if(is(typeof({ mixin(enumMixinStr__POSIX_ASYNCHRONOUS_IO); }))) {
            mixin(enumMixinStr__POSIX_ASYNCHRONOUS_IO);
        }
    }




    static if(!is(typeof(_POSIX_REALTIME_SIGNALS))) {
        private enum enumMixinStr__POSIX_REALTIME_SIGNALS = `enum _POSIX_REALTIME_SIGNALS = 200809L;`;
        static if(is(typeof({ mixin(enumMixinStr__POSIX_REALTIME_SIGNALS); }))) {
            mixin(enumMixinStr__POSIX_REALTIME_SIGNALS);
        }
    }




    static if(!is(typeof(_POSIX_SEMAPHORES))) {
        private enum enumMixinStr__POSIX_SEMAPHORES = `enum _POSIX_SEMAPHORES = 200809L;`;
        static if(is(typeof({ mixin(enumMixinStr__POSIX_SEMAPHORES); }))) {
            mixin(enumMixinStr__POSIX_SEMAPHORES);
        }
    }




    static if(!is(typeof(_POSIX_THREAD_ROBUST_PRIO_PROTECT))) {
        private enum enumMixinStr__POSIX_THREAD_ROBUST_PRIO_PROTECT = `enum _POSIX_THREAD_ROBUST_PRIO_PROTECT = - 1;`;
        static if(is(typeof({ mixin(enumMixinStr__POSIX_THREAD_ROBUST_PRIO_PROTECT); }))) {
            mixin(enumMixinStr__POSIX_THREAD_ROBUST_PRIO_PROTECT);
        }
    }




    static if(!is(typeof(_POSIX_THREAD_ROBUST_PRIO_INHERIT))) {
        private enum enumMixinStr__POSIX_THREAD_ROBUST_PRIO_INHERIT = `enum _POSIX_THREAD_ROBUST_PRIO_INHERIT = 200809L;`;
        static if(is(typeof({ mixin(enumMixinStr__POSIX_THREAD_ROBUST_PRIO_INHERIT); }))) {
            mixin(enumMixinStr__POSIX_THREAD_ROBUST_PRIO_INHERIT);
        }
    }




    static if(!is(typeof(_POSIX_THREAD_PRIO_PROTECT))) {
        private enum enumMixinStr__POSIX_THREAD_PRIO_PROTECT = `enum _POSIX_THREAD_PRIO_PROTECT = 200809L;`;
        static if(is(typeof({ mixin(enumMixinStr__POSIX_THREAD_PRIO_PROTECT); }))) {
            mixin(enumMixinStr__POSIX_THREAD_PRIO_PROTECT);
        }
    }




    static if(!is(typeof(_POSIX_THREAD_PRIO_INHERIT))) {
        private enum enumMixinStr__POSIX_THREAD_PRIO_INHERIT = `enum _POSIX_THREAD_PRIO_INHERIT = 200809L;`;
        static if(is(typeof({ mixin(enumMixinStr__POSIX_THREAD_PRIO_INHERIT); }))) {
            mixin(enumMixinStr__POSIX_THREAD_PRIO_INHERIT);
        }
    }




    static if(!is(typeof(_POSIX_THREAD_ATTR_STACKADDR))) {
        private enum enumMixinStr__POSIX_THREAD_ATTR_STACKADDR = `enum _POSIX_THREAD_ATTR_STACKADDR = 200809L;`;
        static if(is(typeof({ mixin(enumMixinStr__POSIX_THREAD_ATTR_STACKADDR); }))) {
            mixin(enumMixinStr__POSIX_THREAD_ATTR_STACKADDR);
        }
    }




    static if(!is(typeof(_POSIX_THREAD_ATTR_STACKSIZE))) {
        private enum enumMixinStr__POSIX_THREAD_ATTR_STACKSIZE = `enum _POSIX_THREAD_ATTR_STACKSIZE = 200809L;`;
        static if(is(typeof({ mixin(enumMixinStr__POSIX_THREAD_ATTR_STACKSIZE); }))) {
            mixin(enumMixinStr__POSIX_THREAD_ATTR_STACKSIZE);
        }
    }




    static if(!is(typeof(_POSIX_THREAD_PRIORITY_SCHEDULING))) {
        private enum enumMixinStr__POSIX_THREAD_PRIORITY_SCHEDULING = `enum _POSIX_THREAD_PRIORITY_SCHEDULING = 200809L;`;
        static if(is(typeof({ mixin(enumMixinStr__POSIX_THREAD_PRIORITY_SCHEDULING); }))) {
            mixin(enumMixinStr__POSIX_THREAD_PRIORITY_SCHEDULING);
        }
    }




    static if(!is(typeof(_POSIX_THREAD_SAFE_FUNCTIONS))) {
        private enum enumMixinStr__POSIX_THREAD_SAFE_FUNCTIONS = `enum _POSIX_THREAD_SAFE_FUNCTIONS = 200809L;`;
        static if(is(typeof({ mixin(enumMixinStr__POSIX_THREAD_SAFE_FUNCTIONS); }))) {
            mixin(enumMixinStr__POSIX_THREAD_SAFE_FUNCTIONS);
        }
    }




    static if(!is(typeof(_POSIX_REENTRANT_FUNCTIONS))) {
        private enum enumMixinStr__POSIX_REENTRANT_FUNCTIONS = `enum _POSIX_REENTRANT_FUNCTIONS = 1;`;
        static if(is(typeof({ mixin(enumMixinStr__POSIX_REENTRANT_FUNCTIONS); }))) {
            mixin(enumMixinStr__POSIX_REENTRANT_FUNCTIONS);
        }
    }




    static if(!is(typeof(_POSIX_THREADS))) {
        private enum enumMixinStr__POSIX_THREADS = `enum _POSIX_THREADS = 200809L;`;
        static if(is(typeof({ mixin(enumMixinStr__POSIX_THREADS); }))) {
            mixin(enumMixinStr__POSIX_THREADS);
        }
    }




    static if(!is(typeof(_XOPEN_SHM))) {
        private enum enumMixinStr__XOPEN_SHM = `enum _XOPEN_SHM = 1;`;
        static if(is(typeof({ mixin(enumMixinStr__XOPEN_SHM); }))) {
            mixin(enumMixinStr__XOPEN_SHM);
        }
    }




    static if(!is(typeof(_XOPEN_REALTIME_THREADS))) {
        private enum enumMixinStr__XOPEN_REALTIME_THREADS = `enum _XOPEN_REALTIME_THREADS = 1;`;
        static if(is(typeof({ mixin(enumMixinStr__XOPEN_REALTIME_THREADS); }))) {
            mixin(enumMixinStr__XOPEN_REALTIME_THREADS);
        }
    }




    static if(!is(typeof(_XOPEN_REALTIME))) {
        private enum enumMixinStr__XOPEN_REALTIME = `enum _XOPEN_REALTIME = 1;`;
        static if(is(typeof({ mixin(enumMixinStr__XOPEN_REALTIME); }))) {
            mixin(enumMixinStr__XOPEN_REALTIME);
        }
    }




    static if(!is(typeof(_POSIX_NO_TRUNC))) {
        private enum enumMixinStr__POSIX_NO_TRUNC = `enum _POSIX_NO_TRUNC = 1;`;
        static if(is(typeof({ mixin(enumMixinStr__POSIX_NO_TRUNC); }))) {
            mixin(enumMixinStr__POSIX_NO_TRUNC);
        }
    }




    static if(!is(typeof(_POSIX_VDISABLE))) {
        private enum enumMixinStr__POSIX_VDISABLE = `enum _POSIX_VDISABLE = '\0';`;
        static if(is(typeof({ mixin(enumMixinStr__POSIX_VDISABLE); }))) {
            mixin(enumMixinStr__POSIX_VDISABLE);
        }
    }




    static if(!is(typeof(_POSIX_CHOWN_RESTRICTED))) {
        private enum enumMixinStr__POSIX_CHOWN_RESTRICTED = `enum _POSIX_CHOWN_RESTRICTED = 0;`;
        static if(is(typeof({ mixin(enumMixinStr__POSIX_CHOWN_RESTRICTED); }))) {
            mixin(enumMixinStr__POSIX_CHOWN_RESTRICTED);
        }
    }




    static if(!is(typeof(_POSIX_MEMORY_PROTECTION))) {
        private enum enumMixinStr__POSIX_MEMORY_PROTECTION = `enum _POSIX_MEMORY_PROTECTION = 200809L;`;
        static if(is(typeof({ mixin(enumMixinStr__POSIX_MEMORY_PROTECTION); }))) {
            mixin(enumMixinStr__POSIX_MEMORY_PROTECTION);
        }
    }




    static if(!is(typeof(_POSIX_MEMLOCK_RANGE))) {
        private enum enumMixinStr__POSIX_MEMLOCK_RANGE = `enum _POSIX_MEMLOCK_RANGE = 200809L;`;
        static if(is(typeof({ mixin(enumMixinStr__POSIX_MEMLOCK_RANGE); }))) {
            mixin(enumMixinStr__POSIX_MEMLOCK_RANGE);
        }
    }




    static if(!is(typeof(_POSIX_MEMLOCK))) {
        private enum enumMixinStr__POSIX_MEMLOCK = `enum _POSIX_MEMLOCK = 200809L;`;
        static if(is(typeof({ mixin(enumMixinStr__POSIX_MEMLOCK); }))) {
            mixin(enumMixinStr__POSIX_MEMLOCK);
        }
    }




    static if(!is(typeof(_BITS_TYPES___LOCALE_T_H))) {
        private enum enumMixinStr__BITS_TYPES___LOCALE_T_H = `enum _BITS_TYPES___LOCALE_T_H = 1;`;
        static if(is(typeof({ mixin(enumMixinStr__BITS_TYPES___LOCALE_T_H); }))) {
            mixin(enumMixinStr__BITS_TYPES___LOCALE_T_H);
        }
    }




    static if(!is(typeof(_POSIX_MAPPED_FILES))) {
        private enum enumMixinStr__POSIX_MAPPED_FILES = `enum _POSIX_MAPPED_FILES = 200809L;`;
        static if(is(typeof({ mixin(enumMixinStr__POSIX_MAPPED_FILES); }))) {
            mixin(enumMixinStr__POSIX_MAPPED_FILES);
        }
    }




    static if(!is(typeof(_POSIX_FSYNC))) {
        private enum enumMixinStr__POSIX_FSYNC = `enum _POSIX_FSYNC = 200809L;`;
        static if(is(typeof({ mixin(enumMixinStr__POSIX_FSYNC); }))) {
            mixin(enumMixinStr__POSIX_FSYNC);
        }
    }






    static if(!is(typeof(_SIGSET_NWORDS))) {
        private enum enumMixinStr__SIGSET_NWORDS = `enum _SIGSET_NWORDS = ( 1024 / ( 8 * ( unsigned long int ) .sizeof ) );`;
        static if(is(typeof({ mixin(enumMixinStr__SIGSET_NWORDS); }))) {
            mixin(enumMixinStr__SIGSET_NWORDS);
        }
    }




    static if(!is(typeof(_POSIX_SYNCHRONIZED_IO))) {
        private enum enumMixinStr__POSIX_SYNCHRONIZED_IO = `enum _POSIX_SYNCHRONIZED_IO = 200809L;`;
        static if(is(typeof({ mixin(enumMixinStr__POSIX_SYNCHRONIZED_IO); }))) {
            mixin(enumMixinStr__POSIX_SYNCHRONIZED_IO);
        }
    }




    static if(!is(typeof(_POSIX_PRIORITY_SCHEDULING))) {
        private enum enumMixinStr__POSIX_PRIORITY_SCHEDULING = `enum _POSIX_PRIORITY_SCHEDULING = 200809L;`;
        static if(is(typeof({ mixin(enumMixinStr__POSIX_PRIORITY_SCHEDULING); }))) {
            mixin(enumMixinStr__POSIX_PRIORITY_SCHEDULING);
        }
    }




    static if(!is(typeof(_POSIX_SAVED_IDS))) {
        private enum enumMixinStr__POSIX_SAVED_IDS = `enum _POSIX_SAVED_IDS = 1;`;
        static if(is(typeof({ mixin(enumMixinStr__POSIX_SAVED_IDS); }))) {
            mixin(enumMixinStr__POSIX_SAVED_IDS);
        }
    }




    static if(!is(typeof(__clock_t_defined))) {
        private enum enumMixinStr___clock_t_defined = `enum __clock_t_defined = 1;`;
        static if(is(typeof({ mixin(enumMixinStr___clock_t_defined); }))) {
            mixin(enumMixinStr___clock_t_defined);
        }
    }




    static if(!is(typeof(_POSIX_JOB_CONTROL))) {
        private enum enumMixinStr__POSIX_JOB_CONTROL = `enum _POSIX_JOB_CONTROL = 1;`;
        static if(is(typeof({ mixin(enumMixinStr__POSIX_JOB_CONTROL); }))) {
            mixin(enumMixinStr__POSIX_JOB_CONTROL);
        }
    }




    static if(!is(typeof(_BITS_POSIX_OPT_H))) {
        private enum enumMixinStr__BITS_POSIX_OPT_H = `enum _BITS_POSIX_OPT_H = 1;`;
        static if(is(typeof({ mixin(enumMixinStr__BITS_POSIX_OPT_H); }))) {
            mixin(enumMixinStr__BITS_POSIX_OPT_H);
        }
    }




    static if(!is(typeof(__clockid_t_defined))) {
        private enum enumMixinStr___clockid_t_defined = `enum __clockid_t_defined = 1;`;
        static if(is(typeof({ mixin(enumMixinStr___clockid_t_defined); }))) {
            mixin(enumMixinStr___clockid_t_defined);
        }
    }




    static if(!is(typeof(__LDOUBLE_REDIRECTS_TO_FLOAT128_ABI))) {
        private enum enumMixinStr___LDOUBLE_REDIRECTS_TO_FLOAT128_ABI = `enum __LDOUBLE_REDIRECTS_TO_FLOAT128_ABI = 0;`;
        static if(is(typeof({ mixin(enumMixinStr___LDOUBLE_REDIRECTS_TO_FLOAT128_ABI); }))) {
            mixin(enumMixinStr___LDOUBLE_REDIRECTS_TO_FLOAT128_ABI);
        }
    }




    static if(!is(typeof(__GLIBC_USE_IEC_60559_TYPES_EXT))) {
        private enum enumMixinStr___GLIBC_USE_IEC_60559_TYPES_EXT = `enum __GLIBC_USE_IEC_60559_TYPES_EXT = 0;`;
        static if(is(typeof({ mixin(enumMixinStr___GLIBC_USE_IEC_60559_TYPES_EXT); }))) {
            mixin(enumMixinStr___GLIBC_USE_IEC_60559_TYPES_EXT);
        }
    }




    static if(!is(typeof(_BITS_TYPES_LOCALE_T_H))) {
        private enum enumMixinStr__BITS_TYPES_LOCALE_T_H = `enum _BITS_TYPES_LOCALE_T_H = 1;`;
        static if(is(typeof({ mixin(enumMixinStr__BITS_TYPES_LOCALE_T_H); }))) {
            mixin(enumMixinStr__BITS_TYPES_LOCALE_T_H);
        }
    }




    static if(!is(typeof(__GLIBC_USE_IEC_60559_FUNCS_EXT_C2X))) {
        private enum enumMixinStr___GLIBC_USE_IEC_60559_FUNCS_EXT_C2X = `enum __GLIBC_USE_IEC_60559_FUNCS_EXT_C2X = 0;`;
        static if(is(typeof({ mixin(enumMixinStr___GLIBC_USE_IEC_60559_FUNCS_EXT_C2X); }))) {
            mixin(enumMixinStr___GLIBC_USE_IEC_60559_FUNCS_EXT_C2X);
        }
    }




    static if(!is(typeof(__GLIBC_USE_IEC_60559_FUNCS_EXT))) {
        private enum enumMixinStr___GLIBC_USE_IEC_60559_FUNCS_EXT = `enum __GLIBC_USE_IEC_60559_FUNCS_EXT = 0;`;
        static if(is(typeof({ mixin(enumMixinStr___GLIBC_USE_IEC_60559_FUNCS_EXT); }))) {
            mixin(enumMixinStr___GLIBC_USE_IEC_60559_FUNCS_EXT);
        }
    }




    static if(!is(typeof(__sigset_t_defined))) {
        private enum enumMixinStr___sigset_t_defined = `enum __sigset_t_defined = 1;`;
        static if(is(typeof({ mixin(enumMixinStr___sigset_t_defined); }))) {
            mixin(enumMixinStr___sigset_t_defined);
        }
    }




    static if(!is(typeof(__GLIBC_USE_IEC_60559_BFP_EXT_C2X))) {
        private enum enumMixinStr___GLIBC_USE_IEC_60559_BFP_EXT_C2X = `enum __GLIBC_USE_IEC_60559_BFP_EXT_C2X = 0;`;
        static if(is(typeof({ mixin(enumMixinStr___GLIBC_USE_IEC_60559_BFP_EXT_C2X); }))) {
            mixin(enumMixinStr___GLIBC_USE_IEC_60559_BFP_EXT_C2X);
        }
    }




    static if(!is(typeof(__GLIBC_USE_IEC_60559_BFP_EXT))) {
        private enum enumMixinStr___GLIBC_USE_IEC_60559_BFP_EXT = `enum __GLIBC_USE_IEC_60559_BFP_EXT = 0;`;
        static if(is(typeof({ mixin(enumMixinStr___GLIBC_USE_IEC_60559_BFP_EXT); }))) {
            mixin(enumMixinStr___GLIBC_USE_IEC_60559_BFP_EXT);
        }
    }




    static if(!is(typeof(_STRUCT_TIMESPEC))) {
        private enum enumMixinStr__STRUCT_TIMESPEC = `enum _STRUCT_TIMESPEC = 1;`;
        static if(is(typeof({ mixin(enumMixinStr__STRUCT_TIMESPEC); }))) {
            mixin(enumMixinStr__STRUCT_TIMESPEC);
        }
    }




    static if(!is(typeof(__GLIBC_USE_LIB_EXT2))) {
        private enum enumMixinStr___GLIBC_USE_LIB_EXT2 = `enum __GLIBC_USE_LIB_EXT2 = 0;`;
        static if(is(typeof({ mixin(enumMixinStr___GLIBC_USE_LIB_EXT2); }))) {
            mixin(enumMixinStr___GLIBC_USE_LIB_EXT2);
        }
    }




    static if(!is(typeof(SIOCPROTOPRIVATE))) {
        private enum enumMixinStr_SIOCPROTOPRIVATE = `enum SIOCPROTOPRIVATE = 0x89E0;`;
        static if(is(typeof({ mixin(enumMixinStr_SIOCPROTOPRIVATE); }))) {
            mixin(enumMixinStr_SIOCPROTOPRIVATE);
        }
    }




    static if(!is(typeof(SIOCDEVPRIVATE))) {
        private enum enumMixinStr_SIOCDEVPRIVATE = `enum SIOCDEVPRIVATE = 0x89F0;`;
        static if(is(typeof({ mixin(enumMixinStr_SIOCDEVPRIVATE); }))) {
            mixin(enumMixinStr_SIOCDEVPRIVATE);
        }
    }




    static if(!is(typeof(SIOCDELDLCI))) {
        private enum enumMixinStr_SIOCDELDLCI = `enum SIOCDELDLCI = 0x8981;`;
        static if(is(typeof({ mixin(enumMixinStr_SIOCDELDLCI); }))) {
            mixin(enumMixinStr_SIOCDELDLCI);
        }
    }




    static if(!is(typeof(SIOCADDDLCI))) {
        private enum enumMixinStr_SIOCADDDLCI = `enum SIOCADDDLCI = 0x8980;`;
        static if(is(typeof({ mixin(enumMixinStr_SIOCADDDLCI); }))) {
            mixin(enumMixinStr_SIOCADDDLCI);
        }
    }




    static if(!is(typeof(SIOCSIFMAP))) {
        private enum enumMixinStr_SIOCSIFMAP = `enum SIOCSIFMAP = 0x8971;`;
        static if(is(typeof({ mixin(enumMixinStr_SIOCSIFMAP); }))) {
            mixin(enumMixinStr_SIOCSIFMAP);
        }
    }




    static if(!is(typeof(SIOCGIFMAP))) {
        private enum enumMixinStr_SIOCGIFMAP = `enum SIOCGIFMAP = 0x8970;`;
        static if(is(typeof({ mixin(enumMixinStr_SIOCGIFMAP); }))) {
            mixin(enumMixinStr_SIOCGIFMAP);
        }
    }




    static if(!is(typeof(__timeval_defined))) {
        private enum enumMixinStr___timeval_defined = `enum __timeval_defined = 1;`;
        static if(is(typeof({ mixin(enumMixinStr___timeval_defined); }))) {
            mixin(enumMixinStr___timeval_defined);
        }
    }




    static if(!is(typeof(SIOCSRARP))) {
        private enum enumMixinStr_SIOCSRARP = `enum SIOCSRARP = 0x8962;`;
        static if(is(typeof({ mixin(enumMixinStr_SIOCSRARP); }))) {
            mixin(enumMixinStr_SIOCSRARP);
        }
    }




    static if(!is(typeof(SIOCGRARP))) {
        private enum enumMixinStr_SIOCGRARP = `enum SIOCGRARP = 0x8961;`;
        static if(is(typeof({ mixin(enumMixinStr_SIOCGRARP); }))) {
            mixin(enumMixinStr_SIOCGRARP);
        }
    }




    static if(!is(typeof(__time_t_defined))) {
        private enum enumMixinStr___time_t_defined = `enum __time_t_defined = 1;`;
        static if(is(typeof({ mixin(enumMixinStr___time_t_defined); }))) {
            mixin(enumMixinStr___time_t_defined);
        }
    }




    static if(!is(typeof(SIOCDRARP))) {
        private enum enumMixinStr_SIOCDRARP = `enum SIOCDRARP = 0x8960;`;
        static if(is(typeof({ mixin(enumMixinStr_SIOCDRARP); }))) {
            mixin(enumMixinStr_SIOCDRARP);
        }
    }




    static if(!is(typeof(SIOCSARP))) {
        private enum enumMixinStr_SIOCSARP = `enum SIOCSARP = 0x8955;`;
        static if(is(typeof({ mixin(enumMixinStr_SIOCSARP); }))) {
            mixin(enumMixinStr_SIOCSARP);
        }
    }




    static if(!is(typeof(__timer_t_defined))) {
        private enum enumMixinStr___timer_t_defined = `enum __timer_t_defined = 1;`;
        static if(is(typeof({ mixin(enumMixinStr___timer_t_defined); }))) {
            mixin(enumMixinStr___timer_t_defined);
        }
    }




    static if(!is(typeof(SIOCGARP))) {
        private enum enumMixinStr_SIOCGARP = `enum SIOCGARP = 0x8954;`;
        static if(is(typeof({ mixin(enumMixinStr_SIOCGARP); }))) {
            mixin(enumMixinStr_SIOCGARP);
        }
    }




    static if(!is(typeof(SIOCDARP))) {
        private enum enumMixinStr_SIOCDARP = `enum SIOCDARP = 0x8953;`;
        static if(is(typeof({ mixin(enumMixinStr_SIOCDARP); }))) {
            mixin(enumMixinStr_SIOCDARP);
        }
    }




    static if(!is(typeof(SIOCSIFTXQLEN))) {
        private enum enumMixinStr_SIOCSIFTXQLEN = `enum SIOCSIFTXQLEN = 0x8943;`;
        static if(is(typeof({ mixin(enumMixinStr_SIOCSIFTXQLEN); }))) {
            mixin(enumMixinStr_SIOCSIFTXQLEN);
        }
    }




    static if(!is(typeof(_BITS_TYPESIZES_H))) {
        private enum enumMixinStr__BITS_TYPESIZES_H = `enum _BITS_TYPESIZES_H = 1;`;
        static if(is(typeof({ mixin(enumMixinStr__BITS_TYPESIZES_H); }))) {
            mixin(enumMixinStr__BITS_TYPESIZES_H);
        }
    }




    static if(!is(typeof(SIOCGIFTXQLEN))) {
        private enum enumMixinStr_SIOCGIFTXQLEN = `enum SIOCGIFTXQLEN = 0x8942;`;
        static if(is(typeof({ mixin(enumMixinStr_SIOCGIFTXQLEN); }))) {
            mixin(enumMixinStr_SIOCGIFTXQLEN);
        }
    }




    static if(!is(typeof(__SYSCALL_SLONG_TYPE))) {
        private enum enumMixinStr___SYSCALL_SLONG_TYPE = `enum __SYSCALL_SLONG_TYPE = long int;`;
        static if(is(typeof({ mixin(enumMixinStr___SYSCALL_SLONG_TYPE); }))) {
            mixin(enumMixinStr___SYSCALL_SLONG_TYPE);
        }
    }




    static if(!is(typeof(__SYSCALL_ULONG_TYPE))) {
        private enum enumMixinStr___SYSCALL_ULONG_TYPE = `enum __SYSCALL_ULONG_TYPE = unsigned long int;`;
        static if(is(typeof({ mixin(enumMixinStr___SYSCALL_ULONG_TYPE); }))) {
            mixin(enumMixinStr___SYSCALL_ULONG_TYPE);
        }
    }




    static if(!is(typeof(__DEV_T_TYPE))) {
        private enum enumMixinStr___DEV_T_TYPE = `enum __DEV_T_TYPE = unsigned long int;`;
        static if(is(typeof({ mixin(enumMixinStr___DEV_T_TYPE); }))) {
            mixin(enumMixinStr___DEV_T_TYPE);
        }
    }




    static if(!is(typeof(__UID_T_TYPE))) {
        private enum enumMixinStr___UID_T_TYPE = `enum __UID_T_TYPE = unsigned int;`;
        static if(is(typeof({ mixin(enumMixinStr___UID_T_TYPE); }))) {
            mixin(enumMixinStr___UID_T_TYPE);
        }
    }




    static if(!is(typeof(__GID_T_TYPE))) {
        private enum enumMixinStr___GID_T_TYPE = `enum __GID_T_TYPE = unsigned int;`;
        static if(is(typeof({ mixin(enumMixinStr___GID_T_TYPE); }))) {
            mixin(enumMixinStr___GID_T_TYPE);
        }
    }




    static if(!is(typeof(__INO_T_TYPE))) {
        private enum enumMixinStr___INO_T_TYPE = `enum __INO_T_TYPE = unsigned long int;`;
        static if(is(typeof({ mixin(enumMixinStr___INO_T_TYPE); }))) {
            mixin(enumMixinStr___INO_T_TYPE);
        }
    }




    static if(!is(typeof(__INO64_T_TYPE))) {
        private enum enumMixinStr___INO64_T_TYPE = `enum __INO64_T_TYPE = unsigned long int;`;
        static if(is(typeof({ mixin(enumMixinStr___INO64_T_TYPE); }))) {
            mixin(enumMixinStr___INO64_T_TYPE);
        }
    }




    static if(!is(typeof(__MODE_T_TYPE))) {
        private enum enumMixinStr___MODE_T_TYPE = `enum __MODE_T_TYPE = unsigned int;`;
        static if(is(typeof({ mixin(enumMixinStr___MODE_T_TYPE); }))) {
            mixin(enumMixinStr___MODE_T_TYPE);
        }
    }




    static if(!is(typeof(SIOCSIFBR))) {
        private enum enumMixinStr_SIOCSIFBR = `enum SIOCSIFBR = 0x8941;`;
        static if(is(typeof({ mixin(enumMixinStr_SIOCSIFBR); }))) {
            mixin(enumMixinStr_SIOCSIFBR);
        }
    }




    static if(!is(typeof(__NLINK_T_TYPE))) {
        private enum enumMixinStr___NLINK_T_TYPE = `enum __NLINK_T_TYPE = unsigned long int;`;
        static if(is(typeof({ mixin(enumMixinStr___NLINK_T_TYPE); }))) {
            mixin(enumMixinStr___NLINK_T_TYPE);
        }
    }




    static if(!is(typeof(__FSWORD_T_TYPE))) {
        private enum enumMixinStr___FSWORD_T_TYPE = `enum __FSWORD_T_TYPE = long int;`;
        static if(is(typeof({ mixin(enumMixinStr___FSWORD_T_TYPE); }))) {
            mixin(enumMixinStr___FSWORD_T_TYPE);
        }
    }




    static if(!is(typeof(__OFF_T_TYPE))) {
        private enum enumMixinStr___OFF_T_TYPE = `enum __OFF_T_TYPE = long int;`;
        static if(is(typeof({ mixin(enumMixinStr___OFF_T_TYPE); }))) {
            mixin(enumMixinStr___OFF_T_TYPE);
        }
    }




    static if(!is(typeof(__OFF64_T_TYPE))) {
        private enum enumMixinStr___OFF64_T_TYPE = `enum __OFF64_T_TYPE = long int;`;
        static if(is(typeof({ mixin(enumMixinStr___OFF64_T_TYPE); }))) {
            mixin(enumMixinStr___OFF64_T_TYPE);
        }
    }




    static if(!is(typeof(__PID_T_TYPE))) {
        private enum enumMixinStr___PID_T_TYPE = `enum __PID_T_TYPE = int;`;
        static if(is(typeof({ mixin(enumMixinStr___PID_T_TYPE); }))) {
            mixin(enumMixinStr___PID_T_TYPE);
        }
    }




    static if(!is(typeof(__RLIM_T_TYPE))) {
        private enum enumMixinStr___RLIM_T_TYPE = `enum __RLIM_T_TYPE = unsigned long int;`;
        static if(is(typeof({ mixin(enumMixinStr___RLIM_T_TYPE); }))) {
            mixin(enumMixinStr___RLIM_T_TYPE);
        }
    }




    static if(!is(typeof(__RLIM64_T_TYPE))) {
        private enum enumMixinStr___RLIM64_T_TYPE = `enum __RLIM64_T_TYPE = unsigned long int;`;
        static if(is(typeof({ mixin(enumMixinStr___RLIM64_T_TYPE); }))) {
            mixin(enumMixinStr___RLIM64_T_TYPE);
        }
    }




    static if(!is(typeof(__BLKCNT_T_TYPE))) {
        private enum enumMixinStr___BLKCNT_T_TYPE = `enum __BLKCNT_T_TYPE = long int;`;
        static if(is(typeof({ mixin(enumMixinStr___BLKCNT_T_TYPE); }))) {
            mixin(enumMixinStr___BLKCNT_T_TYPE);
        }
    }




    static if(!is(typeof(__BLKCNT64_T_TYPE))) {
        private enum enumMixinStr___BLKCNT64_T_TYPE = `enum __BLKCNT64_T_TYPE = long int;`;
        static if(is(typeof({ mixin(enumMixinStr___BLKCNT64_T_TYPE); }))) {
            mixin(enumMixinStr___BLKCNT64_T_TYPE);
        }
    }




    static if(!is(typeof(__FSBLKCNT_T_TYPE))) {
        private enum enumMixinStr___FSBLKCNT_T_TYPE = `enum __FSBLKCNT_T_TYPE = unsigned long int;`;
        static if(is(typeof({ mixin(enumMixinStr___FSBLKCNT_T_TYPE); }))) {
            mixin(enumMixinStr___FSBLKCNT_T_TYPE);
        }
    }




    static if(!is(typeof(__FSBLKCNT64_T_TYPE))) {
        private enum enumMixinStr___FSBLKCNT64_T_TYPE = `enum __FSBLKCNT64_T_TYPE = unsigned long int;`;
        static if(is(typeof({ mixin(enumMixinStr___FSBLKCNT64_T_TYPE); }))) {
            mixin(enumMixinStr___FSBLKCNT64_T_TYPE);
        }
    }




    static if(!is(typeof(__FSFILCNT_T_TYPE))) {
        private enum enumMixinStr___FSFILCNT_T_TYPE = `enum __FSFILCNT_T_TYPE = unsigned long int;`;
        static if(is(typeof({ mixin(enumMixinStr___FSFILCNT_T_TYPE); }))) {
            mixin(enumMixinStr___FSFILCNT_T_TYPE);
        }
    }




    static if(!is(typeof(__FSFILCNT64_T_TYPE))) {
        private enum enumMixinStr___FSFILCNT64_T_TYPE = `enum __FSFILCNT64_T_TYPE = unsigned long int;`;
        static if(is(typeof({ mixin(enumMixinStr___FSFILCNT64_T_TYPE); }))) {
            mixin(enumMixinStr___FSFILCNT64_T_TYPE);
        }
    }




    static if(!is(typeof(__ID_T_TYPE))) {
        private enum enumMixinStr___ID_T_TYPE = `enum __ID_T_TYPE = unsigned int;`;
        static if(is(typeof({ mixin(enumMixinStr___ID_T_TYPE); }))) {
            mixin(enumMixinStr___ID_T_TYPE);
        }
    }




    static if(!is(typeof(__CLOCK_T_TYPE))) {
        private enum enumMixinStr___CLOCK_T_TYPE = `enum __CLOCK_T_TYPE = long int;`;
        static if(is(typeof({ mixin(enumMixinStr___CLOCK_T_TYPE); }))) {
            mixin(enumMixinStr___CLOCK_T_TYPE);
        }
    }




    static if(!is(typeof(__TIME_T_TYPE))) {
        private enum enumMixinStr___TIME_T_TYPE = `enum __TIME_T_TYPE = long int;`;
        static if(is(typeof({ mixin(enumMixinStr___TIME_T_TYPE); }))) {
            mixin(enumMixinStr___TIME_T_TYPE);
        }
    }




    static if(!is(typeof(__USECONDS_T_TYPE))) {
        private enum enumMixinStr___USECONDS_T_TYPE = `enum __USECONDS_T_TYPE = unsigned int;`;
        static if(is(typeof({ mixin(enumMixinStr___USECONDS_T_TYPE); }))) {
            mixin(enumMixinStr___USECONDS_T_TYPE);
        }
    }




    static if(!is(typeof(__SUSECONDS_T_TYPE))) {
        private enum enumMixinStr___SUSECONDS_T_TYPE = `enum __SUSECONDS_T_TYPE = long int;`;
        static if(is(typeof({ mixin(enumMixinStr___SUSECONDS_T_TYPE); }))) {
            mixin(enumMixinStr___SUSECONDS_T_TYPE);
        }
    }




    static if(!is(typeof(__SUSECONDS64_T_TYPE))) {
        private enum enumMixinStr___SUSECONDS64_T_TYPE = `enum __SUSECONDS64_T_TYPE = long int;`;
        static if(is(typeof({ mixin(enumMixinStr___SUSECONDS64_T_TYPE); }))) {
            mixin(enumMixinStr___SUSECONDS64_T_TYPE);
        }
    }




    static if(!is(typeof(__DADDR_T_TYPE))) {
        private enum enumMixinStr___DADDR_T_TYPE = `enum __DADDR_T_TYPE = int;`;
        static if(is(typeof({ mixin(enumMixinStr___DADDR_T_TYPE); }))) {
            mixin(enumMixinStr___DADDR_T_TYPE);
        }
    }




    static if(!is(typeof(__KEY_T_TYPE))) {
        private enum enumMixinStr___KEY_T_TYPE = `enum __KEY_T_TYPE = int;`;
        static if(is(typeof({ mixin(enumMixinStr___KEY_T_TYPE); }))) {
            mixin(enumMixinStr___KEY_T_TYPE);
        }
    }




    static if(!is(typeof(__CLOCKID_T_TYPE))) {
        private enum enumMixinStr___CLOCKID_T_TYPE = `enum __CLOCKID_T_TYPE = int;`;
        static if(is(typeof({ mixin(enumMixinStr___CLOCKID_T_TYPE); }))) {
            mixin(enumMixinStr___CLOCKID_T_TYPE);
        }
    }




    static if(!is(typeof(__TIMER_T_TYPE))) {
        private enum enumMixinStr___TIMER_T_TYPE = `enum __TIMER_T_TYPE = void *;`;
        static if(is(typeof({ mixin(enumMixinStr___TIMER_T_TYPE); }))) {
            mixin(enumMixinStr___TIMER_T_TYPE);
        }
    }




    static if(!is(typeof(__BLKSIZE_T_TYPE))) {
        private enum enumMixinStr___BLKSIZE_T_TYPE = `enum __BLKSIZE_T_TYPE = long int;`;
        static if(is(typeof({ mixin(enumMixinStr___BLKSIZE_T_TYPE); }))) {
            mixin(enumMixinStr___BLKSIZE_T_TYPE);
        }
    }




    static if(!is(typeof(__FSID_T_TYPE))) {
        private enum enumMixinStr___FSID_T_TYPE = `enum __FSID_T_TYPE = { int __val [ 2 ] ; };`;
        static if(is(typeof({ mixin(enumMixinStr___FSID_T_TYPE); }))) {
            mixin(enumMixinStr___FSID_T_TYPE);
        }
    }




    static if(!is(typeof(__SSIZE_T_TYPE))) {
        private enum enumMixinStr___SSIZE_T_TYPE = `enum __SSIZE_T_TYPE = long int;`;
        static if(is(typeof({ mixin(enumMixinStr___SSIZE_T_TYPE); }))) {
            mixin(enumMixinStr___SSIZE_T_TYPE);
        }
    }




    static if(!is(typeof(__CPU_MASK_TYPE))) {
        private enum enumMixinStr___CPU_MASK_TYPE = `enum __CPU_MASK_TYPE = unsigned long int;`;
        static if(is(typeof({ mixin(enumMixinStr___CPU_MASK_TYPE); }))) {
            mixin(enumMixinStr___CPU_MASK_TYPE);
        }
    }




    static if(!is(typeof(SIOCGIFBR))) {
        private enum enumMixinStr_SIOCGIFBR = `enum SIOCGIFBR = 0x8940;`;
        static if(is(typeof({ mixin(enumMixinStr_SIOCGIFBR); }))) {
            mixin(enumMixinStr_SIOCGIFBR);
        }
    }




    static if(!is(typeof(__OFF_T_MATCHES_OFF64_T))) {
        private enum enumMixinStr___OFF_T_MATCHES_OFF64_T = `enum __OFF_T_MATCHES_OFF64_T = 1;`;
        static if(is(typeof({ mixin(enumMixinStr___OFF_T_MATCHES_OFF64_T); }))) {
            mixin(enumMixinStr___OFF_T_MATCHES_OFF64_T);
        }
    }




    static if(!is(typeof(__INO_T_MATCHES_INO64_T))) {
        private enum enumMixinStr___INO_T_MATCHES_INO64_T = `enum __INO_T_MATCHES_INO64_T = 1;`;
        static if(is(typeof({ mixin(enumMixinStr___INO_T_MATCHES_INO64_T); }))) {
            mixin(enumMixinStr___INO_T_MATCHES_INO64_T);
        }
    }




    static if(!is(typeof(__RLIM_T_MATCHES_RLIM64_T))) {
        private enum enumMixinStr___RLIM_T_MATCHES_RLIM64_T = `enum __RLIM_T_MATCHES_RLIM64_T = 1;`;
        static if(is(typeof({ mixin(enumMixinStr___RLIM_T_MATCHES_RLIM64_T); }))) {
            mixin(enumMixinStr___RLIM_T_MATCHES_RLIM64_T);
        }
    }




    static if(!is(typeof(__STATFS_MATCHES_STATFS64))) {
        private enum enumMixinStr___STATFS_MATCHES_STATFS64 = `enum __STATFS_MATCHES_STATFS64 = 1;`;
        static if(is(typeof({ mixin(enumMixinStr___STATFS_MATCHES_STATFS64); }))) {
            mixin(enumMixinStr___STATFS_MATCHES_STATFS64);
        }
    }




    static if(!is(typeof(__KERNEL_OLD_TIMEVAL_MATCHES_TIMEVAL64))) {
        private enum enumMixinStr___KERNEL_OLD_TIMEVAL_MATCHES_TIMEVAL64 = `enum __KERNEL_OLD_TIMEVAL_MATCHES_TIMEVAL64 = 1;`;
        static if(is(typeof({ mixin(enumMixinStr___KERNEL_OLD_TIMEVAL_MATCHES_TIMEVAL64); }))) {
            mixin(enumMixinStr___KERNEL_OLD_TIMEVAL_MATCHES_TIMEVAL64);
        }
    }




    static if(!is(typeof(__FD_SETSIZE))) {
        private enum enumMixinStr___FD_SETSIZE = `enum __FD_SETSIZE = 1024;`;
        static if(is(typeof({ mixin(enumMixinStr___FD_SETSIZE); }))) {
            mixin(enumMixinStr___FD_SETSIZE);
        }
    }




    static if(!is(typeof(SIOCGIFCOUNT))) {
        private enum enumMixinStr_SIOCGIFCOUNT = `enum SIOCGIFCOUNT = 0x8938;`;
        static if(is(typeof({ mixin(enumMixinStr_SIOCGIFCOUNT); }))) {
            mixin(enumMixinStr_SIOCGIFCOUNT);
        }
    }




    static if(!is(typeof(_BITS_UINTN_IDENTITY_H))) {
        private enum enumMixinStr__BITS_UINTN_IDENTITY_H = `enum _BITS_UINTN_IDENTITY_H = 1;`;
        static if(is(typeof({ mixin(enumMixinStr__BITS_UINTN_IDENTITY_H); }))) {
            mixin(enumMixinStr__BITS_UINTN_IDENTITY_H);
        }
    }




    static if(!is(typeof(SIOCSIFHWBROADCAST))) {
        private enum enumMixinStr_SIOCSIFHWBROADCAST = `enum SIOCSIFHWBROADCAST = 0x8937;`;
        static if(is(typeof({ mixin(enumMixinStr_SIOCSIFHWBROADCAST); }))) {
            mixin(enumMixinStr_SIOCSIFHWBROADCAST);
        }
    }




    static if(!is(typeof(SIOCDIFADDR))) {
        private enum enumMixinStr_SIOCDIFADDR = `enum SIOCDIFADDR = 0x8936;`;
        static if(is(typeof({ mixin(enumMixinStr_SIOCDIFADDR); }))) {
            mixin(enumMixinStr_SIOCDIFADDR);
        }
    }




    static if(!is(typeof(SIOCGIFPFLAGS))) {
        private enum enumMixinStr_SIOCGIFPFLAGS = `enum SIOCGIFPFLAGS = 0x8935;`;
        static if(is(typeof({ mixin(enumMixinStr_SIOCGIFPFLAGS); }))) {
            mixin(enumMixinStr_SIOCGIFPFLAGS);
        }
    }




    static if(!is(typeof(SIOCSIFPFLAGS))) {
        private enum enumMixinStr_SIOCSIFPFLAGS = `enum SIOCSIFPFLAGS = 0x8934;`;
        static if(is(typeof({ mixin(enumMixinStr_SIOCSIFPFLAGS); }))) {
            mixin(enumMixinStr_SIOCSIFPFLAGS);
        }
    }




    static if(!is(typeof(SIOGIFINDEX))) {
        private enum enumMixinStr_SIOGIFINDEX = `enum SIOGIFINDEX = SIOCGIFINDEX;`;
        static if(is(typeof({ mixin(enumMixinStr_SIOGIFINDEX); }))) {
            mixin(enumMixinStr_SIOGIFINDEX);
        }
    }




    static if(!is(typeof(SIOCGIFINDEX))) {
        private enum enumMixinStr_SIOCGIFINDEX = `enum SIOCGIFINDEX = 0x8933;`;
        static if(is(typeof({ mixin(enumMixinStr_SIOCGIFINDEX); }))) {
            mixin(enumMixinStr_SIOCGIFINDEX);
        }
    }




    static if(!is(typeof(SIOCDELMULTI))) {
        private enum enumMixinStr_SIOCDELMULTI = `enum SIOCDELMULTI = 0x8932;`;
        static if(is(typeof({ mixin(enumMixinStr_SIOCDELMULTI); }))) {
            mixin(enumMixinStr_SIOCDELMULTI);
        }
    }




    static if(!is(typeof(SIOCADDMULTI))) {
        private enum enumMixinStr_SIOCADDMULTI = `enum SIOCADDMULTI = 0x8931;`;
        static if(is(typeof({ mixin(enumMixinStr_SIOCADDMULTI); }))) {
            mixin(enumMixinStr_SIOCADDMULTI);
        }
    }




    static if(!is(typeof(SIOCSIFSLAVE))) {
        private enum enumMixinStr_SIOCSIFSLAVE = `enum SIOCSIFSLAVE = 0x8930;`;
        static if(is(typeof({ mixin(enumMixinStr_SIOCSIFSLAVE); }))) {
            mixin(enumMixinStr_SIOCSIFSLAVE);
        }
    }




    static if(!is(typeof(__WORDSIZE))) {
        private enum enumMixinStr___WORDSIZE = `enum __WORDSIZE = 64;`;
        static if(is(typeof({ mixin(enumMixinStr___WORDSIZE); }))) {
            mixin(enumMixinStr___WORDSIZE);
        }
    }




    static if(!is(typeof(SIOCGIFSLAVE))) {
        private enum enumMixinStr_SIOCGIFSLAVE = `enum SIOCGIFSLAVE = 0x8929;`;
        static if(is(typeof({ mixin(enumMixinStr_SIOCGIFSLAVE); }))) {
            mixin(enumMixinStr_SIOCGIFSLAVE);
        }
    }




    static if(!is(typeof(SIOCGIFHWADDR))) {
        private enum enumMixinStr_SIOCGIFHWADDR = `enum SIOCGIFHWADDR = 0x8927;`;
        static if(is(typeof({ mixin(enumMixinStr_SIOCGIFHWADDR); }))) {
            mixin(enumMixinStr_SIOCGIFHWADDR);
        }
    }




    static if(!is(typeof(SIOCSIFENCAP))) {
        private enum enumMixinStr_SIOCSIFENCAP = `enum SIOCSIFENCAP = 0x8926;`;
        static if(is(typeof({ mixin(enumMixinStr_SIOCSIFENCAP); }))) {
            mixin(enumMixinStr_SIOCSIFENCAP);
        }
    }




    static if(!is(typeof(SIOCGIFENCAP))) {
        private enum enumMixinStr_SIOCGIFENCAP = `enum SIOCGIFENCAP = 0x8925;`;
        static if(is(typeof({ mixin(enumMixinStr_SIOCGIFENCAP); }))) {
            mixin(enumMixinStr_SIOCGIFENCAP);
        }
    }




    static if(!is(typeof(__WORDSIZE_TIME64_COMPAT32))) {
        private enum enumMixinStr___WORDSIZE_TIME64_COMPAT32 = `enum __WORDSIZE_TIME64_COMPAT32 = 1;`;
        static if(is(typeof({ mixin(enumMixinStr___WORDSIZE_TIME64_COMPAT32); }))) {
            mixin(enumMixinStr___WORDSIZE_TIME64_COMPAT32);
        }
    }




    static if(!is(typeof(__SYSCALL_WORDSIZE))) {
        private enum enumMixinStr___SYSCALL_WORDSIZE = `enum __SYSCALL_WORDSIZE = 64;`;
        static if(is(typeof({ mixin(enumMixinStr___SYSCALL_WORDSIZE); }))) {
            mixin(enumMixinStr___SYSCALL_WORDSIZE);
        }
    }




    static if(!is(typeof(_ENDIAN_H))) {
        private enum enumMixinStr__ENDIAN_H = `enum _ENDIAN_H = 1;`;
        static if(is(typeof({ mixin(enumMixinStr__ENDIAN_H); }))) {
            mixin(enumMixinStr__ENDIAN_H);
        }
    }




    static if(!is(typeof(SIOCSIFHWADDR))) {
        private enum enumMixinStr_SIOCSIFHWADDR = `enum SIOCSIFHWADDR = 0x8924;`;
        static if(is(typeof({ mixin(enumMixinStr_SIOCSIFHWADDR); }))) {
            mixin(enumMixinStr_SIOCSIFHWADDR);
        }
    }




    static if(!is(typeof(SIOCSIFNAME))) {
        private enum enumMixinStr_SIOCSIFNAME = `enum SIOCSIFNAME = 0x8923;`;
        static if(is(typeof({ mixin(enumMixinStr_SIOCSIFNAME); }))) {
            mixin(enumMixinStr_SIOCSIFNAME);
        }
    }




    static if(!is(typeof(SIOCSIFMTU))) {
        private enum enumMixinStr_SIOCSIFMTU = `enum SIOCSIFMTU = 0x8922;`;
        static if(is(typeof({ mixin(enumMixinStr_SIOCSIFMTU); }))) {
            mixin(enumMixinStr_SIOCSIFMTU);
        }
    }




    static if(!is(typeof(LITTLE_ENDIAN))) {
        private enum enumMixinStr_LITTLE_ENDIAN = `enum LITTLE_ENDIAN = __LITTLE_ENDIAN;`;
        static if(is(typeof({ mixin(enumMixinStr_LITTLE_ENDIAN); }))) {
            mixin(enumMixinStr_LITTLE_ENDIAN);
        }
    }




    static if(!is(typeof(BIG_ENDIAN))) {
        private enum enumMixinStr_BIG_ENDIAN = `enum BIG_ENDIAN = __BIG_ENDIAN;`;
        static if(is(typeof({ mixin(enumMixinStr_BIG_ENDIAN); }))) {
            mixin(enumMixinStr_BIG_ENDIAN);
        }
    }




    static if(!is(typeof(PDP_ENDIAN))) {
        private enum enumMixinStr_PDP_ENDIAN = `enum PDP_ENDIAN = __PDP_ENDIAN;`;
        static if(is(typeof({ mixin(enumMixinStr_PDP_ENDIAN); }))) {
            mixin(enumMixinStr_PDP_ENDIAN);
        }
    }




    static if(!is(typeof(BYTE_ORDER))) {
        private enum enumMixinStr_BYTE_ORDER = `enum BYTE_ORDER = __BYTE_ORDER;`;
        static if(is(typeof({ mixin(enumMixinStr_BYTE_ORDER); }))) {
            mixin(enumMixinStr_BYTE_ORDER);
        }
    }




    static if(!is(typeof(SIOCGIFMTU))) {
        private enum enumMixinStr_SIOCGIFMTU = `enum SIOCGIFMTU = 0x8921;`;
        static if(is(typeof({ mixin(enumMixinStr_SIOCGIFMTU); }))) {
            mixin(enumMixinStr_SIOCGIFMTU);
        }
    }




    static if(!is(typeof(SIOCSIFMEM))) {
        private enum enumMixinStr_SIOCSIFMEM = `enum SIOCSIFMEM = 0x8920;`;
        static if(is(typeof({ mixin(enumMixinStr_SIOCSIFMEM); }))) {
            mixin(enumMixinStr_SIOCSIFMEM);
        }
    }




    static if(!is(typeof(SIOCGIFMEM))) {
        private enum enumMixinStr_SIOCGIFMEM = `enum SIOCGIFMEM = 0x891f;`;
        static if(is(typeof({ mixin(enumMixinStr_SIOCGIFMEM); }))) {
            mixin(enumMixinStr_SIOCGIFMEM);
        }
    }




    static if(!is(typeof(SIOCSIFMETRIC))) {
        private enum enumMixinStr_SIOCSIFMETRIC = `enum SIOCSIFMETRIC = 0x891e;`;
        static if(is(typeof({ mixin(enumMixinStr_SIOCSIFMETRIC); }))) {
            mixin(enumMixinStr_SIOCSIFMETRIC);
        }
    }




    static if(!is(typeof(SIOCGIFMETRIC))) {
        private enum enumMixinStr_SIOCGIFMETRIC = `enum SIOCGIFMETRIC = 0x891d;`;
        static if(is(typeof({ mixin(enumMixinStr_SIOCGIFMETRIC); }))) {
            mixin(enumMixinStr_SIOCGIFMETRIC);
        }
    }
    static if(!is(typeof(_ERRNO_H))) {
        private enum enumMixinStr__ERRNO_H = `enum _ERRNO_H = 1;`;
        static if(is(typeof({ mixin(enumMixinStr__ERRNO_H); }))) {
            mixin(enumMixinStr__ERRNO_H);
        }
    }




    static if(!is(typeof(SIOCSIFNETMASK))) {
        private enum enumMixinStr_SIOCSIFNETMASK = `enum SIOCSIFNETMASK = 0x891c;`;
        static if(is(typeof({ mixin(enumMixinStr_SIOCSIFNETMASK); }))) {
            mixin(enumMixinStr_SIOCSIFNETMASK);
        }
    }




    static if(!is(typeof(SIOCGIFNETMASK))) {
        private enum enumMixinStr_SIOCGIFNETMASK = `enum SIOCGIFNETMASK = 0x891b;`;
        static if(is(typeof({ mixin(enumMixinStr_SIOCGIFNETMASK); }))) {
            mixin(enumMixinStr_SIOCGIFNETMASK);
        }
    }




    static if(!is(typeof(SIOCSIFBRDADDR))) {
        private enum enumMixinStr_SIOCSIFBRDADDR = `enum SIOCSIFBRDADDR = 0x891a;`;
        static if(is(typeof({ mixin(enumMixinStr_SIOCSIFBRDADDR); }))) {
            mixin(enumMixinStr_SIOCSIFBRDADDR);
        }
    }




    static if(!is(typeof(SIOCGIFBRDADDR))) {
        private enum enumMixinStr_SIOCGIFBRDADDR = `enum SIOCGIFBRDADDR = 0x8919;`;
        static if(is(typeof({ mixin(enumMixinStr_SIOCGIFBRDADDR); }))) {
            mixin(enumMixinStr_SIOCGIFBRDADDR);
        }
    }




    static if(!is(typeof(SIOCSIFDSTADDR))) {
        private enum enumMixinStr_SIOCSIFDSTADDR = `enum SIOCSIFDSTADDR = 0x8918;`;
        static if(is(typeof({ mixin(enumMixinStr_SIOCSIFDSTADDR); }))) {
            mixin(enumMixinStr_SIOCSIFDSTADDR);
        }
    }




    static if(!is(typeof(SIOCGIFDSTADDR))) {
        private enum enumMixinStr_SIOCGIFDSTADDR = `enum SIOCGIFDSTADDR = 0x8917;`;
        static if(is(typeof({ mixin(enumMixinStr_SIOCGIFDSTADDR); }))) {
            mixin(enumMixinStr_SIOCGIFDSTADDR);
        }
    }




    static if(!is(typeof(errno))) {
        private enum enumMixinStr_errno = `enum errno = ( * __errno_location ( ) );`;
        static if(is(typeof({ mixin(enumMixinStr_errno); }))) {
            mixin(enumMixinStr_errno);
        }
    }




    static if(!is(typeof(SIOCSIFADDR))) {
        private enum enumMixinStr_SIOCSIFADDR = `enum SIOCSIFADDR = 0x8916;`;
        static if(is(typeof({ mixin(enumMixinStr_SIOCSIFADDR); }))) {
            mixin(enumMixinStr_SIOCSIFADDR);
        }
    }




    static if(!is(typeof(_FCNTL_H))) {
        private enum enumMixinStr__FCNTL_H = `enum _FCNTL_H = 1;`;
        static if(is(typeof({ mixin(enumMixinStr__FCNTL_H); }))) {
            mixin(enumMixinStr__FCNTL_H);
        }
    }




    static if(!is(typeof(SIOCGIFADDR))) {
        private enum enumMixinStr_SIOCGIFADDR = `enum SIOCGIFADDR = 0x8915;`;
        static if(is(typeof({ mixin(enumMixinStr_SIOCGIFADDR); }))) {
            mixin(enumMixinStr_SIOCGIFADDR);
        }
    }




    static if(!is(typeof(SIOCSIFFLAGS))) {
        private enum enumMixinStr_SIOCSIFFLAGS = `enum SIOCSIFFLAGS = 0x8914;`;
        static if(is(typeof({ mixin(enumMixinStr_SIOCSIFFLAGS); }))) {
            mixin(enumMixinStr_SIOCSIFFLAGS);
        }
    }




    static if(!is(typeof(SIOCGIFFLAGS))) {
        private enum enumMixinStr_SIOCGIFFLAGS = `enum SIOCGIFFLAGS = 0x8913;`;
        static if(is(typeof({ mixin(enumMixinStr_SIOCGIFFLAGS); }))) {
            mixin(enumMixinStr_SIOCGIFFLAGS);
        }
    }




    static if(!is(typeof(SIOCGIFCONF))) {
        private enum enumMixinStr_SIOCGIFCONF = `enum SIOCGIFCONF = 0x8912;`;
        static if(is(typeof({ mixin(enumMixinStr_SIOCGIFCONF); }))) {
            mixin(enumMixinStr_SIOCGIFCONF);
        }
    }




    static if(!is(typeof(SIOCSIFLINK))) {
        private enum enumMixinStr_SIOCSIFLINK = `enum SIOCSIFLINK = 0x8911;`;
        static if(is(typeof({ mixin(enumMixinStr_SIOCSIFLINK); }))) {
            mixin(enumMixinStr_SIOCSIFLINK);
        }
    }






    static if(!is(typeof(SIOCGIFNAME))) {
        private enum enumMixinStr_SIOCGIFNAME = `enum SIOCGIFNAME = 0x8910;`;
        static if(is(typeof({ mixin(enumMixinStr_SIOCGIFNAME); }))) {
            mixin(enumMixinStr_SIOCGIFNAME);
        }
    }






    static if(!is(typeof(SIOCRTMSG))) {
        private enum enumMixinStr_SIOCRTMSG = `enum SIOCRTMSG = 0x890D;`;
        static if(is(typeof({ mixin(enumMixinStr_SIOCRTMSG); }))) {
            mixin(enumMixinStr_SIOCRTMSG);
        }
    }




    static if(!is(typeof(SIOCDELRT))) {
        private enum enumMixinStr_SIOCDELRT = `enum SIOCDELRT = 0x890C;`;
        static if(is(typeof({ mixin(enumMixinStr_SIOCDELRT); }))) {
            mixin(enumMixinStr_SIOCDELRT);
        }
    }




    static if(!is(typeof(SIOCADDRT))) {
        private enum enumMixinStr_SIOCADDRT = `enum SIOCADDRT = 0x890B;`;
        static if(is(typeof({ mixin(enumMixinStr_SIOCADDRT); }))) {
            mixin(enumMixinStr_SIOCADDRT);
        }
    }




    static if(!is(typeof(N_HCI))) {
        private enum enumMixinStr_N_HCI = `enum N_HCI = 15;`;
        static if(is(typeof({ mixin(enumMixinStr_N_HCI); }))) {
            mixin(enumMixinStr_N_HCI);
        }
    }




    static if(!is(typeof(N_SYNC_PPP))) {
        private enum enumMixinStr_N_SYNC_PPP = `enum N_SYNC_PPP = 14;`;
        static if(is(typeof({ mixin(enumMixinStr_N_SYNC_PPP); }))) {
            mixin(enumMixinStr_N_SYNC_PPP);
        }
    }




    static if(!is(typeof(N_HDLC))) {
        private enum enumMixinStr_N_HDLC = `enum N_HDLC = 13;`;
        static if(is(typeof({ mixin(enumMixinStr_N_HDLC); }))) {
            mixin(enumMixinStr_N_HDLC);
        }
    }




    static if(!is(typeof(S_IFMT))) {
        private enum enumMixinStr_S_IFMT = `enum S_IFMT = std.conv.octal!170000;`;
        static if(is(typeof({ mixin(enumMixinStr_S_IFMT); }))) {
            mixin(enumMixinStr_S_IFMT);
        }
    }




    static if(!is(typeof(S_IFDIR))) {
        private enum enumMixinStr_S_IFDIR = `enum S_IFDIR = std.conv.octal!40000;`;
        static if(is(typeof({ mixin(enumMixinStr_S_IFDIR); }))) {
            mixin(enumMixinStr_S_IFDIR);
        }
    }




    static if(!is(typeof(S_IFCHR))) {
        private enum enumMixinStr_S_IFCHR = `enum S_IFCHR = std.conv.octal!20000;`;
        static if(is(typeof({ mixin(enumMixinStr_S_IFCHR); }))) {
            mixin(enumMixinStr_S_IFCHR);
        }
    }




    static if(!is(typeof(S_IFBLK))) {
        private enum enumMixinStr_S_IFBLK = `enum S_IFBLK = std.conv.octal!60000;`;
        static if(is(typeof({ mixin(enumMixinStr_S_IFBLK); }))) {
            mixin(enumMixinStr_S_IFBLK);
        }
    }




    static if(!is(typeof(S_IFREG))) {
        private enum enumMixinStr_S_IFREG = `enum S_IFREG = std.conv.octal!100000;`;
        static if(is(typeof({ mixin(enumMixinStr_S_IFREG); }))) {
            mixin(enumMixinStr_S_IFREG);
        }
    }




    static if(!is(typeof(N_SMSBLOCK))) {
        private enum enumMixinStr_N_SMSBLOCK = `enum N_SMSBLOCK = 12;`;
        static if(is(typeof({ mixin(enumMixinStr_N_SMSBLOCK); }))) {
            mixin(enumMixinStr_N_SMSBLOCK);
        }
    }




    static if(!is(typeof(S_IFIFO))) {
        private enum enumMixinStr_S_IFIFO = `enum S_IFIFO = std.conv.octal!10000;`;
        static if(is(typeof({ mixin(enumMixinStr_S_IFIFO); }))) {
            mixin(enumMixinStr_S_IFIFO);
        }
    }




    static if(!is(typeof(N_IRDA))) {
        private enum enumMixinStr_N_IRDA = `enum N_IRDA = 11;`;
        static if(is(typeof({ mixin(enumMixinStr_N_IRDA); }))) {
            mixin(enumMixinStr_N_IRDA);
        }
    }




    static if(!is(typeof(S_IFLNK))) {
        private enum enumMixinStr_S_IFLNK = `enum S_IFLNK = std.conv.octal!120000;`;
        static if(is(typeof({ mixin(enumMixinStr_S_IFLNK); }))) {
            mixin(enumMixinStr_S_IFLNK);
        }
    }




    static if(!is(typeof(N_PROFIBUS_FDL))) {
        private enum enumMixinStr_N_PROFIBUS_FDL = `enum N_PROFIBUS_FDL = 10;`;
        static if(is(typeof({ mixin(enumMixinStr_N_PROFIBUS_FDL); }))) {
            mixin(enumMixinStr_N_PROFIBUS_FDL);
        }
    }




    static if(!is(typeof(N_R3964))) {
        private enum enumMixinStr_N_R3964 = `enum N_R3964 = 9;`;
        static if(is(typeof({ mixin(enumMixinStr_N_R3964); }))) {
            mixin(enumMixinStr_N_R3964);
        }
    }




    static if(!is(typeof(S_IFSOCK))) {
        private enum enumMixinStr_S_IFSOCK = `enum S_IFSOCK = std.conv.octal!140000;`;
        static if(is(typeof({ mixin(enumMixinStr_S_IFSOCK); }))) {
            mixin(enumMixinStr_S_IFSOCK);
        }
    }




    static if(!is(typeof(S_ISUID))) {
        private enum enumMixinStr_S_ISUID = `enum S_ISUID = std.conv.octal!4000;`;
        static if(is(typeof({ mixin(enumMixinStr_S_ISUID); }))) {
            mixin(enumMixinStr_S_ISUID);
        }
    }




    static if(!is(typeof(S_ISGID))) {
        private enum enumMixinStr_S_ISGID = `enum S_ISGID = std.conv.octal!2000;`;
        static if(is(typeof({ mixin(enumMixinStr_S_ISGID); }))) {
            mixin(enumMixinStr_S_ISGID);
        }
    }




    static if(!is(typeof(N_MASC))) {
        private enum enumMixinStr_N_MASC = `enum N_MASC = 8;`;
        static if(is(typeof({ mixin(enumMixinStr_N_MASC); }))) {
            mixin(enumMixinStr_N_MASC);
        }
    }




    static if(!is(typeof(S_ISVTX))) {
        private enum enumMixinStr_S_ISVTX = `enum S_ISVTX = std.conv.octal!1000;`;
        static if(is(typeof({ mixin(enumMixinStr_S_ISVTX); }))) {
            mixin(enumMixinStr_S_ISVTX);
        }
    }




    static if(!is(typeof(S_IRUSR))) {
        private enum enumMixinStr_S_IRUSR = `enum S_IRUSR = std.conv.octal!400;`;
        static if(is(typeof({ mixin(enumMixinStr_S_IRUSR); }))) {
            mixin(enumMixinStr_S_IRUSR);
        }
    }




    static if(!is(typeof(S_IWUSR))) {
        private enum enumMixinStr_S_IWUSR = `enum S_IWUSR = std.conv.octal!200;`;
        static if(is(typeof({ mixin(enumMixinStr_S_IWUSR); }))) {
            mixin(enumMixinStr_S_IWUSR);
        }
    }




    static if(!is(typeof(S_IXUSR))) {
        private enum enumMixinStr_S_IXUSR = `enum S_IXUSR = std.conv.octal!100;`;
        static if(is(typeof({ mixin(enumMixinStr_S_IXUSR); }))) {
            mixin(enumMixinStr_S_IXUSR);
        }
    }




    static if(!is(typeof(S_IRWXU))) {
        private enum enumMixinStr_S_IRWXU = `enum S_IRWXU = ( std.conv.octal!400 | std.conv.octal!200 | std.conv.octal!100 );`;
        static if(is(typeof({ mixin(enumMixinStr_S_IRWXU); }))) {
            mixin(enumMixinStr_S_IRWXU);
        }
    }




    static if(!is(typeof(S_IRGRP))) {
        private enum enumMixinStr_S_IRGRP = `enum S_IRGRP = ( std.conv.octal!400 >> 3 );`;
        static if(is(typeof({ mixin(enumMixinStr_S_IRGRP); }))) {
            mixin(enumMixinStr_S_IRGRP);
        }
    }




    static if(!is(typeof(S_IWGRP))) {
        private enum enumMixinStr_S_IWGRP = `enum S_IWGRP = ( std.conv.octal!200 >> 3 );`;
        static if(is(typeof({ mixin(enumMixinStr_S_IWGRP); }))) {
            mixin(enumMixinStr_S_IWGRP);
        }
    }




    static if(!is(typeof(S_IXGRP))) {
        private enum enumMixinStr_S_IXGRP = `enum S_IXGRP = ( std.conv.octal!100 >> 3 );`;
        static if(is(typeof({ mixin(enumMixinStr_S_IXGRP); }))) {
            mixin(enumMixinStr_S_IXGRP);
        }
    }




    static if(!is(typeof(S_IRWXG))) {
        private enum enumMixinStr_S_IRWXG = `enum S_IRWXG = ( ( std.conv.octal!400 | std.conv.octal!200 | std.conv.octal!100 ) >> 3 );`;
        static if(is(typeof({ mixin(enumMixinStr_S_IRWXG); }))) {
            mixin(enumMixinStr_S_IRWXG);
        }
    }




    static if(!is(typeof(S_IROTH))) {
        private enum enumMixinStr_S_IROTH = `enum S_IROTH = ( ( std.conv.octal!400 >> 3 ) >> 3 );`;
        static if(is(typeof({ mixin(enumMixinStr_S_IROTH); }))) {
            mixin(enumMixinStr_S_IROTH);
        }
    }




    static if(!is(typeof(S_IWOTH))) {
        private enum enumMixinStr_S_IWOTH = `enum S_IWOTH = ( ( std.conv.octal!200 >> 3 ) >> 3 );`;
        static if(is(typeof({ mixin(enumMixinStr_S_IWOTH); }))) {
            mixin(enumMixinStr_S_IWOTH);
        }
    }




    static if(!is(typeof(S_IXOTH))) {
        private enum enumMixinStr_S_IXOTH = `enum S_IXOTH = ( ( std.conv.octal!100 >> 3 ) >> 3 );`;
        static if(is(typeof({ mixin(enumMixinStr_S_IXOTH); }))) {
            mixin(enumMixinStr_S_IXOTH);
        }
    }




    static if(!is(typeof(S_IRWXO))) {
        private enum enumMixinStr_S_IRWXO = `enum S_IRWXO = ( ( ( std.conv.octal!400 | std.conv.octal!200 | std.conv.octal!100 ) >> 3 ) >> 3 );`;
        static if(is(typeof({ mixin(enumMixinStr_S_IRWXO); }))) {
            mixin(enumMixinStr_S_IRWXO);
        }
    }




    static if(!is(typeof(N_6PACK))) {
        private enum enumMixinStr_N_6PACK = `enum N_6PACK = 7;`;
        static if(is(typeof({ mixin(enumMixinStr_N_6PACK); }))) {
            mixin(enumMixinStr_N_6PACK);
        }
    }




    static if(!is(typeof(N_X25))) {
        private enum enumMixinStr_N_X25 = `enum N_X25 = 6;`;
        static if(is(typeof({ mixin(enumMixinStr_N_X25); }))) {
            mixin(enumMixinStr_N_X25);
        }
    }




    static if(!is(typeof(N_AX25))) {
        private enum enumMixinStr_N_AX25 = `enum N_AX25 = 5;`;
        static if(is(typeof({ mixin(enumMixinStr_N_AX25); }))) {
            mixin(enumMixinStr_N_AX25);
        }
    }




    static if(!is(typeof(SEEK_SET))) {
        private enum enumMixinStr_SEEK_SET = `enum SEEK_SET = 0;`;
        static if(is(typeof({ mixin(enumMixinStr_SEEK_SET); }))) {
            mixin(enumMixinStr_SEEK_SET);
        }
    }




    static if(!is(typeof(SEEK_CUR))) {
        private enum enumMixinStr_SEEK_CUR = `enum SEEK_CUR = 1;`;
        static if(is(typeof({ mixin(enumMixinStr_SEEK_CUR); }))) {
            mixin(enumMixinStr_SEEK_CUR);
        }
    }




    static if(!is(typeof(SEEK_END))) {
        private enum enumMixinStr_SEEK_END = `enum SEEK_END = 2;`;
        static if(is(typeof({ mixin(enumMixinStr_SEEK_END); }))) {
            mixin(enumMixinStr_SEEK_END);
        }
    }




    static if(!is(typeof(N_STRIP))) {
        private enum enumMixinStr_N_STRIP = `enum N_STRIP = 4;`;
        static if(is(typeof({ mixin(enumMixinStr_N_STRIP); }))) {
            mixin(enumMixinStr_N_STRIP);
        }
    }




    static if(!is(typeof(AT_FDCWD))) {
        private enum enumMixinStr_AT_FDCWD = `enum AT_FDCWD = - 100;`;
        static if(is(typeof({ mixin(enumMixinStr_AT_FDCWD); }))) {
            mixin(enumMixinStr_AT_FDCWD);
        }
    }




    static if(!is(typeof(AT_SYMLINK_NOFOLLOW))) {
        private enum enumMixinStr_AT_SYMLINK_NOFOLLOW = `enum AT_SYMLINK_NOFOLLOW = 0x100;`;
        static if(is(typeof({ mixin(enumMixinStr_AT_SYMLINK_NOFOLLOW); }))) {
            mixin(enumMixinStr_AT_SYMLINK_NOFOLLOW);
        }
    }




    static if(!is(typeof(AT_REMOVEDIR))) {
        private enum enumMixinStr_AT_REMOVEDIR = `enum AT_REMOVEDIR = 0x200;`;
        static if(is(typeof({ mixin(enumMixinStr_AT_REMOVEDIR); }))) {
            mixin(enumMixinStr_AT_REMOVEDIR);
        }
    }




    static if(!is(typeof(AT_SYMLINK_FOLLOW))) {
        private enum enumMixinStr_AT_SYMLINK_FOLLOW = `enum AT_SYMLINK_FOLLOW = 0x400;`;
        static if(is(typeof({ mixin(enumMixinStr_AT_SYMLINK_FOLLOW); }))) {
            mixin(enumMixinStr_AT_SYMLINK_FOLLOW);
        }
    }




    static if(!is(typeof(AT_EACCESS))) {
        private enum enumMixinStr_AT_EACCESS = `enum AT_EACCESS = 0x200;`;
        static if(is(typeof({ mixin(enumMixinStr_AT_EACCESS); }))) {
            mixin(enumMixinStr_AT_EACCESS);
        }
    }




    static if(!is(typeof(N_PPP))) {
        private enum enumMixinStr_N_PPP = `enum N_PPP = 3;`;
        static if(is(typeof({ mixin(enumMixinStr_N_PPP); }))) {
            mixin(enumMixinStr_N_PPP);
        }
    }




    static if(!is(typeof(N_MOUSE))) {
        private enum enumMixinStr_N_MOUSE = `enum N_MOUSE = 2;`;
        static if(is(typeof({ mixin(enumMixinStr_N_MOUSE); }))) {
            mixin(enumMixinStr_N_MOUSE);
        }
    }




    static if(!is(typeof(N_SLIP))) {
        private enum enumMixinStr_N_SLIP = `enum N_SLIP = 1;`;
        static if(is(typeof({ mixin(enumMixinStr_N_SLIP); }))) {
            mixin(enumMixinStr_N_SLIP);
        }
    }




    static if(!is(typeof(N_TTY))) {
        private enum enumMixinStr_N_TTY = `enum N_TTY = 0;`;
        static if(is(typeof({ mixin(enumMixinStr_N_TTY); }))) {
            mixin(enumMixinStr_N_TTY);
        }
    }




    static if(!is(typeof(TIOCM_RI))) {
        private enum enumMixinStr_TIOCM_RI = `enum TIOCM_RI = TIOCM_RNG;`;
        static if(is(typeof({ mixin(enumMixinStr_TIOCM_RI); }))) {
            mixin(enumMixinStr_TIOCM_RI);
        }
    }




    static if(!is(typeof(TIOCM_CD))) {
        private enum enumMixinStr_TIOCM_CD = `enum TIOCM_CD = TIOCM_CAR;`;
        static if(is(typeof({ mixin(enumMixinStr_TIOCM_CD); }))) {
            mixin(enumMixinStr_TIOCM_CD);
        }
    }




    static if(!is(typeof(TIOCM_DSR))) {
        private enum enumMixinStr_TIOCM_DSR = `enum TIOCM_DSR = 0x100;`;
        static if(is(typeof({ mixin(enumMixinStr_TIOCM_DSR); }))) {
            mixin(enumMixinStr_TIOCM_DSR);
        }
    }




    static if(!is(typeof(TIOCM_RNG))) {
        private enum enumMixinStr_TIOCM_RNG = `enum TIOCM_RNG = 0x080;`;
        static if(is(typeof({ mixin(enumMixinStr_TIOCM_RNG); }))) {
            mixin(enumMixinStr_TIOCM_RNG);
        }
    }




    static if(!is(typeof(TIOCM_CAR))) {
        private enum enumMixinStr_TIOCM_CAR = `enum TIOCM_CAR = 0x040;`;
        static if(is(typeof({ mixin(enumMixinStr_TIOCM_CAR); }))) {
            mixin(enumMixinStr_TIOCM_CAR);
        }
    }




    static if(!is(typeof(TIOCM_CTS))) {
        private enum enumMixinStr_TIOCM_CTS = `enum TIOCM_CTS = 0x020;`;
        static if(is(typeof({ mixin(enumMixinStr_TIOCM_CTS); }))) {
            mixin(enumMixinStr_TIOCM_CTS);
        }
    }




    static if(!is(typeof(TIOCM_SR))) {
        private enum enumMixinStr_TIOCM_SR = `enum TIOCM_SR = 0x010;`;
        static if(is(typeof({ mixin(enumMixinStr_TIOCM_SR); }))) {
            mixin(enumMixinStr_TIOCM_SR);
        }
    }




    static if(!is(typeof(TIOCM_ST))) {
        private enum enumMixinStr_TIOCM_ST = `enum TIOCM_ST = 0x008;`;
        static if(is(typeof({ mixin(enumMixinStr_TIOCM_ST); }))) {
            mixin(enumMixinStr_TIOCM_ST);
        }
    }




    static if(!is(typeof(TIOCM_RTS))) {
        private enum enumMixinStr_TIOCM_RTS = `enum TIOCM_RTS = 0x004;`;
        static if(is(typeof({ mixin(enumMixinStr_TIOCM_RTS); }))) {
            mixin(enumMixinStr_TIOCM_RTS);
        }
    }




    static if(!is(typeof(TIOCM_DTR))) {
        private enum enumMixinStr_TIOCM_DTR = `enum TIOCM_DTR = 0x002;`;
        static if(is(typeof({ mixin(enumMixinStr_TIOCM_DTR); }))) {
            mixin(enumMixinStr_TIOCM_DTR);
        }
    }




    static if(!is(typeof(TIOCM_LE))) {
        private enum enumMixinStr_TIOCM_LE = `enum TIOCM_LE = 0x001;`;
        static if(is(typeof({ mixin(enumMixinStr_TIOCM_LE); }))) {
            mixin(enumMixinStr_TIOCM_LE);
        }
    }




    static if(!is(typeof(NCC))) {
        private enum enumMixinStr_NCC = `enum NCC = 8;`;
        static if(is(typeof({ mixin(enumMixinStr_NCC); }))) {
            mixin(enumMixinStr_NCC);
        }
    }




    static if(!is(typeof(_GETOPT_POSIX_H))) {
        private enum enumMixinStr__GETOPT_POSIX_H = `enum _GETOPT_POSIX_H = 1;`;
        static if(is(typeof({ mixin(enumMixinStr__GETOPT_POSIX_H); }))) {
            mixin(enumMixinStr__GETOPT_POSIX_H);
        }
    }




    static if(!is(typeof(_GETOPT_CORE_H))) {
        private enum enumMixinStr__GETOPT_CORE_H = `enum _GETOPT_CORE_H = 1;`;
        static if(is(typeof({ mixin(enumMixinStr__GETOPT_CORE_H); }))) {
            mixin(enumMixinStr__GETOPT_CORE_H);
        }
    }




    static if(!is(typeof(_FEATURES_H))) {
        private enum enumMixinStr__FEATURES_H = `enum _FEATURES_H = 1;`;
        static if(is(typeof({ mixin(enumMixinStr__FEATURES_H); }))) {
            mixin(enumMixinStr__FEATURES_H);
        }
    }






    static if(!is(typeof(F_SETLKW64))) {
        private enum enumMixinStr_F_SETLKW64 = `enum F_SETLKW64 = 7;`;
        static if(is(typeof({ mixin(enumMixinStr_F_SETLKW64); }))) {
            mixin(enumMixinStr_F_SETLKW64);
        }
    }




    static if(!is(typeof(F_SETLK64))) {
        private enum enumMixinStr_F_SETLK64 = `enum F_SETLK64 = 6;`;
        static if(is(typeof({ mixin(enumMixinStr_F_SETLK64); }))) {
            mixin(enumMixinStr_F_SETLK64);
        }
    }






    static if(!is(typeof(F_GETLK64))) {
        private enum enumMixinStr_F_GETLK64 = `enum F_GETLK64 = 5;`;
        static if(is(typeof({ mixin(enumMixinStr_F_GETLK64); }))) {
            mixin(enumMixinStr_F_GETLK64);
        }
    }




    static if(!is(typeof(__O_LARGEFILE))) {
        private enum enumMixinStr___O_LARGEFILE = `enum __O_LARGEFILE = 0;`;
        static if(is(typeof({ mixin(enumMixinStr___O_LARGEFILE); }))) {
            mixin(enumMixinStr___O_LARGEFILE);
        }
    }
    static if(!is(typeof(_DEFAULT_SOURCE))) {
        private enum enumMixinStr__DEFAULT_SOURCE = `enum _DEFAULT_SOURCE = 1;`;
        static if(is(typeof({ mixin(enumMixinStr__DEFAULT_SOURCE); }))) {
            mixin(enumMixinStr__DEFAULT_SOURCE);
        }
    }




    static if(!is(typeof(POSIX_FADV_NOREUSE))) {
        private enum enumMixinStr_POSIX_FADV_NOREUSE = `enum POSIX_FADV_NOREUSE = __POSIX_FADV_NOREUSE;`;
        static if(is(typeof({ mixin(enumMixinStr_POSIX_FADV_NOREUSE); }))) {
            mixin(enumMixinStr_POSIX_FADV_NOREUSE);
        }
    }




    static if(!is(typeof(POSIX_FADV_DONTNEED))) {
        private enum enumMixinStr_POSIX_FADV_DONTNEED = `enum POSIX_FADV_DONTNEED = __POSIX_FADV_DONTNEED;`;
        static if(is(typeof({ mixin(enumMixinStr_POSIX_FADV_DONTNEED); }))) {
            mixin(enumMixinStr_POSIX_FADV_DONTNEED);
        }
    }




    static if(!is(typeof(__GLIBC_USE_ISOC2X))) {
        private enum enumMixinStr___GLIBC_USE_ISOC2X = `enum __GLIBC_USE_ISOC2X = 0;`;
        static if(is(typeof({ mixin(enumMixinStr___GLIBC_USE_ISOC2X); }))) {
            mixin(enumMixinStr___GLIBC_USE_ISOC2X);
        }
    }




    static if(!is(typeof(POSIX_FADV_WILLNEED))) {
        private enum enumMixinStr_POSIX_FADV_WILLNEED = `enum POSIX_FADV_WILLNEED = 3;`;
        static if(is(typeof({ mixin(enumMixinStr_POSIX_FADV_WILLNEED); }))) {
            mixin(enumMixinStr_POSIX_FADV_WILLNEED);
        }
    }




    static if(!is(typeof(POSIX_FADV_SEQUENTIAL))) {
        private enum enumMixinStr_POSIX_FADV_SEQUENTIAL = `enum POSIX_FADV_SEQUENTIAL = 2;`;
        static if(is(typeof({ mixin(enumMixinStr_POSIX_FADV_SEQUENTIAL); }))) {
            mixin(enumMixinStr_POSIX_FADV_SEQUENTIAL);
        }
    }




    static if(!is(typeof(__USE_ISOC11))) {
        private enum enumMixinStr___USE_ISOC11 = `enum __USE_ISOC11 = 1;`;
        static if(is(typeof({ mixin(enumMixinStr___USE_ISOC11); }))) {
            mixin(enumMixinStr___USE_ISOC11);
        }
    }




    static if(!is(typeof(POSIX_FADV_RANDOM))) {
        private enum enumMixinStr_POSIX_FADV_RANDOM = `enum POSIX_FADV_RANDOM = 1;`;
        static if(is(typeof({ mixin(enumMixinStr_POSIX_FADV_RANDOM); }))) {
            mixin(enumMixinStr_POSIX_FADV_RANDOM);
        }
    }




    static if(!is(typeof(POSIX_FADV_NORMAL))) {
        private enum enumMixinStr_POSIX_FADV_NORMAL = `enum POSIX_FADV_NORMAL = 0;`;
        static if(is(typeof({ mixin(enumMixinStr_POSIX_FADV_NORMAL); }))) {
            mixin(enumMixinStr_POSIX_FADV_NORMAL);
        }
    }




    static if(!is(typeof(__USE_ISOC99))) {
        private enum enumMixinStr___USE_ISOC99 = `enum __USE_ISOC99 = 1;`;
        static if(is(typeof({ mixin(enumMixinStr___USE_ISOC99); }))) {
            mixin(enumMixinStr___USE_ISOC99);
        }
    }




    static if(!is(typeof(__POSIX_FADV_NOREUSE))) {
        private enum enumMixinStr___POSIX_FADV_NOREUSE = `enum __POSIX_FADV_NOREUSE = 5;`;
        static if(is(typeof({ mixin(enumMixinStr___POSIX_FADV_NOREUSE); }))) {
            mixin(enumMixinStr___POSIX_FADV_NOREUSE);
        }
    }




    static if(!is(typeof(__POSIX_FADV_DONTNEED))) {
        private enum enumMixinStr___POSIX_FADV_DONTNEED = `enum __POSIX_FADV_DONTNEED = 4;`;
        static if(is(typeof({ mixin(enumMixinStr___POSIX_FADV_DONTNEED); }))) {
            mixin(enumMixinStr___POSIX_FADV_DONTNEED);
        }
    }




    static if(!is(typeof(__USE_ISOC95))) {
        private enum enumMixinStr___USE_ISOC95 = `enum __USE_ISOC95 = 1;`;
        static if(is(typeof({ mixin(enumMixinStr___USE_ISOC95); }))) {
            mixin(enumMixinStr___USE_ISOC95);
        }
    }




    static if(!is(typeof(FNDELAY))) {
        private enum enumMixinStr_FNDELAY = `enum FNDELAY = O_NDELAY;`;
        static if(is(typeof({ mixin(enumMixinStr_FNDELAY); }))) {
            mixin(enumMixinStr_FNDELAY);
        }
    }




    static if(!is(typeof(__USE_POSIX_IMPLICITLY))) {
        private enum enumMixinStr___USE_POSIX_IMPLICITLY = `enum __USE_POSIX_IMPLICITLY = 1;`;
        static if(is(typeof({ mixin(enumMixinStr___USE_POSIX_IMPLICITLY); }))) {
            mixin(enumMixinStr___USE_POSIX_IMPLICITLY);
        }
    }




    static if(!is(typeof(_POSIX_SOURCE))) {
        private enum enumMixinStr__POSIX_SOURCE = `enum _POSIX_SOURCE = 1;`;
        static if(is(typeof({ mixin(enumMixinStr__POSIX_SOURCE); }))) {
            mixin(enumMixinStr__POSIX_SOURCE);
        }
    }




    static if(!is(typeof(_POSIX_C_SOURCE))) {
        private enum enumMixinStr__POSIX_C_SOURCE = `enum _POSIX_C_SOURCE = 200809L;`;
        static if(is(typeof({ mixin(enumMixinStr__POSIX_C_SOURCE); }))) {
            mixin(enumMixinStr__POSIX_C_SOURCE);
        }
    }




    static if(!is(typeof(FNONBLOCK))) {
        private enum enumMixinStr_FNONBLOCK = `enum FNONBLOCK = O_NONBLOCK;`;
        static if(is(typeof({ mixin(enumMixinStr_FNONBLOCK); }))) {
            mixin(enumMixinStr_FNONBLOCK);
        }
    }




    static if(!is(typeof(FASYNC))) {
        private enum enumMixinStr_FASYNC = `enum FASYNC = O_ASYNC;`;
        static if(is(typeof({ mixin(enumMixinStr_FASYNC); }))) {
            mixin(enumMixinStr_FASYNC);
        }
    }




    static if(!is(typeof(FFSYNC))) {
        private enum enumMixinStr_FFSYNC = `enum FFSYNC = O_FSYNC;`;
        static if(is(typeof({ mixin(enumMixinStr_FFSYNC); }))) {
            mixin(enumMixinStr_FFSYNC);
        }
    }




    static if(!is(typeof(FAPPEND))) {
        private enum enumMixinStr_FAPPEND = `enum FAPPEND = O_APPEND;`;
        static if(is(typeof({ mixin(enumMixinStr_FAPPEND); }))) {
            mixin(enumMixinStr_FAPPEND);
        }
    }




    static if(!is(typeof(LOCK_UN))) {
        private enum enumMixinStr_LOCK_UN = `enum LOCK_UN = 8;`;
        static if(is(typeof({ mixin(enumMixinStr_LOCK_UN); }))) {
            mixin(enumMixinStr_LOCK_UN);
        }
    }




    static if(!is(typeof(LOCK_NB))) {
        private enum enumMixinStr_LOCK_NB = `enum LOCK_NB = 4;`;
        static if(is(typeof({ mixin(enumMixinStr_LOCK_NB); }))) {
            mixin(enumMixinStr_LOCK_NB);
        }
    }




    static if(!is(typeof(LOCK_EX))) {
        private enum enumMixinStr_LOCK_EX = `enum LOCK_EX = 2;`;
        static if(is(typeof({ mixin(enumMixinStr_LOCK_EX); }))) {
            mixin(enumMixinStr_LOCK_EX);
        }
    }




    static if(!is(typeof(__USE_POSIX))) {
        private enum enumMixinStr___USE_POSIX = `enum __USE_POSIX = 1;`;
        static if(is(typeof({ mixin(enumMixinStr___USE_POSIX); }))) {
            mixin(enumMixinStr___USE_POSIX);
        }
    }




    static if(!is(typeof(LOCK_SH))) {
        private enum enumMixinStr_LOCK_SH = `enum LOCK_SH = 1;`;
        static if(is(typeof({ mixin(enumMixinStr_LOCK_SH); }))) {
            mixin(enumMixinStr_LOCK_SH);
        }
    }




    static if(!is(typeof(F_SHLCK))) {
        private enum enumMixinStr_F_SHLCK = `enum F_SHLCK = 8;`;
        static if(is(typeof({ mixin(enumMixinStr_F_SHLCK); }))) {
            mixin(enumMixinStr_F_SHLCK);
        }
    }




    static if(!is(typeof(__USE_POSIX2))) {
        private enum enumMixinStr___USE_POSIX2 = `enum __USE_POSIX2 = 1;`;
        static if(is(typeof({ mixin(enumMixinStr___USE_POSIX2); }))) {
            mixin(enumMixinStr___USE_POSIX2);
        }
    }




    static if(!is(typeof(F_EXLCK))) {
        private enum enumMixinStr_F_EXLCK = `enum F_EXLCK = 4;`;
        static if(is(typeof({ mixin(enumMixinStr_F_EXLCK); }))) {
            mixin(enumMixinStr_F_EXLCK);
        }
    }




    static if(!is(typeof(F_UNLCK))) {
        private enum enumMixinStr_F_UNLCK = `enum F_UNLCK = 2;`;
        static if(is(typeof({ mixin(enumMixinStr_F_UNLCK); }))) {
            mixin(enumMixinStr_F_UNLCK);
        }
    }




    static if(!is(typeof(__USE_POSIX199309))) {
        private enum enumMixinStr___USE_POSIX199309 = `enum __USE_POSIX199309 = 1;`;
        static if(is(typeof({ mixin(enumMixinStr___USE_POSIX199309); }))) {
            mixin(enumMixinStr___USE_POSIX199309);
        }
    }




    static if(!is(typeof(F_WRLCK))) {
        private enum enumMixinStr_F_WRLCK = `enum F_WRLCK = 1;`;
        static if(is(typeof({ mixin(enumMixinStr_F_WRLCK); }))) {
            mixin(enumMixinStr_F_WRLCK);
        }
    }




    static if(!is(typeof(F_RDLCK))) {
        private enum enumMixinStr_F_RDLCK = `enum F_RDLCK = 0;`;
        static if(is(typeof({ mixin(enumMixinStr_F_RDLCK); }))) {
            mixin(enumMixinStr_F_RDLCK);
        }
    }




    static if(!is(typeof(__USE_POSIX199506))) {
        private enum enumMixinStr___USE_POSIX199506 = `enum __USE_POSIX199506 = 1;`;
        static if(is(typeof({ mixin(enumMixinStr___USE_POSIX199506); }))) {
            mixin(enumMixinStr___USE_POSIX199506);
        }
    }




    static if(!is(typeof(FD_CLOEXEC))) {
        private enum enumMixinStr_FD_CLOEXEC = `enum FD_CLOEXEC = 1;`;
        static if(is(typeof({ mixin(enumMixinStr_FD_CLOEXEC); }))) {
            mixin(enumMixinStr_FD_CLOEXEC);
        }
    }




    static if(!is(typeof(F_DUPFD_CLOEXEC))) {
        private enum enumMixinStr_F_DUPFD_CLOEXEC = `enum F_DUPFD_CLOEXEC = 1030;`;
        static if(is(typeof({ mixin(enumMixinStr_F_DUPFD_CLOEXEC); }))) {
            mixin(enumMixinStr_F_DUPFD_CLOEXEC);
        }
    }




    static if(!is(typeof(__USE_XOPEN2K))) {
        private enum enumMixinStr___USE_XOPEN2K = `enum __USE_XOPEN2K = 1;`;
        static if(is(typeof({ mixin(enumMixinStr___USE_XOPEN2K); }))) {
            mixin(enumMixinStr___USE_XOPEN2K);
        }
    }




    static if(!is(typeof(__F_GETOWN_EX))) {
        private enum enumMixinStr___F_GETOWN_EX = `enum __F_GETOWN_EX = 16;`;
        static if(is(typeof({ mixin(enumMixinStr___F_GETOWN_EX); }))) {
            mixin(enumMixinStr___F_GETOWN_EX);
        }
    }




    static if(!is(typeof(__F_SETOWN_EX))) {
        private enum enumMixinStr___F_SETOWN_EX = `enum __F_SETOWN_EX = 15;`;
        static if(is(typeof({ mixin(enumMixinStr___F_SETOWN_EX); }))) {
            mixin(enumMixinStr___F_SETOWN_EX);
        }
    }




    static if(!is(typeof(__USE_XOPEN2K8))) {
        private enum enumMixinStr___USE_XOPEN2K8 = `enum __USE_XOPEN2K8 = 1;`;
        static if(is(typeof({ mixin(enumMixinStr___USE_XOPEN2K8); }))) {
            mixin(enumMixinStr___USE_XOPEN2K8);
        }
    }




    static if(!is(typeof(_ATFILE_SOURCE))) {
        private enum enumMixinStr__ATFILE_SOURCE = `enum _ATFILE_SOURCE = 1;`;
        static if(is(typeof({ mixin(enumMixinStr__ATFILE_SOURCE); }))) {
            mixin(enumMixinStr__ATFILE_SOURCE);
        }
    }




    static if(!is(typeof(__F_GETSIG))) {
        private enum enumMixinStr___F_GETSIG = `enum __F_GETSIG = 11;`;
        static if(is(typeof({ mixin(enumMixinStr___F_GETSIG); }))) {
            mixin(enumMixinStr___F_GETSIG);
        }
    }




    static if(!is(typeof(__USE_MISC))) {
        private enum enumMixinStr___USE_MISC = `enum __USE_MISC = 1;`;
        static if(is(typeof({ mixin(enumMixinStr___USE_MISC); }))) {
            mixin(enumMixinStr___USE_MISC);
        }
    }




    static if(!is(typeof(__F_SETSIG))) {
        private enum enumMixinStr___F_SETSIG = `enum __F_SETSIG = 10;`;
        static if(is(typeof({ mixin(enumMixinStr___F_SETSIG); }))) {
            mixin(enumMixinStr___F_SETSIG);
        }
    }




    static if(!is(typeof(__USE_ATFILE))) {
        private enum enumMixinStr___USE_ATFILE = `enum __USE_ATFILE = 1;`;
        static if(is(typeof({ mixin(enumMixinStr___USE_ATFILE); }))) {
            mixin(enumMixinStr___USE_ATFILE);
        }
    }




    static if(!is(typeof(__USE_FORTIFY_LEVEL))) {
        private enum enumMixinStr___USE_FORTIFY_LEVEL = `enum __USE_FORTIFY_LEVEL = 0;`;
        static if(is(typeof({ mixin(enumMixinStr___USE_FORTIFY_LEVEL); }))) {
            mixin(enumMixinStr___USE_FORTIFY_LEVEL);
        }
    }




    static if(!is(typeof(F_GETOWN))) {
        private enum enumMixinStr_F_GETOWN = `enum F_GETOWN = __F_GETOWN;`;
        static if(is(typeof({ mixin(enumMixinStr_F_GETOWN); }))) {
            mixin(enumMixinStr_F_GETOWN);
        }
    }




    static if(!is(typeof(__GLIBC_USE_DEPRECATED_GETS))) {
        private enum enumMixinStr___GLIBC_USE_DEPRECATED_GETS = `enum __GLIBC_USE_DEPRECATED_GETS = 0;`;
        static if(is(typeof({ mixin(enumMixinStr___GLIBC_USE_DEPRECATED_GETS); }))) {
            mixin(enumMixinStr___GLIBC_USE_DEPRECATED_GETS);
        }
    }




    static if(!is(typeof(F_SETOWN))) {
        private enum enumMixinStr_F_SETOWN = `enum F_SETOWN = __F_SETOWN;`;
        static if(is(typeof({ mixin(enumMixinStr_F_SETOWN); }))) {
            mixin(enumMixinStr_F_SETOWN);
        }
    }




    static if(!is(typeof(__F_GETOWN))) {
        private enum enumMixinStr___F_GETOWN = `enum __F_GETOWN = 9;`;
        static if(is(typeof({ mixin(enumMixinStr___F_GETOWN); }))) {
            mixin(enumMixinStr___F_GETOWN);
        }
    }




    static if(!is(typeof(__GLIBC_USE_DEPRECATED_SCANF))) {
        private enum enumMixinStr___GLIBC_USE_DEPRECATED_SCANF = `enum __GLIBC_USE_DEPRECATED_SCANF = 0;`;
        static if(is(typeof({ mixin(enumMixinStr___GLIBC_USE_DEPRECATED_SCANF); }))) {
            mixin(enumMixinStr___GLIBC_USE_DEPRECATED_SCANF);
        }
    }




    static if(!is(typeof(__F_SETOWN))) {
        private enum enumMixinStr___F_SETOWN = `enum __F_SETOWN = 8;`;
        static if(is(typeof({ mixin(enumMixinStr___F_SETOWN); }))) {
            mixin(enumMixinStr___F_SETOWN);
        }
    }




    static if(!is(typeof(__GNU_LIBRARY__))) {
        private enum enumMixinStr___GNU_LIBRARY__ = `enum __GNU_LIBRARY__ = 6;`;
        static if(is(typeof({ mixin(enumMixinStr___GNU_LIBRARY__); }))) {
            mixin(enumMixinStr___GNU_LIBRARY__);
        }
    }




    static if(!is(typeof(__GLIBC__))) {
        private enum enumMixinStr___GLIBC__ = `enum __GLIBC__ = 2;`;
        static if(is(typeof({ mixin(enumMixinStr___GLIBC__); }))) {
            mixin(enumMixinStr___GLIBC__);
        }
    }




    static if(!is(typeof(__GLIBC_MINOR__))) {
        private enum enumMixinStr___GLIBC_MINOR__ = `enum __GLIBC_MINOR__ = 32;`;
        static if(is(typeof({ mixin(enumMixinStr___GLIBC_MINOR__); }))) {
            mixin(enumMixinStr___GLIBC_MINOR__);
        }
    }






    static if(!is(typeof(F_SETFL))) {
        private enum enumMixinStr_F_SETFL = `enum F_SETFL = 4;`;
        static if(is(typeof({ mixin(enumMixinStr_F_SETFL); }))) {
            mixin(enumMixinStr_F_SETFL);
        }
    }




    static if(!is(typeof(F_GETFL))) {
        private enum enumMixinStr_F_GETFL = `enum F_GETFL = 3;`;
        static if(is(typeof({ mixin(enumMixinStr_F_GETFL); }))) {
            mixin(enumMixinStr_F_GETFL);
        }
    }




    static if(!is(typeof(F_SETFD))) {
        private enum enumMixinStr_F_SETFD = `enum F_SETFD = 2;`;
        static if(is(typeof({ mixin(enumMixinStr_F_SETFD); }))) {
            mixin(enumMixinStr_F_SETFD);
        }
    }




    static if(!is(typeof(F_GETFD))) {
        private enum enumMixinStr_F_GETFD = `enum F_GETFD = 1;`;
        static if(is(typeof({ mixin(enumMixinStr_F_GETFD); }))) {
            mixin(enumMixinStr_F_GETFD);
        }
    }




    static if(!is(typeof(F_DUPFD))) {
        private enum enumMixinStr_F_DUPFD = `enum F_DUPFD = 0;`;
        static if(is(typeof({ mixin(enumMixinStr_F_DUPFD); }))) {
            mixin(enumMixinStr_F_DUPFD);
        }
    }




    static if(!is(typeof(O_RSYNC))) {
        private enum enumMixinStr_O_RSYNC = `enum O_RSYNC = O_SYNC;`;
        static if(is(typeof({ mixin(enumMixinStr_O_RSYNC); }))) {
            mixin(enumMixinStr_O_RSYNC);
        }
    }
    static if(!is(typeof(O_DSYNC))) {
        private enum enumMixinStr_O_DSYNC = `enum O_DSYNC = __O_DSYNC;`;
        static if(is(typeof({ mixin(enumMixinStr_O_DSYNC); }))) {
            mixin(enumMixinStr_O_DSYNC);
        }
    }




    static if(!is(typeof(O_CLOEXEC))) {
        private enum enumMixinStr_O_CLOEXEC = `enum O_CLOEXEC = __O_CLOEXEC;`;
        static if(is(typeof({ mixin(enumMixinStr_O_CLOEXEC); }))) {
            mixin(enumMixinStr_O_CLOEXEC);
        }
    }




    static if(!is(typeof(O_NOFOLLOW))) {
        private enum enumMixinStr_O_NOFOLLOW = `enum O_NOFOLLOW = __O_NOFOLLOW;`;
        static if(is(typeof({ mixin(enumMixinStr_O_NOFOLLOW); }))) {
            mixin(enumMixinStr_O_NOFOLLOW);
        }
    }




    static if(!is(typeof(O_DIRECTORY))) {
        private enum enumMixinStr_O_DIRECTORY = `enum O_DIRECTORY = __O_DIRECTORY;`;
        static if(is(typeof({ mixin(enumMixinStr_O_DIRECTORY); }))) {
            mixin(enumMixinStr_O_DIRECTORY);
        }
    }




    static if(!is(typeof(F_SETLKW))) {
        private enum enumMixinStr_F_SETLKW = `enum F_SETLKW = 7;`;
        static if(is(typeof({ mixin(enumMixinStr_F_SETLKW); }))) {
            mixin(enumMixinStr_F_SETLKW);
        }
    }






    static if(!is(typeof(F_SETLK))) {
        private enum enumMixinStr_F_SETLK = `enum F_SETLK = 6;`;
        static if(is(typeof({ mixin(enumMixinStr_F_SETLK); }))) {
            mixin(enumMixinStr_F_SETLK);
        }
    }




    static if(!is(typeof(F_GETLK))) {
        private enum enumMixinStr_F_GETLK = `enum F_GETLK = 5;`;
        static if(is(typeof({ mixin(enumMixinStr_F_GETLK); }))) {
            mixin(enumMixinStr_F_GETLK);
        }
    }




    static if(!is(typeof(__O_TMPFILE))) {
        private enum enumMixinStr___O_TMPFILE = `enum __O_TMPFILE = ( 020000000 | __O_DIRECTORY );`;
        static if(is(typeof({ mixin(enumMixinStr___O_TMPFILE); }))) {
            mixin(enumMixinStr___O_TMPFILE);
        }
    }




    static if(!is(typeof(__O_DSYNC))) {
        private enum enumMixinStr___O_DSYNC = `enum __O_DSYNC = std.conv.octal!10000;`;
        static if(is(typeof({ mixin(enumMixinStr___O_DSYNC); }))) {
            mixin(enumMixinStr___O_DSYNC);
        }
    }




    static if(!is(typeof(__O_PATH))) {
        private enum enumMixinStr___O_PATH = `enum __O_PATH = std.conv.octal!10000000;`;
        static if(is(typeof({ mixin(enumMixinStr___O_PATH); }))) {
            mixin(enumMixinStr___O_PATH);
        }
    }




    static if(!is(typeof(__O_NOATIME))) {
        private enum enumMixinStr___O_NOATIME = `enum __O_NOATIME = std.conv.octal!1000000;`;
        static if(is(typeof({ mixin(enumMixinStr___O_NOATIME); }))) {
            mixin(enumMixinStr___O_NOATIME);
        }
    }




    static if(!is(typeof(__O_DIRECT))) {
        private enum enumMixinStr___O_DIRECT = `enum __O_DIRECT = std.conv.octal!40000;`;
        static if(is(typeof({ mixin(enumMixinStr___O_DIRECT); }))) {
            mixin(enumMixinStr___O_DIRECT);
        }
    }




    static if(!is(typeof(__O_CLOEXEC))) {
        private enum enumMixinStr___O_CLOEXEC = `enum __O_CLOEXEC = std.conv.octal!2000000;`;
        static if(is(typeof({ mixin(enumMixinStr___O_CLOEXEC); }))) {
            mixin(enumMixinStr___O_CLOEXEC);
        }
    }




    static if(!is(typeof(__O_NOFOLLOW))) {
        private enum enumMixinStr___O_NOFOLLOW = `enum __O_NOFOLLOW = std.conv.octal!400000;`;
        static if(is(typeof({ mixin(enumMixinStr___O_NOFOLLOW); }))) {
            mixin(enumMixinStr___O_NOFOLLOW);
        }
    }






    static if(!is(typeof(__O_DIRECTORY))) {
        private enum enumMixinStr___O_DIRECTORY = `enum __O_DIRECTORY = std.conv.octal!200000;`;
        static if(is(typeof({ mixin(enumMixinStr___O_DIRECTORY); }))) {
            mixin(enumMixinStr___O_DIRECTORY);
        }
    }




    static if(!is(typeof(O_ASYNC))) {
        private enum enumMixinStr_O_ASYNC = `enum O_ASYNC = std.conv.octal!20000;`;
        static if(is(typeof({ mixin(enumMixinStr_O_ASYNC); }))) {
            mixin(enumMixinStr_O_ASYNC);
        }
    }






    static if(!is(typeof(O_FSYNC))) {
        private enum enumMixinStr_O_FSYNC = `enum O_FSYNC = O_SYNC;`;
        static if(is(typeof({ mixin(enumMixinStr_O_FSYNC); }))) {
            mixin(enumMixinStr_O_FSYNC);
        }
    }




    static if(!is(typeof(O_SYNC))) {
        private enum enumMixinStr_O_SYNC = `enum O_SYNC = std.conv.octal!4010000;`;
        static if(is(typeof({ mixin(enumMixinStr_O_SYNC); }))) {
            mixin(enumMixinStr_O_SYNC);
        }
    }




    static if(!is(typeof(O_NDELAY))) {
        private enum enumMixinStr_O_NDELAY = `enum O_NDELAY = O_NONBLOCK;`;
        static if(is(typeof({ mixin(enumMixinStr_O_NDELAY); }))) {
            mixin(enumMixinStr_O_NDELAY);
        }
    }




    static if(!is(typeof(O_NONBLOCK))) {
        private enum enumMixinStr_O_NONBLOCK = `enum O_NONBLOCK = std.conv.octal!4000;`;
        static if(is(typeof({ mixin(enumMixinStr_O_NONBLOCK); }))) {
            mixin(enumMixinStr_O_NONBLOCK);
        }
    }




    static if(!is(typeof(O_APPEND))) {
        private enum enumMixinStr_O_APPEND = `enum O_APPEND = std.conv.octal!2000;`;
        static if(is(typeof({ mixin(enumMixinStr_O_APPEND); }))) {
            mixin(enumMixinStr_O_APPEND);
        }
    }




    static if(!is(typeof(O_TRUNC))) {
        private enum enumMixinStr_O_TRUNC = `enum O_TRUNC = std.conv.octal!1000;`;
        static if(is(typeof({ mixin(enumMixinStr_O_TRUNC); }))) {
            mixin(enumMixinStr_O_TRUNC);
        }
    }




    static if(!is(typeof(O_NOCTTY))) {
        private enum enumMixinStr_O_NOCTTY = `enum O_NOCTTY = std.conv.octal!400;`;
        static if(is(typeof({ mixin(enumMixinStr_O_NOCTTY); }))) {
            mixin(enumMixinStr_O_NOCTTY);
        }
    }




    static if(!is(typeof(O_EXCL))) {
        private enum enumMixinStr_O_EXCL = `enum O_EXCL = std.conv.octal!200;`;
        static if(is(typeof({ mixin(enumMixinStr_O_EXCL); }))) {
            mixin(enumMixinStr_O_EXCL);
        }
    }




    static if(!is(typeof(O_CREAT))) {
        private enum enumMixinStr_O_CREAT = `enum O_CREAT = std.conv.octal!100;`;
        static if(is(typeof({ mixin(enumMixinStr_O_CREAT); }))) {
            mixin(enumMixinStr_O_CREAT);
        }
    }




    static if(!is(typeof(O_RDWR))) {
        private enum enumMixinStr_O_RDWR = `enum O_RDWR = std.conv.octal!2;`;
        static if(is(typeof({ mixin(enumMixinStr_O_RDWR); }))) {
            mixin(enumMixinStr_O_RDWR);
        }
    }




    static if(!is(typeof(O_WRONLY))) {
        private enum enumMixinStr_O_WRONLY = `enum O_WRONLY = std.conv.octal!1;`;
        static if(is(typeof({ mixin(enumMixinStr_O_WRONLY); }))) {
            mixin(enumMixinStr_O_WRONLY);
        }
    }




    static if(!is(typeof(O_RDONLY))) {
        private enum enumMixinStr_O_RDONLY = `enum O_RDONLY = 0;`;
        static if(is(typeof({ mixin(enumMixinStr_O_RDONLY); }))) {
            mixin(enumMixinStr_O_RDONLY);
        }
    }




    static if(!is(typeof(O_ACCMODE))) {
        private enum enumMixinStr_O_ACCMODE = `enum O_ACCMODE = std.conv.octal!3;`;
        static if(is(typeof({ mixin(enumMixinStr_O_ACCMODE); }))) {
            mixin(enumMixinStr_O_ACCMODE);
        }
    }




    static if(!is(typeof(ENOTSUP))) {
        private enum enumMixinStr_ENOTSUP = `enum ENOTSUP = EOPNOTSUPP;`;
        static if(is(typeof({ mixin(enumMixinStr_ENOTSUP); }))) {
            mixin(enumMixinStr_ENOTSUP);
        }
    }




    static if(!is(typeof(_BITS_ERRNO_H))) {
        private enum enumMixinStr__BITS_ERRNO_H = `enum _BITS_ERRNO_H = 1;`;
        static if(is(typeof({ mixin(enumMixinStr__BITS_ERRNO_H); }))) {
            mixin(enumMixinStr__BITS_ERRNO_H);
        }
    }




    static if(!is(typeof(__LP64_OFF64_LDFLAGS))) {
        private enum enumMixinStr___LP64_OFF64_LDFLAGS = `enum __LP64_OFF64_LDFLAGS = "-m64";`;
        static if(is(typeof({ mixin(enumMixinStr___LP64_OFF64_LDFLAGS); }))) {
            mixin(enumMixinStr___LP64_OFF64_LDFLAGS);
        }
    }




    static if(!is(typeof(__LP64_OFF64_CFLAGS))) {
        private enum enumMixinStr___LP64_OFF64_CFLAGS = `enum __LP64_OFF64_CFLAGS = "-m64";`;
        static if(is(typeof({ mixin(enumMixinStr___LP64_OFF64_CFLAGS); }))) {
            mixin(enumMixinStr___LP64_OFF64_CFLAGS);
        }
    }




    static if(!is(typeof(__ILP32_OFFBIG_LDFLAGS))) {
        private enum enumMixinStr___ILP32_OFFBIG_LDFLAGS = `enum __ILP32_OFFBIG_LDFLAGS = "-m32";`;
        static if(is(typeof({ mixin(enumMixinStr___ILP32_OFFBIG_LDFLAGS); }))) {
            mixin(enumMixinStr___ILP32_OFFBIG_LDFLAGS);
        }
    }




    static if(!is(typeof(__ILP32_OFFBIG_CFLAGS))) {
        private enum enumMixinStr___ILP32_OFFBIG_CFLAGS = `enum __ILP32_OFFBIG_CFLAGS = "-m32 -D_LARGEFILE_SOURCE -D_FILE_OFFSET_BITS=64";`;
        static if(is(typeof({ mixin(enumMixinStr___ILP32_OFFBIG_CFLAGS); }))) {
            mixin(enumMixinStr___ILP32_OFFBIG_CFLAGS);
        }
    }




    static if(!is(typeof(__ILP32_OFF32_LDFLAGS))) {
        private enum enumMixinStr___ILP32_OFF32_LDFLAGS = `enum __ILP32_OFF32_LDFLAGS = "-m32";`;
        static if(is(typeof({ mixin(enumMixinStr___ILP32_OFF32_LDFLAGS); }))) {
            mixin(enumMixinStr___ILP32_OFF32_LDFLAGS);
        }
    }




    static if(!is(typeof(__ILP32_OFF32_CFLAGS))) {
        private enum enumMixinStr___ILP32_OFF32_CFLAGS = `enum __ILP32_OFF32_CFLAGS = "-m32";`;
        static if(is(typeof({ mixin(enumMixinStr___ILP32_OFF32_CFLAGS); }))) {
            mixin(enumMixinStr___ILP32_OFF32_CFLAGS);
        }
    }




    static if(!is(typeof(_XBS5_LP64_OFF64))) {
        private enum enumMixinStr__XBS5_LP64_OFF64 = `enum _XBS5_LP64_OFF64 = 1;`;
        static if(is(typeof({ mixin(enumMixinStr__XBS5_LP64_OFF64); }))) {
            mixin(enumMixinStr__XBS5_LP64_OFF64);
        }
    }




    static if(!is(typeof(_POSIX_V6_LP64_OFF64))) {
        private enum enumMixinStr__POSIX_V6_LP64_OFF64 = `enum _POSIX_V6_LP64_OFF64 = 1;`;
        static if(is(typeof({ mixin(enumMixinStr__POSIX_V6_LP64_OFF64); }))) {
            mixin(enumMixinStr__POSIX_V6_LP64_OFF64);
        }
    }




    static if(!is(typeof(_POSIX_V7_LP64_OFF64))) {
        private enum enumMixinStr__POSIX_V7_LP64_OFF64 = `enum _POSIX_V7_LP64_OFF64 = 1;`;
        static if(is(typeof({ mixin(enumMixinStr__POSIX_V7_LP64_OFF64); }))) {
            mixin(enumMixinStr__POSIX_V7_LP64_OFF64);
        }
    }




    static if(!is(typeof(_XBS5_LPBIG_OFFBIG))) {
        private enum enumMixinStr__XBS5_LPBIG_OFFBIG = `enum _XBS5_LPBIG_OFFBIG = - 1;`;
        static if(is(typeof({ mixin(enumMixinStr__XBS5_LPBIG_OFFBIG); }))) {
            mixin(enumMixinStr__XBS5_LPBIG_OFFBIG);
        }
    }




    static if(!is(typeof(_POSIX_V6_LPBIG_OFFBIG))) {
        private enum enumMixinStr__POSIX_V6_LPBIG_OFFBIG = `enum _POSIX_V6_LPBIG_OFFBIG = - 1;`;
        static if(is(typeof({ mixin(enumMixinStr__POSIX_V6_LPBIG_OFFBIG); }))) {
            mixin(enumMixinStr__POSIX_V6_LPBIG_OFFBIG);
        }
    }




    static if(!is(typeof(_POSIX_V7_LPBIG_OFFBIG))) {
        private enum enumMixinStr__POSIX_V7_LPBIG_OFFBIG = `enum _POSIX_V7_LPBIG_OFFBIG = - 1;`;
        static if(is(typeof({ mixin(enumMixinStr__POSIX_V7_LPBIG_OFFBIG); }))) {
            mixin(enumMixinStr__POSIX_V7_LPBIG_OFFBIG);
        }
    }




    static if(!is(typeof(__BYTE_ORDER))) {
        private enum enumMixinStr___BYTE_ORDER = `enum __BYTE_ORDER = __LITTLE_ENDIAN;`;
        static if(is(typeof({ mixin(enumMixinStr___BYTE_ORDER); }))) {
            mixin(enumMixinStr___BYTE_ORDER);
        }
    }




    static if(!is(typeof(_BITS_ENDIANNESS_H))) {
        private enum enumMixinStr__BITS_ENDIANNESS_H = `enum _BITS_ENDIANNESS_H = 1;`;
        static if(is(typeof({ mixin(enumMixinStr__BITS_ENDIANNESS_H); }))) {
            mixin(enumMixinStr__BITS_ENDIANNESS_H);
        }
    }






    static if(!is(typeof(__FLOAT_WORD_ORDER))) {
        private enum enumMixinStr___FLOAT_WORD_ORDER = `enum __FLOAT_WORD_ORDER = __LITTLE_ENDIAN;`;
        static if(is(typeof({ mixin(enumMixinStr___FLOAT_WORD_ORDER); }))) {
            mixin(enumMixinStr___FLOAT_WORD_ORDER);
        }
    }




    static if(!is(typeof(__PDP_ENDIAN))) {
        private enum enumMixinStr___PDP_ENDIAN = `enum __PDP_ENDIAN = 3412;`;
        static if(is(typeof({ mixin(enumMixinStr___PDP_ENDIAN); }))) {
            mixin(enumMixinStr___PDP_ENDIAN);
        }
    }




    static if(!is(typeof(__BIG_ENDIAN))) {
        private enum enumMixinStr___BIG_ENDIAN = `enum __BIG_ENDIAN = 4321;`;
        static if(is(typeof({ mixin(enumMixinStr___BIG_ENDIAN); }))) {
            mixin(enumMixinStr___BIG_ENDIAN);
        }
    }




    static if(!is(typeof(__LITTLE_ENDIAN))) {
        private enum enumMixinStr___LITTLE_ENDIAN = `enum __LITTLE_ENDIAN = 1234;`;
        static if(is(typeof({ mixin(enumMixinStr___LITTLE_ENDIAN); }))) {
            mixin(enumMixinStr___LITTLE_ENDIAN);
        }
    }




    static if(!is(typeof(_BITS_ENDIAN_H))) {
        private enum enumMixinStr__BITS_ENDIAN_H = `enum _BITS_ENDIAN_H = 1;`;
        static if(is(typeof({ mixin(enumMixinStr__BITS_ENDIAN_H); }))) {
            mixin(enumMixinStr__BITS_ENDIAN_H);
        }
    }




    static if(!is(typeof(_CS_V7_ENV))) {
        private enum enumMixinStr__CS_V7_ENV = `enum _CS_V7_ENV = _CS_V7_ENV;`;
        static if(is(typeof({ mixin(enumMixinStr__CS_V7_ENV); }))) {
            mixin(enumMixinStr__CS_V7_ENV);
        }
    }




    static if(!is(typeof(_CS_V6_ENV))) {
        private enum enumMixinStr__CS_V6_ENV = `enum _CS_V6_ENV = _CS_V6_ENV;`;
        static if(is(typeof({ mixin(enumMixinStr__CS_V6_ENV); }))) {
            mixin(enumMixinStr__CS_V6_ENV);
        }
    }




    static if(!is(typeof(_CS_POSIX_V7_LPBIG_OFFBIG_LINTFLAGS))) {
        private enum enumMixinStr__CS_POSIX_V7_LPBIG_OFFBIG_LINTFLAGS = `enum _CS_POSIX_V7_LPBIG_OFFBIG_LINTFLAGS = _CS_POSIX_V7_LPBIG_OFFBIG_LINTFLAGS;`;
        static if(is(typeof({ mixin(enumMixinStr__CS_POSIX_V7_LPBIG_OFFBIG_LINTFLAGS); }))) {
            mixin(enumMixinStr__CS_POSIX_V7_LPBIG_OFFBIG_LINTFLAGS);
        }
    }




    static if(!is(typeof(_CS_POSIX_V7_LPBIG_OFFBIG_LIBS))) {
        private enum enumMixinStr__CS_POSIX_V7_LPBIG_OFFBIG_LIBS = `enum _CS_POSIX_V7_LPBIG_OFFBIG_LIBS = _CS_POSIX_V7_LPBIG_OFFBIG_LIBS;`;
        static if(is(typeof({ mixin(enumMixinStr__CS_POSIX_V7_LPBIG_OFFBIG_LIBS); }))) {
            mixin(enumMixinStr__CS_POSIX_V7_LPBIG_OFFBIG_LIBS);
        }
    }




    static if(!is(typeof(_CS_POSIX_V7_LPBIG_OFFBIG_LDFLAGS))) {
        private enum enumMixinStr__CS_POSIX_V7_LPBIG_OFFBIG_LDFLAGS = `enum _CS_POSIX_V7_LPBIG_OFFBIG_LDFLAGS = _CS_POSIX_V7_LPBIG_OFFBIG_LDFLAGS;`;
        static if(is(typeof({ mixin(enumMixinStr__CS_POSIX_V7_LPBIG_OFFBIG_LDFLAGS); }))) {
            mixin(enumMixinStr__CS_POSIX_V7_LPBIG_OFFBIG_LDFLAGS);
        }
    }




    static if(!is(typeof(_CS_POSIX_V7_LPBIG_OFFBIG_CFLAGS))) {
        private enum enumMixinStr__CS_POSIX_V7_LPBIG_OFFBIG_CFLAGS = `enum _CS_POSIX_V7_LPBIG_OFFBIG_CFLAGS = _CS_POSIX_V7_LPBIG_OFFBIG_CFLAGS;`;
        static if(is(typeof({ mixin(enumMixinStr__CS_POSIX_V7_LPBIG_OFFBIG_CFLAGS); }))) {
            mixin(enumMixinStr__CS_POSIX_V7_LPBIG_OFFBIG_CFLAGS);
        }
    }




    static if(!is(typeof(_CS_POSIX_V7_LP64_OFF64_LINTFLAGS))) {
        private enum enumMixinStr__CS_POSIX_V7_LP64_OFF64_LINTFLAGS = `enum _CS_POSIX_V7_LP64_OFF64_LINTFLAGS = _CS_POSIX_V7_LP64_OFF64_LINTFLAGS;`;
        static if(is(typeof({ mixin(enumMixinStr__CS_POSIX_V7_LP64_OFF64_LINTFLAGS); }))) {
            mixin(enumMixinStr__CS_POSIX_V7_LP64_OFF64_LINTFLAGS);
        }
    }




    static if(!is(typeof(_CS_POSIX_V7_LP64_OFF64_LIBS))) {
        private enum enumMixinStr__CS_POSIX_V7_LP64_OFF64_LIBS = `enum _CS_POSIX_V7_LP64_OFF64_LIBS = _CS_POSIX_V7_LP64_OFF64_LIBS;`;
        static if(is(typeof({ mixin(enumMixinStr__CS_POSIX_V7_LP64_OFF64_LIBS); }))) {
            mixin(enumMixinStr__CS_POSIX_V7_LP64_OFF64_LIBS);
        }
    }




    static if(!is(typeof(_CS_POSIX_V7_LP64_OFF64_LDFLAGS))) {
        private enum enumMixinStr__CS_POSIX_V7_LP64_OFF64_LDFLAGS = `enum _CS_POSIX_V7_LP64_OFF64_LDFLAGS = _CS_POSIX_V7_LP64_OFF64_LDFLAGS;`;
        static if(is(typeof({ mixin(enumMixinStr__CS_POSIX_V7_LP64_OFF64_LDFLAGS); }))) {
            mixin(enumMixinStr__CS_POSIX_V7_LP64_OFF64_LDFLAGS);
        }
    }




    static if(!is(typeof(_CS_POSIX_V7_LP64_OFF64_CFLAGS))) {
        private enum enumMixinStr__CS_POSIX_V7_LP64_OFF64_CFLAGS = `enum _CS_POSIX_V7_LP64_OFF64_CFLAGS = _CS_POSIX_V7_LP64_OFF64_CFLAGS;`;
        static if(is(typeof({ mixin(enumMixinStr__CS_POSIX_V7_LP64_OFF64_CFLAGS); }))) {
            mixin(enumMixinStr__CS_POSIX_V7_LP64_OFF64_CFLAGS);
        }
    }




    static if(!is(typeof(_CS_POSIX_V7_ILP32_OFFBIG_LINTFLAGS))) {
        private enum enumMixinStr__CS_POSIX_V7_ILP32_OFFBIG_LINTFLAGS = `enum _CS_POSIX_V7_ILP32_OFFBIG_LINTFLAGS = _CS_POSIX_V7_ILP32_OFFBIG_LINTFLAGS;`;
        static if(is(typeof({ mixin(enumMixinStr__CS_POSIX_V7_ILP32_OFFBIG_LINTFLAGS); }))) {
            mixin(enumMixinStr__CS_POSIX_V7_ILP32_OFFBIG_LINTFLAGS);
        }
    }




    static if(!is(typeof(_CS_POSIX_V7_ILP32_OFFBIG_LIBS))) {
        private enum enumMixinStr__CS_POSIX_V7_ILP32_OFFBIG_LIBS = `enum _CS_POSIX_V7_ILP32_OFFBIG_LIBS = _CS_POSIX_V7_ILP32_OFFBIG_LIBS;`;
        static if(is(typeof({ mixin(enumMixinStr__CS_POSIX_V7_ILP32_OFFBIG_LIBS); }))) {
            mixin(enumMixinStr__CS_POSIX_V7_ILP32_OFFBIG_LIBS);
        }
    }




    static if(!is(typeof(_CS_POSIX_V7_ILP32_OFFBIG_LDFLAGS))) {
        private enum enumMixinStr__CS_POSIX_V7_ILP32_OFFBIG_LDFLAGS = `enum _CS_POSIX_V7_ILP32_OFFBIG_LDFLAGS = _CS_POSIX_V7_ILP32_OFFBIG_LDFLAGS;`;
        static if(is(typeof({ mixin(enumMixinStr__CS_POSIX_V7_ILP32_OFFBIG_LDFLAGS); }))) {
            mixin(enumMixinStr__CS_POSIX_V7_ILP32_OFFBIG_LDFLAGS);
        }
    }




    static if(!is(typeof(_CS_POSIX_V7_ILP32_OFFBIG_CFLAGS))) {
        private enum enumMixinStr__CS_POSIX_V7_ILP32_OFFBIG_CFLAGS = `enum _CS_POSIX_V7_ILP32_OFFBIG_CFLAGS = _CS_POSIX_V7_ILP32_OFFBIG_CFLAGS;`;
        static if(is(typeof({ mixin(enumMixinStr__CS_POSIX_V7_ILP32_OFFBIG_CFLAGS); }))) {
            mixin(enumMixinStr__CS_POSIX_V7_ILP32_OFFBIG_CFLAGS);
        }
    }




    static if(!is(typeof(_CS_POSIX_V7_ILP32_OFF32_LINTFLAGS))) {
        private enum enumMixinStr__CS_POSIX_V7_ILP32_OFF32_LINTFLAGS = `enum _CS_POSIX_V7_ILP32_OFF32_LINTFLAGS = _CS_POSIX_V7_ILP32_OFF32_LINTFLAGS;`;
        static if(is(typeof({ mixin(enumMixinStr__CS_POSIX_V7_ILP32_OFF32_LINTFLAGS); }))) {
            mixin(enumMixinStr__CS_POSIX_V7_ILP32_OFF32_LINTFLAGS);
        }
    }




    static if(!is(typeof(_CS_POSIX_V7_ILP32_OFF32_LIBS))) {
        private enum enumMixinStr__CS_POSIX_V7_ILP32_OFF32_LIBS = `enum _CS_POSIX_V7_ILP32_OFF32_LIBS = _CS_POSIX_V7_ILP32_OFF32_LIBS;`;
        static if(is(typeof({ mixin(enumMixinStr__CS_POSIX_V7_ILP32_OFF32_LIBS); }))) {
            mixin(enumMixinStr__CS_POSIX_V7_ILP32_OFF32_LIBS);
        }
    }




    static if(!is(typeof(_CS_POSIX_V7_ILP32_OFF32_LDFLAGS))) {
        private enum enumMixinStr__CS_POSIX_V7_ILP32_OFF32_LDFLAGS = `enum _CS_POSIX_V7_ILP32_OFF32_LDFLAGS = _CS_POSIX_V7_ILP32_OFF32_LDFLAGS;`;
        static if(is(typeof({ mixin(enumMixinStr__CS_POSIX_V7_ILP32_OFF32_LDFLAGS); }))) {
            mixin(enumMixinStr__CS_POSIX_V7_ILP32_OFF32_LDFLAGS);
        }
    }




    static if(!is(typeof(_CS_POSIX_V7_ILP32_OFF32_CFLAGS))) {
        private enum enumMixinStr__CS_POSIX_V7_ILP32_OFF32_CFLAGS = `enum _CS_POSIX_V7_ILP32_OFF32_CFLAGS = _CS_POSIX_V7_ILP32_OFF32_CFLAGS;`;
        static if(is(typeof({ mixin(enumMixinStr__CS_POSIX_V7_ILP32_OFF32_CFLAGS); }))) {
            mixin(enumMixinStr__CS_POSIX_V7_ILP32_OFF32_CFLAGS);
        }
    }




    static if(!is(typeof(_CS_POSIX_V6_LPBIG_OFFBIG_LINTFLAGS))) {
        private enum enumMixinStr__CS_POSIX_V6_LPBIG_OFFBIG_LINTFLAGS = `enum _CS_POSIX_V6_LPBIG_OFFBIG_LINTFLAGS = _CS_POSIX_V6_LPBIG_OFFBIG_LINTFLAGS;`;
        static if(is(typeof({ mixin(enumMixinStr__CS_POSIX_V6_LPBIG_OFFBIG_LINTFLAGS); }))) {
            mixin(enumMixinStr__CS_POSIX_V6_LPBIG_OFFBIG_LINTFLAGS);
        }
    }




    static if(!is(typeof(_CS_POSIX_V6_LPBIG_OFFBIG_LIBS))) {
        private enum enumMixinStr__CS_POSIX_V6_LPBIG_OFFBIG_LIBS = `enum _CS_POSIX_V6_LPBIG_OFFBIG_LIBS = _CS_POSIX_V6_LPBIG_OFFBIG_LIBS;`;
        static if(is(typeof({ mixin(enumMixinStr__CS_POSIX_V6_LPBIG_OFFBIG_LIBS); }))) {
            mixin(enumMixinStr__CS_POSIX_V6_LPBIG_OFFBIG_LIBS);
        }
    }




    static if(!is(typeof(_CS_POSIX_V6_LPBIG_OFFBIG_LDFLAGS))) {
        private enum enumMixinStr__CS_POSIX_V6_LPBIG_OFFBIG_LDFLAGS = `enum _CS_POSIX_V6_LPBIG_OFFBIG_LDFLAGS = _CS_POSIX_V6_LPBIG_OFFBIG_LDFLAGS;`;
        static if(is(typeof({ mixin(enumMixinStr__CS_POSIX_V6_LPBIG_OFFBIG_LDFLAGS); }))) {
            mixin(enumMixinStr__CS_POSIX_V6_LPBIG_OFFBIG_LDFLAGS);
        }
    }




    static if(!is(typeof(_CS_POSIX_V6_LPBIG_OFFBIG_CFLAGS))) {
        private enum enumMixinStr__CS_POSIX_V6_LPBIG_OFFBIG_CFLAGS = `enum _CS_POSIX_V6_LPBIG_OFFBIG_CFLAGS = _CS_POSIX_V6_LPBIG_OFFBIG_CFLAGS;`;
        static if(is(typeof({ mixin(enumMixinStr__CS_POSIX_V6_LPBIG_OFFBIG_CFLAGS); }))) {
            mixin(enumMixinStr__CS_POSIX_V6_LPBIG_OFFBIG_CFLAGS);
        }
    }




    static if(!is(typeof(_CS_POSIX_V6_LP64_OFF64_LINTFLAGS))) {
        private enum enumMixinStr__CS_POSIX_V6_LP64_OFF64_LINTFLAGS = `enum _CS_POSIX_V6_LP64_OFF64_LINTFLAGS = _CS_POSIX_V6_LP64_OFF64_LINTFLAGS;`;
        static if(is(typeof({ mixin(enumMixinStr__CS_POSIX_V6_LP64_OFF64_LINTFLAGS); }))) {
            mixin(enumMixinStr__CS_POSIX_V6_LP64_OFF64_LINTFLAGS);
        }
    }




    static if(!is(typeof(_CS_POSIX_V6_LP64_OFF64_LIBS))) {
        private enum enumMixinStr__CS_POSIX_V6_LP64_OFF64_LIBS = `enum _CS_POSIX_V6_LP64_OFF64_LIBS = _CS_POSIX_V6_LP64_OFF64_LIBS;`;
        static if(is(typeof({ mixin(enumMixinStr__CS_POSIX_V6_LP64_OFF64_LIBS); }))) {
            mixin(enumMixinStr__CS_POSIX_V6_LP64_OFF64_LIBS);
        }
    }




    static if(!is(typeof(_CS_POSIX_V6_LP64_OFF64_LDFLAGS))) {
        private enum enumMixinStr__CS_POSIX_V6_LP64_OFF64_LDFLAGS = `enum _CS_POSIX_V6_LP64_OFF64_LDFLAGS = _CS_POSIX_V6_LP64_OFF64_LDFLAGS;`;
        static if(is(typeof({ mixin(enumMixinStr__CS_POSIX_V6_LP64_OFF64_LDFLAGS); }))) {
            mixin(enumMixinStr__CS_POSIX_V6_LP64_OFF64_LDFLAGS);
        }
    }




    static if(!is(typeof(_CS_POSIX_V6_LP64_OFF64_CFLAGS))) {
        private enum enumMixinStr__CS_POSIX_V6_LP64_OFF64_CFLAGS = `enum _CS_POSIX_V6_LP64_OFF64_CFLAGS = _CS_POSIX_V6_LP64_OFF64_CFLAGS;`;
        static if(is(typeof({ mixin(enumMixinStr__CS_POSIX_V6_LP64_OFF64_CFLAGS); }))) {
            mixin(enumMixinStr__CS_POSIX_V6_LP64_OFF64_CFLAGS);
        }
    }




    static if(!is(typeof(_CS_POSIX_V6_ILP32_OFFBIG_LINTFLAGS))) {
        private enum enumMixinStr__CS_POSIX_V6_ILP32_OFFBIG_LINTFLAGS = `enum _CS_POSIX_V6_ILP32_OFFBIG_LINTFLAGS = _CS_POSIX_V6_ILP32_OFFBIG_LINTFLAGS;`;
        static if(is(typeof({ mixin(enumMixinStr__CS_POSIX_V6_ILP32_OFFBIG_LINTFLAGS); }))) {
            mixin(enumMixinStr__CS_POSIX_V6_ILP32_OFFBIG_LINTFLAGS);
        }
    }




    static if(!is(typeof(_CS_POSIX_V6_ILP32_OFFBIG_LIBS))) {
        private enum enumMixinStr__CS_POSIX_V6_ILP32_OFFBIG_LIBS = `enum _CS_POSIX_V6_ILP32_OFFBIG_LIBS = _CS_POSIX_V6_ILP32_OFFBIG_LIBS;`;
        static if(is(typeof({ mixin(enumMixinStr__CS_POSIX_V6_ILP32_OFFBIG_LIBS); }))) {
            mixin(enumMixinStr__CS_POSIX_V6_ILP32_OFFBIG_LIBS);
        }
    }




    static if(!is(typeof(_CS_POSIX_V6_ILP32_OFFBIG_LDFLAGS))) {
        private enum enumMixinStr__CS_POSIX_V6_ILP32_OFFBIG_LDFLAGS = `enum _CS_POSIX_V6_ILP32_OFFBIG_LDFLAGS = _CS_POSIX_V6_ILP32_OFFBIG_LDFLAGS;`;
        static if(is(typeof({ mixin(enumMixinStr__CS_POSIX_V6_ILP32_OFFBIG_LDFLAGS); }))) {
            mixin(enumMixinStr__CS_POSIX_V6_ILP32_OFFBIG_LDFLAGS);
        }
    }




    static if(!is(typeof(_CS_POSIX_V6_ILP32_OFFBIG_CFLAGS))) {
        private enum enumMixinStr__CS_POSIX_V6_ILP32_OFFBIG_CFLAGS = `enum _CS_POSIX_V6_ILP32_OFFBIG_CFLAGS = _CS_POSIX_V6_ILP32_OFFBIG_CFLAGS;`;
        static if(is(typeof({ mixin(enumMixinStr__CS_POSIX_V6_ILP32_OFFBIG_CFLAGS); }))) {
            mixin(enumMixinStr__CS_POSIX_V6_ILP32_OFFBIG_CFLAGS);
        }
    }




    static if(!is(typeof(_CS_POSIX_V6_ILP32_OFF32_LINTFLAGS))) {
        private enum enumMixinStr__CS_POSIX_V6_ILP32_OFF32_LINTFLAGS = `enum _CS_POSIX_V6_ILP32_OFF32_LINTFLAGS = _CS_POSIX_V6_ILP32_OFF32_LINTFLAGS;`;
        static if(is(typeof({ mixin(enumMixinStr__CS_POSIX_V6_ILP32_OFF32_LINTFLAGS); }))) {
            mixin(enumMixinStr__CS_POSIX_V6_ILP32_OFF32_LINTFLAGS);
        }
    }




    static if(!is(typeof(_CS_POSIX_V6_ILP32_OFF32_LIBS))) {
        private enum enumMixinStr__CS_POSIX_V6_ILP32_OFF32_LIBS = `enum _CS_POSIX_V6_ILP32_OFF32_LIBS = _CS_POSIX_V6_ILP32_OFF32_LIBS;`;
        static if(is(typeof({ mixin(enumMixinStr__CS_POSIX_V6_ILP32_OFF32_LIBS); }))) {
            mixin(enumMixinStr__CS_POSIX_V6_ILP32_OFF32_LIBS);
        }
    }




    static if(!is(typeof(_CS_POSIX_V6_ILP32_OFF32_LDFLAGS))) {
        private enum enumMixinStr__CS_POSIX_V6_ILP32_OFF32_LDFLAGS = `enum _CS_POSIX_V6_ILP32_OFF32_LDFLAGS = _CS_POSIX_V6_ILP32_OFF32_LDFLAGS;`;
        static if(is(typeof({ mixin(enumMixinStr__CS_POSIX_V6_ILP32_OFF32_LDFLAGS); }))) {
            mixin(enumMixinStr__CS_POSIX_V6_ILP32_OFF32_LDFLAGS);
        }
    }




    static if(!is(typeof(_CS_POSIX_V6_ILP32_OFF32_CFLAGS))) {
        private enum enumMixinStr__CS_POSIX_V6_ILP32_OFF32_CFLAGS = `enum _CS_POSIX_V6_ILP32_OFF32_CFLAGS = _CS_POSIX_V6_ILP32_OFF32_CFLAGS;`;
        static if(is(typeof({ mixin(enumMixinStr__CS_POSIX_V6_ILP32_OFF32_CFLAGS); }))) {
            mixin(enumMixinStr__CS_POSIX_V6_ILP32_OFF32_CFLAGS);
        }
    }




    static if(!is(typeof(_CS_XBS5_LPBIG_OFFBIG_LINTFLAGS))) {
        private enum enumMixinStr__CS_XBS5_LPBIG_OFFBIG_LINTFLAGS = `enum _CS_XBS5_LPBIG_OFFBIG_LINTFLAGS = _CS_XBS5_LPBIG_OFFBIG_LINTFLAGS;`;
        static if(is(typeof({ mixin(enumMixinStr__CS_XBS5_LPBIG_OFFBIG_LINTFLAGS); }))) {
            mixin(enumMixinStr__CS_XBS5_LPBIG_OFFBIG_LINTFLAGS);
        }
    }




    static if(!is(typeof(_CS_XBS5_LPBIG_OFFBIG_LIBS))) {
        private enum enumMixinStr__CS_XBS5_LPBIG_OFFBIG_LIBS = `enum _CS_XBS5_LPBIG_OFFBIG_LIBS = _CS_XBS5_LPBIG_OFFBIG_LIBS;`;
        static if(is(typeof({ mixin(enumMixinStr__CS_XBS5_LPBIG_OFFBIG_LIBS); }))) {
            mixin(enumMixinStr__CS_XBS5_LPBIG_OFFBIG_LIBS);
        }
    }




    static if(!is(typeof(_CS_XBS5_LPBIG_OFFBIG_LDFLAGS))) {
        private enum enumMixinStr__CS_XBS5_LPBIG_OFFBIG_LDFLAGS = `enum _CS_XBS5_LPBIG_OFFBIG_LDFLAGS = _CS_XBS5_LPBIG_OFFBIG_LDFLAGS;`;
        static if(is(typeof({ mixin(enumMixinStr__CS_XBS5_LPBIG_OFFBIG_LDFLAGS); }))) {
            mixin(enumMixinStr__CS_XBS5_LPBIG_OFFBIG_LDFLAGS);
        }
    }




    static if(!is(typeof(_CS_XBS5_LPBIG_OFFBIG_CFLAGS))) {
        private enum enumMixinStr__CS_XBS5_LPBIG_OFFBIG_CFLAGS = `enum _CS_XBS5_LPBIG_OFFBIG_CFLAGS = _CS_XBS5_LPBIG_OFFBIG_CFLAGS;`;
        static if(is(typeof({ mixin(enumMixinStr__CS_XBS5_LPBIG_OFFBIG_CFLAGS); }))) {
            mixin(enumMixinStr__CS_XBS5_LPBIG_OFFBIG_CFLAGS);
        }
    }




    static if(!is(typeof(_CS_XBS5_LP64_OFF64_LINTFLAGS))) {
        private enum enumMixinStr__CS_XBS5_LP64_OFF64_LINTFLAGS = `enum _CS_XBS5_LP64_OFF64_LINTFLAGS = _CS_XBS5_LP64_OFF64_LINTFLAGS;`;
        static if(is(typeof({ mixin(enumMixinStr__CS_XBS5_LP64_OFF64_LINTFLAGS); }))) {
            mixin(enumMixinStr__CS_XBS5_LP64_OFF64_LINTFLAGS);
        }
    }




    static if(!is(typeof(_CS_XBS5_LP64_OFF64_LIBS))) {
        private enum enumMixinStr__CS_XBS5_LP64_OFF64_LIBS = `enum _CS_XBS5_LP64_OFF64_LIBS = _CS_XBS5_LP64_OFF64_LIBS;`;
        static if(is(typeof({ mixin(enumMixinStr__CS_XBS5_LP64_OFF64_LIBS); }))) {
            mixin(enumMixinStr__CS_XBS5_LP64_OFF64_LIBS);
        }
    }




    static if(!is(typeof(_CS_XBS5_LP64_OFF64_LDFLAGS))) {
        private enum enumMixinStr__CS_XBS5_LP64_OFF64_LDFLAGS = `enum _CS_XBS5_LP64_OFF64_LDFLAGS = _CS_XBS5_LP64_OFF64_LDFLAGS;`;
        static if(is(typeof({ mixin(enumMixinStr__CS_XBS5_LP64_OFF64_LDFLAGS); }))) {
            mixin(enumMixinStr__CS_XBS5_LP64_OFF64_LDFLAGS);
        }
    }




    static if(!is(typeof(_CS_XBS5_LP64_OFF64_CFLAGS))) {
        private enum enumMixinStr__CS_XBS5_LP64_OFF64_CFLAGS = `enum _CS_XBS5_LP64_OFF64_CFLAGS = _CS_XBS5_LP64_OFF64_CFLAGS;`;
        static if(is(typeof({ mixin(enumMixinStr__CS_XBS5_LP64_OFF64_CFLAGS); }))) {
            mixin(enumMixinStr__CS_XBS5_LP64_OFF64_CFLAGS);
        }
    }




    static if(!is(typeof(_CS_XBS5_ILP32_OFFBIG_LINTFLAGS))) {
        private enum enumMixinStr__CS_XBS5_ILP32_OFFBIG_LINTFLAGS = `enum _CS_XBS5_ILP32_OFFBIG_LINTFLAGS = _CS_XBS5_ILP32_OFFBIG_LINTFLAGS;`;
        static if(is(typeof({ mixin(enumMixinStr__CS_XBS5_ILP32_OFFBIG_LINTFLAGS); }))) {
            mixin(enumMixinStr__CS_XBS5_ILP32_OFFBIG_LINTFLAGS);
        }
    }




    static if(!is(typeof(_CS_XBS5_ILP32_OFFBIG_LIBS))) {
        private enum enumMixinStr__CS_XBS5_ILP32_OFFBIG_LIBS = `enum _CS_XBS5_ILP32_OFFBIG_LIBS = _CS_XBS5_ILP32_OFFBIG_LIBS;`;
        static if(is(typeof({ mixin(enumMixinStr__CS_XBS5_ILP32_OFFBIG_LIBS); }))) {
            mixin(enumMixinStr__CS_XBS5_ILP32_OFFBIG_LIBS);
        }
    }




    static if(!is(typeof(_CS_XBS5_ILP32_OFFBIG_LDFLAGS))) {
        private enum enumMixinStr__CS_XBS5_ILP32_OFFBIG_LDFLAGS = `enum _CS_XBS5_ILP32_OFFBIG_LDFLAGS = _CS_XBS5_ILP32_OFFBIG_LDFLAGS;`;
        static if(is(typeof({ mixin(enumMixinStr__CS_XBS5_ILP32_OFFBIG_LDFLAGS); }))) {
            mixin(enumMixinStr__CS_XBS5_ILP32_OFFBIG_LDFLAGS);
        }
    }




    static if(!is(typeof(_CS_XBS5_ILP32_OFFBIG_CFLAGS))) {
        private enum enumMixinStr__CS_XBS5_ILP32_OFFBIG_CFLAGS = `enum _CS_XBS5_ILP32_OFFBIG_CFLAGS = _CS_XBS5_ILP32_OFFBIG_CFLAGS;`;
        static if(is(typeof({ mixin(enumMixinStr__CS_XBS5_ILP32_OFFBIG_CFLAGS); }))) {
            mixin(enumMixinStr__CS_XBS5_ILP32_OFFBIG_CFLAGS);
        }
    }




    static if(!is(typeof(_CS_XBS5_ILP32_OFF32_LINTFLAGS))) {
        private enum enumMixinStr__CS_XBS5_ILP32_OFF32_LINTFLAGS = `enum _CS_XBS5_ILP32_OFF32_LINTFLAGS = _CS_XBS5_ILP32_OFF32_LINTFLAGS;`;
        static if(is(typeof({ mixin(enumMixinStr__CS_XBS5_ILP32_OFF32_LINTFLAGS); }))) {
            mixin(enumMixinStr__CS_XBS5_ILP32_OFF32_LINTFLAGS);
        }
    }




    static if(!is(typeof(_CS_XBS5_ILP32_OFF32_LIBS))) {
        private enum enumMixinStr__CS_XBS5_ILP32_OFF32_LIBS = `enum _CS_XBS5_ILP32_OFF32_LIBS = _CS_XBS5_ILP32_OFF32_LIBS;`;
        static if(is(typeof({ mixin(enumMixinStr__CS_XBS5_ILP32_OFF32_LIBS); }))) {
            mixin(enumMixinStr__CS_XBS5_ILP32_OFF32_LIBS);
        }
    }




    static if(!is(typeof(_CS_XBS5_ILP32_OFF32_LDFLAGS))) {
        private enum enumMixinStr__CS_XBS5_ILP32_OFF32_LDFLAGS = `enum _CS_XBS5_ILP32_OFF32_LDFLAGS = _CS_XBS5_ILP32_OFF32_LDFLAGS;`;
        static if(is(typeof({ mixin(enumMixinStr__CS_XBS5_ILP32_OFF32_LDFLAGS); }))) {
            mixin(enumMixinStr__CS_XBS5_ILP32_OFF32_LDFLAGS);
        }
    }




    static if(!is(typeof(_CS_XBS5_ILP32_OFF32_CFLAGS))) {
        private enum enumMixinStr__CS_XBS5_ILP32_OFF32_CFLAGS = `enum _CS_XBS5_ILP32_OFF32_CFLAGS = _CS_XBS5_ILP32_OFF32_CFLAGS;`;
        static if(is(typeof({ mixin(enumMixinStr__CS_XBS5_ILP32_OFF32_CFLAGS); }))) {
            mixin(enumMixinStr__CS_XBS5_ILP32_OFF32_CFLAGS);
        }
    }




    static if(!is(typeof(_CS_LFS64_LINTFLAGS))) {
        private enum enumMixinStr__CS_LFS64_LINTFLAGS = `enum _CS_LFS64_LINTFLAGS = _CS_LFS64_LINTFLAGS;`;
        static if(is(typeof({ mixin(enumMixinStr__CS_LFS64_LINTFLAGS); }))) {
            mixin(enumMixinStr__CS_LFS64_LINTFLAGS);
        }
    }




    static if(!is(typeof(_CS_LFS64_LIBS))) {
        private enum enumMixinStr__CS_LFS64_LIBS = `enum _CS_LFS64_LIBS = _CS_LFS64_LIBS;`;
        static if(is(typeof({ mixin(enumMixinStr__CS_LFS64_LIBS); }))) {
            mixin(enumMixinStr__CS_LFS64_LIBS);
        }
    }




    static if(!is(typeof(_CS_LFS64_LDFLAGS))) {
        private enum enumMixinStr__CS_LFS64_LDFLAGS = `enum _CS_LFS64_LDFLAGS = _CS_LFS64_LDFLAGS;`;
        static if(is(typeof({ mixin(enumMixinStr__CS_LFS64_LDFLAGS); }))) {
            mixin(enumMixinStr__CS_LFS64_LDFLAGS);
        }
    }




    static if(!is(typeof(_CS_LFS64_CFLAGS))) {
        private enum enumMixinStr__CS_LFS64_CFLAGS = `enum _CS_LFS64_CFLAGS = _CS_LFS64_CFLAGS;`;
        static if(is(typeof({ mixin(enumMixinStr__CS_LFS64_CFLAGS); }))) {
            mixin(enumMixinStr__CS_LFS64_CFLAGS);
        }
    }




    static if(!is(typeof(_CS_LFS_LINTFLAGS))) {
        private enum enumMixinStr__CS_LFS_LINTFLAGS = `enum _CS_LFS_LINTFLAGS = _CS_LFS_LINTFLAGS;`;
        static if(is(typeof({ mixin(enumMixinStr__CS_LFS_LINTFLAGS); }))) {
            mixin(enumMixinStr__CS_LFS_LINTFLAGS);
        }
    }




    static if(!is(typeof(_CS_LFS_LIBS))) {
        private enum enumMixinStr__CS_LFS_LIBS = `enum _CS_LFS_LIBS = _CS_LFS_LIBS;`;
        static if(is(typeof({ mixin(enumMixinStr__CS_LFS_LIBS); }))) {
            mixin(enumMixinStr__CS_LFS_LIBS);
        }
    }




    static if(!is(typeof(_CS_LFS_LDFLAGS))) {
        private enum enumMixinStr__CS_LFS_LDFLAGS = `enum _CS_LFS_LDFLAGS = _CS_LFS_LDFLAGS;`;
        static if(is(typeof({ mixin(enumMixinStr__CS_LFS_LDFLAGS); }))) {
            mixin(enumMixinStr__CS_LFS_LDFLAGS);
        }
    }




    static if(!is(typeof(_CS_LFS_CFLAGS))) {
        private enum enumMixinStr__CS_LFS_CFLAGS = `enum _CS_LFS_CFLAGS = _CS_LFS_CFLAGS;`;
        static if(is(typeof({ mixin(enumMixinStr__CS_LFS_CFLAGS); }))) {
            mixin(enumMixinStr__CS_LFS_CFLAGS);
        }
    }




    static if(!is(typeof(LIBEVDEV_DEPRECATED))) {
        private enum enumMixinStr_LIBEVDEV_DEPRECATED = `enum LIBEVDEV_DEPRECATED = __attribute__ ( ( deprecated ) );`;
        static if(is(typeof({ mixin(enumMixinStr_LIBEVDEV_DEPRECATED); }))) {
            mixin(enumMixinStr_LIBEVDEV_DEPRECATED);
        }
    }




    static if(!is(typeof(_CS_POSIX_V7_WIDTH_RESTRICTED_ENVS))) {
        private enum enumMixinStr__CS_POSIX_V7_WIDTH_RESTRICTED_ENVS = `enum _CS_POSIX_V7_WIDTH_RESTRICTED_ENVS = _CS_V7_WIDTH_RESTRICTED_ENVS;`;
        static if(is(typeof({ mixin(enumMixinStr__CS_POSIX_V7_WIDTH_RESTRICTED_ENVS); }))) {
            mixin(enumMixinStr__CS_POSIX_V7_WIDTH_RESTRICTED_ENVS);
        }
    }






    static if(!is(typeof(INPUT_PROP_POINTER))) {
        private enum enumMixinStr_INPUT_PROP_POINTER = `enum INPUT_PROP_POINTER = 0x00;`;
        static if(is(typeof({ mixin(enumMixinStr_INPUT_PROP_POINTER); }))) {
            mixin(enumMixinStr_INPUT_PROP_POINTER);
        }
    }




    static if(!is(typeof(INPUT_PROP_DIRECT))) {
        private enum enumMixinStr_INPUT_PROP_DIRECT = `enum INPUT_PROP_DIRECT = 0x01;`;
        static if(is(typeof({ mixin(enumMixinStr_INPUT_PROP_DIRECT); }))) {
            mixin(enumMixinStr_INPUT_PROP_DIRECT);
        }
    }




    static if(!is(typeof(INPUT_PROP_BUTTONPAD))) {
        private enum enumMixinStr_INPUT_PROP_BUTTONPAD = `enum INPUT_PROP_BUTTONPAD = 0x02;`;
        static if(is(typeof({ mixin(enumMixinStr_INPUT_PROP_BUTTONPAD); }))) {
            mixin(enumMixinStr_INPUT_PROP_BUTTONPAD);
        }
    }




    static if(!is(typeof(INPUT_PROP_SEMI_MT))) {
        private enum enumMixinStr_INPUT_PROP_SEMI_MT = `enum INPUT_PROP_SEMI_MT = 0x03;`;
        static if(is(typeof({ mixin(enumMixinStr_INPUT_PROP_SEMI_MT); }))) {
            mixin(enumMixinStr_INPUT_PROP_SEMI_MT);
        }
    }




    static if(!is(typeof(INPUT_PROP_TOPBUTTONPAD))) {
        private enum enumMixinStr_INPUT_PROP_TOPBUTTONPAD = `enum INPUT_PROP_TOPBUTTONPAD = 0x04;`;
        static if(is(typeof({ mixin(enumMixinStr_INPUT_PROP_TOPBUTTONPAD); }))) {
            mixin(enumMixinStr_INPUT_PROP_TOPBUTTONPAD);
        }
    }




    static if(!is(typeof(INPUT_PROP_POINTING_STICK))) {
        private enum enumMixinStr_INPUT_PROP_POINTING_STICK = `enum INPUT_PROP_POINTING_STICK = 0x05;`;
        static if(is(typeof({ mixin(enumMixinStr_INPUT_PROP_POINTING_STICK); }))) {
            mixin(enumMixinStr_INPUT_PROP_POINTING_STICK);
        }
    }




    static if(!is(typeof(INPUT_PROP_ACCELEROMETER))) {
        private enum enumMixinStr_INPUT_PROP_ACCELEROMETER = `enum INPUT_PROP_ACCELEROMETER = 0x06;`;
        static if(is(typeof({ mixin(enumMixinStr_INPUT_PROP_ACCELEROMETER); }))) {
            mixin(enumMixinStr_INPUT_PROP_ACCELEROMETER);
        }
    }




    static if(!is(typeof(INPUT_PROP_MAX))) {
        private enum enumMixinStr_INPUT_PROP_MAX = `enum INPUT_PROP_MAX = 0x1f;`;
        static if(is(typeof({ mixin(enumMixinStr_INPUT_PROP_MAX); }))) {
            mixin(enumMixinStr_INPUT_PROP_MAX);
        }
    }




    static if(!is(typeof(INPUT_PROP_CNT))) {
        private enum enumMixinStr_INPUT_PROP_CNT = `enum INPUT_PROP_CNT = ( 0x1f + 1 );`;
        static if(is(typeof({ mixin(enumMixinStr_INPUT_PROP_CNT); }))) {
            mixin(enumMixinStr_INPUT_PROP_CNT);
        }
    }




    static if(!is(typeof(EV_SYN))) {
        private enum enumMixinStr_EV_SYN = `enum EV_SYN = 0x00;`;
        static if(is(typeof({ mixin(enumMixinStr_EV_SYN); }))) {
            mixin(enumMixinStr_EV_SYN);
        }
    }




    static if(!is(typeof(EV_KEY))) {
        private enum enumMixinStr_EV_KEY = `enum EV_KEY = 0x01;`;
        static if(is(typeof({ mixin(enumMixinStr_EV_KEY); }))) {
            mixin(enumMixinStr_EV_KEY);
        }
    }




    static if(!is(typeof(EV_REL))) {
        private enum enumMixinStr_EV_REL = `enum EV_REL = 0x02;`;
        static if(is(typeof({ mixin(enumMixinStr_EV_REL); }))) {
            mixin(enumMixinStr_EV_REL);
        }
    }




    static if(!is(typeof(EV_ABS))) {
        private enum enumMixinStr_EV_ABS = `enum EV_ABS = 0x03;`;
        static if(is(typeof({ mixin(enumMixinStr_EV_ABS); }))) {
            mixin(enumMixinStr_EV_ABS);
        }
    }




    static if(!is(typeof(EV_MSC))) {
        private enum enumMixinStr_EV_MSC = `enum EV_MSC = 0x04;`;
        static if(is(typeof({ mixin(enumMixinStr_EV_MSC); }))) {
            mixin(enumMixinStr_EV_MSC);
        }
    }




    static if(!is(typeof(EV_SW))) {
        private enum enumMixinStr_EV_SW = `enum EV_SW = 0x05;`;
        static if(is(typeof({ mixin(enumMixinStr_EV_SW); }))) {
            mixin(enumMixinStr_EV_SW);
        }
    }




    static if(!is(typeof(EV_LED))) {
        private enum enumMixinStr_EV_LED = `enum EV_LED = 0x11;`;
        static if(is(typeof({ mixin(enumMixinStr_EV_LED); }))) {
            mixin(enumMixinStr_EV_LED);
        }
    }




    static if(!is(typeof(EV_SND))) {
        private enum enumMixinStr_EV_SND = `enum EV_SND = 0x12;`;
        static if(is(typeof({ mixin(enumMixinStr_EV_SND); }))) {
            mixin(enumMixinStr_EV_SND);
        }
    }




    static if(!is(typeof(EV_REP))) {
        private enum enumMixinStr_EV_REP = `enum EV_REP = 0x14;`;
        static if(is(typeof({ mixin(enumMixinStr_EV_REP); }))) {
            mixin(enumMixinStr_EV_REP);
        }
    }




    static if(!is(typeof(EV_FF))) {
        private enum enumMixinStr_EV_FF = `enum EV_FF = 0x15;`;
        static if(is(typeof({ mixin(enumMixinStr_EV_FF); }))) {
            mixin(enumMixinStr_EV_FF);
        }
    }




    static if(!is(typeof(EV_PWR))) {
        private enum enumMixinStr_EV_PWR = `enum EV_PWR = 0x16;`;
        static if(is(typeof({ mixin(enumMixinStr_EV_PWR); }))) {
            mixin(enumMixinStr_EV_PWR);
        }
    }




    static if(!is(typeof(EV_FF_STATUS))) {
        private enum enumMixinStr_EV_FF_STATUS = `enum EV_FF_STATUS = 0x17;`;
        static if(is(typeof({ mixin(enumMixinStr_EV_FF_STATUS); }))) {
            mixin(enumMixinStr_EV_FF_STATUS);
        }
    }




    static if(!is(typeof(EV_MAX))) {
        private enum enumMixinStr_EV_MAX = `enum EV_MAX = 0x1f;`;
        static if(is(typeof({ mixin(enumMixinStr_EV_MAX); }))) {
            mixin(enumMixinStr_EV_MAX);
        }
    }




    static if(!is(typeof(EV_CNT))) {
        private enum enumMixinStr_EV_CNT = `enum EV_CNT = ( 0x1f + 1 );`;
        static if(is(typeof({ mixin(enumMixinStr_EV_CNT); }))) {
            mixin(enumMixinStr_EV_CNT);
        }
    }




    static if(!is(typeof(SYN_REPORT))) {
        private enum enumMixinStr_SYN_REPORT = `enum SYN_REPORT = 0;`;
        static if(is(typeof({ mixin(enumMixinStr_SYN_REPORT); }))) {
            mixin(enumMixinStr_SYN_REPORT);
        }
    }




    static if(!is(typeof(SYN_CONFIG))) {
        private enum enumMixinStr_SYN_CONFIG = `enum SYN_CONFIG = 1;`;
        static if(is(typeof({ mixin(enumMixinStr_SYN_CONFIG); }))) {
            mixin(enumMixinStr_SYN_CONFIG);
        }
    }




    static if(!is(typeof(SYN_MT_REPORT))) {
        private enum enumMixinStr_SYN_MT_REPORT = `enum SYN_MT_REPORT = 2;`;
        static if(is(typeof({ mixin(enumMixinStr_SYN_MT_REPORT); }))) {
            mixin(enumMixinStr_SYN_MT_REPORT);
        }
    }




    static if(!is(typeof(SYN_DROPPED))) {
        private enum enumMixinStr_SYN_DROPPED = `enum SYN_DROPPED = 3;`;
        static if(is(typeof({ mixin(enumMixinStr_SYN_DROPPED); }))) {
            mixin(enumMixinStr_SYN_DROPPED);
        }
    }




    static if(!is(typeof(SYN_MAX))) {
        private enum enumMixinStr_SYN_MAX = `enum SYN_MAX = 0xf;`;
        static if(is(typeof({ mixin(enumMixinStr_SYN_MAX); }))) {
            mixin(enumMixinStr_SYN_MAX);
        }
    }




    static if(!is(typeof(SYN_CNT))) {
        private enum enumMixinStr_SYN_CNT = `enum SYN_CNT = ( 0xf + 1 );`;
        static if(is(typeof({ mixin(enumMixinStr_SYN_CNT); }))) {
            mixin(enumMixinStr_SYN_CNT);
        }
    }




    static if(!is(typeof(KEY_RESERVED))) {
        private enum enumMixinStr_KEY_RESERVED = `enum KEY_RESERVED = 0;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_RESERVED); }))) {
            mixin(enumMixinStr_KEY_RESERVED);
        }
    }




    static if(!is(typeof(KEY_ESC))) {
        private enum enumMixinStr_KEY_ESC = `enum KEY_ESC = 1;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_ESC); }))) {
            mixin(enumMixinStr_KEY_ESC);
        }
    }




    static if(!is(typeof(KEY_1))) {
        private enum enumMixinStr_KEY_1 = `enum KEY_1 = 2;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_1); }))) {
            mixin(enumMixinStr_KEY_1);
        }
    }




    static if(!is(typeof(KEY_2))) {
        private enum enumMixinStr_KEY_2 = `enum KEY_2 = 3;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_2); }))) {
            mixin(enumMixinStr_KEY_2);
        }
    }




    static if(!is(typeof(KEY_3))) {
        private enum enumMixinStr_KEY_3 = `enum KEY_3 = 4;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_3); }))) {
            mixin(enumMixinStr_KEY_3);
        }
    }




    static if(!is(typeof(KEY_4))) {
        private enum enumMixinStr_KEY_4 = `enum KEY_4 = 5;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_4); }))) {
            mixin(enumMixinStr_KEY_4);
        }
    }




    static if(!is(typeof(KEY_5))) {
        private enum enumMixinStr_KEY_5 = `enum KEY_5 = 6;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_5); }))) {
            mixin(enumMixinStr_KEY_5);
        }
    }




    static if(!is(typeof(KEY_6))) {
        private enum enumMixinStr_KEY_6 = `enum KEY_6 = 7;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_6); }))) {
            mixin(enumMixinStr_KEY_6);
        }
    }




    static if(!is(typeof(KEY_7))) {
        private enum enumMixinStr_KEY_7 = `enum KEY_7 = 8;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_7); }))) {
            mixin(enumMixinStr_KEY_7);
        }
    }




    static if(!is(typeof(KEY_8))) {
        private enum enumMixinStr_KEY_8 = `enum KEY_8 = 9;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_8); }))) {
            mixin(enumMixinStr_KEY_8);
        }
    }




    static if(!is(typeof(KEY_9))) {
        private enum enumMixinStr_KEY_9 = `enum KEY_9 = 10;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_9); }))) {
            mixin(enumMixinStr_KEY_9);
        }
    }




    static if(!is(typeof(KEY_0))) {
        private enum enumMixinStr_KEY_0 = `enum KEY_0 = 11;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_0); }))) {
            mixin(enumMixinStr_KEY_0);
        }
    }




    static if(!is(typeof(KEY_MINUS))) {
        private enum enumMixinStr_KEY_MINUS = `enum KEY_MINUS = 12;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_MINUS); }))) {
            mixin(enumMixinStr_KEY_MINUS);
        }
    }




    static if(!is(typeof(KEY_EQUAL))) {
        private enum enumMixinStr_KEY_EQUAL = `enum KEY_EQUAL = 13;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_EQUAL); }))) {
            mixin(enumMixinStr_KEY_EQUAL);
        }
    }




    static if(!is(typeof(KEY_BACKSPACE))) {
        private enum enumMixinStr_KEY_BACKSPACE = `enum KEY_BACKSPACE = 14;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_BACKSPACE); }))) {
            mixin(enumMixinStr_KEY_BACKSPACE);
        }
    }




    static if(!is(typeof(KEY_TAB))) {
        private enum enumMixinStr_KEY_TAB = `enum KEY_TAB = 15;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_TAB); }))) {
            mixin(enumMixinStr_KEY_TAB);
        }
    }




    static if(!is(typeof(KEY_Q))) {
        private enum enumMixinStr_KEY_Q = `enum KEY_Q = 16;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_Q); }))) {
            mixin(enumMixinStr_KEY_Q);
        }
    }




    static if(!is(typeof(KEY_W))) {
        private enum enumMixinStr_KEY_W = `enum KEY_W = 17;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_W); }))) {
            mixin(enumMixinStr_KEY_W);
        }
    }




    static if(!is(typeof(KEY_E))) {
        private enum enumMixinStr_KEY_E = `enum KEY_E = 18;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_E); }))) {
            mixin(enumMixinStr_KEY_E);
        }
    }




    static if(!is(typeof(KEY_R))) {
        private enum enumMixinStr_KEY_R = `enum KEY_R = 19;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_R); }))) {
            mixin(enumMixinStr_KEY_R);
        }
    }




    static if(!is(typeof(KEY_T))) {
        private enum enumMixinStr_KEY_T = `enum KEY_T = 20;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_T); }))) {
            mixin(enumMixinStr_KEY_T);
        }
    }




    static if(!is(typeof(KEY_Y))) {
        private enum enumMixinStr_KEY_Y = `enum KEY_Y = 21;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_Y); }))) {
            mixin(enumMixinStr_KEY_Y);
        }
    }




    static if(!is(typeof(KEY_U))) {
        private enum enumMixinStr_KEY_U = `enum KEY_U = 22;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_U); }))) {
            mixin(enumMixinStr_KEY_U);
        }
    }




    static if(!is(typeof(KEY_I))) {
        private enum enumMixinStr_KEY_I = `enum KEY_I = 23;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_I); }))) {
            mixin(enumMixinStr_KEY_I);
        }
    }




    static if(!is(typeof(KEY_O))) {
        private enum enumMixinStr_KEY_O = `enum KEY_O = 24;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_O); }))) {
            mixin(enumMixinStr_KEY_O);
        }
    }




    static if(!is(typeof(KEY_P))) {
        private enum enumMixinStr_KEY_P = `enum KEY_P = 25;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_P); }))) {
            mixin(enumMixinStr_KEY_P);
        }
    }




    static if(!is(typeof(KEY_LEFTBRACE))) {
        private enum enumMixinStr_KEY_LEFTBRACE = `enum KEY_LEFTBRACE = 26;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_LEFTBRACE); }))) {
            mixin(enumMixinStr_KEY_LEFTBRACE);
        }
    }




    static if(!is(typeof(KEY_RIGHTBRACE))) {
        private enum enumMixinStr_KEY_RIGHTBRACE = `enum KEY_RIGHTBRACE = 27;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_RIGHTBRACE); }))) {
            mixin(enumMixinStr_KEY_RIGHTBRACE);
        }
    }




    static if(!is(typeof(KEY_ENTER))) {
        private enum enumMixinStr_KEY_ENTER = `enum KEY_ENTER = 28;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_ENTER); }))) {
            mixin(enumMixinStr_KEY_ENTER);
        }
    }




    static if(!is(typeof(KEY_LEFTCTRL))) {
        private enum enumMixinStr_KEY_LEFTCTRL = `enum KEY_LEFTCTRL = 29;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_LEFTCTRL); }))) {
            mixin(enumMixinStr_KEY_LEFTCTRL);
        }
    }




    static if(!is(typeof(KEY_A))) {
        private enum enumMixinStr_KEY_A = `enum KEY_A = 30;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_A); }))) {
            mixin(enumMixinStr_KEY_A);
        }
    }




    static if(!is(typeof(KEY_S))) {
        private enum enumMixinStr_KEY_S = `enum KEY_S = 31;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_S); }))) {
            mixin(enumMixinStr_KEY_S);
        }
    }




    static if(!is(typeof(KEY_D))) {
        private enum enumMixinStr_KEY_D = `enum KEY_D = 32;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_D); }))) {
            mixin(enumMixinStr_KEY_D);
        }
    }




    static if(!is(typeof(KEY_F))) {
        private enum enumMixinStr_KEY_F = `enum KEY_F = 33;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_F); }))) {
            mixin(enumMixinStr_KEY_F);
        }
    }




    static if(!is(typeof(KEY_G))) {
        private enum enumMixinStr_KEY_G = `enum KEY_G = 34;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_G); }))) {
            mixin(enumMixinStr_KEY_G);
        }
    }




    static if(!is(typeof(KEY_H))) {
        private enum enumMixinStr_KEY_H = `enum KEY_H = 35;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_H); }))) {
            mixin(enumMixinStr_KEY_H);
        }
    }




    static if(!is(typeof(KEY_J))) {
        private enum enumMixinStr_KEY_J = `enum KEY_J = 36;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_J); }))) {
            mixin(enumMixinStr_KEY_J);
        }
    }




    static if(!is(typeof(KEY_K))) {
        private enum enumMixinStr_KEY_K = `enum KEY_K = 37;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_K); }))) {
            mixin(enumMixinStr_KEY_K);
        }
    }




    static if(!is(typeof(KEY_L))) {
        private enum enumMixinStr_KEY_L = `enum KEY_L = 38;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_L); }))) {
            mixin(enumMixinStr_KEY_L);
        }
    }




    static if(!is(typeof(KEY_SEMICOLON))) {
        private enum enumMixinStr_KEY_SEMICOLON = `enum KEY_SEMICOLON = 39;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_SEMICOLON); }))) {
            mixin(enumMixinStr_KEY_SEMICOLON);
        }
    }




    static if(!is(typeof(KEY_APOSTROPHE))) {
        private enum enumMixinStr_KEY_APOSTROPHE = `enum KEY_APOSTROPHE = 40;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_APOSTROPHE); }))) {
            mixin(enumMixinStr_KEY_APOSTROPHE);
        }
    }




    static if(!is(typeof(KEY_GRAVE))) {
        private enum enumMixinStr_KEY_GRAVE = `enum KEY_GRAVE = 41;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_GRAVE); }))) {
            mixin(enumMixinStr_KEY_GRAVE);
        }
    }




    static if(!is(typeof(KEY_LEFTSHIFT))) {
        private enum enumMixinStr_KEY_LEFTSHIFT = `enum KEY_LEFTSHIFT = 42;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_LEFTSHIFT); }))) {
            mixin(enumMixinStr_KEY_LEFTSHIFT);
        }
    }




    static if(!is(typeof(KEY_BACKSLASH))) {
        private enum enumMixinStr_KEY_BACKSLASH = `enum KEY_BACKSLASH = 43;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_BACKSLASH); }))) {
            mixin(enumMixinStr_KEY_BACKSLASH);
        }
    }




    static if(!is(typeof(KEY_Z))) {
        private enum enumMixinStr_KEY_Z = `enum KEY_Z = 44;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_Z); }))) {
            mixin(enumMixinStr_KEY_Z);
        }
    }




    static if(!is(typeof(KEY_X))) {
        private enum enumMixinStr_KEY_X = `enum KEY_X = 45;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_X); }))) {
            mixin(enumMixinStr_KEY_X);
        }
    }




    static if(!is(typeof(KEY_C))) {
        private enum enumMixinStr_KEY_C = `enum KEY_C = 46;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_C); }))) {
            mixin(enumMixinStr_KEY_C);
        }
    }




    static if(!is(typeof(KEY_V))) {
        private enum enumMixinStr_KEY_V = `enum KEY_V = 47;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_V); }))) {
            mixin(enumMixinStr_KEY_V);
        }
    }




    static if(!is(typeof(KEY_B))) {
        private enum enumMixinStr_KEY_B = `enum KEY_B = 48;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_B); }))) {
            mixin(enumMixinStr_KEY_B);
        }
    }




    static if(!is(typeof(KEY_N))) {
        private enum enumMixinStr_KEY_N = `enum KEY_N = 49;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_N); }))) {
            mixin(enumMixinStr_KEY_N);
        }
    }




    static if(!is(typeof(KEY_M))) {
        private enum enumMixinStr_KEY_M = `enum KEY_M = 50;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_M); }))) {
            mixin(enumMixinStr_KEY_M);
        }
    }




    static if(!is(typeof(KEY_COMMA))) {
        private enum enumMixinStr_KEY_COMMA = `enum KEY_COMMA = 51;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_COMMA); }))) {
            mixin(enumMixinStr_KEY_COMMA);
        }
    }




    static if(!is(typeof(KEY_DOT))) {
        private enum enumMixinStr_KEY_DOT = `enum KEY_DOT = 52;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_DOT); }))) {
            mixin(enumMixinStr_KEY_DOT);
        }
    }




    static if(!is(typeof(KEY_SLASH))) {
        private enum enumMixinStr_KEY_SLASH = `enum KEY_SLASH = 53;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_SLASH); }))) {
            mixin(enumMixinStr_KEY_SLASH);
        }
    }




    static if(!is(typeof(KEY_RIGHTSHIFT))) {
        private enum enumMixinStr_KEY_RIGHTSHIFT = `enum KEY_RIGHTSHIFT = 54;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_RIGHTSHIFT); }))) {
            mixin(enumMixinStr_KEY_RIGHTSHIFT);
        }
    }




    static if(!is(typeof(KEY_KPASTERISK))) {
        private enum enumMixinStr_KEY_KPASTERISK = `enum KEY_KPASTERISK = 55;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_KPASTERISK); }))) {
            mixin(enumMixinStr_KEY_KPASTERISK);
        }
    }




    static if(!is(typeof(KEY_LEFTALT))) {
        private enum enumMixinStr_KEY_LEFTALT = `enum KEY_LEFTALT = 56;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_LEFTALT); }))) {
            mixin(enumMixinStr_KEY_LEFTALT);
        }
    }




    static if(!is(typeof(KEY_SPACE))) {
        private enum enumMixinStr_KEY_SPACE = `enum KEY_SPACE = 57;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_SPACE); }))) {
            mixin(enumMixinStr_KEY_SPACE);
        }
    }




    static if(!is(typeof(KEY_CAPSLOCK))) {
        private enum enumMixinStr_KEY_CAPSLOCK = `enum KEY_CAPSLOCK = 58;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_CAPSLOCK); }))) {
            mixin(enumMixinStr_KEY_CAPSLOCK);
        }
    }




    static if(!is(typeof(KEY_F1))) {
        private enum enumMixinStr_KEY_F1 = `enum KEY_F1 = 59;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_F1); }))) {
            mixin(enumMixinStr_KEY_F1);
        }
    }




    static if(!is(typeof(KEY_F2))) {
        private enum enumMixinStr_KEY_F2 = `enum KEY_F2 = 60;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_F2); }))) {
            mixin(enumMixinStr_KEY_F2);
        }
    }




    static if(!is(typeof(KEY_F3))) {
        private enum enumMixinStr_KEY_F3 = `enum KEY_F3 = 61;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_F3); }))) {
            mixin(enumMixinStr_KEY_F3);
        }
    }




    static if(!is(typeof(KEY_F4))) {
        private enum enumMixinStr_KEY_F4 = `enum KEY_F4 = 62;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_F4); }))) {
            mixin(enumMixinStr_KEY_F4);
        }
    }




    static if(!is(typeof(KEY_F5))) {
        private enum enumMixinStr_KEY_F5 = `enum KEY_F5 = 63;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_F5); }))) {
            mixin(enumMixinStr_KEY_F5);
        }
    }




    static if(!is(typeof(KEY_F6))) {
        private enum enumMixinStr_KEY_F6 = `enum KEY_F6 = 64;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_F6); }))) {
            mixin(enumMixinStr_KEY_F6);
        }
    }




    static if(!is(typeof(KEY_F7))) {
        private enum enumMixinStr_KEY_F7 = `enum KEY_F7 = 65;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_F7); }))) {
            mixin(enumMixinStr_KEY_F7);
        }
    }




    static if(!is(typeof(KEY_F8))) {
        private enum enumMixinStr_KEY_F8 = `enum KEY_F8 = 66;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_F8); }))) {
            mixin(enumMixinStr_KEY_F8);
        }
    }




    static if(!is(typeof(KEY_F9))) {
        private enum enumMixinStr_KEY_F9 = `enum KEY_F9 = 67;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_F9); }))) {
            mixin(enumMixinStr_KEY_F9);
        }
    }




    static if(!is(typeof(KEY_F10))) {
        private enum enumMixinStr_KEY_F10 = `enum KEY_F10 = 68;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_F10); }))) {
            mixin(enumMixinStr_KEY_F10);
        }
    }




    static if(!is(typeof(KEY_NUMLOCK))) {
        private enum enumMixinStr_KEY_NUMLOCK = `enum KEY_NUMLOCK = 69;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_NUMLOCK); }))) {
            mixin(enumMixinStr_KEY_NUMLOCK);
        }
    }




    static if(!is(typeof(KEY_SCROLLLOCK))) {
        private enum enumMixinStr_KEY_SCROLLLOCK = `enum KEY_SCROLLLOCK = 70;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_SCROLLLOCK); }))) {
            mixin(enumMixinStr_KEY_SCROLLLOCK);
        }
    }




    static if(!is(typeof(KEY_KP7))) {
        private enum enumMixinStr_KEY_KP7 = `enum KEY_KP7 = 71;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_KP7); }))) {
            mixin(enumMixinStr_KEY_KP7);
        }
    }




    static if(!is(typeof(KEY_KP8))) {
        private enum enumMixinStr_KEY_KP8 = `enum KEY_KP8 = 72;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_KP8); }))) {
            mixin(enumMixinStr_KEY_KP8);
        }
    }




    static if(!is(typeof(KEY_KP9))) {
        private enum enumMixinStr_KEY_KP9 = `enum KEY_KP9 = 73;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_KP9); }))) {
            mixin(enumMixinStr_KEY_KP9);
        }
    }




    static if(!is(typeof(KEY_KPMINUS))) {
        private enum enumMixinStr_KEY_KPMINUS = `enum KEY_KPMINUS = 74;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_KPMINUS); }))) {
            mixin(enumMixinStr_KEY_KPMINUS);
        }
    }




    static if(!is(typeof(KEY_KP4))) {
        private enum enumMixinStr_KEY_KP4 = `enum KEY_KP4 = 75;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_KP4); }))) {
            mixin(enumMixinStr_KEY_KP4);
        }
    }




    static if(!is(typeof(KEY_KP5))) {
        private enum enumMixinStr_KEY_KP5 = `enum KEY_KP5 = 76;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_KP5); }))) {
            mixin(enumMixinStr_KEY_KP5);
        }
    }




    static if(!is(typeof(KEY_KP6))) {
        private enum enumMixinStr_KEY_KP6 = `enum KEY_KP6 = 77;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_KP6); }))) {
            mixin(enumMixinStr_KEY_KP6);
        }
    }




    static if(!is(typeof(KEY_KPPLUS))) {
        private enum enumMixinStr_KEY_KPPLUS = `enum KEY_KPPLUS = 78;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_KPPLUS); }))) {
            mixin(enumMixinStr_KEY_KPPLUS);
        }
    }




    static if(!is(typeof(KEY_KP1))) {
        private enum enumMixinStr_KEY_KP1 = `enum KEY_KP1 = 79;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_KP1); }))) {
            mixin(enumMixinStr_KEY_KP1);
        }
    }




    static if(!is(typeof(KEY_KP2))) {
        private enum enumMixinStr_KEY_KP2 = `enum KEY_KP2 = 80;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_KP2); }))) {
            mixin(enumMixinStr_KEY_KP2);
        }
    }




    static if(!is(typeof(KEY_KP3))) {
        private enum enumMixinStr_KEY_KP3 = `enum KEY_KP3 = 81;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_KP3); }))) {
            mixin(enumMixinStr_KEY_KP3);
        }
    }




    static if(!is(typeof(KEY_KP0))) {
        private enum enumMixinStr_KEY_KP0 = `enum KEY_KP0 = 82;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_KP0); }))) {
            mixin(enumMixinStr_KEY_KP0);
        }
    }




    static if(!is(typeof(KEY_KPDOT))) {
        private enum enumMixinStr_KEY_KPDOT = `enum KEY_KPDOT = 83;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_KPDOT); }))) {
            mixin(enumMixinStr_KEY_KPDOT);
        }
    }




    static if(!is(typeof(KEY_ZENKAKUHANKAKU))) {
        private enum enumMixinStr_KEY_ZENKAKUHANKAKU = `enum KEY_ZENKAKUHANKAKU = 85;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_ZENKAKUHANKAKU); }))) {
            mixin(enumMixinStr_KEY_ZENKAKUHANKAKU);
        }
    }




    static if(!is(typeof(KEY_102ND))) {
        private enum enumMixinStr_KEY_102ND = `enum KEY_102ND = 86;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_102ND); }))) {
            mixin(enumMixinStr_KEY_102ND);
        }
    }




    static if(!is(typeof(KEY_F11))) {
        private enum enumMixinStr_KEY_F11 = `enum KEY_F11 = 87;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_F11); }))) {
            mixin(enumMixinStr_KEY_F11);
        }
    }




    static if(!is(typeof(KEY_F12))) {
        private enum enumMixinStr_KEY_F12 = `enum KEY_F12 = 88;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_F12); }))) {
            mixin(enumMixinStr_KEY_F12);
        }
    }




    static if(!is(typeof(KEY_RO))) {
        private enum enumMixinStr_KEY_RO = `enum KEY_RO = 89;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_RO); }))) {
            mixin(enumMixinStr_KEY_RO);
        }
    }




    static if(!is(typeof(KEY_KATAKANA))) {
        private enum enumMixinStr_KEY_KATAKANA = `enum KEY_KATAKANA = 90;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_KATAKANA); }))) {
            mixin(enumMixinStr_KEY_KATAKANA);
        }
    }




    static if(!is(typeof(KEY_HIRAGANA))) {
        private enum enumMixinStr_KEY_HIRAGANA = `enum KEY_HIRAGANA = 91;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_HIRAGANA); }))) {
            mixin(enumMixinStr_KEY_HIRAGANA);
        }
    }




    static if(!is(typeof(KEY_HENKAN))) {
        private enum enumMixinStr_KEY_HENKAN = `enum KEY_HENKAN = 92;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_HENKAN); }))) {
            mixin(enumMixinStr_KEY_HENKAN);
        }
    }




    static if(!is(typeof(KEY_KATAKANAHIRAGANA))) {
        private enum enumMixinStr_KEY_KATAKANAHIRAGANA = `enum KEY_KATAKANAHIRAGANA = 93;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_KATAKANAHIRAGANA); }))) {
            mixin(enumMixinStr_KEY_KATAKANAHIRAGANA);
        }
    }




    static if(!is(typeof(KEY_MUHENKAN))) {
        private enum enumMixinStr_KEY_MUHENKAN = `enum KEY_MUHENKAN = 94;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_MUHENKAN); }))) {
            mixin(enumMixinStr_KEY_MUHENKAN);
        }
    }




    static if(!is(typeof(KEY_KPJPCOMMA))) {
        private enum enumMixinStr_KEY_KPJPCOMMA = `enum KEY_KPJPCOMMA = 95;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_KPJPCOMMA); }))) {
            mixin(enumMixinStr_KEY_KPJPCOMMA);
        }
    }




    static if(!is(typeof(KEY_KPENTER))) {
        private enum enumMixinStr_KEY_KPENTER = `enum KEY_KPENTER = 96;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_KPENTER); }))) {
            mixin(enumMixinStr_KEY_KPENTER);
        }
    }




    static if(!is(typeof(KEY_RIGHTCTRL))) {
        private enum enumMixinStr_KEY_RIGHTCTRL = `enum KEY_RIGHTCTRL = 97;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_RIGHTCTRL); }))) {
            mixin(enumMixinStr_KEY_RIGHTCTRL);
        }
    }




    static if(!is(typeof(KEY_KPSLASH))) {
        private enum enumMixinStr_KEY_KPSLASH = `enum KEY_KPSLASH = 98;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_KPSLASH); }))) {
            mixin(enumMixinStr_KEY_KPSLASH);
        }
    }




    static if(!is(typeof(KEY_SYSRQ))) {
        private enum enumMixinStr_KEY_SYSRQ = `enum KEY_SYSRQ = 99;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_SYSRQ); }))) {
            mixin(enumMixinStr_KEY_SYSRQ);
        }
    }




    static if(!is(typeof(KEY_RIGHTALT))) {
        private enum enumMixinStr_KEY_RIGHTALT = `enum KEY_RIGHTALT = 100;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_RIGHTALT); }))) {
            mixin(enumMixinStr_KEY_RIGHTALT);
        }
    }




    static if(!is(typeof(KEY_LINEFEED))) {
        private enum enumMixinStr_KEY_LINEFEED = `enum KEY_LINEFEED = 101;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_LINEFEED); }))) {
            mixin(enumMixinStr_KEY_LINEFEED);
        }
    }




    static if(!is(typeof(KEY_HOME))) {
        private enum enumMixinStr_KEY_HOME = `enum KEY_HOME = 102;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_HOME); }))) {
            mixin(enumMixinStr_KEY_HOME);
        }
    }




    static if(!is(typeof(KEY_UP))) {
        private enum enumMixinStr_KEY_UP = `enum KEY_UP = 103;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_UP); }))) {
            mixin(enumMixinStr_KEY_UP);
        }
    }




    static if(!is(typeof(KEY_PAGEUP))) {
        private enum enumMixinStr_KEY_PAGEUP = `enum KEY_PAGEUP = 104;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_PAGEUP); }))) {
            mixin(enumMixinStr_KEY_PAGEUP);
        }
    }




    static if(!is(typeof(KEY_LEFT))) {
        private enum enumMixinStr_KEY_LEFT = `enum KEY_LEFT = 105;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_LEFT); }))) {
            mixin(enumMixinStr_KEY_LEFT);
        }
    }




    static if(!is(typeof(KEY_RIGHT))) {
        private enum enumMixinStr_KEY_RIGHT = `enum KEY_RIGHT = 106;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_RIGHT); }))) {
            mixin(enumMixinStr_KEY_RIGHT);
        }
    }




    static if(!is(typeof(KEY_END))) {
        private enum enumMixinStr_KEY_END = `enum KEY_END = 107;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_END); }))) {
            mixin(enumMixinStr_KEY_END);
        }
    }




    static if(!is(typeof(KEY_DOWN))) {
        private enum enumMixinStr_KEY_DOWN = `enum KEY_DOWN = 108;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_DOWN); }))) {
            mixin(enumMixinStr_KEY_DOWN);
        }
    }




    static if(!is(typeof(KEY_PAGEDOWN))) {
        private enum enumMixinStr_KEY_PAGEDOWN = `enum KEY_PAGEDOWN = 109;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_PAGEDOWN); }))) {
            mixin(enumMixinStr_KEY_PAGEDOWN);
        }
    }




    static if(!is(typeof(KEY_INSERT))) {
        private enum enumMixinStr_KEY_INSERT = `enum KEY_INSERT = 110;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_INSERT); }))) {
            mixin(enumMixinStr_KEY_INSERT);
        }
    }




    static if(!is(typeof(KEY_DELETE))) {
        private enum enumMixinStr_KEY_DELETE = `enum KEY_DELETE = 111;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_DELETE); }))) {
            mixin(enumMixinStr_KEY_DELETE);
        }
    }




    static if(!is(typeof(KEY_MACRO))) {
        private enum enumMixinStr_KEY_MACRO = `enum KEY_MACRO = 112;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_MACRO); }))) {
            mixin(enumMixinStr_KEY_MACRO);
        }
    }




    static if(!is(typeof(KEY_MUTE))) {
        private enum enumMixinStr_KEY_MUTE = `enum KEY_MUTE = 113;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_MUTE); }))) {
            mixin(enumMixinStr_KEY_MUTE);
        }
    }




    static if(!is(typeof(KEY_VOLUMEDOWN))) {
        private enum enumMixinStr_KEY_VOLUMEDOWN = `enum KEY_VOLUMEDOWN = 114;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_VOLUMEDOWN); }))) {
            mixin(enumMixinStr_KEY_VOLUMEDOWN);
        }
    }




    static if(!is(typeof(KEY_VOLUMEUP))) {
        private enum enumMixinStr_KEY_VOLUMEUP = `enum KEY_VOLUMEUP = 115;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_VOLUMEUP); }))) {
            mixin(enumMixinStr_KEY_VOLUMEUP);
        }
    }




    static if(!is(typeof(KEY_POWER))) {
        private enum enumMixinStr_KEY_POWER = `enum KEY_POWER = 116;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_POWER); }))) {
            mixin(enumMixinStr_KEY_POWER);
        }
    }




    static if(!is(typeof(KEY_KPEQUAL))) {
        private enum enumMixinStr_KEY_KPEQUAL = `enum KEY_KPEQUAL = 117;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_KPEQUAL); }))) {
            mixin(enumMixinStr_KEY_KPEQUAL);
        }
    }




    static if(!is(typeof(KEY_KPPLUSMINUS))) {
        private enum enumMixinStr_KEY_KPPLUSMINUS = `enum KEY_KPPLUSMINUS = 118;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_KPPLUSMINUS); }))) {
            mixin(enumMixinStr_KEY_KPPLUSMINUS);
        }
    }




    static if(!is(typeof(KEY_PAUSE))) {
        private enum enumMixinStr_KEY_PAUSE = `enum KEY_PAUSE = 119;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_PAUSE); }))) {
            mixin(enumMixinStr_KEY_PAUSE);
        }
    }




    static if(!is(typeof(KEY_SCALE))) {
        private enum enumMixinStr_KEY_SCALE = `enum KEY_SCALE = 120;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_SCALE); }))) {
            mixin(enumMixinStr_KEY_SCALE);
        }
    }




    static if(!is(typeof(KEY_KPCOMMA))) {
        private enum enumMixinStr_KEY_KPCOMMA = `enum KEY_KPCOMMA = 121;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_KPCOMMA); }))) {
            mixin(enumMixinStr_KEY_KPCOMMA);
        }
    }




    static if(!is(typeof(KEY_HANGEUL))) {
        private enum enumMixinStr_KEY_HANGEUL = `enum KEY_HANGEUL = 122;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_HANGEUL); }))) {
            mixin(enumMixinStr_KEY_HANGEUL);
        }
    }




    static if(!is(typeof(KEY_HANGUEL))) {
        private enum enumMixinStr_KEY_HANGUEL = `enum KEY_HANGUEL = 122;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_HANGUEL); }))) {
            mixin(enumMixinStr_KEY_HANGUEL);
        }
    }




    static if(!is(typeof(KEY_HANJA))) {
        private enum enumMixinStr_KEY_HANJA = `enum KEY_HANJA = 123;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_HANJA); }))) {
            mixin(enumMixinStr_KEY_HANJA);
        }
    }




    static if(!is(typeof(KEY_YEN))) {
        private enum enumMixinStr_KEY_YEN = `enum KEY_YEN = 124;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_YEN); }))) {
            mixin(enumMixinStr_KEY_YEN);
        }
    }




    static if(!is(typeof(KEY_LEFTMETA))) {
        private enum enumMixinStr_KEY_LEFTMETA = `enum KEY_LEFTMETA = 125;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_LEFTMETA); }))) {
            mixin(enumMixinStr_KEY_LEFTMETA);
        }
    }




    static if(!is(typeof(KEY_RIGHTMETA))) {
        private enum enumMixinStr_KEY_RIGHTMETA = `enum KEY_RIGHTMETA = 126;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_RIGHTMETA); }))) {
            mixin(enumMixinStr_KEY_RIGHTMETA);
        }
    }




    static if(!is(typeof(KEY_COMPOSE))) {
        private enum enumMixinStr_KEY_COMPOSE = `enum KEY_COMPOSE = 127;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_COMPOSE); }))) {
            mixin(enumMixinStr_KEY_COMPOSE);
        }
    }




    static if(!is(typeof(KEY_STOP))) {
        private enum enumMixinStr_KEY_STOP = `enum KEY_STOP = 128;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_STOP); }))) {
            mixin(enumMixinStr_KEY_STOP);
        }
    }




    static if(!is(typeof(KEY_AGAIN))) {
        private enum enumMixinStr_KEY_AGAIN = `enum KEY_AGAIN = 129;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_AGAIN); }))) {
            mixin(enumMixinStr_KEY_AGAIN);
        }
    }




    static if(!is(typeof(KEY_PROPS))) {
        private enum enumMixinStr_KEY_PROPS = `enum KEY_PROPS = 130;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_PROPS); }))) {
            mixin(enumMixinStr_KEY_PROPS);
        }
    }




    static if(!is(typeof(KEY_UNDO))) {
        private enum enumMixinStr_KEY_UNDO = `enum KEY_UNDO = 131;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_UNDO); }))) {
            mixin(enumMixinStr_KEY_UNDO);
        }
    }




    static if(!is(typeof(KEY_FRONT))) {
        private enum enumMixinStr_KEY_FRONT = `enum KEY_FRONT = 132;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_FRONT); }))) {
            mixin(enumMixinStr_KEY_FRONT);
        }
    }




    static if(!is(typeof(KEY_COPY))) {
        private enum enumMixinStr_KEY_COPY = `enum KEY_COPY = 133;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_COPY); }))) {
            mixin(enumMixinStr_KEY_COPY);
        }
    }




    static if(!is(typeof(KEY_OPEN))) {
        private enum enumMixinStr_KEY_OPEN = `enum KEY_OPEN = 134;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_OPEN); }))) {
            mixin(enumMixinStr_KEY_OPEN);
        }
    }




    static if(!is(typeof(KEY_PASTE))) {
        private enum enumMixinStr_KEY_PASTE = `enum KEY_PASTE = 135;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_PASTE); }))) {
            mixin(enumMixinStr_KEY_PASTE);
        }
    }




    static if(!is(typeof(KEY_FIND))) {
        private enum enumMixinStr_KEY_FIND = `enum KEY_FIND = 136;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_FIND); }))) {
            mixin(enumMixinStr_KEY_FIND);
        }
    }




    static if(!is(typeof(KEY_CUT))) {
        private enum enumMixinStr_KEY_CUT = `enum KEY_CUT = 137;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_CUT); }))) {
            mixin(enumMixinStr_KEY_CUT);
        }
    }




    static if(!is(typeof(KEY_HELP))) {
        private enum enumMixinStr_KEY_HELP = `enum KEY_HELP = 138;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_HELP); }))) {
            mixin(enumMixinStr_KEY_HELP);
        }
    }




    static if(!is(typeof(KEY_MENU))) {
        private enum enumMixinStr_KEY_MENU = `enum KEY_MENU = 139;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_MENU); }))) {
            mixin(enumMixinStr_KEY_MENU);
        }
    }




    static if(!is(typeof(KEY_CALC))) {
        private enum enumMixinStr_KEY_CALC = `enum KEY_CALC = 140;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_CALC); }))) {
            mixin(enumMixinStr_KEY_CALC);
        }
    }




    static if(!is(typeof(KEY_SETUP))) {
        private enum enumMixinStr_KEY_SETUP = `enum KEY_SETUP = 141;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_SETUP); }))) {
            mixin(enumMixinStr_KEY_SETUP);
        }
    }




    static if(!is(typeof(KEY_SLEEP))) {
        private enum enumMixinStr_KEY_SLEEP = `enum KEY_SLEEP = 142;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_SLEEP); }))) {
            mixin(enumMixinStr_KEY_SLEEP);
        }
    }




    static if(!is(typeof(KEY_WAKEUP))) {
        private enum enumMixinStr_KEY_WAKEUP = `enum KEY_WAKEUP = 143;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_WAKEUP); }))) {
            mixin(enumMixinStr_KEY_WAKEUP);
        }
    }




    static if(!is(typeof(KEY_FILE))) {
        private enum enumMixinStr_KEY_FILE = `enum KEY_FILE = 144;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_FILE); }))) {
            mixin(enumMixinStr_KEY_FILE);
        }
    }




    static if(!is(typeof(KEY_SENDFILE))) {
        private enum enumMixinStr_KEY_SENDFILE = `enum KEY_SENDFILE = 145;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_SENDFILE); }))) {
            mixin(enumMixinStr_KEY_SENDFILE);
        }
    }




    static if(!is(typeof(KEY_DELETEFILE))) {
        private enum enumMixinStr_KEY_DELETEFILE = `enum KEY_DELETEFILE = 146;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_DELETEFILE); }))) {
            mixin(enumMixinStr_KEY_DELETEFILE);
        }
    }




    static if(!is(typeof(KEY_XFER))) {
        private enum enumMixinStr_KEY_XFER = `enum KEY_XFER = 147;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_XFER); }))) {
            mixin(enumMixinStr_KEY_XFER);
        }
    }




    static if(!is(typeof(KEY_PROG1))) {
        private enum enumMixinStr_KEY_PROG1 = `enum KEY_PROG1 = 148;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_PROG1); }))) {
            mixin(enumMixinStr_KEY_PROG1);
        }
    }




    static if(!is(typeof(KEY_PROG2))) {
        private enum enumMixinStr_KEY_PROG2 = `enum KEY_PROG2 = 149;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_PROG2); }))) {
            mixin(enumMixinStr_KEY_PROG2);
        }
    }




    static if(!is(typeof(KEY_WWW))) {
        private enum enumMixinStr_KEY_WWW = `enum KEY_WWW = 150;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_WWW); }))) {
            mixin(enumMixinStr_KEY_WWW);
        }
    }




    static if(!is(typeof(KEY_MSDOS))) {
        private enum enumMixinStr_KEY_MSDOS = `enum KEY_MSDOS = 151;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_MSDOS); }))) {
            mixin(enumMixinStr_KEY_MSDOS);
        }
    }




    static if(!is(typeof(KEY_COFFEE))) {
        private enum enumMixinStr_KEY_COFFEE = `enum KEY_COFFEE = 152;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_COFFEE); }))) {
            mixin(enumMixinStr_KEY_COFFEE);
        }
    }




    static if(!is(typeof(KEY_SCREENLOCK))) {
        private enum enumMixinStr_KEY_SCREENLOCK = `enum KEY_SCREENLOCK = 152;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_SCREENLOCK); }))) {
            mixin(enumMixinStr_KEY_SCREENLOCK);
        }
    }




    static if(!is(typeof(KEY_ROTATE_DISPLAY))) {
        private enum enumMixinStr_KEY_ROTATE_DISPLAY = `enum KEY_ROTATE_DISPLAY = 153;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_ROTATE_DISPLAY); }))) {
            mixin(enumMixinStr_KEY_ROTATE_DISPLAY);
        }
    }




    static if(!is(typeof(KEY_DIRECTION))) {
        private enum enumMixinStr_KEY_DIRECTION = `enum KEY_DIRECTION = 153;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_DIRECTION); }))) {
            mixin(enumMixinStr_KEY_DIRECTION);
        }
    }




    static if(!is(typeof(KEY_CYCLEWINDOWS))) {
        private enum enumMixinStr_KEY_CYCLEWINDOWS = `enum KEY_CYCLEWINDOWS = 154;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_CYCLEWINDOWS); }))) {
            mixin(enumMixinStr_KEY_CYCLEWINDOWS);
        }
    }




    static if(!is(typeof(KEY_MAIL))) {
        private enum enumMixinStr_KEY_MAIL = `enum KEY_MAIL = 155;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_MAIL); }))) {
            mixin(enumMixinStr_KEY_MAIL);
        }
    }




    static if(!is(typeof(KEY_BOOKMARKS))) {
        private enum enumMixinStr_KEY_BOOKMARKS = `enum KEY_BOOKMARKS = 156;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_BOOKMARKS); }))) {
            mixin(enumMixinStr_KEY_BOOKMARKS);
        }
    }




    static if(!is(typeof(KEY_COMPUTER))) {
        private enum enumMixinStr_KEY_COMPUTER = `enum KEY_COMPUTER = 157;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_COMPUTER); }))) {
            mixin(enumMixinStr_KEY_COMPUTER);
        }
    }




    static if(!is(typeof(KEY_BACK))) {
        private enum enumMixinStr_KEY_BACK = `enum KEY_BACK = 158;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_BACK); }))) {
            mixin(enumMixinStr_KEY_BACK);
        }
    }




    static if(!is(typeof(KEY_FORWARD))) {
        private enum enumMixinStr_KEY_FORWARD = `enum KEY_FORWARD = 159;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_FORWARD); }))) {
            mixin(enumMixinStr_KEY_FORWARD);
        }
    }




    static if(!is(typeof(KEY_CLOSECD))) {
        private enum enumMixinStr_KEY_CLOSECD = `enum KEY_CLOSECD = 160;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_CLOSECD); }))) {
            mixin(enumMixinStr_KEY_CLOSECD);
        }
    }




    static if(!is(typeof(KEY_EJECTCD))) {
        private enum enumMixinStr_KEY_EJECTCD = `enum KEY_EJECTCD = 161;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_EJECTCD); }))) {
            mixin(enumMixinStr_KEY_EJECTCD);
        }
    }




    static if(!is(typeof(KEY_EJECTCLOSECD))) {
        private enum enumMixinStr_KEY_EJECTCLOSECD = `enum KEY_EJECTCLOSECD = 162;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_EJECTCLOSECD); }))) {
            mixin(enumMixinStr_KEY_EJECTCLOSECD);
        }
    }




    static if(!is(typeof(KEY_NEXTSONG))) {
        private enum enumMixinStr_KEY_NEXTSONG = `enum KEY_NEXTSONG = 163;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_NEXTSONG); }))) {
            mixin(enumMixinStr_KEY_NEXTSONG);
        }
    }




    static if(!is(typeof(KEY_PLAYPAUSE))) {
        private enum enumMixinStr_KEY_PLAYPAUSE = `enum KEY_PLAYPAUSE = 164;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_PLAYPAUSE); }))) {
            mixin(enumMixinStr_KEY_PLAYPAUSE);
        }
    }




    static if(!is(typeof(KEY_PREVIOUSSONG))) {
        private enum enumMixinStr_KEY_PREVIOUSSONG = `enum KEY_PREVIOUSSONG = 165;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_PREVIOUSSONG); }))) {
            mixin(enumMixinStr_KEY_PREVIOUSSONG);
        }
    }




    static if(!is(typeof(KEY_STOPCD))) {
        private enum enumMixinStr_KEY_STOPCD = `enum KEY_STOPCD = 166;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_STOPCD); }))) {
            mixin(enumMixinStr_KEY_STOPCD);
        }
    }




    static if(!is(typeof(KEY_RECORD))) {
        private enum enumMixinStr_KEY_RECORD = `enum KEY_RECORD = 167;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_RECORD); }))) {
            mixin(enumMixinStr_KEY_RECORD);
        }
    }




    static if(!is(typeof(KEY_REWIND))) {
        private enum enumMixinStr_KEY_REWIND = `enum KEY_REWIND = 168;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_REWIND); }))) {
            mixin(enumMixinStr_KEY_REWIND);
        }
    }




    static if(!is(typeof(KEY_PHONE))) {
        private enum enumMixinStr_KEY_PHONE = `enum KEY_PHONE = 169;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_PHONE); }))) {
            mixin(enumMixinStr_KEY_PHONE);
        }
    }




    static if(!is(typeof(KEY_ISO))) {
        private enum enumMixinStr_KEY_ISO = `enum KEY_ISO = 170;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_ISO); }))) {
            mixin(enumMixinStr_KEY_ISO);
        }
    }




    static if(!is(typeof(KEY_CONFIG))) {
        private enum enumMixinStr_KEY_CONFIG = `enum KEY_CONFIG = 171;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_CONFIG); }))) {
            mixin(enumMixinStr_KEY_CONFIG);
        }
    }




    static if(!is(typeof(KEY_HOMEPAGE))) {
        private enum enumMixinStr_KEY_HOMEPAGE = `enum KEY_HOMEPAGE = 172;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_HOMEPAGE); }))) {
            mixin(enumMixinStr_KEY_HOMEPAGE);
        }
    }




    static if(!is(typeof(KEY_REFRESH))) {
        private enum enumMixinStr_KEY_REFRESH = `enum KEY_REFRESH = 173;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_REFRESH); }))) {
            mixin(enumMixinStr_KEY_REFRESH);
        }
    }




    static if(!is(typeof(KEY_EXIT))) {
        private enum enumMixinStr_KEY_EXIT = `enum KEY_EXIT = 174;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_EXIT); }))) {
            mixin(enumMixinStr_KEY_EXIT);
        }
    }




    static if(!is(typeof(KEY_MOVE))) {
        private enum enumMixinStr_KEY_MOVE = `enum KEY_MOVE = 175;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_MOVE); }))) {
            mixin(enumMixinStr_KEY_MOVE);
        }
    }




    static if(!is(typeof(KEY_EDIT))) {
        private enum enumMixinStr_KEY_EDIT = `enum KEY_EDIT = 176;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_EDIT); }))) {
            mixin(enumMixinStr_KEY_EDIT);
        }
    }




    static if(!is(typeof(KEY_SCROLLUP))) {
        private enum enumMixinStr_KEY_SCROLLUP = `enum KEY_SCROLLUP = 177;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_SCROLLUP); }))) {
            mixin(enumMixinStr_KEY_SCROLLUP);
        }
    }




    static if(!is(typeof(KEY_SCROLLDOWN))) {
        private enum enumMixinStr_KEY_SCROLLDOWN = `enum KEY_SCROLLDOWN = 178;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_SCROLLDOWN); }))) {
            mixin(enumMixinStr_KEY_SCROLLDOWN);
        }
    }




    static if(!is(typeof(KEY_KPLEFTPAREN))) {
        private enum enumMixinStr_KEY_KPLEFTPAREN = `enum KEY_KPLEFTPAREN = 179;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_KPLEFTPAREN); }))) {
            mixin(enumMixinStr_KEY_KPLEFTPAREN);
        }
    }




    static if(!is(typeof(KEY_KPRIGHTPAREN))) {
        private enum enumMixinStr_KEY_KPRIGHTPAREN = `enum KEY_KPRIGHTPAREN = 180;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_KPRIGHTPAREN); }))) {
            mixin(enumMixinStr_KEY_KPRIGHTPAREN);
        }
    }




    static if(!is(typeof(KEY_NEW))) {
        private enum enumMixinStr_KEY_NEW = `enum KEY_NEW = 181;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_NEW); }))) {
            mixin(enumMixinStr_KEY_NEW);
        }
    }




    static if(!is(typeof(KEY_REDO))) {
        private enum enumMixinStr_KEY_REDO = `enum KEY_REDO = 182;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_REDO); }))) {
            mixin(enumMixinStr_KEY_REDO);
        }
    }




    static if(!is(typeof(KEY_F13))) {
        private enum enumMixinStr_KEY_F13 = `enum KEY_F13 = 183;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_F13); }))) {
            mixin(enumMixinStr_KEY_F13);
        }
    }




    static if(!is(typeof(KEY_F14))) {
        private enum enumMixinStr_KEY_F14 = `enum KEY_F14 = 184;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_F14); }))) {
            mixin(enumMixinStr_KEY_F14);
        }
    }




    static if(!is(typeof(KEY_F15))) {
        private enum enumMixinStr_KEY_F15 = `enum KEY_F15 = 185;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_F15); }))) {
            mixin(enumMixinStr_KEY_F15);
        }
    }




    static if(!is(typeof(KEY_F16))) {
        private enum enumMixinStr_KEY_F16 = `enum KEY_F16 = 186;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_F16); }))) {
            mixin(enumMixinStr_KEY_F16);
        }
    }




    static if(!is(typeof(KEY_F17))) {
        private enum enumMixinStr_KEY_F17 = `enum KEY_F17 = 187;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_F17); }))) {
            mixin(enumMixinStr_KEY_F17);
        }
    }




    static if(!is(typeof(KEY_F18))) {
        private enum enumMixinStr_KEY_F18 = `enum KEY_F18 = 188;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_F18); }))) {
            mixin(enumMixinStr_KEY_F18);
        }
    }




    static if(!is(typeof(KEY_F19))) {
        private enum enumMixinStr_KEY_F19 = `enum KEY_F19 = 189;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_F19); }))) {
            mixin(enumMixinStr_KEY_F19);
        }
    }




    static if(!is(typeof(KEY_F20))) {
        private enum enumMixinStr_KEY_F20 = `enum KEY_F20 = 190;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_F20); }))) {
            mixin(enumMixinStr_KEY_F20);
        }
    }




    static if(!is(typeof(KEY_F21))) {
        private enum enumMixinStr_KEY_F21 = `enum KEY_F21 = 191;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_F21); }))) {
            mixin(enumMixinStr_KEY_F21);
        }
    }




    static if(!is(typeof(KEY_F22))) {
        private enum enumMixinStr_KEY_F22 = `enum KEY_F22 = 192;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_F22); }))) {
            mixin(enumMixinStr_KEY_F22);
        }
    }




    static if(!is(typeof(KEY_F23))) {
        private enum enumMixinStr_KEY_F23 = `enum KEY_F23 = 193;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_F23); }))) {
            mixin(enumMixinStr_KEY_F23);
        }
    }




    static if(!is(typeof(KEY_F24))) {
        private enum enumMixinStr_KEY_F24 = `enum KEY_F24 = 194;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_F24); }))) {
            mixin(enumMixinStr_KEY_F24);
        }
    }




    static if(!is(typeof(KEY_PLAYCD))) {
        private enum enumMixinStr_KEY_PLAYCD = `enum KEY_PLAYCD = 200;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_PLAYCD); }))) {
            mixin(enumMixinStr_KEY_PLAYCD);
        }
    }




    static if(!is(typeof(KEY_PAUSECD))) {
        private enum enumMixinStr_KEY_PAUSECD = `enum KEY_PAUSECD = 201;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_PAUSECD); }))) {
            mixin(enumMixinStr_KEY_PAUSECD);
        }
    }




    static if(!is(typeof(KEY_PROG3))) {
        private enum enumMixinStr_KEY_PROG3 = `enum KEY_PROG3 = 202;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_PROG3); }))) {
            mixin(enumMixinStr_KEY_PROG3);
        }
    }




    static if(!is(typeof(KEY_PROG4))) {
        private enum enumMixinStr_KEY_PROG4 = `enum KEY_PROG4 = 203;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_PROG4); }))) {
            mixin(enumMixinStr_KEY_PROG4);
        }
    }




    static if(!is(typeof(KEY_DASHBOARD))) {
        private enum enumMixinStr_KEY_DASHBOARD = `enum KEY_DASHBOARD = 204;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_DASHBOARD); }))) {
            mixin(enumMixinStr_KEY_DASHBOARD);
        }
    }




    static if(!is(typeof(KEY_SUSPEND))) {
        private enum enumMixinStr_KEY_SUSPEND = `enum KEY_SUSPEND = 205;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_SUSPEND); }))) {
            mixin(enumMixinStr_KEY_SUSPEND);
        }
    }




    static if(!is(typeof(KEY_CLOSE))) {
        private enum enumMixinStr_KEY_CLOSE = `enum KEY_CLOSE = 206;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_CLOSE); }))) {
            mixin(enumMixinStr_KEY_CLOSE);
        }
    }




    static if(!is(typeof(KEY_PLAY))) {
        private enum enumMixinStr_KEY_PLAY = `enum KEY_PLAY = 207;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_PLAY); }))) {
            mixin(enumMixinStr_KEY_PLAY);
        }
    }




    static if(!is(typeof(KEY_FASTFORWARD))) {
        private enum enumMixinStr_KEY_FASTFORWARD = `enum KEY_FASTFORWARD = 208;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_FASTFORWARD); }))) {
            mixin(enumMixinStr_KEY_FASTFORWARD);
        }
    }




    static if(!is(typeof(KEY_BASSBOOST))) {
        private enum enumMixinStr_KEY_BASSBOOST = `enum KEY_BASSBOOST = 209;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_BASSBOOST); }))) {
            mixin(enumMixinStr_KEY_BASSBOOST);
        }
    }




    static if(!is(typeof(KEY_PRINT))) {
        private enum enumMixinStr_KEY_PRINT = `enum KEY_PRINT = 210;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_PRINT); }))) {
            mixin(enumMixinStr_KEY_PRINT);
        }
    }




    static if(!is(typeof(KEY_HP))) {
        private enum enumMixinStr_KEY_HP = `enum KEY_HP = 211;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_HP); }))) {
            mixin(enumMixinStr_KEY_HP);
        }
    }




    static if(!is(typeof(KEY_CAMERA))) {
        private enum enumMixinStr_KEY_CAMERA = `enum KEY_CAMERA = 212;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_CAMERA); }))) {
            mixin(enumMixinStr_KEY_CAMERA);
        }
    }




    static if(!is(typeof(KEY_SOUND))) {
        private enum enumMixinStr_KEY_SOUND = `enum KEY_SOUND = 213;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_SOUND); }))) {
            mixin(enumMixinStr_KEY_SOUND);
        }
    }




    static if(!is(typeof(KEY_QUESTION))) {
        private enum enumMixinStr_KEY_QUESTION = `enum KEY_QUESTION = 214;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_QUESTION); }))) {
            mixin(enumMixinStr_KEY_QUESTION);
        }
    }




    static if(!is(typeof(KEY_EMAIL))) {
        private enum enumMixinStr_KEY_EMAIL = `enum KEY_EMAIL = 215;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_EMAIL); }))) {
            mixin(enumMixinStr_KEY_EMAIL);
        }
    }




    static if(!is(typeof(KEY_CHAT))) {
        private enum enumMixinStr_KEY_CHAT = `enum KEY_CHAT = 216;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_CHAT); }))) {
            mixin(enumMixinStr_KEY_CHAT);
        }
    }




    static if(!is(typeof(KEY_SEARCH))) {
        private enum enumMixinStr_KEY_SEARCH = `enum KEY_SEARCH = 217;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_SEARCH); }))) {
            mixin(enumMixinStr_KEY_SEARCH);
        }
    }




    static if(!is(typeof(KEY_CONNECT))) {
        private enum enumMixinStr_KEY_CONNECT = `enum KEY_CONNECT = 218;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_CONNECT); }))) {
            mixin(enumMixinStr_KEY_CONNECT);
        }
    }




    static if(!is(typeof(KEY_FINANCE))) {
        private enum enumMixinStr_KEY_FINANCE = `enum KEY_FINANCE = 219;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_FINANCE); }))) {
            mixin(enumMixinStr_KEY_FINANCE);
        }
    }




    static if(!is(typeof(KEY_SPORT))) {
        private enum enumMixinStr_KEY_SPORT = `enum KEY_SPORT = 220;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_SPORT); }))) {
            mixin(enumMixinStr_KEY_SPORT);
        }
    }




    static if(!is(typeof(KEY_SHOP))) {
        private enum enumMixinStr_KEY_SHOP = `enum KEY_SHOP = 221;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_SHOP); }))) {
            mixin(enumMixinStr_KEY_SHOP);
        }
    }




    static if(!is(typeof(KEY_ALTERASE))) {
        private enum enumMixinStr_KEY_ALTERASE = `enum KEY_ALTERASE = 222;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_ALTERASE); }))) {
            mixin(enumMixinStr_KEY_ALTERASE);
        }
    }




    static if(!is(typeof(KEY_CANCEL))) {
        private enum enumMixinStr_KEY_CANCEL = `enum KEY_CANCEL = 223;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_CANCEL); }))) {
            mixin(enumMixinStr_KEY_CANCEL);
        }
    }




    static if(!is(typeof(KEY_BRIGHTNESSDOWN))) {
        private enum enumMixinStr_KEY_BRIGHTNESSDOWN = `enum KEY_BRIGHTNESSDOWN = 224;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_BRIGHTNESSDOWN); }))) {
            mixin(enumMixinStr_KEY_BRIGHTNESSDOWN);
        }
    }




    static if(!is(typeof(KEY_BRIGHTNESSUP))) {
        private enum enumMixinStr_KEY_BRIGHTNESSUP = `enum KEY_BRIGHTNESSUP = 225;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_BRIGHTNESSUP); }))) {
            mixin(enumMixinStr_KEY_BRIGHTNESSUP);
        }
    }




    static if(!is(typeof(KEY_MEDIA))) {
        private enum enumMixinStr_KEY_MEDIA = `enum KEY_MEDIA = 226;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_MEDIA); }))) {
            mixin(enumMixinStr_KEY_MEDIA);
        }
    }




    static if(!is(typeof(KEY_SWITCHVIDEOMODE))) {
        private enum enumMixinStr_KEY_SWITCHVIDEOMODE = `enum KEY_SWITCHVIDEOMODE = 227;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_SWITCHVIDEOMODE); }))) {
            mixin(enumMixinStr_KEY_SWITCHVIDEOMODE);
        }
    }




    static if(!is(typeof(KEY_KBDILLUMTOGGLE))) {
        private enum enumMixinStr_KEY_KBDILLUMTOGGLE = `enum KEY_KBDILLUMTOGGLE = 228;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_KBDILLUMTOGGLE); }))) {
            mixin(enumMixinStr_KEY_KBDILLUMTOGGLE);
        }
    }




    static if(!is(typeof(KEY_KBDILLUMDOWN))) {
        private enum enumMixinStr_KEY_KBDILLUMDOWN = `enum KEY_KBDILLUMDOWN = 229;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_KBDILLUMDOWN); }))) {
            mixin(enumMixinStr_KEY_KBDILLUMDOWN);
        }
    }




    static if(!is(typeof(KEY_KBDILLUMUP))) {
        private enum enumMixinStr_KEY_KBDILLUMUP = `enum KEY_KBDILLUMUP = 230;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_KBDILLUMUP); }))) {
            mixin(enumMixinStr_KEY_KBDILLUMUP);
        }
    }




    static if(!is(typeof(KEY_SEND))) {
        private enum enumMixinStr_KEY_SEND = `enum KEY_SEND = 231;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_SEND); }))) {
            mixin(enumMixinStr_KEY_SEND);
        }
    }




    static if(!is(typeof(KEY_REPLY))) {
        private enum enumMixinStr_KEY_REPLY = `enum KEY_REPLY = 232;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_REPLY); }))) {
            mixin(enumMixinStr_KEY_REPLY);
        }
    }




    static if(!is(typeof(KEY_FORWARDMAIL))) {
        private enum enumMixinStr_KEY_FORWARDMAIL = `enum KEY_FORWARDMAIL = 233;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_FORWARDMAIL); }))) {
            mixin(enumMixinStr_KEY_FORWARDMAIL);
        }
    }




    static if(!is(typeof(KEY_SAVE))) {
        private enum enumMixinStr_KEY_SAVE = `enum KEY_SAVE = 234;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_SAVE); }))) {
            mixin(enumMixinStr_KEY_SAVE);
        }
    }




    static if(!is(typeof(KEY_DOCUMENTS))) {
        private enum enumMixinStr_KEY_DOCUMENTS = `enum KEY_DOCUMENTS = 235;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_DOCUMENTS); }))) {
            mixin(enumMixinStr_KEY_DOCUMENTS);
        }
    }




    static if(!is(typeof(KEY_BATTERY))) {
        private enum enumMixinStr_KEY_BATTERY = `enum KEY_BATTERY = 236;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_BATTERY); }))) {
            mixin(enumMixinStr_KEY_BATTERY);
        }
    }




    static if(!is(typeof(KEY_BLUETOOTH))) {
        private enum enumMixinStr_KEY_BLUETOOTH = `enum KEY_BLUETOOTH = 237;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_BLUETOOTH); }))) {
            mixin(enumMixinStr_KEY_BLUETOOTH);
        }
    }




    static if(!is(typeof(KEY_WLAN))) {
        private enum enumMixinStr_KEY_WLAN = `enum KEY_WLAN = 238;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_WLAN); }))) {
            mixin(enumMixinStr_KEY_WLAN);
        }
    }




    static if(!is(typeof(KEY_UWB))) {
        private enum enumMixinStr_KEY_UWB = `enum KEY_UWB = 239;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_UWB); }))) {
            mixin(enumMixinStr_KEY_UWB);
        }
    }




    static if(!is(typeof(KEY_UNKNOWN))) {
        private enum enumMixinStr_KEY_UNKNOWN = `enum KEY_UNKNOWN = 240;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_UNKNOWN); }))) {
            mixin(enumMixinStr_KEY_UNKNOWN);
        }
    }




    static if(!is(typeof(KEY_VIDEO_NEXT))) {
        private enum enumMixinStr_KEY_VIDEO_NEXT = `enum KEY_VIDEO_NEXT = 241;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_VIDEO_NEXT); }))) {
            mixin(enumMixinStr_KEY_VIDEO_NEXT);
        }
    }




    static if(!is(typeof(KEY_VIDEO_PREV))) {
        private enum enumMixinStr_KEY_VIDEO_PREV = `enum KEY_VIDEO_PREV = 242;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_VIDEO_PREV); }))) {
            mixin(enumMixinStr_KEY_VIDEO_PREV);
        }
    }




    static if(!is(typeof(KEY_BRIGHTNESS_CYCLE))) {
        private enum enumMixinStr_KEY_BRIGHTNESS_CYCLE = `enum KEY_BRIGHTNESS_CYCLE = 243;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_BRIGHTNESS_CYCLE); }))) {
            mixin(enumMixinStr_KEY_BRIGHTNESS_CYCLE);
        }
    }




    static if(!is(typeof(KEY_BRIGHTNESS_AUTO))) {
        private enum enumMixinStr_KEY_BRIGHTNESS_AUTO = `enum KEY_BRIGHTNESS_AUTO = 244;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_BRIGHTNESS_AUTO); }))) {
            mixin(enumMixinStr_KEY_BRIGHTNESS_AUTO);
        }
    }




    static if(!is(typeof(KEY_BRIGHTNESS_ZERO))) {
        private enum enumMixinStr_KEY_BRIGHTNESS_ZERO = `enum KEY_BRIGHTNESS_ZERO = 244;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_BRIGHTNESS_ZERO); }))) {
            mixin(enumMixinStr_KEY_BRIGHTNESS_ZERO);
        }
    }




    static if(!is(typeof(KEY_DISPLAY_OFF))) {
        private enum enumMixinStr_KEY_DISPLAY_OFF = `enum KEY_DISPLAY_OFF = 245;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_DISPLAY_OFF); }))) {
            mixin(enumMixinStr_KEY_DISPLAY_OFF);
        }
    }




    static if(!is(typeof(KEY_WWAN))) {
        private enum enumMixinStr_KEY_WWAN = `enum KEY_WWAN = 246;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_WWAN); }))) {
            mixin(enumMixinStr_KEY_WWAN);
        }
    }




    static if(!is(typeof(KEY_WIMAX))) {
        private enum enumMixinStr_KEY_WIMAX = `enum KEY_WIMAX = 246;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_WIMAX); }))) {
            mixin(enumMixinStr_KEY_WIMAX);
        }
    }




    static if(!is(typeof(KEY_RFKILL))) {
        private enum enumMixinStr_KEY_RFKILL = `enum KEY_RFKILL = 247;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_RFKILL); }))) {
            mixin(enumMixinStr_KEY_RFKILL);
        }
    }




    static if(!is(typeof(KEY_MICMUTE))) {
        private enum enumMixinStr_KEY_MICMUTE = `enum KEY_MICMUTE = 248;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_MICMUTE); }))) {
            mixin(enumMixinStr_KEY_MICMUTE);
        }
    }




    static if(!is(typeof(BTN_MISC))) {
        private enum enumMixinStr_BTN_MISC = `enum BTN_MISC = 0x100;`;
        static if(is(typeof({ mixin(enumMixinStr_BTN_MISC); }))) {
            mixin(enumMixinStr_BTN_MISC);
        }
    }




    static if(!is(typeof(BTN_0))) {
        private enum enumMixinStr_BTN_0 = `enum BTN_0 = 0x100;`;
        static if(is(typeof({ mixin(enumMixinStr_BTN_0); }))) {
            mixin(enumMixinStr_BTN_0);
        }
    }




    static if(!is(typeof(BTN_1))) {
        private enum enumMixinStr_BTN_1 = `enum BTN_1 = 0x101;`;
        static if(is(typeof({ mixin(enumMixinStr_BTN_1); }))) {
            mixin(enumMixinStr_BTN_1);
        }
    }




    static if(!is(typeof(BTN_2))) {
        private enum enumMixinStr_BTN_2 = `enum BTN_2 = 0x102;`;
        static if(is(typeof({ mixin(enumMixinStr_BTN_2); }))) {
            mixin(enumMixinStr_BTN_2);
        }
    }




    static if(!is(typeof(BTN_3))) {
        private enum enumMixinStr_BTN_3 = `enum BTN_3 = 0x103;`;
        static if(is(typeof({ mixin(enumMixinStr_BTN_3); }))) {
            mixin(enumMixinStr_BTN_3);
        }
    }




    static if(!is(typeof(BTN_4))) {
        private enum enumMixinStr_BTN_4 = `enum BTN_4 = 0x104;`;
        static if(is(typeof({ mixin(enumMixinStr_BTN_4); }))) {
            mixin(enumMixinStr_BTN_4);
        }
    }




    static if(!is(typeof(BTN_5))) {
        private enum enumMixinStr_BTN_5 = `enum BTN_5 = 0x105;`;
        static if(is(typeof({ mixin(enumMixinStr_BTN_5); }))) {
            mixin(enumMixinStr_BTN_5);
        }
    }




    static if(!is(typeof(BTN_6))) {
        private enum enumMixinStr_BTN_6 = `enum BTN_6 = 0x106;`;
        static if(is(typeof({ mixin(enumMixinStr_BTN_6); }))) {
            mixin(enumMixinStr_BTN_6);
        }
    }




    static if(!is(typeof(BTN_7))) {
        private enum enumMixinStr_BTN_7 = `enum BTN_7 = 0x107;`;
        static if(is(typeof({ mixin(enumMixinStr_BTN_7); }))) {
            mixin(enumMixinStr_BTN_7);
        }
    }




    static if(!is(typeof(BTN_8))) {
        private enum enumMixinStr_BTN_8 = `enum BTN_8 = 0x108;`;
        static if(is(typeof({ mixin(enumMixinStr_BTN_8); }))) {
            mixin(enumMixinStr_BTN_8);
        }
    }




    static if(!is(typeof(BTN_9))) {
        private enum enumMixinStr_BTN_9 = `enum BTN_9 = 0x109;`;
        static if(is(typeof({ mixin(enumMixinStr_BTN_9); }))) {
            mixin(enumMixinStr_BTN_9);
        }
    }




    static if(!is(typeof(BTN_MOUSE))) {
        private enum enumMixinStr_BTN_MOUSE = `enum BTN_MOUSE = 0x110;`;
        static if(is(typeof({ mixin(enumMixinStr_BTN_MOUSE); }))) {
            mixin(enumMixinStr_BTN_MOUSE);
        }
    }




    static if(!is(typeof(BTN_LEFT))) {
        private enum enumMixinStr_BTN_LEFT = `enum BTN_LEFT = 0x110;`;
        static if(is(typeof({ mixin(enumMixinStr_BTN_LEFT); }))) {
            mixin(enumMixinStr_BTN_LEFT);
        }
    }




    static if(!is(typeof(BTN_RIGHT))) {
        private enum enumMixinStr_BTN_RIGHT = `enum BTN_RIGHT = 0x111;`;
        static if(is(typeof({ mixin(enumMixinStr_BTN_RIGHT); }))) {
            mixin(enumMixinStr_BTN_RIGHT);
        }
    }




    static if(!is(typeof(BTN_MIDDLE))) {
        private enum enumMixinStr_BTN_MIDDLE = `enum BTN_MIDDLE = 0x112;`;
        static if(is(typeof({ mixin(enumMixinStr_BTN_MIDDLE); }))) {
            mixin(enumMixinStr_BTN_MIDDLE);
        }
    }




    static if(!is(typeof(BTN_SIDE))) {
        private enum enumMixinStr_BTN_SIDE = `enum BTN_SIDE = 0x113;`;
        static if(is(typeof({ mixin(enumMixinStr_BTN_SIDE); }))) {
            mixin(enumMixinStr_BTN_SIDE);
        }
    }




    static if(!is(typeof(BTN_EXTRA))) {
        private enum enumMixinStr_BTN_EXTRA = `enum BTN_EXTRA = 0x114;`;
        static if(is(typeof({ mixin(enumMixinStr_BTN_EXTRA); }))) {
            mixin(enumMixinStr_BTN_EXTRA);
        }
    }




    static if(!is(typeof(BTN_FORWARD))) {
        private enum enumMixinStr_BTN_FORWARD = `enum BTN_FORWARD = 0x115;`;
        static if(is(typeof({ mixin(enumMixinStr_BTN_FORWARD); }))) {
            mixin(enumMixinStr_BTN_FORWARD);
        }
    }




    static if(!is(typeof(BTN_BACK))) {
        private enum enumMixinStr_BTN_BACK = `enum BTN_BACK = 0x116;`;
        static if(is(typeof({ mixin(enumMixinStr_BTN_BACK); }))) {
            mixin(enumMixinStr_BTN_BACK);
        }
    }




    static if(!is(typeof(BTN_TASK))) {
        private enum enumMixinStr_BTN_TASK = `enum BTN_TASK = 0x117;`;
        static if(is(typeof({ mixin(enumMixinStr_BTN_TASK); }))) {
            mixin(enumMixinStr_BTN_TASK);
        }
    }




    static if(!is(typeof(BTN_JOYSTICK))) {
        private enum enumMixinStr_BTN_JOYSTICK = `enum BTN_JOYSTICK = 0x120;`;
        static if(is(typeof({ mixin(enumMixinStr_BTN_JOYSTICK); }))) {
            mixin(enumMixinStr_BTN_JOYSTICK);
        }
    }




    static if(!is(typeof(BTN_TRIGGER))) {
        private enum enumMixinStr_BTN_TRIGGER = `enum BTN_TRIGGER = 0x120;`;
        static if(is(typeof({ mixin(enumMixinStr_BTN_TRIGGER); }))) {
            mixin(enumMixinStr_BTN_TRIGGER);
        }
    }




    static if(!is(typeof(BTN_THUMB))) {
        private enum enumMixinStr_BTN_THUMB = `enum BTN_THUMB = 0x121;`;
        static if(is(typeof({ mixin(enumMixinStr_BTN_THUMB); }))) {
            mixin(enumMixinStr_BTN_THUMB);
        }
    }




    static if(!is(typeof(BTN_THUMB2))) {
        private enum enumMixinStr_BTN_THUMB2 = `enum BTN_THUMB2 = 0x122;`;
        static if(is(typeof({ mixin(enumMixinStr_BTN_THUMB2); }))) {
            mixin(enumMixinStr_BTN_THUMB2);
        }
    }




    static if(!is(typeof(BTN_TOP))) {
        private enum enumMixinStr_BTN_TOP = `enum BTN_TOP = 0x123;`;
        static if(is(typeof({ mixin(enumMixinStr_BTN_TOP); }))) {
            mixin(enumMixinStr_BTN_TOP);
        }
    }




    static if(!is(typeof(BTN_TOP2))) {
        private enum enumMixinStr_BTN_TOP2 = `enum BTN_TOP2 = 0x124;`;
        static if(is(typeof({ mixin(enumMixinStr_BTN_TOP2); }))) {
            mixin(enumMixinStr_BTN_TOP2);
        }
    }




    static if(!is(typeof(BTN_PINKIE))) {
        private enum enumMixinStr_BTN_PINKIE = `enum BTN_PINKIE = 0x125;`;
        static if(is(typeof({ mixin(enumMixinStr_BTN_PINKIE); }))) {
            mixin(enumMixinStr_BTN_PINKIE);
        }
    }




    static if(!is(typeof(BTN_BASE))) {
        private enum enumMixinStr_BTN_BASE = `enum BTN_BASE = 0x126;`;
        static if(is(typeof({ mixin(enumMixinStr_BTN_BASE); }))) {
            mixin(enumMixinStr_BTN_BASE);
        }
    }




    static if(!is(typeof(BTN_BASE2))) {
        private enum enumMixinStr_BTN_BASE2 = `enum BTN_BASE2 = 0x127;`;
        static if(is(typeof({ mixin(enumMixinStr_BTN_BASE2); }))) {
            mixin(enumMixinStr_BTN_BASE2);
        }
    }




    static if(!is(typeof(BTN_BASE3))) {
        private enum enumMixinStr_BTN_BASE3 = `enum BTN_BASE3 = 0x128;`;
        static if(is(typeof({ mixin(enumMixinStr_BTN_BASE3); }))) {
            mixin(enumMixinStr_BTN_BASE3);
        }
    }




    static if(!is(typeof(BTN_BASE4))) {
        private enum enumMixinStr_BTN_BASE4 = `enum BTN_BASE4 = 0x129;`;
        static if(is(typeof({ mixin(enumMixinStr_BTN_BASE4); }))) {
            mixin(enumMixinStr_BTN_BASE4);
        }
    }




    static if(!is(typeof(BTN_BASE5))) {
        private enum enumMixinStr_BTN_BASE5 = `enum BTN_BASE5 = 0x12a;`;
        static if(is(typeof({ mixin(enumMixinStr_BTN_BASE5); }))) {
            mixin(enumMixinStr_BTN_BASE5);
        }
    }




    static if(!is(typeof(BTN_BASE6))) {
        private enum enumMixinStr_BTN_BASE6 = `enum BTN_BASE6 = 0x12b;`;
        static if(is(typeof({ mixin(enumMixinStr_BTN_BASE6); }))) {
            mixin(enumMixinStr_BTN_BASE6);
        }
    }




    static if(!is(typeof(BTN_DEAD))) {
        private enum enumMixinStr_BTN_DEAD = `enum BTN_DEAD = 0x12f;`;
        static if(is(typeof({ mixin(enumMixinStr_BTN_DEAD); }))) {
            mixin(enumMixinStr_BTN_DEAD);
        }
    }




    static if(!is(typeof(BTN_GAMEPAD))) {
        private enum enumMixinStr_BTN_GAMEPAD = `enum BTN_GAMEPAD = 0x130;`;
        static if(is(typeof({ mixin(enumMixinStr_BTN_GAMEPAD); }))) {
            mixin(enumMixinStr_BTN_GAMEPAD);
        }
    }




    static if(!is(typeof(BTN_SOUTH))) {
        private enum enumMixinStr_BTN_SOUTH = `enum BTN_SOUTH = 0x130;`;
        static if(is(typeof({ mixin(enumMixinStr_BTN_SOUTH); }))) {
            mixin(enumMixinStr_BTN_SOUTH);
        }
    }




    static if(!is(typeof(BTN_A))) {
        private enum enumMixinStr_BTN_A = `enum BTN_A = 0x130;`;
        static if(is(typeof({ mixin(enumMixinStr_BTN_A); }))) {
            mixin(enumMixinStr_BTN_A);
        }
    }




    static if(!is(typeof(BTN_EAST))) {
        private enum enumMixinStr_BTN_EAST = `enum BTN_EAST = 0x131;`;
        static if(is(typeof({ mixin(enumMixinStr_BTN_EAST); }))) {
            mixin(enumMixinStr_BTN_EAST);
        }
    }




    static if(!is(typeof(BTN_B))) {
        private enum enumMixinStr_BTN_B = `enum BTN_B = 0x131;`;
        static if(is(typeof({ mixin(enumMixinStr_BTN_B); }))) {
            mixin(enumMixinStr_BTN_B);
        }
    }




    static if(!is(typeof(BTN_C))) {
        private enum enumMixinStr_BTN_C = `enum BTN_C = 0x132;`;
        static if(is(typeof({ mixin(enumMixinStr_BTN_C); }))) {
            mixin(enumMixinStr_BTN_C);
        }
    }




    static if(!is(typeof(BTN_NORTH))) {
        private enum enumMixinStr_BTN_NORTH = `enum BTN_NORTH = 0x133;`;
        static if(is(typeof({ mixin(enumMixinStr_BTN_NORTH); }))) {
            mixin(enumMixinStr_BTN_NORTH);
        }
    }




    static if(!is(typeof(BTN_X))) {
        private enum enumMixinStr_BTN_X = `enum BTN_X = 0x133;`;
        static if(is(typeof({ mixin(enumMixinStr_BTN_X); }))) {
            mixin(enumMixinStr_BTN_X);
        }
    }




    static if(!is(typeof(BTN_WEST))) {
        private enum enumMixinStr_BTN_WEST = `enum BTN_WEST = 0x134;`;
        static if(is(typeof({ mixin(enumMixinStr_BTN_WEST); }))) {
            mixin(enumMixinStr_BTN_WEST);
        }
    }




    static if(!is(typeof(BTN_Y))) {
        private enum enumMixinStr_BTN_Y = `enum BTN_Y = 0x134;`;
        static if(is(typeof({ mixin(enumMixinStr_BTN_Y); }))) {
            mixin(enumMixinStr_BTN_Y);
        }
    }




    static if(!is(typeof(BTN_Z))) {
        private enum enumMixinStr_BTN_Z = `enum BTN_Z = 0x135;`;
        static if(is(typeof({ mixin(enumMixinStr_BTN_Z); }))) {
            mixin(enumMixinStr_BTN_Z);
        }
    }




    static if(!is(typeof(BTN_TL))) {
        private enum enumMixinStr_BTN_TL = `enum BTN_TL = 0x136;`;
        static if(is(typeof({ mixin(enumMixinStr_BTN_TL); }))) {
            mixin(enumMixinStr_BTN_TL);
        }
    }




    static if(!is(typeof(BTN_TR))) {
        private enum enumMixinStr_BTN_TR = `enum BTN_TR = 0x137;`;
        static if(is(typeof({ mixin(enumMixinStr_BTN_TR); }))) {
            mixin(enumMixinStr_BTN_TR);
        }
    }




    static if(!is(typeof(BTN_TL2))) {
        private enum enumMixinStr_BTN_TL2 = `enum BTN_TL2 = 0x138;`;
        static if(is(typeof({ mixin(enumMixinStr_BTN_TL2); }))) {
            mixin(enumMixinStr_BTN_TL2);
        }
    }




    static if(!is(typeof(BTN_TR2))) {
        private enum enumMixinStr_BTN_TR2 = `enum BTN_TR2 = 0x139;`;
        static if(is(typeof({ mixin(enumMixinStr_BTN_TR2); }))) {
            mixin(enumMixinStr_BTN_TR2);
        }
    }




    static if(!is(typeof(BTN_SELECT))) {
        private enum enumMixinStr_BTN_SELECT = `enum BTN_SELECT = 0x13a;`;
        static if(is(typeof({ mixin(enumMixinStr_BTN_SELECT); }))) {
            mixin(enumMixinStr_BTN_SELECT);
        }
    }




    static if(!is(typeof(BTN_START))) {
        private enum enumMixinStr_BTN_START = `enum BTN_START = 0x13b;`;
        static if(is(typeof({ mixin(enumMixinStr_BTN_START); }))) {
            mixin(enumMixinStr_BTN_START);
        }
    }




    static if(!is(typeof(BTN_MODE))) {
        private enum enumMixinStr_BTN_MODE = `enum BTN_MODE = 0x13c;`;
        static if(is(typeof({ mixin(enumMixinStr_BTN_MODE); }))) {
            mixin(enumMixinStr_BTN_MODE);
        }
    }




    static if(!is(typeof(BTN_THUMBL))) {
        private enum enumMixinStr_BTN_THUMBL = `enum BTN_THUMBL = 0x13d;`;
        static if(is(typeof({ mixin(enumMixinStr_BTN_THUMBL); }))) {
            mixin(enumMixinStr_BTN_THUMBL);
        }
    }




    static if(!is(typeof(BTN_THUMBR))) {
        private enum enumMixinStr_BTN_THUMBR = `enum BTN_THUMBR = 0x13e;`;
        static if(is(typeof({ mixin(enumMixinStr_BTN_THUMBR); }))) {
            mixin(enumMixinStr_BTN_THUMBR);
        }
    }




    static if(!is(typeof(BTN_DIGI))) {
        private enum enumMixinStr_BTN_DIGI = `enum BTN_DIGI = 0x140;`;
        static if(is(typeof({ mixin(enumMixinStr_BTN_DIGI); }))) {
            mixin(enumMixinStr_BTN_DIGI);
        }
    }




    static if(!is(typeof(BTN_TOOL_PEN))) {
        private enum enumMixinStr_BTN_TOOL_PEN = `enum BTN_TOOL_PEN = 0x140;`;
        static if(is(typeof({ mixin(enumMixinStr_BTN_TOOL_PEN); }))) {
            mixin(enumMixinStr_BTN_TOOL_PEN);
        }
    }




    static if(!is(typeof(BTN_TOOL_RUBBER))) {
        private enum enumMixinStr_BTN_TOOL_RUBBER = `enum BTN_TOOL_RUBBER = 0x141;`;
        static if(is(typeof({ mixin(enumMixinStr_BTN_TOOL_RUBBER); }))) {
            mixin(enumMixinStr_BTN_TOOL_RUBBER);
        }
    }




    static if(!is(typeof(BTN_TOOL_BRUSH))) {
        private enum enumMixinStr_BTN_TOOL_BRUSH = `enum BTN_TOOL_BRUSH = 0x142;`;
        static if(is(typeof({ mixin(enumMixinStr_BTN_TOOL_BRUSH); }))) {
            mixin(enumMixinStr_BTN_TOOL_BRUSH);
        }
    }




    static if(!is(typeof(BTN_TOOL_PENCIL))) {
        private enum enumMixinStr_BTN_TOOL_PENCIL = `enum BTN_TOOL_PENCIL = 0x143;`;
        static if(is(typeof({ mixin(enumMixinStr_BTN_TOOL_PENCIL); }))) {
            mixin(enumMixinStr_BTN_TOOL_PENCIL);
        }
    }




    static if(!is(typeof(BTN_TOOL_AIRBRUSH))) {
        private enum enumMixinStr_BTN_TOOL_AIRBRUSH = `enum BTN_TOOL_AIRBRUSH = 0x144;`;
        static if(is(typeof({ mixin(enumMixinStr_BTN_TOOL_AIRBRUSH); }))) {
            mixin(enumMixinStr_BTN_TOOL_AIRBRUSH);
        }
    }




    static if(!is(typeof(BTN_TOOL_FINGER))) {
        private enum enumMixinStr_BTN_TOOL_FINGER = `enum BTN_TOOL_FINGER = 0x145;`;
        static if(is(typeof({ mixin(enumMixinStr_BTN_TOOL_FINGER); }))) {
            mixin(enumMixinStr_BTN_TOOL_FINGER);
        }
    }




    static if(!is(typeof(BTN_TOOL_MOUSE))) {
        private enum enumMixinStr_BTN_TOOL_MOUSE = `enum BTN_TOOL_MOUSE = 0x146;`;
        static if(is(typeof({ mixin(enumMixinStr_BTN_TOOL_MOUSE); }))) {
            mixin(enumMixinStr_BTN_TOOL_MOUSE);
        }
    }




    static if(!is(typeof(BTN_TOOL_LENS))) {
        private enum enumMixinStr_BTN_TOOL_LENS = `enum BTN_TOOL_LENS = 0x147;`;
        static if(is(typeof({ mixin(enumMixinStr_BTN_TOOL_LENS); }))) {
            mixin(enumMixinStr_BTN_TOOL_LENS);
        }
    }




    static if(!is(typeof(BTN_TOOL_QUINTTAP))) {
        private enum enumMixinStr_BTN_TOOL_QUINTTAP = `enum BTN_TOOL_QUINTTAP = 0x148;`;
        static if(is(typeof({ mixin(enumMixinStr_BTN_TOOL_QUINTTAP); }))) {
            mixin(enumMixinStr_BTN_TOOL_QUINTTAP);
        }
    }




    static if(!is(typeof(BTN_STYLUS3))) {
        private enum enumMixinStr_BTN_STYLUS3 = `enum BTN_STYLUS3 = 0x149;`;
        static if(is(typeof({ mixin(enumMixinStr_BTN_STYLUS3); }))) {
            mixin(enumMixinStr_BTN_STYLUS3);
        }
    }




    static if(!is(typeof(BTN_TOUCH))) {
        private enum enumMixinStr_BTN_TOUCH = `enum BTN_TOUCH = 0x14a;`;
        static if(is(typeof({ mixin(enumMixinStr_BTN_TOUCH); }))) {
            mixin(enumMixinStr_BTN_TOUCH);
        }
    }




    static if(!is(typeof(BTN_STYLUS))) {
        private enum enumMixinStr_BTN_STYLUS = `enum BTN_STYLUS = 0x14b;`;
        static if(is(typeof({ mixin(enumMixinStr_BTN_STYLUS); }))) {
            mixin(enumMixinStr_BTN_STYLUS);
        }
    }




    static if(!is(typeof(BTN_STYLUS2))) {
        private enum enumMixinStr_BTN_STYLUS2 = `enum BTN_STYLUS2 = 0x14c;`;
        static if(is(typeof({ mixin(enumMixinStr_BTN_STYLUS2); }))) {
            mixin(enumMixinStr_BTN_STYLUS2);
        }
    }




    static if(!is(typeof(BTN_TOOL_DOUBLETAP))) {
        private enum enumMixinStr_BTN_TOOL_DOUBLETAP = `enum BTN_TOOL_DOUBLETAP = 0x14d;`;
        static if(is(typeof({ mixin(enumMixinStr_BTN_TOOL_DOUBLETAP); }))) {
            mixin(enumMixinStr_BTN_TOOL_DOUBLETAP);
        }
    }




    static if(!is(typeof(BTN_TOOL_TRIPLETAP))) {
        private enum enumMixinStr_BTN_TOOL_TRIPLETAP = `enum BTN_TOOL_TRIPLETAP = 0x14e;`;
        static if(is(typeof({ mixin(enumMixinStr_BTN_TOOL_TRIPLETAP); }))) {
            mixin(enumMixinStr_BTN_TOOL_TRIPLETAP);
        }
    }




    static if(!is(typeof(BTN_TOOL_QUADTAP))) {
        private enum enumMixinStr_BTN_TOOL_QUADTAP = `enum BTN_TOOL_QUADTAP = 0x14f;`;
        static if(is(typeof({ mixin(enumMixinStr_BTN_TOOL_QUADTAP); }))) {
            mixin(enumMixinStr_BTN_TOOL_QUADTAP);
        }
    }




    static if(!is(typeof(BTN_WHEEL))) {
        private enum enumMixinStr_BTN_WHEEL = `enum BTN_WHEEL = 0x150;`;
        static if(is(typeof({ mixin(enumMixinStr_BTN_WHEEL); }))) {
            mixin(enumMixinStr_BTN_WHEEL);
        }
    }




    static if(!is(typeof(BTN_GEAR_DOWN))) {
        private enum enumMixinStr_BTN_GEAR_DOWN = `enum BTN_GEAR_DOWN = 0x150;`;
        static if(is(typeof({ mixin(enumMixinStr_BTN_GEAR_DOWN); }))) {
            mixin(enumMixinStr_BTN_GEAR_DOWN);
        }
    }




    static if(!is(typeof(BTN_GEAR_UP))) {
        private enum enumMixinStr_BTN_GEAR_UP = `enum BTN_GEAR_UP = 0x151;`;
        static if(is(typeof({ mixin(enumMixinStr_BTN_GEAR_UP); }))) {
            mixin(enumMixinStr_BTN_GEAR_UP);
        }
    }




    static if(!is(typeof(KEY_OK))) {
        private enum enumMixinStr_KEY_OK = `enum KEY_OK = 0x160;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_OK); }))) {
            mixin(enumMixinStr_KEY_OK);
        }
    }




    static if(!is(typeof(KEY_SELECT))) {
        private enum enumMixinStr_KEY_SELECT = `enum KEY_SELECT = 0x161;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_SELECT); }))) {
            mixin(enumMixinStr_KEY_SELECT);
        }
    }




    static if(!is(typeof(KEY_GOTO))) {
        private enum enumMixinStr_KEY_GOTO = `enum KEY_GOTO = 0x162;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_GOTO); }))) {
            mixin(enumMixinStr_KEY_GOTO);
        }
    }




    static if(!is(typeof(KEY_CLEAR))) {
        private enum enumMixinStr_KEY_CLEAR = `enum KEY_CLEAR = 0x163;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_CLEAR); }))) {
            mixin(enumMixinStr_KEY_CLEAR);
        }
    }




    static if(!is(typeof(KEY_POWER2))) {
        private enum enumMixinStr_KEY_POWER2 = `enum KEY_POWER2 = 0x164;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_POWER2); }))) {
            mixin(enumMixinStr_KEY_POWER2);
        }
    }




    static if(!is(typeof(KEY_OPTION))) {
        private enum enumMixinStr_KEY_OPTION = `enum KEY_OPTION = 0x165;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_OPTION); }))) {
            mixin(enumMixinStr_KEY_OPTION);
        }
    }




    static if(!is(typeof(KEY_INFO))) {
        private enum enumMixinStr_KEY_INFO = `enum KEY_INFO = 0x166;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_INFO); }))) {
            mixin(enumMixinStr_KEY_INFO);
        }
    }




    static if(!is(typeof(KEY_TIME))) {
        private enum enumMixinStr_KEY_TIME = `enum KEY_TIME = 0x167;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_TIME); }))) {
            mixin(enumMixinStr_KEY_TIME);
        }
    }




    static if(!is(typeof(KEY_VENDOR))) {
        private enum enumMixinStr_KEY_VENDOR = `enum KEY_VENDOR = 0x168;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_VENDOR); }))) {
            mixin(enumMixinStr_KEY_VENDOR);
        }
    }




    static if(!is(typeof(KEY_ARCHIVE))) {
        private enum enumMixinStr_KEY_ARCHIVE = `enum KEY_ARCHIVE = 0x169;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_ARCHIVE); }))) {
            mixin(enumMixinStr_KEY_ARCHIVE);
        }
    }




    static if(!is(typeof(KEY_PROGRAM))) {
        private enum enumMixinStr_KEY_PROGRAM = `enum KEY_PROGRAM = 0x16a;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_PROGRAM); }))) {
            mixin(enumMixinStr_KEY_PROGRAM);
        }
    }




    static if(!is(typeof(KEY_CHANNEL))) {
        private enum enumMixinStr_KEY_CHANNEL = `enum KEY_CHANNEL = 0x16b;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_CHANNEL); }))) {
            mixin(enumMixinStr_KEY_CHANNEL);
        }
    }




    static if(!is(typeof(KEY_FAVORITES))) {
        private enum enumMixinStr_KEY_FAVORITES = `enum KEY_FAVORITES = 0x16c;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_FAVORITES); }))) {
            mixin(enumMixinStr_KEY_FAVORITES);
        }
    }




    static if(!is(typeof(KEY_EPG))) {
        private enum enumMixinStr_KEY_EPG = `enum KEY_EPG = 0x16d;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_EPG); }))) {
            mixin(enumMixinStr_KEY_EPG);
        }
    }




    static if(!is(typeof(KEY_PVR))) {
        private enum enumMixinStr_KEY_PVR = `enum KEY_PVR = 0x16e;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_PVR); }))) {
            mixin(enumMixinStr_KEY_PVR);
        }
    }




    static if(!is(typeof(KEY_MHP))) {
        private enum enumMixinStr_KEY_MHP = `enum KEY_MHP = 0x16f;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_MHP); }))) {
            mixin(enumMixinStr_KEY_MHP);
        }
    }




    static if(!is(typeof(KEY_LANGUAGE))) {
        private enum enumMixinStr_KEY_LANGUAGE = `enum KEY_LANGUAGE = 0x170;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_LANGUAGE); }))) {
            mixin(enumMixinStr_KEY_LANGUAGE);
        }
    }




    static if(!is(typeof(KEY_TITLE))) {
        private enum enumMixinStr_KEY_TITLE = `enum KEY_TITLE = 0x171;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_TITLE); }))) {
            mixin(enumMixinStr_KEY_TITLE);
        }
    }




    static if(!is(typeof(KEY_SUBTITLE))) {
        private enum enumMixinStr_KEY_SUBTITLE = `enum KEY_SUBTITLE = 0x172;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_SUBTITLE); }))) {
            mixin(enumMixinStr_KEY_SUBTITLE);
        }
    }




    static if(!is(typeof(KEY_ANGLE))) {
        private enum enumMixinStr_KEY_ANGLE = `enum KEY_ANGLE = 0x173;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_ANGLE); }))) {
            mixin(enumMixinStr_KEY_ANGLE);
        }
    }




    static if(!is(typeof(KEY_FULL_SCREEN))) {
        private enum enumMixinStr_KEY_FULL_SCREEN = `enum KEY_FULL_SCREEN = 0x174;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_FULL_SCREEN); }))) {
            mixin(enumMixinStr_KEY_FULL_SCREEN);
        }
    }




    static if(!is(typeof(KEY_ZOOM))) {
        private enum enumMixinStr_KEY_ZOOM = `enum KEY_ZOOM = 0x174;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_ZOOM); }))) {
            mixin(enumMixinStr_KEY_ZOOM);
        }
    }




    static if(!is(typeof(KEY_MODE))) {
        private enum enumMixinStr_KEY_MODE = `enum KEY_MODE = 0x175;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_MODE); }))) {
            mixin(enumMixinStr_KEY_MODE);
        }
    }




    static if(!is(typeof(KEY_KEYBOARD))) {
        private enum enumMixinStr_KEY_KEYBOARD = `enum KEY_KEYBOARD = 0x176;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_KEYBOARD); }))) {
            mixin(enumMixinStr_KEY_KEYBOARD);
        }
    }




    static if(!is(typeof(KEY_ASPECT_RATIO))) {
        private enum enumMixinStr_KEY_ASPECT_RATIO = `enum KEY_ASPECT_RATIO = 0x177;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_ASPECT_RATIO); }))) {
            mixin(enumMixinStr_KEY_ASPECT_RATIO);
        }
    }




    static if(!is(typeof(KEY_SCREEN))) {
        private enum enumMixinStr_KEY_SCREEN = `enum KEY_SCREEN = 0x177;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_SCREEN); }))) {
            mixin(enumMixinStr_KEY_SCREEN);
        }
    }




    static if(!is(typeof(KEY_PC))) {
        private enum enumMixinStr_KEY_PC = `enum KEY_PC = 0x178;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_PC); }))) {
            mixin(enumMixinStr_KEY_PC);
        }
    }




    static if(!is(typeof(KEY_TV))) {
        private enum enumMixinStr_KEY_TV = `enum KEY_TV = 0x179;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_TV); }))) {
            mixin(enumMixinStr_KEY_TV);
        }
    }




    static if(!is(typeof(KEY_TV2))) {
        private enum enumMixinStr_KEY_TV2 = `enum KEY_TV2 = 0x17a;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_TV2); }))) {
            mixin(enumMixinStr_KEY_TV2);
        }
    }




    static if(!is(typeof(KEY_VCR))) {
        private enum enumMixinStr_KEY_VCR = `enum KEY_VCR = 0x17b;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_VCR); }))) {
            mixin(enumMixinStr_KEY_VCR);
        }
    }




    static if(!is(typeof(KEY_VCR2))) {
        private enum enumMixinStr_KEY_VCR2 = `enum KEY_VCR2 = 0x17c;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_VCR2); }))) {
            mixin(enumMixinStr_KEY_VCR2);
        }
    }




    static if(!is(typeof(KEY_SAT))) {
        private enum enumMixinStr_KEY_SAT = `enum KEY_SAT = 0x17d;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_SAT); }))) {
            mixin(enumMixinStr_KEY_SAT);
        }
    }




    static if(!is(typeof(KEY_SAT2))) {
        private enum enumMixinStr_KEY_SAT2 = `enum KEY_SAT2 = 0x17e;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_SAT2); }))) {
            mixin(enumMixinStr_KEY_SAT2);
        }
    }




    static if(!is(typeof(KEY_CD))) {
        private enum enumMixinStr_KEY_CD = `enum KEY_CD = 0x17f;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_CD); }))) {
            mixin(enumMixinStr_KEY_CD);
        }
    }




    static if(!is(typeof(KEY_TAPE))) {
        private enum enumMixinStr_KEY_TAPE = `enum KEY_TAPE = 0x180;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_TAPE); }))) {
            mixin(enumMixinStr_KEY_TAPE);
        }
    }




    static if(!is(typeof(KEY_RADIO))) {
        private enum enumMixinStr_KEY_RADIO = `enum KEY_RADIO = 0x181;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_RADIO); }))) {
            mixin(enumMixinStr_KEY_RADIO);
        }
    }




    static if(!is(typeof(KEY_TUNER))) {
        private enum enumMixinStr_KEY_TUNER = `enum KEY_TUNER = 0x182;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_TUNER); }))) {
            mixin(enumMixinStr_KEY_TUNER);
        }
    }




    static if(!is(typeof(KEY_PLAYER))) {
        private enum enumMixinStr_KEY_PLAYER = `enum KEY_PLAYER = 0x183;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_PLAYER); }))) {
            mixin(enumMixinStr_KEY_PLAYER);
        }
    }




    static if(!is(typeof(KEY_TEXT))) {
        private enum enumMixinStr_KEY_TEXT = `enum KEY_TEXT = 0x184;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_TEXT); }))) {
            mixin(enumMixinStr_KEY_TEXT);
        }
    }




    static if(!is(typeof(KEY_DVD))) {
        private enum enumMixinStr_KEY_DVD = `enum KEY_DVD = 0x185;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_DVD); }))) {
            mixin(enumMixinStr_KEY_DVD);
        }
    }




    static if(!is(typeof(KEY_AUX))) {
        private enum enumMixinStr_KEY_AUX = `enum KEY_AUX = 0x186;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_AUX); }))) {
            mixin(enumMixinStr_KEY_AUX);
        }
    }




    static if(!is(typeof(KEY_MP3))) {
        private enum enumMixinStr_KEY_MP3 = `enum KEY_MP3 = 0x187;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_MP3); }))) {
            mixin(enumMixinStr_KEY_MP3);
        }
    }




    static if(!is(typeof(KEY_AUDIO))) {
        private enum enumMixinStr_KEY_AUDIO = `enum KEY_AUDIO = 0x188;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_AUDIO); }))) {
            mixin(enumMixinStr_KEY_AUDIO);
        }
    }




    static if(!is(typeof(KEY_VIDEO))) {
        private enum enumMixinStr_KEY_VIDEO = `enum KEY_VIDEO = 0x189;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_VIDEO); }))) {
            mixin(enumMixinStr_KEY_VIDEO);
        }
    }




    static if(!is(typeof(KEY_DIRECTORY))) {
        private enum enumMixinStr_KEY_DIRECTORY = `enum KEY_DIRECTORY = 0x18a;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_DIRECTORY); }))) {
            mixin(enumMixinStr_KEY_DIRECTORY);
        }
    }




    static if(!is(typeof(KEY_LIST))) {
        private enum enumMixinStr_KEY_LIST = `enum KEY_LIST = 0x18b;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_LIST); }))) {
            mixin(enumMixinStr_KEY_LIST);
        }
    }




    static if(!is(typeof(KEY_MEMO))) {
        private enum enumMixinStr_KEY_MEMO = `enum KEY_MEMO = 0x18c;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_MEMO); }))) {
            mixin(enumMixinStr_KEY_MEMO);
        }
    }




    static if(!is(typeof(KEY_CALENDAR))) {
        private enum enumMixinStr_KEY_CALENDAR = `enum KEY_CALENDAR = 0x18d;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_CALENDAR); }))) {
            mixin(enumMixinStr_KEY_CALENDAR);
        }
    }




    static if(!is(typeof(KEY_RED))) {
        private enum enumMixinStr_KEY_RED = `enum KEY_RED = 0x18e;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_RED); }))) {
            mixin(enumMixinStr_KEY_RED);
        }
    }




    static if(!is(typeof(KEY_GREEN))) {
        private enum enumMixinStr_KEY_GREEN = `enum KEY_GREEN = 0x18f;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_GREEN); }))) {
            mixin(enumMixinStr_KEY_GREEN);
        }
    }




    static if(!is(typeof(KEY_YELLOW))) {
        private enum enumMixinStr_KEY_YELLOW = `enum KEY_YELLOW = 0x190;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_YELLOW); }))) {
            mixin(enumMixinStr_KEY_YELLOW);
        }
    }




    static if(!is(typeof(KEY_BLUE))) {
        private enum enumMixinStr_KEY_BLUE = `enum KEY_BLUE = 0x191;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_BLUE); }))) {
            mixin(enumMixinStr_KEY_BLUE);
        }
    }




    static if(!is(typeof(KEY_CHANNELUP))) {
        private enum enumMixinStr_KEY_CHANNELUP = `enum KEY_CHANNELUP = 0x192;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_CHANNELUP); }))) {
            mixin(enumMixinStr_KEY_CHANNELUP);
        }
    }




    static if(!is(typeof(KEY_CHANNELDOWN))) {
        private enum enumMixinStr_KEY_CHANNELDOWN = `enum KEY_CHANNELDOWN = 0x193;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_CHANNELDOWN); }))) {
            mixin(enumMixinStr_KEY_CHANNELDOWN);
        }
    }




    static if(!is(typeof(KEY_FIRST))) {
        private enum enumMixinStr_KEY_FIRST = `enum KEY_FIRST = 0x194;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_FIRST); }))) {
            mixin(enumMixinStr_KEY_FIRST);
        }
    }




    static if(!is(typeof(KEY_LAST))) {
        private enum enumMixinStr_KEY_LAST = `enum KEY_LAST = 0x195;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_LAST); }))) {
            mixin(enumMixinStr_KEY_LAST);
        }
    }




    static if(!is(typeof(KEY_AB))) {
        private enum enumMixinStr_KEY_AB = `enum KEY_AB = 0x196;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_AB); }))) {
            mixin(enumMixinStr_KEY_AB);
        }
    }




    static if(!is(typeof(KEY_NEXT))) {
        private enum enumMixinStr_KEY_NEXT = `enum KEY_NEXT = 0x197;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_NEXT); }))) {
            mixin(enumMixinStr_KEY_NEXT);
        }
    }




    static if(!is(typeof(KEY_RESTART))) {
        private enum enumMixinStr_KEY_RESTART = `enum KEY_RESTART = 0x198;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_RESTART); }))) {
            mixin(enumMixinStr_KEY_RESTART);
        }
    }




    static if(!is(typeof(KEY_SLOW))) {
        private enum enumMixinStr_KEY_SLOW = `enum KEY_SLOW = 0x199;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_SLOW); }))) {
            mixin(enumMixinStr_KEY_SLOW);
        }
    }




    static if(!is(typeof(KEY_SHUFFLE))) {
        private enum enumMixinStr_KEY_SHUFFLE = `enum KEY_SHUFFLE = 0x19a;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_SHUFFLE); }))) {
            mixin(enumMixinStr_KEY_SHUFFLE);
        }
    }




    static if(!is(typeof(KEY_BREAK))) {
        private enum enumMixinStr_KEY_BREAK = `enum KEY_BREAK = 0x19b;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_BREAK); }))) {
            mixin(enumMixinStr_KEY_BREAK);
        }
    }




    static if(!is(typeof(KEY_PREVIOUS))) {
        private enum enumMixinStr_KEY_PREVIOUS = `enum KEY_PREVIOUS = 0x19c;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_PREVIOUS); }))) {
            mixin(enumMixinStr_KEY_PREVIOUS);
        }
    }




    static if(!is(typeof(KEY_DIGITS))) {
        private enum enumMixinStr_KEY_DIGITS = `enum KEY_DIGITS = 0x19d;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_DIGITS); }))) {
            mixin(enumMixinStr_KEY_DIGITS);
        }
    }




    static if(!is(typeof(KEY_TEEN))) {
        private enum enumMixinStr_KEY_TEEN = `enum KEY_TEEN = 0x19e;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_TEEN); }))) {
            mixin(enumMixinStr_KEY_TEEN);
        }
    }




    static if(!is(typeof(KEY_TWEN))) {
        private enum enumMixinStr_KEY_TWEN = `enum KEY_TWEN = 0x19f;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_TWEN); }))) {
            mixin(enumMixinStr_KEY_TWEN);
        }
    }




    static if(!is(typeof(KEY_VIDEOPHONE))) {
        private enum enumMixinStr_KEY_VIDEOPHONE = `enum KEY_VIDEOPHONE = 0x1a0;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_VIDEOPHONE); }))) {
            mixin(enumMixinStr_KEY_VIDEOPHONE);
        }
    }




    static if(!is(typeof(KEY_GAMES))) {
        private enum enumMixinStr_KEY_GAMES = `enum KEY_GAMES = 0x1a1;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_GAMES); }))) {
            mixin(enumMixinStr_KEY_GAMES);
        }
    }




    static if(!is(typeof(KEY_ZOOMIN))) {
        private enum enumMixinStr_KEY_ZOOMIN = `enum KEY_ZOOMIN = 0x1a2;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_ZOOMIN); }))) {
            mixin(enumMixinStr_KEY_ZOOMIN);
        }
    }




    static if(!is(typeof(KEY_ZOOMOUT))) {
        private enum enumMixinStr_KEY_ZOOMOUT = `enum KEY_ZOOMOUT = 0x1a3;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_ZOOMOUT); }))) {
            mixin(enumMixinStr_KEY_ZOOMOUT);
        }
    }




    static if(!is(typeof(KEY_ZOOMRESET))) {
        private enum enumMixinStr_KEY_ZOOMRESET = `enum KEY_ZOOMRESET = 0x1a4;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_ZOOMRESET); }))) {
            mixin(enumMixinStr_KEY_ZOOMRESET);
        }
    }




    static if(!is(typeof(KEY_WORDPROCESSOR))) {
        private enum enumMixinStr_KEY_WORDPROCESSOR = `enum KEY_WORDPROCESSOR = 0x1a5;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_WORDPROCESSOR); }))) {
            mixin(enumMixinStr_KEY_WORDPROCESSOR);
        }
    }




    static if(!is(typeof(KEY_EDITOR))) {
        private enum enumMixinStr_KEY_EDITOR = `enum KEY_EDITOR = 0x1a6;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_EDITOR); }))) {
            mixin(enumMixinStr_KEY_EDITOR);
        }
    }




    static if(!is(typeof(KEY_SPREADSHEET))) {
        private enum enumMixinStr_KEY_SPREADSHEET = `enum KEY_SPREADSHEET = 0x1a7;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_SPREADSHEET); }))) {
            mixin(enumMixinStr_KEY_SPREADSHEET);
        }
    }




    static if(!is(typeof(KEY_GRAPHICSEDITOR))) {
        private enum enumMixinStr_KEY_GRAPHICSEDITOR = `enum KEY_GRAPHICSEDITOR = 0x1a8;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_GRAPHICSEDITOR); }))) {
            mixin(enumMixinStr_KEY_GRAPHICSEDITOR);
        }
    }




    static if(!is(typeof(KEY_PRESENTATION))) {
        private enum enumMixinStr_KEY_PRESENTATION = `enum KEY_PRESENTATION = 0x1a9;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_PRESENTATION); }))) {
            mixin(enumMixinStr_KEY_PRESENTATION);
        }
    }




    static if(!is(typeof(KEY_DATABASE))) {
        private enum enumMixinStr_KEY_DATABASE = `enum KEY_DATABASE = 0x1aa;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_DATABASE); }))) {
            mixin(enumMixinStr_KEY_DATABASE);
        }
    }




    static if(!is(typeof(KEY_NEWS))) {
        private enum enumMixinStr_KEY_NEWS = `enum KEY_NEWS = 0x1ab;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_NEWS); }))) {
            mixin(enumMixinStr_KEY_NEWS);
        }
    }




    static if(!is(typeof(KEY_VOICEMAIL))) {
        private enum enumMixinStr_KEY_VOICEMAIL = `enum KEY_VOICEMAIL = 0x1ac;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_VOICEMAIL); }))) {
            mixin(enumMixinStr_KEY_VOICEMAIL);
        }
    }




    static if(!is(typeof(KEY_ADDRESSBOOK))) {
        private enum enumMixinStr_KEY_ADDRESSBOOK = `enum KEY_ADDRESSBOOK = 0x1ad;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_ADDRESSBOOK); }))) {
            mixin(enumMixinStr_KEY_ADDRESSBOOK);
        }
    }




    static if(!is(typeof(KEY_MESSENGER))) {
        private enum enumMixinStr_KEY_MESSENGER = `enum KEY_MESSENGER = 0x1ae;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_MESSENGER); }))) {
            mixin(enumMixinStr_KEY_MESSENGER);
        }
    }




    static if(!is(typeof(KEY_DISPLAYTOGGLE))) {
        private enum enumMixinStr_KEY_DISPLAYTOGGLE = `enum KEY_DISPLAYTOGGLE = 0x1af;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_DISPLAYTOGGLE); }))) {
            mixin(enumMixinStr_KEY_DISPLAYTOGGLE);
        }
    }




    static if(!is(typeof(KEY_BRIGHTNESS_TOGGLE))) {
        private enum enumMixinStr_KEY_BRIGHTNESS_TOGGLE = `enum KEY_BRIGHTNESS_TOGGLE = 0x1af;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_BRIGHTNESS_TOGGLE); }))) {
            mixin(enumMixinStr_KEY_BRIGHTNESS_TOGGLE);
        }
    }




    static if(!is(typeof(KEY_SPELLCHECK))) {
        private enum enumMixinStr_KEY_SPELLCHECK = `enum KEY_SPELLCHECK = 0x1b0;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_SPELLCHECK); }))) {
            mixin(enumMixinStr_KEY_SPELLCHECK);
        }
    }




    static if(!is(typeof(KEY_LOGOFF))) {
        private enum enumMixinStr_KEY_LOGOFF = `enum KEY_LOGOFF = 0x1b1;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_LOGOFF); }))) {
            mixin(enumMixinStr_KEY_LOGOFF);
        }
    }




    static if(!is(typeof(KEY_DOLLAR))) {
        private enum enumMixinStr_KEY_DOLLAR = `enum KEY_DOLLAR = 0x1b2;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_DOLLAR); }))) {
            mixin(enumMixinStr_KEY_DOLLAR);
        }
    }




    static if(!is(typeof(KEY_EURO))) {
        private enum enumMixinStr_KEY_EURO = `enum KEY_EURO = 0x1b3;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_EURO); }))) {
            mixin(enumMixinStr_KEY_EURO);
        }
    }




    static if(!is(typeof(KEY_FRAMEBACK))) {
        private enum enumMixinStr_KEY_FRAMEBACK = `enum KEY_FRAMEBACK = 0x1b4;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_FRAMEBACK); }))) {
            mixin(enumMixinStr_KEY_FRAMEBACK);
        }
    }




    static if(!is(typeof(KEY_FRAMEFORWARD))) {
        private enum enumMixinStr_KEY_FRAMEFORWARD = `enum KEY_FRAMEFORWARD = 0x1b5;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_FRAMEFORWARD); }))) {
            mixin(enumMixinStr_KEY_FRAMEFORWARD);
        }
    }




    static if(!is(typeof(KEY_CONTEXT_MENU))) {
        private enum enumMixinStr_KEY_CONTEXT_MENU = `enum KEY_CONTEXT_MENU = 0x1b6;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_CONTEXT_MENU); }))) {
            mixin(enumMixinStr_KEY_CONTEXT_MENU);
        }
    }




    static if(!is(typeof(KEY_MEDIA_REPEAT))) {
        private enum enumMixinStr_KEY_MEDIA_REPEAT = `enum KEY_MEDIA_REPEAT = 0x1b7;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_MEDIA_REPEAT); }))) {
            mixin(enumMixinStr_KEY_MEDIA_REPEAT);
        }
    }




    static if(!is(typeof(KEY_10CHANNELSUP))) {
        private enum enumMixinStr_KEY_10CHANNELSUP = `enum KEY_10CHANNELSUP = 0x1b8;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_10CHANNELSUP); }))) {
            mixin(enumMixinStr_KEY_10CHANNELSUP);
        }
    }




    static if(!is(typeof(KEY_10CHANNELSDOWN))) {
        private enum enumMixinStr_KEY_10CHANNELSDOWN = `enum KEY_10CHANNELSDOWN = 0x1b9;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_10CHANNELSDOWN); }))) {
            mixin(enumMixinStr_KEY_10CHANNELSDOWN);
        }
    }




    static if(!is(typeof(KEY_IMAGES))) {
        private enum enumMixinStr_KEY_IMAGES = `enum KEY_IMAGES = 0x1ba;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_IMAGES); }))) {
            mixin(enumMixinStr_KEY_IMAGES);
        }
    }




    static if(!is(typeof(KEY_DEL_EOL))) {
        private enum enumMixinStr_KEY_DEL_EOL = `enum KEY_DEL_EOL = 0x1c0;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_DEL_EOL); }))) {
            mixin(enumMixinStr_KEY_DEL_EOL);
        }
    }




    static if(!is(typeof(KEY_DEL_EOS))) {
        private enum enumMixinStr_KEY_DEL_EOS = `enum KEY_DEL_EOS = 0x1c1;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_DEL_EOS); }))) {
            mixin(enumMixinStr_KEY_DEL_EOS);
        }
    }




    static if(!is(typeof(KEY_INS_LINE))) {
        private enum enumMixinStr_KEY_INS_LINE = `enum KEY_INS_LINE = 0x1c2;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_INS_LINE); }))) {
            mixin(enumMixinStr_KEY_INS_LINE);
        }
    }




    static if(!is(typeof(KEY_DEL_LINE))) {
        private enum enumMixinStr_KEY_DEL_LINE = `enum KEY_DEL_LINE = 0x1c3;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_DEL_LINE); }))) {
            mixin(enumMixinStr_KEY_DEL_LINE);
        }
    }




    static if(!is(typeof(KEY_FN))) {
        private enum enumMixinStr_KEY_FN = `enum KEY_FN = 0x1d0;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_FN); }))) {
            mixin(enumMixinStr_KEY_FN);
        }
    }




    static if(!is(typeof(KEY_FN_ESC))) {
        private enum enumMixinStr_KEY_FN_ESC = `enum KEY_FN_ESC = 0x1d1;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_FN_ESC); }))) {
            mixin(enumMixinStr_KEY_FN_ESC);
        }
    }




    static if(!is(typeof(KEY_FN_F1))) {
        private enum enumMixinStr_KEY_FN_F1 = `enum KEY_FN_F1 = 0x1d2;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_FN_F1); }))) {
            mixin(enumMixinStr_KEY_FN_F1);
        }
    }




    static if(!is(typeof(KEY_FN_F2))) {
        private enum enumMixinStr_KEY_FN_F2 = `enum KEY_FN_F2 = 0x1d3;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_FN_F2); }))) {
            mixin(enumMixinStr_KEY_FN_F2);
        }
    }




    static if(!is(typeof(KEY_FN_F3))) {
        private enum enumMixinStr_KEY_FN_F3 = `enum KEY_FN_F3 = 0x1d4;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_FN_F3); }))) {
            mixin(enumMixinStr_KEY_FN_F3);
        }
    }




    static if(!is(typeof(KEY_FN_F4))) {
        private enum enumMixinStr_KEY_FN_F4 = `enum KEY_FN_F4 = 0x1d5;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_FN_F4); }))) {
            mixin(enumMixinStr_KEY_FN_F4);
        }
    }




    static if(!is(typeof(KEY_FN_F5))) {
        private enum enumMixinStr_KEY_FN_F5 = `enum KEY_FN_F5 = 0x1d6;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_FN_F5); }))) {
            mixin(enumMixinStr_KEY_FN_F5);
        }
    }




    static if(!is(typeof(KEY_FN_F6))) {
        private enum enumMixinStr_KEY_FN_F6 = `enum KEY_FN_F6 = 0x1d7;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_FN_F6); }))) {
            mixin(enumMixinStr_KEY_FN_F6);
        }
    }




    static if(!is(typeof(KEY_FN_F7))) {
        private enum enumMixinStr_KEY_FN_F7 = `enum KEY_FN_F7 = 0x1d8;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_FN_F7); }))) {
            mixin(enumMixinStr_KEY_FN_F7);
        }
    }




    static if(!is(typeof(KEY_FN_F8))) {
        private enum enumMixinStr_KEY_FN_F8 = `enum KEY_FN_F8 = 0x1d9;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_FN_F8); }))) {
            mixin(enumMixinStr_KEY_FN_F8);
        }
    }




    static if(!is(typeof(KEY_FN_F9))) {
        private enum enumMixinStr_KEY_FN_F9 = `enum KEY_FN_F9 = 0x1da;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_FN_F9); }))) {
            mixin(enumMixinStr_KEY_FN_F9);
        }
    }




    static if(!is(typeof(KEY_FN_F10))) {
        private enum enumMixinStr_KEY_FN_F10 = `enum KEY_FN_F10 = 0x1db;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_FN_F10); }))) {
            mixin(enumMixinStr_KEY_FN_F10);
        }
    }




    static if(!is(typeof(KEY_FN_F11))) {
        private enum enumMixinStr_KEY_FN_F11 = `enum KEY_FN_F11 = 0x1dc;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_FN_F11); }))) {
            mixin(enumMixinStr_KEY_FN_F11);
        }
    }




    static if(!is(typeof(KEY_FN_F12))) {
        private enum enumMixinStr_KEY_FN_F12 = `enum KEY_FN_F12 = 0x1dd;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_FN_F12); }))) {
            mixin(enumMixinStr_KEY_FN_F12);
        }
    }




    static if(!is(typeof(KEY_FN_1))) {
        private enum enumMixinStr_KEY_FN_1 = `enum KEY_FN_1 = 0x1de;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_FN_1); }))) {
            mixin(enumMixinStr_KEY_FN_1);
        }
    }




    static if(!is(typeof(KEY_FN_2))) {
        private enum enumMixinStr_KEY_FN_2 = `enum KEY_FN_2 = 0x1df;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_FN_2); }))) {
            mixin(enumMixinStr_KEY_FN_2);
        }
    }




    static if(!is(typeof(KEY_FN_D))) {
        private enum enumMixinStr_KEY_FN_D = `enum KEY_FN_D = 0x1e0;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_FN_D); }))) {
            mixin(enumMixinStr_KEY_FN_D);
        }
    }




    static if(!is(typeof(KEY_FN_E))) {
        private enum enumMixinStr_KEY_FN_E = `enum KEY_FN_E = 0x1e1;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_FN_E); }))) {
            mixin(enumMixinStr_KEY_FN_E);
        }
    }




    static if(!is(typeof(KEY_FN_F))) {
        private enum enumMixinStr_KEY_FN_F = `enum KEY_FN_F = 0x1e2;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_FN_F); }))) {
            mixin(enumMixinStr_KEY_FN_F);
        }
    }




    static if(!is(typeof(KEY_FN_S))) {
        private enum enumMixinStr_KEY_FN_S = `enum KEY_FN_S = 0x1e3;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_FN_S); }))) {
            mixin(enumMixinStr_KEY_FN_S);
        }
    }




    static if(!is(typeof(KEY_FN_B))) {
        private enum enumMixinStr_KEY_FN_B = `enum KEY_FN_B = 0x1e4;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_FN_B); }))) {
            mixin(enumMixinStr_KEY_FN_B);
        }
    }




    static if(!is(typeof(KEY_BRL_DOT1))) {
        private enum enumMixinStr_KEY_BRL_DOT1 = `enum KEY_BRL_DOT1 = 0x1f1;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_BRL_DOT1); }))) {
            mixin(enumMixinStr_KEY_BRL_DOT1);
        }
    }




    static if(!is(typeof(KEY_BRL_DOT2))) {
        private enum enumMixinStr_KEY_BRL_DOT2 = `enum KEY_BRL_DOT2 = 0x1f2;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_BRL_DOT2); }))) {
            mixin(enumMixinStr_KEY_BRL_DOT2);
        }
    }




    static if(!is(typeof(KEY_BRL_DOT3))) {
        private enum enumMixinStr_KEY_BRL_DOT3 = `enum KEY_BRL_DOT3 = 0x1f3;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_BRL_DOT3); }))) {
            mixin(enumMixinStr_KEY_BRL_DOT3);
        }
    }




    static if(!is(typeof(KEY_BRL_DOT4))) {
        private enum enumMixinStr_KEY_BRL_DOT4 = `enum KEY_BRL_DOT4 = 0x1f4;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_BRL_DOT4); }))) {
            mixin(enumMixinStr_KEY_BRL_DOT4);
        }
    }




    static if(!is(typeof(KEY_BRL_DOT5))) {
        private enum enumMixinStr_KEY_BRL_DOT5 = `enum KEY_BRL_DOT5 = 0x1f5;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_BRL_DOT5); }))) {
            mixin(enumMixinStr_KEY_BRL_DOT5);
        }
    }




    static if(!is(typeof(KEY_BRL_DOT6))) {
        private enum enumMixinStr_KEY_BRL_DOT6 = `enum KEY_BRL_DOT6 = 0x1f6;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_BRL_DOT6); }))) {
            mixin(enumMixinStr_KEY_BRL_DOT6);
        }
    }




    static if(!is(typeof(KEY_BRL_DOT7))) {
        private enum enumMixinStr_KEY_BRL_DOT7 = `enum KEY_BRL_DOT7 = 0x1f7;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_BRL_DOT7); }))) {
            mixin(enumMixinStr_KEY_BRL_DOT7);
        }
    }




    static if(!is(typeof(KEY_BRL_DOT8))) {
        private enum enumMixinStr_KEY_BRL_DOT8 = `enum KEY_BRL_DOT8 = 0x1f8;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_BRL_DOT8); }))) {
            mixin(enumMixinStr_KEY_BRL_DOT8);
        }
    }




    static if(!is(typeof(KEY_BRL_DOT9))) {
        private enum enumMixinStr_KEY_BRL_DOT9 = `enum KEY_BRL_DOT9 = 0x1f9;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_BRL_DOT9); }))) {
            mixin(enumMixinStr_KEY_BRL_DOT9);
        }
    }




    static if(!is(typeof(KEY_BRL_DOT10))) {
        private enum enumMixinStr_KEY_BRL_DOT10 = `enum KEY_BRL_DOT10 = 0x1fa;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_BRL_DOT10); }))) {
            mixin(enumMixinStr_KEY_BRL_DOT10);
        }
    }




    static if(!is(typeof(KEY_NUMERIC_0))) {
        private enum enumMixinStr_KEY_NUMERIC_0 = `enum KEY_NUMERIC_0 = 0x200;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_NUMERIC_0); }))) {
            mixin(enumMixinStr_KEY_NUMERIC_0);
        }
    }




    static if(!is(typeof(KEY_NUMERIC_1))) {
        private enum enumMixinStr_KEY_NUMERIC_1 = `enum KEY_NUMERIC_1 = 0x201;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_NUMERIC_1); }))) {
            mixin(enumMixinStr_KEY_NUMERIC_1);
        }
    }




    static if(!is(typeof(KEY_NUMERIC_2))) {
        private enum enumMixinStr_KEY_NUMERIC_2 = `enum KEY_NUMERIC_2 = 0x202;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_NUMERIC_2); }))) {
            mixin(enumMixinStr_KEY_NUMERIC_2);
        }
    }




    static if(!is(typeof(KEY_NUMERIC_3))) {
        private enum enumMixinStr_KEY_NUMERIC_3 = `enum KEY_NUMERIC_3 = 0x203;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_NUMERIC_3); }))) {
            mixin(enumMixinStr_KEY_NUMERIC_3);
        }
    }




    static if(!is(typeof(KEY_NUMERIC_4))) {
        private enum enumMixinStr_KEY_NUMERIC_4 = `enum KEY_NUMERIC_4 = 0x204;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_NUMERIC_4); }))) {
            mixin(enumMixinStr_KEY_NUMERIC_4);
        }
    }




    static if(!is(typeof(KEY_NUMERIC_5))) {
        private enum enumMixinStr_KEY_NUMERIC_5 = `enum KEY_NUMERIC_5 = 0x205;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_NUMERIC_5); }))) {
            mixin(enumMixinStr_KEY_NUMERIC_5);
        }
    }




    static if(!is(typeof(KEY_NUMERIC_6))) {
        private enum enumMixinStr_KEY_NUMERIC_6 = `enum KEY_NUMERIC_6 = 0x206;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_NUMERIC_6); }))) {
            mixin(enumMixinStr_KEY_NUMERIC_6);
        }
    }




    static if(!is(typeof(KEY_NUMERIC_7))) {
        private enum enumMixinStr_KEY_NUMERIC_7 = `enum KEY_NUMERIC_7 = 0x207;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_NUMERIC_7); }))) {
            mixin(enumMixinStr_KEY_NUMERIC_7);
        }
    }




    static if(!is(typeof(KEY_NUMERIC_8))) {
        private enum enumMixinStr_KEY_NUMERIC_8 = `enum KEY_NUMERIC_8 = 0x208;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_NUMERIC_8); }))) {
            mixin(enumMixinStr_KEY_NUMERIC_8);
        }
    }




    static if(!is(typeof(KEY_NUMERIC_9))) {
        private enum enumMixinStr_KEY_NUMERIC_9 = `enum KEY_NUMERIC_9 = 0x209;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_NUMERIC_9); }))) {
            mixin(enumMixinStr_KEY_NUMERIC_9);
        }
    }




    static if(!is(typeof(KEY_NUMERIC_STAR))) {
        private enum enumMixinStr_KEY_NUMERIC_STAR = `enum KEY_NUMERIC_STAR = 0x20a;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_NUMERIC_STAR); }))) {
            mixin(enumMixinStr_KEY_NUMERIC_STAR);
        }
    }




    static if(!is(typeof(KEY_NUMERIC_POUND))) {
        private enum enumMixinStr_KEY_NUMERIC_POUND = `enum KEY_NUMERIC_POUND = 0x20b;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_NUMERIC_POUND); }))) {
            mixin(enumMixinStr_KEY_NUMERIC_POUND);
        }
    }




    static if(!is(typeof(KEY_NUMERIC_A))) {
        private enum enumMixinStr_KEY_NUMERIC_A = `enum KEY_NUMERIC_A = 0x20c;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_NUMERIC_A); }))) {
            mixin(enumMixinStr_KEY_NUMERIC_A);
        }
    }




    static if(!is(typeof(KEY_NUMERIC_B))) {
        private enum enumMixinStr_KEY_NUMERIC_B = `enum KEY_NUMERIC_B = 0x20d;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_NUMERIC_B); }))) {
            mixin(enumMixinStr_KEY_NUMERIC_B);
        }
    }




    static if(!is(typeof(KEY_NUMERIC_C))) {
        private enum enumMixinStr_KEY_NUMERIC_C = `enum KEY_NUMERIC_C = 0x20e;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_NUMERIC_C); }))) {
            mixin(enumMixinStr_KEY_NUMERIC_C);
        }
    }




    static if(!is(typeof(KEY_NUMERIC_D))) {
        private enum enumMixinStr_KEY_NUMERIC_D = `enum KEY_NUMERIC_D = 0x20f;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_NUMERIC_D); }))) {
            mixin(enumMixinStr_KEY_NUMERIC_D);
        }
    }




    static if(!is(typeof(KEY_CAMERA_FOCUS))) {
        private enum enumMixinStr_KEY_CAMERA_FOCUS = `enum KEY_CAMERA_FOCUS = 0x210;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_CAMERA_FOCUS); }))) {
            mixin(enumMixinStr_KEY_CAMERA_FOCUS);
        }
    }




    static if(!is(typeof(KEY_WPS_BUTTON))) {
        private enum enumMixinStr_KEY_WPS_BUTTON = `enum KEY_WPS_BUTTON = 0x211;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_WPS_BUTTON); }))) {
            mixin(enumMixinStr_KEY_WPS_BUTTON);
        }
    }




    static if(!is(typeof(KEY_TOUCHPAD_TOGGLE))) {
        private enum enumMixinStr_KEY_TOUCHPAD_TOGGLE = `enum KEY_TOUCHPAD_TOGGLE = 0x212;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_TOUCHPAD_TOGGLE); }))) {
            mixin(enumMixinStr_KEY_TOUCHPAD_TOGGLE);
        }
    }




    static if(!is(typeof(KEY_TOUCHPAD_ON))) {
        private enum enumMixinStr_KEY_TOUCHPAD_ON = `enum KEY_TOUCHPAD_ON = 0x213;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_TOUCHPAD_ON); }))) {
            mixin(enumMixinStr_KEY_TOUCHPAD_ON);
        }
    }




    static if(!is(typeof(KEY_TOUCHPAD_OFF))) {
        private enum enumMixinStr_KEY_TOUCHPAD_OFF = `enum KEY_TOUCHPAD_OFF = 0x214;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_TOUCHPAD_OFF); }))) {
            mixin(enumMixinStr_KEY_TOUCHPAD_OFF);
        }
    }




    static if(!is(typeof(KEY_CAMERA_ZOOMIN))) {
        private enum enumMixinStr_KEY_CAMERA_ZOOMIN = `enum KEY_CAMERA_ZOOMIN = 0x215;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_CAMERA_ZOOMIN); }))) {
            mixin(enumMixinStr_KEY_CAMERA_ZOOMIN);
        }
    }




    static if(!is(typeof(KEY_CAMERA_ZOOMOUT))) {
        private enum enumMixinStr_KEY_CAMERA_ZOOMOUT = `enum KEY_CAMERA_ZOOMOUT = 0x216;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_CAMERA_ZOOMOUT); }))) {
            mixin(enumMixinStr_KEY_CAMERA_ZOOMOUT);
        }
    }




    static if(!is(typeof(KEY_CAMERA_UP))) {
        private enum enumMixinStr_KEY_CAMERA_UP = `enum KEY_CAMERA_UP = 0x217;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_CAMERA_UP); }))) {
            mixin(enumMixinStr_KEY_CAMERA_UP);
        }
    }




    static if(!is(typeof(KEY_CAMERA_DOWN))) {
        private enum enumMixinStr_KEY_CAMERA_DOWN = `enum KEY_CAMERA_DOWN = 0x218;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_CAMERA_DOWN); }))) {
            mixin(enumMixinStr_KEY_CAMERA_DOWN);
        }
    }




    static if(!is(typeof(KEY_CAMERA_LEFT))) {
        private enum enumMixinStr_KEY_CAMERA_LEFT = `enum KEY_CAMERA_LEFT = 0x219;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_CAMERA_LEFT); }))) {
            mixin(enumMixinStr_KEY_CAMERA_LEFT);
        }
    }




    static if(!is(typeof(KEY_CAMERA_RIGHT))) {
        private enum enumMixinStr_KEY_CAMERA_RIGHT = `enum KEY_CAMERA_RIGHT = 0x21a;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_CAMERA_RIGHT); }))) {
            mixin(enumMixinStr_KEY_CAMERA_RIGHT);
        }
    }




    static if(!is(typeof(KEY_ATTENDANT_ON))) {
        private enum enumMixinStr_KEY_ATTENDANT_ON = `enum KEY_ATTENDANT_ON = 0x21b;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_ATTENDANT_ON); }))) {
            mixin(enumMixinStr_KEY_ATTENDANT_ON);
        }
    }




    static if(!is(typeof(KEY_ATTENDANT_OFF))) {
        private enum enumMixinStr_KEY_ATTENDANT_OFF = `enum KEY_ATTENDANT_OFF = 0x21c;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_ATTENDANT_OFF); }))) {
            mixin(enumMixinStr_KEY_ATTENDANT_OFF);
        }
    }




    static if(!is(typeof(KEY_ATTENDANT_TOGGLE))) {
        private enum enumMixinStr_KEY_ATTENDANT_TOGGLE = `enum KEY_ATTENDANT_TOGGLE = 0x21d;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_ATTENDANT_TOGGLE); }))) {
            mixin(enumMixinStr_KEY_ATTENDANT_TOGGLE);
        }
    }




    static if(!is(typeof(KEY_LIGHTS_TOGGLE))) {
        private enum enumMixinStr_KEY_LIGHTS_TOGGLE = `enum KEY_LIGHTS_TOGGLE = 0x21e;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_LIGHTS_TOGGLE); }))) {
            mixin(enumMixinStr_KEY_LIGHTS_TOGGLE);
        }
    }




    static if(!is(typeof(BTN_DPAD_UP))) {
        private enum enumMixinStr_BTN_DPAD_UP = `enum BTN_DPAD_UP = 0x220;`;
        static if(is(typeof({ mixin(enumMixinStr_BTN_DPAD_UP); }))) {
            mixin(enumMixinStr_BTN_DPAD_UP);
        }
    }




    static if(!is(typeof(BTN_DPAD_DOWN))) {
        private enum enumMixinStr_BTN_DPAD_DOWN = `enum BTN_DPAD_DOWN = 0x221;`;
        static if(is(typeof({ mixin(enumMixinStr_BTN_DPAD_DOWN); }))) {
            mixin(enumMixinStr_BTN_DPAD_DOWN);
        }
    }




    static if(!is(typeof(BTN_DPAD_LEFT))) {
        private enum enumMixinStr_BTN_DPAD_LEFT = `enum BTN_DPAD_LEFT = 0x222;`;
        static if(is(typeof({ mixin(enumMixinStr_BTN_DPAD_LEFT); }))) {
            mixin(enumMixinStr_BTN_DPAD_LEFT);
        }
    }




    static if(!is(typeof(BTN_DPAD_RIGHT))) {
        private enum enumMixinStr_BTN_DPAD_RIGHT = `enum BTN_DPAD_RIGHT = 0x223;`;
        static if(is(typeof({ mixin(enumMixinStr_BTN_DPAD_RIGHT); }))) {
            mixin(enumMixinStr_BTN_DPAD_RIGHT);
        }
    }




    static if(!is(typeof(KEY_ALS_TOGGLE))) {
        private enum enumMixinStr_KEY_ALS_TOGGLE = `enum KEY_ALS_TOGGLE = 0x230;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_ALS_TOGGLE); }))) {
            mixin(enumMixinStr_KEY_ALS_TOGGLE);
        }
    }




    static if(!is(typeof(KEY_ROTATE_LOCK_TOGGLE))) {
        private enum enumMixinStr_KEY_ROTATE_LOCK_TOGGLE = `enum KEY_ROTATE_LOCK_TOGGLE = 0x231;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_ROTATE_LOCK_TOGGLE); }))) {
            mixin(enumMixinStr_KEY_ROTATE_LOCK_TOGGLE);
        }
    }




    static if(!is(typeof(KEY_BUTTONCONFIG))) {
        private enum enumMixinStr_KEY_BUTTONCONFIG = `enum KEY_BUTTONCONFIG = 0x240;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_BUTTONCONFIG); }))) {
            mixin(enumMixinStr_KEY_BUTTONCONFIG);
        }
    }




    static if(!is(typeof(KEY_TASKMANAGER))) {
        private enum enumMixinStr_KEY_TASKMANAGER = `enum KEY_TASKMANAGER = 0x241;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_TASKMANAGER); }))) {
            mixin(enumMixinStr_KEY_TASKMANAGER);
        }
    }




    static if(!is(typeof(KEY_JOURNAL))) {
        private enum enumMixinStr_KEY_JOURNAL = `enum KEY_JOURNAL = 0x242;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_JOURNAL); }))) {
            mixin(enumMixinStr_KEY_JOURNAL);
        }
    }




    static if(!is(typeof(KEY_CONTROLPANEL))) {
        private enum enumMixinStr_KEY_CONTROLPANEL = `enum KEY_CONTROLPANEL = 0x243;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_CONTROLPANEL); }))) {
            mixin(enumMixinStr_KEY_CONTROLPANEL);
        }
    }




    static if(!is(typeof(KEY_APPSELECT))) {
        private enum enumMixinStr_KEY_APPSELECT = `enum KEY_APPSELECT = 0x244;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_APPSELECT); }))) {
            mixin(enumMixinStr_KEY_APPSELECT);
        }
    }




    static if(!is(typeof(KEY_SCREENSAVER))) {
        private enum enumMixinStr_KEY_SCREENSAVER = `enum KEY_SCREENSAVER = 0x245;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_SCREENSAVER); }))) {
            mixin(enumMixinStr_KEY_SCREENSAVER);
        }
    }




    static if(!is(typeof(KEY_VOICECOMMAND))) {
        private enum enumMixinStr_KEY_VOICECOMMAND = `enum KEY_VOICECOMMAND = 0x246;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_VOICECOMMAND); }))) {
            mixin(enumMixinStr_KEY_VOICECOMMAND);
        }
    }




    static if(!is(typeof(KEY_ASSISTANT))) {
        private enum enumMixinStr_KEY_ASSISTANT = `enum KEY_ASSISTANT = 0x247;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_ASSISTANT); }))) {
            mixin(enumMixinStr_KEY_ASSISTANT);
        }
    }




    static if(!is(typeof(KEY_KBD_LAYOUT_NEXT))) {
        private enum enumMixinStr_KEY_KBD_LAYOUT_NEXT = `enum KEY_KBD_LAYOUT_NEXT = 0x248;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_KBD_LAYOUT_NEXT); }))) {
            mixin(enumMixinStr_KEY_KBD_LAYOUT_NEXT);
        }
    }




    static if(!is(typeof(KEY_BRIGHTNESS_MIN))) {
        private enum enumMixinStr_KEY_BRIGHTNESS_MIN = `enum KEY_BRIGHTNESS_MIN = 0x250;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_BRIGHTNESS_MIN); }))) {
            mixin(enumMixinStr_KEY_BRIGHTNESS_MIN);
        }
    }




    static if(!is(typeof(KEY_BRIGHTNESS_MAX))) {
        private enum enumMixinStr_KEY_BRIGHTNESS_MAX = `enum KEY_BRIGHTNESS_MAX = 0x251;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_BRIGHTNESS_MAX); }))) {
            mixin(enumMixinStr_KEY_BRIGHTNESS_MAX);
        }
    }




    static if(!is(typeof(KEY_KBDINPUTASSIST_PREV))) {
        private enum enumMixinStr_KEY_KBDINPUTASSIST_PREV = `enum KEY_KBDINPUTASSIST_PREV = 0x260;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_KBDINPUTASSIST_PREV); }))) {
            mixin(enumMixinStr_KEY_KBDINPUTASSIST_PREV);
        }
    }




    static if(!is(typeof(KEY_KBDINPUTASSIST_NEXT))) {
        private enum enumMixinStr_KEY_KBDINPUTASSIST_NEXT = `enum KEY_KBDINPUTASSIST_NEXT = 0x261;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_KBDINPUTASSIST_NEXT); }))) {
            mixin(enumMixinStr_KEY_KBDINPUTASSIST_NEXT);
        }
    }




    static if(!is(typeof(KEY_KBDINPUTASSIST_PREVGROUP))) {
        private enum enumMixinStr_KEY_KBDINPUTASSIST_PREVGROUP = `enum KEY_KBDINPUTASSIST_PREVGROUP = 0x262;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_KBDINPUTASSIST_PREVGROUP); }))) {
            mixin(enumMixinStr_KEY_KBDINPUTASSIST_PREVGROUP);
        }
    }




    static if(!is(typeof(KEY_KBDINPUTASSIST_NEXTGROUP))) {
        private enum enumMixinStr_KEY_KBDINPUTASSIST_NEXTGROUP = `enum KEY_KBDINPUTASSIST_NEXTGROUP = 0x263;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_KBDINPUTASSIST_NEXTGROUP); }))) {
            mixin(enumMixinStr_KEY_KBDINPUTASSIST_NEXTGROUP);
        }
    }




    static if(!is(typeof(KEY_KBDINPUTASSIST_ACCEPT))) {
        private enum enumMixinStr_KEY_KBDINPUTASSIST_ACCEPT = `enum KEY_KBDINPUTASSIST_ACCEPT = 0x264;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_KBDINPUTASSIST_ACCEPT); }))) {
            mixin(enumMixinStr_KEY_KBDINPUTASSIST_ACCEPT);
        }
    }




    static if(!is(typeof(KEY_KBDINPUTASSIST_CANCEL))) {
        private enum enumMixinStr_KEY_KBDINPUTASSIST_CANCEL = `enum KEY_KBDINPUTASSIST_CANCEL = 0x265;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_KBDINPUTASSIST_CANCEL); }))) {
            mixin(enumMixinStr_KEY_KBDINPUTASSIST_CANCEL);
        }
    }




    static if(!is(typeof(KEY_RIGHT_UP))) {
        private enum enumMixinStr_KEY_RIGHT_UP = `enum KEY_RIGHT_UP = 0x266;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_RIGHT_UP); }))) {
            mixin(enumMixinStr_KEY_RIGHT_UP);
        }
    }




    static if(!is(typeof(KEY_RIGHT_DOWN))) {
        private enum enumMixinStr_KEY_RIGHT_DOWN = `enum KEY_RIGHT_DOWN = 0x267;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_RIGHT_DOWN); }))) {
            mixin(enumMixinStr_KEY_RIGHT_DOWN);
        }
    }




    static if(!is(typeof(KEY_LEFT_UP))) {
        private enum enumMixinStr_KEY_LEFT_UP = `enum KEY_LEFT_UP = 0x268;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_LEFT_UP); }))) {
            mixin(enumMixinStr_KEY_LEFT_UP);
        }
    }




    static if(!is(typeof(KEY_LEFT_DOWN))) {
        private enum enumMixinStr_KEY_LEFT_DOWN = `enum KEY_LEFT_DOWN = 0x269;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_LEFT_DOWN); }))) {
            mixin(enumMixinStr_KEY_LEFT_DOWN);
        }
    }




    static if(!is(typeof(KEY_ROOT_MENU))) {
        private enum enumMixinStr_KEY_ROOT_MENU = `enum KEY_ROOT_MENU = 0x26a;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_ROOT_MENU); }))) {
            mixin(enumMixinStr_KEY_ROOT_MENU);
        }
    }




    static if(!is(typeof(KEY_MEDIA_TOP_MENU))) {
        private enum enumMixinStr_KEY_MEDIA_TOP_MENU = `enum KEY_MEDIA_TOP_MENU = 0x26b;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_MEDIA_TOP_MENU); }))) {
            mixin(enumMixinStr_KEY_MEDIA_TOP_MENU);
        }
    }




    static if(!is(typeof(KEY_NUMERIC_11))) {
        private enum enumMixinStr_KEY_NUMERIC_11 = `enum KEY_NUMERIC_11 = 0x26c;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_NUMERIC_11); }))) {
            mixin(enumMixinStr_KEY_NUMERIC_11);
        }
    }




    static if(!is(typeof(KEY_NUMERIC_12))) {
        private enum enumMixinStr_KEY_NUMERIC_12 = `enum KEY_NUMERIC_12 = 0x26d;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_NUMERIC_12); }))) {
            mixin(enumMixinStr_KEY_NUMERIC_12);
        }
    }




    static if(!is(typeof(KEY_AUDIO_DESC))) {
        private enum enumMixinStr_KEY_AUDIO_DESC = `enum KEY_AUDIO_DESC = 0x26e;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_AUDIO_DESC); }))) {
            mixin(enumMixinStr_KEY_AUDIO_DESC);
        }
    }




    static if(!is(typeof(KEY_3D_MODE))) {
        private enum enumMixinStr_KEY_3D_MODE = `enum KEY_3D_MODE = 0x26f;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_3D_MODE); }))) {
            mixin(enumMixinStr_KEY_3D_MODE);
        }
    }




    static if(!is(typeof(KEY_NEXT_FAVORITE))) {
        private enum enumMixinStr_KEY_NEXT_FAVORITE = `enum KEY_NEXT_FAVORITE = 0x270;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_NEXT_FAVORITE); }))) {
            mixin(enumMixinStr_KEY_NEXT_FAVORITE);
        }
    }




    static if(!is(typeof(KEY_STOP_RECORD))) {
        private enum enumMixinStr_KEY_STOP_RECORD = `enum KEY_STOP_RECORD = 0x271;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_STOP_RECORD); }))) {
            mixin(enumMixinStr_KEY_STOP_RECORD);
        }
    }




    static if(!is(typeof(KEY_PAUSE_RECORD))) {
        private enum enumMixinStr_KEY_PAUSE_RECORD = `enum KEY_PAUSE_RECORD = 0x272;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_PAUSE_RECORD); }))) {
            mixin(enumMixinStr_KEY_PAUSE_RECORD);
        }
    }




    static if(!is(typeof(KEY_VOD))) {
        private enum enumMixinStr_KEY_VOD = `enum KEY_VOD = 0x273;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_VOD); }))) {
            mixin(enumMixinStr_KEY_VOD);
        }
    }




    static if(!is(typeof(KEY_UNMUTE))) {
        private enum enumMixinStr_KEY_UNMUTE = `enum KEY_UNMUTE = 0x274;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_UNMUTE); }))) {
            mixin(enumMixinStr_KEY_UNMUTE);
        }
    }




    static if(!is(typeof(KEY_FASTREVERSE))) {
        private enum enumMixinStr_KEY_FASTREVERSE = `enum KEY_FASTREVERSE = 0x275;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_FASTREVERSE); }))) {
            mixin(enumMixinStr_KEY_FASTREVERSE);
        }
    }




    static if(!is(typeof(KEY_SLOWREVERSE))) {
        private enum enumMixinStr_KEY_SLOWREVERSE = `enum KEY_SLOWREVERSE = 0x276;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_SLOWREVERSE); }))) {
            mixin(enumMixinStr_KEY_SLOWREVERSE);
        }
    }




    static if(!is(typeof(KEY_DATA))) {
        private enum enumMixinStr_KEY_DATA = `enum KEY_DATA = 0x277;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_DATA); }))) {
            mixin(enumMixinStr_KEY_DATA);
        }
    }




    static if(!is(typeof(KEY_ONSCREEN_KEYBOARD))) {
        private enum enumMixinStr_KEY_ONSCREEN_KEYBOARD = `enum KEY_ONSCREEN_KEYBOARD = 0x278;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_ONSCREEN_KEYBOARD); }))) {
            mixin(enumMixinStr_KEY_ONSCREEN_KEYBOARD);
        }
    }




    static if(!is(typeof(KEY_PRIVACY_SCREEN_TOGGLE))) {
        private enum enumMixinStr_KEY_PRIVACY_SCREEN_TOGGLE = `enum KEY_PRIVACY_SCREEN_TOGGLE = 0x279;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_PRIVACY_SCREEN_TOGGLE); }))) {
            mixin(enumMixinStr_KEY_PRIVACY_SCREEN_TOGGLE);
        }
    }




    static if(!is(typeof(KEY_SELECTIVE_SCREENSHOT))) {
        private enum enumMixinStr_KEY_SELECTIVE_SCREENSHOT = `enum KEY_SELECTIVE_SCREENSHOT = 0x27a;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_SELECTIVE_SCREENSHOT); }))) {
            mixin(enumMixinStr_KEY_SELECTIVE_SCREENSHOT);
        }
    }




    static if(!is(typeof(KEY_MACRO1))) {
        private enum enumMixinStr_KEY_MACRO1 = `enum KEY_MACRO1 = 0x290;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_MACRO1); }))) {
            mixin(enumMixinStr_KEY_MACRO1);
        }
    }




    static if(!is(typeof(KEY_MACRO2))) {
        private enum enumMixinStr_KEY_MACRO2 = `enum KEY_MACRO2 = 0x291;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_MACRO2); }))) {
            mixin(enumMixinStr_KEY_MACRO2);
        }
    }




    static if(!is(typeof(KEY_MACRO3))) {
        private enum enumMixinStr_KEY_MACRO3 = `enum KEY_MACRO3 = 0x292;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_MACRO3); }))) {
            mixin(enumMixinStr_KEY_MACRO3);
        }
    }




    static if(!is(typeof(KEY_MACRO4))) {
        private enum enumMixinStr_KEY_MACRO4 = `enum KEY_MACRO4 = 0x293;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_MACRO4); }))) {
            mixin(enumMixinStr_KEY_MACRO4);
        }
    }




    static if(!is(typeof(KEY_MACRO5))) {
        private enum enumMixinStr_KEY_MACRO5 = `enum KEY_MACRO5 = 0x294;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_MACRO5); }))) {
            mixin(enumMixinStr_KEY_MACRO5);
        }
    }




    static if(!is(typeof(KEY_MACRO6))) {
        private enum enumMixinStr_KEY_MACRO6 = `enum KEY_MACRO6 = 0x295;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_MACRO6); }))) {
            mixin(enumMixinStr_KEY_MACRO6);
        }
    }




    static if(!is(typeof(KEY_MACRO7))) {
        private enum enumMixinStr_KEY_MACRO7 = `enum KEY_MACRO7 = 0x296;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_MACRO7); }))) {
            mixin(enumMixinStr_KEY_MACRO7);
        }
    }




    static if(!is(typeof(KEY_MACRO8))) {
        private enum enumMixinStr_KEY_MACRO8 = `enum KEY_MACRO8 = 0x297;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_MACRO8); }))) {
            mixin(enumMixinStr_KEY_MACRO8);
        }
    }




    static if(!is(typeof(KEY_MACRO9))) {
        private enum enumMixinStr_KEY_MACRO9 = `enum KEY_MACRO9 = 0x298;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_MACRO9); }))) {
            mixin(enumMixinStr_KEY_MACRO9);
        }
    }




    static if(!is(typeof(KEY_MACRO10))) {
        private enum enumMixinStr_KEY_MACRO10 = `enum KEY_MACRO10 = 0x299;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_MACRO10); }))) {
            mixin(enumMixinStr_KEY_MACRO10);
        }
    }




    static if(!is(typeof(KEY_MACRO11))) {
        private enum enumMixinStr_KEY_MACRO11 = `enum KEY_MACRO11 = 0x29a;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_MACRO11); }))) {
            mixin(enumMixinStr_KEY_MACRO11);
        }
    }




    static if(!is(typeof(KEY_MACRO12))) {
        private enum enumMixinStr_KEY_MACRO12 = `enum KEY_MACRO12 = 0x29b;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_MACRO12); }))) {
            mixin(enumMixinStr_KEY_MACRO12);
        }
    }




    static if(!is(typeof(KEY_MACRO13))) {
        private enum enumMixinStr_KEY_MACRO13 = `enum KEY_MACRO13 = 0x29c;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_MACRO13); }))) {
            mixin(enumMixinStr_KEY_MACRO13);
        }
    }




    static if(!is(typeof(KEY_MACRO14))) {
        private enum enumMixinStr_KEY_MACRO14 = `enum KEY_MACRO14 = 0x29d;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_MACRO14); }))) {
            mixin(enumMixinStr_KEY_MACRO14);
        }
    }




    static if(!is(typeof(KEY_MACRO15))) {
        private enum enumMixinStr_KEY_MACRO15 = `enum KEY_MACRO15 = 0x29e;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_MACRO15); }))) {
            mixin(enumMixinStr_KEY_MACRO15);
        }
    }




    static if(!is(typeof(KEY_MACRO16))) {
        private enum enumMixinStr_KEY_MACRO16 = `enum KEY_MACRO16 = 0x29f;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_MACRO16); }))) {
            mixin(enumMixinStr_KEY_MACRO16);
        }
    }




    static if(!is(typeof(KEY_MACRO17))) {
        private enum enumMixinStr_KEY_MACRO17 = `enum KEY_MACRO17 = 0x2a0;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_MACRO17); }))) {
            mixin(enumMixinStr_KEY_MACRO17);
        }
    }




    static if(!is(typeof(KEY_MACRO18))) {
        private enum enumMixinStr_KEY_MACRO18 = `enum KEY_MACRO18 = 0x2a1;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_MACRO18); }))) {
            mixin(enumMixinStr_KEY_MACRO18);
        }
    }




    static if(!is(typeof(KEY_MACRO19))) {
        private enum enumMixinStr_KEY_MACRO19 = `enum KEY_MACRO19 = 0x2a2;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_MACRO19); }))) {
            mixin(enumMixinStr_KEY_MACRO19);
        }
    }




    static if(!is(typeof(KEY_MACRO20))) {
        private enum enumMixinStr_KEY_MACRO20 = `enum KEY_MACRO20 = 0x2a3;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_MACRO20); }))) {
            mixin(enumMixinStr_KEY_MACRO20);
        }
    }




    static if(!is(typeof(KEY_MACRO21))) {
        private enum enumMixinStr_KEY_MACRO21 = `enum KEY_MACRO21 = 0x2a4;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_MACRO21); }))) {
            mixin(enumMixinStr_KEY_MACRO21);
        }
    }




    static if(!is(typeof(KEY_MACRO22))) {
        private enum enumMixinStr_KEY_MACRO22 = `enum KEY_MACRO22 = 0x2a5;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_MACRO22); }))) {
            mixin(enumMixinStr_KEY_MACRO22);
        }
    }




    static if(!is(typeof(KEY_MACRO23))) {
        private enum enumMixinStr_KEY_MACRO23 = `enum KEY_MACRO23 = 0x2a6;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_MACRO23); }))) {
            mixin(enumMixinStr_KEY_MACRO23);
        }
    }




    static if(!is(typeof(KEY_MACRO24))) {
        private enum enumMixinStr_KEY_MACRO24 = `enum KEY_MACRO24 = 0x2a7;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_MACRO24); }))) {
            mixin(enumMixinStr_KEY_MACRO24);
        }
    }




    static if(!is(typeof(KEY_MACRO25))) {
        private enum enumMixinStr_KEY_MACRO25 = `enum KEY_MACRO25 = 0x2a8;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_MACRO25); }))) {
            mixin(enumMixinStr_KEY_MACRO25);
        }
    }




    static if(!is(typeof(KEY_MACRO26))) {
        private enum enumMixinStr_KEY_MACRO26 = `enum KEY_MACRO26 = 0x2a9;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_MACRO26); }))) {
            mixin(enumMixinStr_KEY_MACRO26);
        }
    }




    static if(!is(typeof(KEY_MACRO27))) {
        private enum enumMixinStr_KEY_MACRO27 = `enum KEY_MACRO27 = 0x2aa;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_MACRO27); }))) {
            mixin(enumMixinStr_KEY_MACRO27);
        }
    }




    static if(!is(typeof(KEY_MACRO28))) {
        private enum enumMixinStr_KEY_MACRO28 = `enum KEY_MACRO28 = 0x2ab;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_MACRO28); }))) {
            mixin(enumMixinStr_KEY_MACRO28);
        }
    }




    static if(!is(typeof(KEY_MACRO29))) {
        private enum enumMixinStr_KEY_MACRO29 = `enum KEY_MACRO29 = 0x2ac;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_MACRO29); }))) {
            mixin(enumMixinStr_KEY_MACRO29);
        }
    }




    static if(!is(typeof(KEY_MACRO30))) {
        private enum enumMixinStr_KEY_MACRO30 = `enum KEY_MACRO30 = 0x2ad;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_MACRO30); }))) {
            mixin(enumMixinStr_KEY_MACRO30);
        }
    }




    static if(!is(typeof(KEY_MACRO_RECORD_START))) {
        private enum enumMixinStr_KEY_MACRO_RECORD_START = `enum KEY_MACRO_RECORD_START = 0x2b0;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_MACRO_RECORD_START); }))) {
            mixin(enumMixinStr_KEY_MACRO_RECORD_START);
        }
    }




    static if(!is(typeof(KEY_MACRO_RECORD_STOP))) {
        private enum enumMixinStr_KEY_MACRO_RECORD_STOP = `enum KEY_MACRO_RECORD_STOP = 0x2b1;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_MACRO_RECORD_STOP); }))) {
            mixin(enumMixinStr_KEY_MACRO_RECORD_STOP);
        }
    }




    static if(!is(typeof(KEY_MACRO_PRESET_CYCLE))) {
        private enum enumMixinStr_KEY_MACRO_PRESET_CYCLE = `enum KEY_MACRO_PRESET_CYCLE = 0x2b2;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_MACRO_PRESET_CYCLE); }))) {
            mixin(enumMixinStr_KEY_MACRO_PRESET_CYCLE);
        }
    }




    static if(!is(typeof(KEY_MACRO_PRESET1))) {
        private enum enumMixinStr_KEY_MACRO_PRESET1 = `enum KEY_MACRO_PRESET1 = 0x2b3;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_MACRO_PRESET1); }))) {
            mixin(enumMixinStr_KEY_MACRO_PRESET1);
        }
    }




    static if(!is(typeof(KEY_MACRO_PRESET2))) {
        private enum enumMixinStr_KEY_MACRO_PRESET2 = `enum KEY_MACRO_PRESET2 = 0x2b4;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_MACRO_PRESET2); }))) {
            mixin(enumMixinStr_KEY_MACRO_PRESET2);
        }
    }




    static if(!is(typeof(KEY_MACRO_PRESET3))) {
        private enum enumMixinStr_KEY_MACRO_PRESET3 = `enum KEY_MACRO_PRESET3 = 0x2b5;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_MACRO_PRESET3); }))) {
            mixin(enumMixinStr_KEY_MACRO_PRESET3);
        }
    }




    static if(!is(typeof(KEY_KBD_LCD_MENU1))) {
        private enum enumMixinStr_KEY_KBD_LCD_MENU1 = `enum KEY_KBD_LCD_MENU1 = 0x2b8;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_KBD_LCD_MENU1); }))) {
            mixin(enumMixinStr_KEY_KBD_LCD_MENU1);
        }
    }




    static if(!is(typeof(KEY_KBD_LCD_MENU2))) {
        private enum enumMixinStr_KEY_KBD_LCD_MENU2 = `enum KEY_KBD_LCD_MENU2 = 0x2b9;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_KBD_LCD_MENU2); }))) {
            mixin(enumMixinStr_KEY_KBD_LCD_MENU2);
        }
    }




    static if(!is(typeof(KEY_KBD_LCD_MENU3))) {
        private enum enumMixinStr_KEY_KBD_LCD_MENU3 = `enum KEY_KBD_LCD_MENU3 = 0x2ba;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_KBD_LCD_MENU3); }))) {
            mixin(enumMixinStr_KEY_KBD_LCD_MENU3);
        }
    }




    static if(!is(typeof(KEY_KBD_LCD_MENU4))) {
        private enum enumMixinStr_KEY_KBD_LCD_MENU4 = `enum KEY_KBD_LCD_MENU4 = 0x2bb;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_KBD_LCD_MENU4); }))) {
            mixin(enumMixinStr_KEY_KBD_LCD_MENU4);
        }
    }




    static if(!is(typeof(KEY_KBD_LCD_MENU5))) {
        private enum enumMixinStr_KEY_KBD_LCD_MENU5 = `enum KEY_KBD_LCD_MENU5 = 0x2bc;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_KBD_LCD_MENU5); }))) {
            mixin(enumMixinStr_KEY_KBD_LCD_MENU5);
        }
    }




    static if(!is(typeof(BTN_TRIGGER_HAPPY))) {
        private enum enumMixinStr_BTN_TRIGGER_HAPPY = `enum BTN_TRIGGER_HAPPY = 0x2c0;`;
        static if(is(typeof({ mixin(enumMixinStr_BTN_TRIGGER_HAPPY); }))) {
            mixin(enumMixinStr_BTN_TRIGGER_HAPPY);
        }
    }




    static if(!is(typeof(BTN_TRIGGER_HAPPY1))) {
        private enum enumMixinStr_BTN_TRIGGER_HAPPY1 = `enum BTN_TRIGGER_HAPPY1 = 0x2c0;`;
        static if(is(typeof({ mixin(enumMixinStr_BTN_TRIGGER_HAPPY1); }))) {
            mixin(enumMixinStr_BTN_TRIGGER_HAPPY1);
        }
    }




    static if(!is(typeof(BTN_TRIGGER_HAPPY2))) {
        private enum enumMixinStr_BTN_TRIGGER_HAPPY2 = `enum BTN_TRIGGER_HAPPY2 = 0x2c1;`;
        static if(is(typeof({ mixin(enumMixinStr_BTN_TRIGGER_HAPPY2); }))) {
            mixin(enumMixinStr_BTN_TRIGGER_HAPPY2);
        }
    }




    static if(!is(typeof(BTN_TRIGGER_HAPPY3))) {
        private enum enumMixinStr_BTN_TRIGGER_HAPPY3 = `enum BTN_TRIGGER_HAPPY3 = 0x2c2;`;
        static if(is(typeof({ mixin(enumMixinStr_BTN_TRIGGER_HAPPY3); }))) {
            mixin(enumMixinStr_BTN_TRIGGER_HAPPY3);
        }
    }




    static if(!is(typeof(BTN_TRIGGER_HAPPY4))) {
        private enum enumMixinStr_BTN_TRIGGER_HAPPY4 = `enum BTN_TRIGGER_HAPPY4 = 0x2c3;`;
        static if(is(typeof({ mixin(enumMixinStr_BTN_TRIGGER_HAPPY4); }))) {
            mixin(enumMixinStr_BTN_TRIGGER_HAPPY4);
        }
    }




    static if(!is(typeof(BTN_TRIGGER_HAPPY5))) {
        private enum enumMixinStr_BTN_TRIGGER_HAPPY5 = `enum BTN_TRIGGER_HAPPY5 = 0x2c4;`;
        static if(is(typeof({ mixin(enumMixinStr_BTN_TRIGGER_HAPPY5); }))) {
            mixin(enumMixinStr_BTN_TRIGGER_HAPPY5);
        }
    }




    static if(!is(typeof(BTN_TRIGGER_HAPPY6))) {
        private enum enumMixinStr_BTN_TRIGGER_HAPPY6 = `enum BTN_TRIGGER_HAPPY6 = 0x2c5;`;
        static if(is(typeof({ mixin(enumMixinStr_BTN_TRIGGER_HAPPY6); }))) {
            mixin(enumMixinStr_BTN_TRIGGER_HAPPY6);
        }
    }




    static if(!is(typeof(BTN_TRIGGER_HAPPY7))) {
        private enum enumMixinStr_BTN_TRIGGER_HAPPY7 = `enum BTN_TRIGGER_HAPPY7 = 0x2c6;`;
        static if(is(typeof({ mixin(enumMixinStr_BTN_TRIGGER_HAPPY7); }))) {
            mixin(enumMixinStr_BTN_TRIGGER_HAPPY7);
        }
    }




    static if(!is(typeof(BTN_TRIGGER_HAPPY8))) {
        private enum enumMixinStr_BTN_TRIGGER_HAPPY8 = `enum BTN_TRIGGER_HAPPY8 = 0x2c7;`;
        static if(is(typeof({ mixin(enumMixinStr_BTN_TRIGGER_HAPPY8); }))) {
            mixin(enumMixinStr_BTN_TRIGGER_HAPPY8);
        }
    }




    static if(!is(typeof(BTN_TRIGGER_HAPPY9))) {
        private enum enumMixinStr_BTN_TRIGGER_HAPPY9 = `enum BTN_TRIGGER_HAPPY9 = 0x2c8;`;
        static if(is(typeof({ mixin(enumMixinStr_BTN_TRIGGER_HAPPY9); }))) {
            mixin(enumMixinStr_BTN_TRIGGER_HAPPY9);
        }
    }




    static if(!is(typeof(BTN_TRIGGER_HAPPY10))) {
        private enum enumMixinStr_BTN_TRIGGER_HAPPY10 = `enum BTN_TRIGGER_HAPPY10 = 0x2c9;`;
        static if(is(typeof({ mixin(enumMixinStr_BTN_TRIGGER_HAPPY10); }))) {
            mixin(enumMixinStr_BTN_TRIGGER_HAPPY10);
        }
    }




    static if(!is(typeof(BTN_TRIGGER_HAPPY11))) {
        private enum enumMixinStr_BTN_TRIGGER_HAPPY11 = `enum BTN_TRIGGER_HAPPY11 = 0x2ca;`;
        static if(is(typeof({ mixin(enumMixinStr_BTN_TRIGGER_HAPPY11); }))) {
            mixin(enumMixinStr_BTN_TRIGGER_HAPPY11);
        }
    }




    static if(!is(typeof(BTN_TRIGGER_HAPPY12))) {
        private enum enumMixinStr_BTN_TRIGGER_HAPPY12 = `enum BTN_TRIGGER_HAPPY12 = 0x2cb;`;
        static if(is(typeof({ mixin(enumMixinStr_BTN_TRIGGER_HAPPY12); }))) {
            mixin(enumMixinStr_BTN_TRIGGER_HAPPY12);
        }
    }




    static if(!is(typeof(BTN_TRIGGER_HAPPY13))) {
        private enum enumMixinStr_BTN_TRIGGER_HAPPY13 = `enum BTN_TRIGGER_HAPPY13 = 0x2cc;`;
        static if(is(typeof({ mixin(enumMixinStr_BTN_TRIGGER_HAPPY13); }))) {
            mixin(enumMixinStr_BTN_TRIGGER_HAPPY13);
        }
    }




    static if(!is(typeof(BTN_TRIGGER_HAPPY14))) {
        private enum enumMixinStr_BTN_TRIGGER_HAPPY14 = `enum BTN_TRIGGER_HAPPY14 = 0x2cd;`;
        static if(is(typeof({ mixin(enumMixinStr_BTN_TRIGGER_HAPPY14); }))) {
            mixin(enumMixinStr_BTN_TRIGGER_HAPPY14);
        }
    }




    static if(!is(typeof(BTN_TRIGGER_HAPPY15))) {
        private enum enumMixinStr_BTN_TRIGGER_HAPPY15 = `enum BTN_TRIGGER_HAPPY15 = 0x2ce;`;
        static if(is(typeof({ mixin(enumMixinStr_BTN_TRIGGER_HAPPY15); }))) {
            mixin(enumMixinStr_BTN_TRIGGER_HAPPY15);
        }
    }




    static if(!is(typeof(BTN_TRIGGER_HAPPY16))) {
        private enum enumMixinStr_BTN_TRIGGER_HAPPY16 = `enum BTN_TRIGGER_HAPPY16 = 0x2cf;`;
        static if(is(typeof({ mixin(enumMixinStr_BTN_TRIGGER_HAPPY16); }))) {
            mixin(enumMixinStr_BTN_TRIGGER_HAPPY16);
        }
    }




    static if(!is(typeof(BTN_TRIGGER_HAPPY17))) {
        private enum enumMixinStr_BTN_TRIGGER_HAPPY17 = `enum BTN_TRIGGER_HAPPY17 = 0x2d0;`;
        static if(is(typeof({ mixin(enumMixinStr_BTN_TRIGGER_HAPPY17); }))) {
            mixin(enumMixinStr_BTN_TRIGGER_HAPPY17);
        }
    }




    static if(!is(typeof(BTN_TRIGGER_HAPPY18))) {
        private enum enumMixinStr_BTN_TRIGGER_HAPPY18 = `enum BTN_TRIGGER_HAPPY18 = 0x2d1;`;
        static if(is(typeof({ mixin(enumMixinStr_BTN_TRIGGER_HAPPY18); }))) {
            mixin(enumMixinStr_BTN_TRIGGER_HAPPY18);
        }
    }




    static if(!is(typeof(BTN_TRIGGER_HAPPY19))) {
        private enum enumMixinStr_BTN_TRIGGER_HAPPY19 = `enum BTN_TRIGGER_HAPPY19 = 0x2d2;`;
        static if(is(typeof({ mixin(enumMixinStr_BTN_TRIGGER_HAPPY19); }))) {
            mixin(enumMixinStr_BTN_TRIGGER_HAPPY19);
        }
    }




    static if(!is(typeof(BTN_TRIGGER_HAPPY20))) {
        private enum enumMixinStr_BTN_TRIGGER_HAPPY20 = `enum BTN_TRIGGER_HAPPY20 = 0x2d3;`;
        static if(is(typeof({ mixin(enumMixinStr_BTN_TRIGGER_HAPPY20); }))) {
            mixin(enumMixinStr_BTN_TRIGGER_HAPPY20);
        }
    }




    static if(!is(typeof(BTN_TRIGGER_HAPPY21))) {
        private enum enumMixinStr_BTN_TRIGGER_HAPPY21 = `enum BTN_TRIGGER_HAPPY21 = 0x2d4;`;
        static if(is(typeof({ mixin(enumMixinStr_BTN_TRIGGER_HAPPY21); }))) {
            mixin(enumMixinStr_BTN_TRIGGER_HAPPY21);
        }
    }




    static if(!is(typeof(BTN_TRIGGER_HAPPY22))) {
        private enum enumMixinStr_BTN_TRIGGER_HAPPY22 = `enum BTN_TRIGGER_HAPPY22 = 0x2d5;`;
        static if(is(typeof({ mixin(enumMixinStr_BTN_TRIGGER_HAPPY22); }))) {
            mixin(enumMixinStr_BTN_TRIGGER_HAPPY22);
        }
    }




    static if(!is(typeof(BTN_TRIGGER_HAPPY23))) {
        private enum enumMixinStr_BTN_TRIGGER_HAPPY23 = `enum BTN_TRIGGER_HAPPY23 = 0x2d6;`;
        static if(is(typeof({ mixin(enumMixinStr_BTN_TRIGGER_HAPPY23); }))) {
            mixin(enumMixinStr_BTN_TRIGGER_HAPPY23);
        }
    }




    static if(!is(typeof(BTN_TRIGGER_HAPPY24))) {
        private enum enumMixinStr_BTN_TRIGGER_HAPPY24 = `enum BTN_TRIGGER_HAPPY24 = 0x2d7;`;
        static if(is(typeof({ mixin(enumMixinStr_BTN_TRIGGER_HAPPY24); }))) {
            mixin(enumMixinStr_BTN_TRIGGER_HAPPY24);
        }
    }




    static if(!is(typeof(BTN_TRIGGER_HAPPY25))) {
        private enum enumMixinStr_BTN_TRIGGER_HAPPY25 = `enum BTN_TRIGGER_HAPPY25 = 0x2d8;`;
        static if(is(typeof({ mixin(enumMixinStr_BTN_TRIGGER_HAPPY25); }))) {
            mixin(enumMixinStr_BTN_TRIGGER_HAPPY25);
        }
    }




    static if(!is(typeof(BTN_TRIGGER_HAPPY26))) {
        private enum enumMixinStr_BTN_TRIGGER_HAPPY26 = `enum BTN_TRIGGER_HAPPY26 = 0x2d9;`;
        static if(is(typeof({ mixin(enumMixinStr_BTN_TRIGGER_HAPPY26); }))) {
            mixin(enumMixinStr_BTN_TRIGGER_HAPPY26);
        }
    }




    static if(!is(typeof(BTN_TRIGGER_HAPPY27))) {
        private enum enumMixinStr_BTN_TRIGGER_HAPPY27 = `enum BTN_TRIGGER_HAPPY27 = 0x2da;`;
        static if(is(typeof({ mixin(enumMixinStr_BTN_TRIGGER_HAPPY27); }))) {
            mixin(enumMixinStr_BTN_TRIGGER_HAPPY27);
        }
    }




    static if(!is(typeof(BTN_TRIGGER_HAPPY28))) {
        private enum enumMixinStr_BTN_TRIGGER_HAPPY28 = `enum BTN_TRIGGER_HAPPY28 = 0x2db;`;
        static if(is(typeof({ mixin(enumMixinStr_BTN_TRIGGER_HAPPY28); }))) {
            mixin(enumMixinStr_BTN_TRIGGER_HAPPY28);
        }
    }




    static if(!is(typeof(BTN_TRIGGER_HAPPY29))) {
        private enum enumMixinStr_BTN_TRIGGER_HAPPY29 = `enum BTN_TRIGGER_HAPPY29 = 0x2dc;`;
        static if(is(typeof({ mixin(enumMixinStr_BTN_TRIGGER_HAPPY29); }))) {
            mixin(enumMixinStr_BTN_TRIGGER_HAPPY29);
        }
    }




    static if(!is(typeof(BTN_TRIGGER_HAPPY30))) {
        private enum enumMixinStr_BTN_TRIGGER_HAPPY30 = `enum BTN_TRIGGER_HAPPY30 = 0x2dd;`;
        static if(is(typeof({ mixin(enumMixinStr_BTN_TRIGGER_HAPPY30); }))) {
            mixin(enumMixinStr_BTN_TRIGGER_HAPPY30);
        }
    }




    static if(!is(typeof(BTN_TRIGGER_HAPPY31))) {
        private enum enumMixinStr_BTN_TRIGGER_HAPPY31 = `enum BTN_TRIGGER_HAPPY31 = 0x2de;`;
        static if(is(typeof({ mixin(enumMixinStr_BTN_TRIGGER_HAPPY31); }))) {
            mixin(enumMixinStr_BTN_TRIGGER_HAPPY31);
        }
    }




    static if(!is(typeof(BTN_TRIGGER_HAPPY32))) {
        private enum enumMixinStr_BTN_TRIGGER_HAPPY32 = `enum BTN_TRIGGER_HAPPY32 = 0x2df;`;
        static if(is(typeof({ mixin(enumMixinStr_BTN_TRIGGER_HAPPY32); }))) {
            mixin(enumMixinStr_BTN_TRIGGER_HAPPY32);
        }
    }




    static if(!is(typeof(BTN_TRIGGER_HAPPY33))) {
        private enum enumMixinStr_BTN_TRIGGER_HAPPY33 = `enum BTN_TRIGGER_HAPPY33 = 0x2e0;`;
        static if(is(typeof({ mixin(enumMixinStr_BTN_TRIGGER_HAPPY33); }))) {
            mixin(enumMixinStr_BTN_TRIGGER_HAPPY33);
        }
    }




    static if(!is(typeof(BTN_TRIGGER_HAPPY34))) {
        private enum enumMixinStr_BTN_TRIGGER_HAPPY34 = `enum BTN_TRIGGER_HAPPY34 = 0x2e1;`;
        static if(is(typeof({ mixin(enumMixinStr_BTN_TRIGGER_HAPPY34); }))) {
            mixin(enumMixinStr_BTN_TRIGGER_HAPPY34);
        }
    }




    static if(!is(typeof(BTN_TRIGGER_HAPPY35))) {
        private enum enumMixinStr_BTN_TRIGGER_HAPPY35 = `enum BTN_TRIGGER_HAPPY35 = 0x2e2;`;
        static if(is(typeof({ mixin(enumMixinStr_BTN_TRIGGER_HAPPY35); }))) {
            mixin(enumMixinStr_BTN_TRIGGER_HAPPY35);
        }
    }




    static if(!is(typeof(BTN_TRIGGER_HAPPY36))) {
        private enum enumMixinStr_BTN_TRIGGER_HAPPY36 = `enum BTN_TRIGGER_HAPPY36 = 0x2e3;`;
        static if(is(typeof({ mixin(enumMixinStr_BTN_TRIGGER_HAPPY36); }))) {
            mixin(enumMixinStr_BTN_TRIGGER_HAPPY36);
        }
    }




    static if(!is(typeof(BTN_TRIGGER_HAPPY37))) {
        private enum enumMixinStr_BTN_TRIGGER_HAPPY37 = `enum BTN_TRIGGER_HAPPY37 = 0x2e4;`;
        static if(is(typeof({ mixin(enumMixinStr_BTN_TRIGGER_HAPPY37); }))) {
            mixin(enumMixinStr_BTN_TRIGGER_HAPPY37);
        }
    }




    static if(!is(typeof(BTN_TRIGGER_HAPPY38))) {
        private enum enumMixinStr_BTN_TRIGGER_HAPPY38 = `enum BTN_TRIGGER_HAPPY38 = 0x2e5;`;
        static if(is(typeof({ mixin(enumMixinStr_BTN_TRIGGER_HAPPY38); }))) {
            mixin(enumMixinStr_BTN_TRIGGER_HAPPY38);
        }
    }




    static if(!is(typeof(BTN_TRIGGER_HAPPY39))) {
        private enum enumMixinStr_BTN_TRIGGER_HAPPY39 = `enum BTN_TRIGGER_HAPPY39 = 0x2e6;`;
        static if(is(typeof({ mixin(enumMixinStr_BTN_TRIGGER_HAPPY39); }))) {
            mixin(enumMixinStr_BTN_TRIGGER_HAPPY39);
        }
    }




    static if(!is(typeof(BTN_TRIGGER_HAPPY40))) {
        private enum enumMixinStr_BTN_TRIGGER_HAPPY40 = `enum BTN_TRIGGER_HAPPY40 = 0x2e7;`;
        static if(is(typeof({ mixin(enumMixinStr_BTN_TRIGGER_HAPPY40); }))) {
            mixin(enumMixinStr_BTN_TRIGGER_HAPPY40);
        }
    }




    static if(!is(typeof(KEY_MIN_INTERESTING))) {
        private enum enumMixinStr_KEY_MIN_INTERESTING = `enum KEY_MIN_INTERESTING = 113;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_MIN_INTERESTING); }))) {
            mixin(enumMixinStr_KEY_MIN_INTERESTING);
        }
    }




    static if(!is(typeof(KEY_MAX))) {
        private enum enumMixinStr_KEY_MAX = `enum KEY_MAX = 0x2ff;`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_MAX); }))) {
            mixin(enumMixinStr_KEY_MAX);
        }
    }




    static if(!is(typeof(KEY_CNT))) {
        private enum enumMixinStr_KEY_CNT = `enum KEY_CNT = ( 0x2ff + 1 );`;
        static if(is(typeof({ mixin(enumMixinStr_KEY_CNT); }))) {
            mixin(enumMixinStr_KEY_CNT);
        }
    }




    static if(!is(typeof(REL_X))) {
        private enum enumMixinStr_REL_X = `enum REL_X = 0x00;`;
        static if(is(typeof({ mixin(enumMixinStr_REL_X); }))) {
            mixin(enumMixinStr_REL_X);
        }
    }




    static if(!is(typeof(REL_Y))) {
        private enum enumMixinStr_REL_Y = `enum REL_Y = 0x01;`;
        static if(is(typeof({ mixin(enumMixinStr_REL_Y); }))) {
            mixin(enumMixinStr_REL_Y);
        }
    }




    static if(!is(typeof(REL_Z))) {
        private enum enumMixinStr_REL_Z = `enum REL_Z = 0x02;`;
        static if(is(typeof({ mixin(enumMixinStr_REL_Z); }))) {
            mixin(enumMixinStr_REL_Z);
        }
    }




    static if(!is(typeof(REL_RX))) {
        private enum enumMixinStr_REL_RX = `enum REL_RX = 0x03;`;
        static if(is(typeof({ mixin(enumMixinStr_REL_RX); }))) {
            mixin(enumMixinStr_REL_RX);
        }
    }




    static if(!is(typeof(REL_RY))) {
        private enum enumMixinStr_REL_RY = `enum REL_RY = 0x04;`;
        static if(is(typeof({ mixin(enumMixinStr_REL_RY); }))) {
            mixin(enumMixinStr_REL_RY);
        }
    }




    static if(!is(typeof(REL_RZ))) {
        private enum enumMixinStr_REL_RZ = `enum REL_RZ = 0x05;`;
        static if(is(typeof({ mixin(enumMixinStr_REL_RZ); }))) {
            mixin(enumMixinStr_REL_RZ);
        }
    }




    static if(!is(typeof(REL_HWHEEL))) {
        private enum enumMixinStr_REL_HWHEEL = `enum REL_HWHEEL = 0x06;`;
        static if(is(typeof({ mixin(enumMixinStr_REL_HWHEEL); }))) {
            mixin(enumMixinStr_REL_HWHEEL);
        }
    }




    static if(!is(typeof(REL_DIAL))) {
        private enum enumMixinStr_REL_DIAL = `enum REL_DIAL = 0x07;`;
        static if(is(typeof({ mixin(enumMixinStr_REL_DIAL); }))) {
            mixin(enumMixinStr_REL_DIAL);
        }
    }




    static if(!is(typeof(REL_WHEEL))) {
        private enum enumMixinStr_REL_WHEEL = `enum REL_WHEEL = 0x08;`;
        static if(is(typeof({ mixin(enumMixinStr_REL_WHEEL); }))) {
            mixin(enumMixinStr_REL_WHEEL);
        }
    }




    static if(!is(typeof(REL_MISC))) {
        private enum enumMixinStr_REL_MISC = `enum REL_MISC = 0x09;`;
        static if(is(typeof({ mixin(enumMixinStr_REL_MISC); }))) {
            mixin(enumMixinStr_REL_MISC);
        }
    }




    static if(!is(typeof(REL_RESERVED))) {
        private enum enumMixinStr_REL_RESERVED = `enum REL_RESERVED = 0x0a;`;
        static if(is(typeof({ mixin(enumMixinStr_REL_RESERVED); }))) {
            mixin(enumMixinStr_REL_RESERVED);
        }
    }




    static if(!is(typeof(REL_WHEEL_HI_RES))) {
        private enum enumMixinStr_REL_WHEEL_HI_RES = `enum REL_WHEEL_HI_RES = 0x0b;`;
        static if(is(typeof({ mixin(enumMixinStr_REL_WHEEL_HI_RES); }))) {
            mixin(enumMixinStr_REL_WHEEL_HI_RES);
        }
    }




    static if(!is(typeof(REL_HWHEEL_HI_RES))) {
        private enum enumMixinStr_REL_HWHEEL_HI_RES = `enum REL_HWHEEL_HI_RES = 0x0c;`;
        static if(is(typeof({ mixin(enumMixinStr_REL_HWHEEL_HI_RES); }))) {
            mixin(enumMixinStr_REL_HWHEEL_HI_RES);
        }
    }




    static if(!is(typeof(REL_MAX))) {
        private enum enumMixinStr_REL_MAX = `enum REL_MAX = 0x0f;`;
        static if(is(typeof({ mixin(enumMixinStr_REL_MAX); }))) {
            mixin(enumMixinStr_REL_MAX);
        }
    }




    static if(!is(typeof(REL_CNT))) {
        private enum enumMixinStr_REL_CNT = `enum REL_CNT = ( 0x0f + 1 );`;
        static if(is(typeof({ mixin(enumMixinStr_REL_CNT); }))) {
            mixin(enumMixinStr_REL_CNT);
        }
    }




    static if(!is(typeof(ABS_X))) {
        private enum enumMixinStr_ABS_X = `enum ABS_X = 0x00;`;
        static if(is(typeof({ mixin(enumMixinStr_ABS_X); }))) {
            mixin(enumMixinStr_ABS_X);
        }
    }




    static if(!is(typeof(ABS_Y))) {
        private enum enumMixinStr_ABS_Y = `enum ABS_Y = 0x01;`;
        static if(is(typeof({ mixin(enumMixinStr_ABS_Y); }))) {
            mixin(enumMixinStr_ABS_Y);
        }
    }




    static if(!is(typeof(ABS_Z))) {
        private enum enumMixinStr_ABS_Z = `enum ABS_Z = 0x02;`;
        static if(is(typeof({ mixin(enumMixinStr_ABS_Z); }))) {
            mixin(enumMixinStr_ABS_Z);
        }
    }




    static if(!is(typeof(ABS_RX))) {
        private enum enumMixinStr_ABS_RX = `enum ABS_RX = 0x03;`;
        static if(is(typeof({ mixin(enumMixinStr_ABS_RX); }))) {
            mixin(enumMixinStr_ABS_RX);
        }
    }




    static if(!is(typeof(ABS_RY))) {
        private enum enumMixinStr_ABS_RY = `enum ABS_RY = 0x04;`;
        static if(is(typeof({ mixin(enumMixinStr_ABS_RY); }))) {
            mixin(enumMixinStr_ABS_RY);
        }
    }




    static if(!is(typeof(ABS_RZ))) {
        private enum enumMixinStr_ABS_RZ = `enum ABS_RZ = 0x05;`;
        static if(is(typeof({ mixin(enumMixinStr_ABS_RZ); }))) {
            mixin(enumMixinStr_ABS_RZ);
        }
    }




    static if(!is(typeof(ABS_THROTTLE))) {
        private enum enumMixinStr_ABS_THROTTLE = `enum ABS_THROTTLE = 0x06;`;
        static if(is(typeof({ mixin(enumMixinStr_ABS_THROTTLE); }))) {
            mixin(enumMixinStr_ABS_THROTTLE);
        }
    }




    static if(!is(typeof(ABS_RUDDER))) {
        private enum enumMixinStr_ABS_RUDDER = `enum ABS_RUDDER = 0x07;`;
        static if(is(typeof({ mixin(enumMixinStr_ABS_RUDDER); }))) {
            mixin(enumMixinStr_ABS_RUDDER);
        }
    }




    static if(!is(typeof(ABS_WHEEL))) {
        private enum enumMixinStr_ABS_WHEEL = `enum ABS_WHEEL = 0x08;`;
        static if(is(typeof({ mixin(enumMixinStr_ABS_WHEEL); }))) {
            mixin(enumMixinStr_ABS_WHEEL);
        }
    }




    static if(!is(typeof(ABS_GAS))) {
        private enum enumMixinStr_ABS_GAS = `enum ABS_GAS = 0x09;`;
        static if(is(typeof({ mixin(enumMixinStr_ABS_GAS); }))) {
            mixin(enumMixinStr_ABS_GAS);
        }
    }




    static if(!is(typeof(ABS_BRAKE))) {
        private enum enumMixinStr_ABS_BRAKE = `enum ABS_BRAKE = 0x0a;`;
        static if(is(typeof({ mixin(enumMixinStr_ABS_BRAKE); }))) {
            mixin(enumMixinStr_ABS_BRAKE);
        }
    }




    static if(!is(typeof(ABS_HAT0X))) {
        private enum enumMixinStr_ABS_HAT0X = `enum ABS_HAT0X = 0x10;`;
        static if(is(typeof({ mixin(enumMixinStr_ABS_HAT0X); }))) {
            mixin(enumMixinStr_ABS_HAT0X);
        }
    }




    static if(!is(typeof(ABS_HAT0Y))) {
        private enum enumMixinStr_ABS_HAT0Y = `enum ABS_HAT0Y = 0x11;`;
        static if(is(typeof({ mixin(enumMixinStr_ABS_HAT0Y); }))) {
            mixin(enumMixinStr_ABS_HAT0Y);
        }
    }




    static if(!is(typeof(ABS_HAT1X))) {
        private enum enumMixinStr_ABS_HAT1X = `enum ABS_HAT1X = 0x12;`;
        static if(is(typeof({ mixin(enumMixinStr_ABS_HAT1X); }))) {
            mixin(enumMixinStr_ABS_HAT1X);
        }
    }




    static if(!is(typeof(ABS_HAT1Y))) {
        private enum enumMixinStr_ABS_HAT1Y = `enum ABS_HAT1Y = 0x13;`;
        static if(is(typeof({ mixin(enumMixinStr_ABS_HAT1Y); }))) {
            mixin(enumMixinStr_ABS_HAT1Y);
        }
    }




    static if(!is(typeof(ABS_HAT2X))) {
        private enum enumMixinStr_ABS_HAT2X = `enum ABS_HAT2X = 0x14;`;
        static if(is(typeof({ mixin(enumMixinStr_ABS_HAT2X); }))) {
            mixin(enumMixinStr_ABS_HAT2X);
        }
    }




    static if(!is(typeof(ABS_HAT2Y))) {
        private enum enumMixinStr_ABS_HAT2Y = `enum ABS_HAT2Y = 0x15;`;
        static if(is(typeof({ mixin(enumMixinStr_ABS_HAT2Y); }))) {
            mixin(enumMixinStr_ABS_HAT2Y);
        }
    }




    static if(!is(typeof(ABS_HAT3X))) {
        private enum enumMixinStr_ABS_HAT3X = `enum ABS_HAT3X = 0x16;`;
        static if(is(typeof({ mixin(enumMixinStr_ABS_HAT3X); }))) {
            mixin(enumMixinStr_ABS_HAT3X);
        }
    }




    static if(!is(typeof(ABS_HAT3Y))) {
        private enum enumMixinStr_ABS_HAT3Y = `enum ABS_HAT3Y = 0x17;`;
        static if(is(typeof({ mixin(enumMixinStr_ABS_HAT3Y); }))) {
            mixin(enumMixinStr_ABS_HAT3Y);
        }
    }




    static if(!is(typeof(ABS_PRESSURE))) {
        private enum enumMixinStr_ABS_PRESSURE = `enum ABS_PRESSURE = 0x18;`;
        static if(is(typeof({ mixin(enumMixinStr_ABS_PRESSURE); }))) {
            mixin(enumMixinStr_ABS_PRESSURE);
        }
    }




    static if(!is(typeof(ABS_DISTANCE))) {
        private enum enumMixinStr_ABS_DISTANCE = `enum ABS_DISTANCE = 0x19;`;
        static if(is(typeof({ mixin(enumMixinStr_ABS_DISTANCE); }))) {
            mixin(enumMixinStr_ABS_DISTANCE);
        }
    }




    static if(!is(typeof(ABS_TILT_X))) {
        private enum enumMixinStr_ABS_TILT_X = `enum ABS_TILT_X = 0x1a;`;
        static if(is(typeof({ mixin(enumMixinStr_ABS_TILT_X); }))) {
            mixin(enumMixinStr_ABS_TILT_X);
        }
    }




    static if(!is(typeof(ABS_TILT_Y))) {
        private enum enumMixinStr_ABS_TILT_Y = `enum ABS_TILT_Y = 0x1b;`;
        static if(is(typeof({ mixin(enumMixinStr_ABS_TILT_Y); }))) {
            mixin(enumMixinStr_ABS_TILT_Y);
        }
    }




    static if(!is(typeof(ABS_TOOL_WIDTH))) {
        private enum enumMixinStr_ABS_TOOL_WIDTH = `enum ABS_TOOL_WIDTH = 0x1c;`;
        static if(is(typeof({ mixin(enumMixinStr_ABS_TOOL_WIDTH); }))) {
            mixin(enumMixinStr_ABS_TOOL_WIDTH);
        }
    }




    static if(!is(typeof(ABS_VOLUME))) {
        private enum enumMixinStr_ABS_VOLUME = `enum ABS_VOLUME = 0x20;`;
        static if(is(typeof({ mixin(enumMixinStr_ABS_VOLUME); }))) {
            mixin(enumMixinStr_ABS_VOLUME);
        }
    }




    static if(!is(typeof(ABS_MISC))) {
        private enum enumMixinStr_ABS_MISC = `enum ABS_MISC = 0x28;`;
        static if(is(typeof({ mixin(enumMixinStr_ABS_MISC); }))) {
            mixin(enumMixinStr_ABS_MISC);
        }
    }




    static if(!is(typeof(ABS_RESERVED))) {
        private enum enumMixinStr_ABS_RESERVED = `enum ABS_RESERVED = 0x2e;`;
        static if(is(typeof({ mixin(enumMixinStr_ABS_RESERVED); }))) {
            mixin(enumMixinStr_ABS_RESERVED);
        }
    }




    static if(!is(typeof(ABS_MT_SLOT))) {
        private enum enumMixinStr_ABS_MT_SLOT = `enum ABS_MT_SLOT = 0x2f;`;
        static if(is(typeof({ mixin(enumMixinStr_ABS_MT_SLOT); }))) {
            mixin(enumMixinStr_ABS_MT_SLOT);
        }
    }




    static if(!is(typeof(ABS_MT_TOUCH_MAJOR))) {
        private enum enumMixinStr_ABS_MT_TOUCH_MAJOR = `enum ABS_MT_TOUCH_MAJOR = 0x30;`;
        static if(is(typeof({ mixin(enumMixinStr_ABS_MT_TOUCH_MAJOR); }))) {
            mixin(enumMixinStr_ABS_MT_TOUCH_MAJOR);
        }
    }




    static if(!is(typeof(ABS_MT_TOUCH_MINOR))) {
        private enum enumMixinStr_ABS_MT_TOUCH_MINOR = `enum ABS_MT_TOUCH_MINOR = 0x31;`;
        static if(is(typeof({ mixin(enumMixinStr_ABS_MT_TOUCH_MINOR); }))) {
            mixin(enumMixinStr_ABS_MT_TOUCH_MINOR);
        }
    }




    static if(!is(typeof(ABS_MT_WIDTH_MAJOR))) {
        private enum enumMixinStr_ABS_MT_WIDTH_MAJOR = `enum ABS_MT_WIDTH_MAJOR = 0x32;`;
        static if(is(typeof({ mixin(enumMixinStr_ABS_MT_WIDTH_MAJOR); }))) {
            mixin(enumMixinStr_ABS_MT_WIDTH_MAJOR);
        }
    }




    static if(!is(typeof(ABS_MT_WIDTH_MINOR))) {
        private enum enumMixinStr_ABS_MT_WIDTH_MINOR = `enum ABS_MT_WIDTH_MINOR = 0x33;`;
        static if(is(typeof({ mixin(enumMixinStr_ABS_MT_WIDTH_MINOR); }))) {
            mixin(enumMixinStr_ABS_MT_WIDTH_MINOR);
        }
    }




    static if(!is(typeof(ABS_MT_ORIENTATION))) {
        private enum enumMixinStr_ABS_MT_ORIENTATION = `enum ABS_MT_ORIENTATION = 0x34;`;
        static if(is(typeof({ mixin(enumMixinStr_ABS_MT_ORIENTATION); }))) {
            mixin(enumMixinStr_ABS_MT_ORIENTATION);
        }
    }




    static if(!is(typeof(ABS_MT_POSITION_X))) {
        private enum enumMixinStr_ABS_MT_POSITION_X = `enum ABS_MT_POSITION_X = 0x35;`;
        static if(is(typeof({ mixin(enumMixinStr_ABS_MT_POSITION_X); }))) {
            mixin(enumMixinStr_ABS_MT_POSITION_X);
        }
    }




    static if(!is(typeof(ABS_MT_POSITION_Y))) {
        private enum enumMixinStr_ABS_MT_POSITION_Y = `enum ABS_MT_POSITION_Y = 0x36;`;
        static if(is(typeof({ mixin(enumMixinStr_ABS_MT_POSITION_Y); }))) {
            mixin(enumMixinStr_ABS_MT_POSITION_Y);
        }
    }




    static if(!is(typeof(ABS_MT_TOOL_TYPE))) {
        private enum enumMixinStr_ABS_MT_TOOL_TYPE = `enum ABS_MT_TOOL_TYPE = 0x37;`;
        static if(is(typeof({ mixin(enumMixinStr_ABS_MT_TOOL_TYPE); }))) {
            mixin(enumMixinStr_ABS_MT_TOOL_TYPE);
        }
    }




    static if(!is(typeof(ABS_MT_BLOB_ID))) {
        private enum enumMixinStr_ABS_MT_BLOB_ID = `enum ABS_MT_BLOB_ID = 0x38;`;
        static if(is(typeof({ mixin(enumMixinStr_ABS_MT_BLOB_ID); }))) {
            mixin(enumMixinStr_ABS_MT_BLOB_ID);
        }
    }




    static if(!is(typeof(ABS_MT_TRACKING_ID))) {
        private enum enumMixinStr_ABS_MT_TRACKING_ID = `enum ABS_MT_TRACKING_ID = 0x39;`;
        static if(is(typeof({ mixin(enumMixinStr_ABS_MT_TRACKING_ID); }))) {
            mixin(enumMixinStr_ABS_MT_TRACKING_ID);
        }
    }




    static if(!is(typeof(ABS_MT_PRESSURE))) {
        private enum enumMixinStr_ABS_MT_PRESSURE = `enum ABS_MT_PRESSURE = 0x3a;`;
        static if(is(typeof({ mixin(enumMixinStr_ABS_MT_PRESSURE); }))) {
            mixin(enumMixinStr_ABS_MT_PRESSURE);
        }
    }




    static if(!is(typeof(ABS_MT_DISTANCE))) {
        private enum enumMixinStr_ABS_MT_DISTANCE = `enum ABS_MT_DISTANCE = 0x3b;`;
        static if(is(typeof({ mixin(enumMixinStr_ABS_MT_DISTANCE); }))) {
            mixin(enumMixinStr_ABS_MT_DISTANCE);
        }
    }




    static if(!is(typeof(ABS_MT_TOOL_X))) {
        private enum enumMixinStr_ABS_MT_TOOL_X = `enum ABS_MT_TOOL_X = 0x3c;`;
        static if(is(typeof({ mixin(enumMixinStr_ABS_MT_TOOL_X); }))) {
            mixin(enumMixinStr_ABS_MT_TOOL_X);
        }
    }




    static if(!is(typeof(ABS_MT_TOOL_Y))) {
        private enum enumMixinStr_ABS_MT_TOOL_Y = `enum ABS_MT_TOOL_Y = 0x3d;`;
        static if(is(typeof({ mixin(enumMixinStr_ABS_MT_TOOL_Y); }))) {
            mixin(enumMixinStr_ABS_MT_TOOL_Y);
        }
    }




    static if(!is(typeof(ABS_MAX))) {
        private enum enumMixinStr_ABS_MAX = `enum ABS_MAX = 0x3f;`;
        static if(is(typeof({ mixin(enumMixinStr_ABS_MAX); }))) {
            mixin(enumMixinStr_ABS_MAX);
        }
    }




    static if(!is(typeof(ABS_CNT))) {
        private enum enumMixinStr_ABS_CNT = `enum ABS_CNT = ( 0x3f + 1 );`;
        static if(is(typeof({ mixin(enumMixinStr_ABS_CNT); }))) {
            mixin(enumMixinStr_ABS_CNT);
        }
    }




    static if(!is(typeof(SW_LID))) {
        private enum enumMixinStr_SW_LID = `enum SW_LID = 0x00;`;
        static if(is(typeof({ mixin(enumMixinStr_SW_LID); }))) {
            mixin(enumMixinStr_SW_LID);
        }
    }




    static if(!is(typeof(SW_TABLET_MODE))) {
        private enum enumMixinStr_SW_TABLET_MODE = `enum SW_TABLET_MODE = 0x01;`;
        static if(is(typeof({ mixin(enumMixinStr_SW_TABLET_MODE); }))) {
            mixin(enumMixinStr_SW_TABLET_MODE);
        }
    }




    static if(!is(typeof(SW_HEADPHONE_INSERT))) {
        private enum enumMixinStr_SW_HEADPHONE_INSERT = `enum SW_HEADPHONE_INSERT = 0x02;`;
        static if(is(typeof({ mixin(enumMixinStr_SW_HEADPHONE_INSERT); }))) {
            mixin(enumMixinStr_SW_HEADPHONE_INSERT);
        }
    }




    static if(!is(typeof(SW_RFKILL_ALL))) {
        private enum enumMixinStr_SW_RFKILL_ALL = `enum SW_RFKILL_ALL = 0x03;`;
        static if(is(typeof({ mixin(enumMixinStr_SW_RFKILL_ALL); }))) {
            mixin(enumMixinStr_SW_RFKILL_ALL);
        }
    }




    static if(!is(typeof(SW_RADIO))) {
        private enum enumMixinStr_SW_RADIO = `enum SW_RADIO = 0x03;`;
        static if(is(typeof({ mixin(enumMixinStr_SW_RADIO); }))) {
            mixin(enumMixinStr_SW_RADIO);
        }
    }




    static if(!is(typeof(SW_MICROPHONE_INSERT))) {
        private enum enumMixinStr_SW_MICROPHONE_INSERT = `enum SW_MICROPHONE_INSERT = 0x04;`;
        static if(is(typeof({ mixin(enumMixinStr_SW_MICROPHONE_INSERT); }))) {
            mixin(enumMixinStr_SW_MICROPHONE_INSERT);
        }
    }




    static if(!is(typeof(SW_DOCK))) {
        private enum enumMixinStr_SW_DOCK = `enum SW_DOCK = 0x05;`;
        static if(is(typeof({ mixin(enumMixinStr_SW_DOCK); }))) {
            mixin(enumMixinStr_SW_DOCK);
        }
    }




    static if(!is(typeof(SW_LINEOUT_INSERT))) {
        private enum enumMixinStr_SW_LINEOUT_INSERT = `enum SW_LINEOUT_INSERT = 0x06;`;
        static if(is(typeof({ mixin(enumMixinStr_SW_LINEOUT_INSERT); }))) {
            mixin(enumMixinStr_SW_LINEOUT_INSERT);
        }
    }




    static if(!is(typeof(SW_JACK_PHYSICAL_INSERT))) {
        private enum enumMixinStr_SW_JACK_PHYSICAL_INSERT = `enum SW_JACK_PHYSICAL_INSERT = 0x07;`;
        static if(is(typeof({ mixin(enumMixinStr_SW_JACK_PHYSICAL_INSERT); }))) {
            mixin(enumMixinStr_SW_JACK_PHYSICAL_INSERT);
        }
    }




    static if(!is(typeof(SW_VIDEOOUT_INSERT))) {
        private enum enumMixinStr_SW_VIDEOOUT_INSERT = `enum SW_VIDEOOUT_INSERT = 0x08;`;
        static if(is(typeof({ mixin(enumMixinStr_SW_VIDEOOUT_INSERT); }))) {
            mixin(enumMixinStr_SW_VIDEOOUT_INSERT);
        }
    }




    static if(!is(typeof(SW_CAMERA_LENS_COVER))) {
        private enum enumMixinStr_SW_CAMERA_LENS_COVER = `enum SW_CAMERA_LENS_COVER = 0x09;`;
        static if(is(typeof({ mixin(enumMixinStr_SW_CAMERA_LENS_COVER); }))) {
            mixin(enumMixinStr_SW_CAMERA_LENS_COVER);
        }
    }




    static if(!is(typeof(SW_KEYPAD_SLIDE))) {
        private enum enumMixinStr_SW_KEYPAD_SLIDE = `enum SW_KEYPAD_SLIDE = 0x0a;`;
        static if(is(typeof({ mixin(enumMixinStr_SW_KEYPAD_SLIDE); }))) {
            mixin(enumMixinStr_SW_KEYPAD_SLIDE);
        }
    }




    static if(!is(typeof(SW_FRONT_PROXIMITY))) {
        private enum enumMixinStr_SW_FRONT_PROXIMITY = `enum SW_FRONT_PROXIMITY = 0x0b;`;
        static if(is(typeof({ mixin(enumMixinStr_SW_FRONT_PROXIMITY); }))) {
            mixin(enumMixinStr_SW_FRONT_PROXIMITY);
        }
    }




    static if(!is(typeof(SW_ROTATE_LOCK))) {
        private enum enumMixinStr_SW_ROTATE_LOCK = `enum SW_ROTATE_LOCK = 0x0c;`;
        static if(is(typeof({ mixin(enumMixinStr_SW_ROTATE_LOCK); }))) {
            mixin(enumMixinStr_SW_ROTATE_LOCK);
        }
    }




    static if(!is(typeof(SW_LINEIN_INSERT))) {
        private enum enumMixinStr_SW_LINEIN_INSERT = `enum SW_LINEIN_INSERT = 0x0d;`;
        static if(is(typeof({ mixin(enumMixinStr_SW_LINEIN_INSERT); }))) {
            mixin(enumMixinStr_SW_LINEIN_INSERT);
        }
    }




    static if(!is(typeof(SW_MUTE_DEVICE))) {
        private enum enumMixinStr_SW_MUTE_DEVICE = `enum SW_MUTE_DEVICE = 0x0e;`;
        static if(is(typeof({ mixin(enumMixinStr_SW_MUTE_DEVICE); }))) {
            mixin(enumMixinStr_SW_MUTE_DEVICE);
        }
    }




    static if(!is(typeof(SW_PEN_INSERTED))) {
        private enum enumMixinStr_SW_PEN_INSERTED = `enum SW_PEN_INSERTED = 0x0f;`;
        static if(is(typeof({ mixin(enumMixinStr_SW_PEN_INSERTED); }))) {
            mixin(enumMixinStr_SW_PEN_INSERTED);
        }
    }




    static if(!is(typeof(SW_MACHINE_COVER))) {
        private enum enumMixinStr_SW_MACHINE_COVER = `enum SW_MACHINE_COVER = 0x10;`;
        static if(is(typeof({ mixin(enumMixinStr_SW_MACHINE_COVER); }))) {
            mixin(enumMixinStr_SW_MACHINE_COVER);
        }
    }




    static if(!is(typeof(SW_MAX))) {
        private enum enumMixinStr_SW_MAX = `enum SW_MAX = 0x10;`;
        static if(is(typeof({ mixin(enumMixinStr_SW_MAX); }))) {
            mixin(enumMixinStr_SW_MAX);
        }
    }




    static if(!is(typeof(SW_CNT))) {
        private enum enumMixinStr_SW_CNT = `enum SW_CNT = ( 0x10 + 1 );`;
        static if(is(typeof({ mixin(enumMixinStr_SW_CNT); }))) {
            mixin(enumMixinStr_SW_CNT);
        }
    }




    static if(!is(typeof(MSC_SERIAL))) {
        private enum enumMixinStr_MSC_SERIAL = `enum MSC_SERIAL = 0x00;`;
        static if(is(typeof({ mixin(enumMixinStr_MSC_SERIAL); }))) {
            mixin(enumMixinStr_MSC_SERIAL);
        }
    }




    static if(!is(typeof(MSC_PULSELED))) {
        private enum enumMixinStr_MSC_PULSELED = `enum MSC_PULSELED = 0x01;`;
        static if(is(typeof({ mixin(enumMixinStr_MSC_PULSELED); }))) {
            mixin(enumMixinStr_MSC_PULSELED);
        }
    }




    static if(!is(typeof(MSC_GESTURE))) {
        private enum enumMixinStr_MSC_GESTURE = `enum MSC_GESTURE = 0x02;`;
        static if(is(typeof({ mixin(enumMixinStr_MSC_GESTURE); }))) {
            mixin(enumMixinStr_MSC_GESTURE);
        }
    }




    static if(!is(typeof(MSC_RAW))) {
        private enum enumMixinStr_MSC_RAW = `enum MSC_RAW = 0x03;`;
        static if(is(typeof({ mixin(enumMixinStr_MSC_RAW); }))) {
            mixin(enumMixinStr_MSC_RAW);
        }
    }




    static if(!is(typeof(MSC_SCAN))) {
        private enum enumMixinStr_MSC_SCAN = `enum MSC_SCAN = 0x04;`;
        static if(is(typeof({ mixin(enumMixinStr_MSC_SCAN); }))) {
            mixin(enumMixinStr_MSC_SCAN);
        }
    }




    static if(!is(typeof(MSC_TIMESTAMP))) {
        private enum enumMixinStr_MSC_TIMESTAMP = `enum MSC_TIMESTAMP = 0x05;`;
        static if(is(typeof({ mixin(enumMixinStr_MSC_TIMESTAMP); }))) {
            mixin(enumMixinStr_MSC_TIMESTAMP);
        }
    }




    static if(!is(typeof(MSC_MAX))) {
        private enum enumMixinStr_MSC_MAX = `enum MSC_MAX = 0x07;`;
        static if(is(typeof({ mixin(enumMixinStr_MSC_MAX); }))) {
            mixin(enumMixinStr_MSC_MAX);
        }
    }




    static if(!is(typeof(MSC_CNT))) {
        private enum enumMixinStr_MSC_CNT = `enum MSC_CNT = ( 0x07 + 1 );`;
        static if(is(typeof({ mixin(enumMixinStr_MSC_CNT); }))) {
            mixin(enumMixinStr_MSC_CNT);
        }
    }




    static if(!is(typeof(LED_NUML))) {
        private enum enumMixinStr_LED_NUML = `enum LED_NUML = 0x00;`;
        static if(is(typeof({ mixin(enumMixinStr_LED_NUML); }))) {
            mixin(enumMixinStr_LED_NUML);
        }
    }




    static if(!is(typeof(LED_CAPSL))) {
        private enum enumMixinStr_LED_CAPSL = `enum LED_CAPSL = 0x01;`;
        static if(is(typeof({ mixin(enumMixinStr_LED_CAPSL); }))) {
            mixin(enumMixinStr_LED_CAPSL);
        }
    }




    static if(!is(typeof(LED_SCROLLL))) {
        private enum enumMixinStr_LED_SCROLLL = `enum LED_SCROLLL = 0x02;`;
        static if(is(typeof({ mixin(enumMixinStr_LED_SCROLLL); }))) {
            mixin(enumMixinStr_LED_SCROLLL);
        }
    }




    static if(!is(typeof(LED_COMPOSE))) {
        private enum enumMixinStr_LED_COMPOSE = `enum LED_COMPOSE = 0x03;`;
        static if(is(typeof({ mixin(enumMixinStr_LED_COMPOSE); }))) {
            mixin(enumMixinStr_LED_COMPOSE);
        }
    }




    static if(!is(typeof(LED_KANA))) {
        private enum enumMixinStr_LED_KANA = `enum LED_KANA = 0x04;`;
        static if(is(typeof({ mixin(enumMixinStr_LED_KANA); }))) {
            mixin(enumMixinStr_LED_KANA);
        }
    }




    static if(!is(typeof(LED_SLEEP))) {
        private enum enumMixinStr_LED_SLEEP = `enum LED_SLEEP = 0x05;`;
        static if(is(typeof({ mixin(enumMixinStr_LED_SLEEP); }))) {
            mixin(enumMixinStr_LED_SLEEP);
        }
    }




    static if(!is(typeof(LED_SUSPEND))) {
        private enum enumMixinStr_LED_SUSPEND = `enum LED_SUSPEND = 0x06;`;
        static if(is(typeof({ mixin(enumMixinStr_LED_SUSPEND); }))) {
            mixin(enumMixinStr_LED_SUSPEND);
        }
    }




    static if(!is(typeof(LED_MUTE))) {
        private enum enumMixinStr_LED_MUTE = `enum LED_MUTE = 0x07;`;
        static if(is(typeof({ mixin(enumMixinStr_LED_MUTE); }))) {
            mixin(enumMixinStr_LED_MUTE);
        }
    }




    static if(!is(typeof(LED_MISC))) {
        private enum enumMixinStr_LED_MISC = `enum LED_MISC = 0x08;`;
        static if(is(typeof({ mixin(enumMixinStr_LED_MISC); }))) {
            mixin(enumMixinStr_LED_MISC);
        }
    }




    static if(!is(typeof(LED_MAIL))) {
        private enum enumMixinStr_LED_MAIL = `enum LED_MAIL = 0x09;`;
        static if(is(typeof({ mixin(enumMixinStr_LED_MAIL); }))) {
            mixin(enumMixinStr_LED_MAIL);
        }
    }




    static if(!is(typeof(LED_CHARGING))) {
        private enum enumMixinStr_LED_CHARGING = `enum LED_CHARGING = 0x0a;`;
        static if(is(typeof({ mixin(enumMixinStr_LED_CHARGING); }))) {
            mixin(enumMixinStr_LED_CHARGING);
        }
    }




    static if(!is(typeof(LED_MAX))) {
        private enum enumMixinStr_LED_MAX = `enum LED_MAX = 0x0f;`;
        static if(is(typeof({ mixin(enumMixinStr_LED_MAX); }))) {
            mixin(enumMixinStr_LED_MAX);
        }
    }




    static if(!is(typeof(LED_CNT))) {
        private enum enumMixinStr_LED_CNT = `enum LED_CNT = ( 0x0f + 1 );`;
        static if(is(typeof({ mixin(enumMixinStr_LED_CNT); }))) {
            mixin(enumMixinStr_LED_CNT);
        }
    }




    static if(!is(typeof(REP_DELAY))) {
        private enum enumMixinStr_REP_DELAY = `enum REP_DELAY = 0x00;`;
        static if(is(typeof({ mixin(enumMixinStr_REP_DELAY); }))) {
            mixin(enumMixinStr_REP_DELAY);
        }
    }




    static if(!is(typeof(REP_PERIOD))) {
        private enum enumMixinStr_REP_PERIOD = `enum REP_PERIOD = 0x01;`;
        static if(is(typeof({ mixin(enumMixinStr_REP_PERIOD); }))) {
            mixin(enumMixinStr_REP_PERIOD);
        }
    }




    static if(!is(typeof(REP_MAX))) {
        private enum enumMixinStr_REP_MAX = `enum REP_MAX = 0x01;`;
        static if(is(typeof({ mixin(enumMixinStr_REP_MAX); }))) {
            mixin(enumMixinStr_REP_MAX);
        }
    }




    static if(!is(typeof(REP_CNT))) {
        private enum enumMixinStr_REP_CNT = `enum REP_CNT = ( 0x01 + 1 );`;
        static if(is(typeof({ mixin(enumMixinStr_REP_CNT); }))) {
            mixin(enumMixinStr_REP_CNT);
        }
    }




    static if(!is(typeof(SND_CLICK))) {
        private enum enumMixinStr_SND_CLICK = `enum SND_CLICK = 0x00;`;
        static if(is(typeof({ mixin(enumMixinStr_SND_CLICK); }))) {
            mixin(enumMixinStr_SND_CLICK);
        }
    }




    static if(!is(typeof(SND_BELL))) {
        private enum enumMixinStr_SND_BELL = `enum SND_BELL = 0x01;`;
        static if(is(typeof({ mixin(enumMixinStr_SND_BELL); }))) {
            mixin(enumMixinStr_SND_BELL);
        }
    }




    static if(!is(typeof(SND_TONE))) {
        private enum enumMixinStr_SND_TONE = `enum SND_TONE = 0x02;`;
        static if(is(typeof({ mixin(enumMixinStr_SND_TONE); }))) {
            mixin(enumMixinStr_SND_TONE);
        }
    }




    static if(!is(typeof(SND_MAX))) {
        private enum enumMixinStr_SND_MAX = `enum SND_MAX = 0x07;`;
        static if(is(typeof({ mixin(enumMixinStr_SND_MAX); }))) {
            mixin(enumMixinStr_SND_MAX);
        }
    }




    static if(!is(typeof(SND_CNT))) {
        private enum enumMixinStr_SND_CNT = `enum SND_CNT = ( 0x07 + 1 );`;
        static if(is(typeof({ mixin(enumMixinStr_SND_CNT); }))) {
            mixin(enumMixinStr_SND_CNT);
        }
    }






    static if(!is(typeof(_CS_V7_WIDTH_RESTRICTED_ENVS))) {
        private enum enumMixinStr__CS_V7_WIDTH_RESTRICTED_ENVS = `enum _CS_V7_WIDTH_RESTRICTED_ENVS = _CS_V7_WIDTH_RESTRICTED_ENVS;`;
        static if(is(typeof({ mixin(enumMixinStr__CS_V7_WIDTH_RESTRICTED_ENVS); }))) {
            mixin(enumMixinStr__CS_V7_WIDTH_RESTRICTED_ENVS);
        }
    }




    static if(!is(typeof(_CS_POSIX_V5_WIDTH_RESTRICTED_ENVS))) {
        private enum enumMixinStr__CS_POSIX_V5_WIDTH_RESTRICTED_ENVS = `enum _CS_POSIX_V5_WIDTH_RESTRICTED_ENVS = _CS_V5_WIDTH_RESTRICTED_ENVS;`;
        static if(is(typeof({ mixin(enumMixinStr__CS_POSIX_V5_WIDTH_RESTRICTED_ENVS); }))) {
            mixin(enumMixinStr__CS_POSIX_V5_WIDTH_RESTRICTED_ENVS);
        }
    }




    static if(!is(typeof(_CS_V5_WIDTH_RESTRICTED_ENVS))) {
        private enum enumMixinStr__CS_V5_WIDTH_RESTRICTED_ENVS = `enum _CS_V5_WIDTH_RESTRICTED_ENVS = _CS_V5_WIDTH_RESTRICTED_ENVS;`;
        static if(is(typeof({ mixin(enumMixinStr__CS_V5_WIDTH_RESTRICTED_ENVS); }))) {
            mixin(enumMixinStr__CS_V5_WIDTH_RESTRICTED_ENVS);
        }
    }




    static if(!is(typeof(_CS_GNU_LIBPTHREAD_VERSION))) {
        private enum enumMixinStr__CS_GNU_LIBPTHREAD_VERSION = `enum _CS_GNU_LIBPTHREAD_VERSION = _CS_GNU_LIBPTHREAD_VERSION;`;
        static if(is(typeof({ mixin(enumMixinStr__CS_GNU_LIBPTHREAD_VERSION); }))) {
            mixin(enumMixinStr__CS_GNU_LIBPTHREAD_VERSION);
        }
    }




    static if(!is(typeof(_CS_GNU_LIBC_VERSION))) {
        private enum enumMixinStr__CS_GNU_LIBC_VERSION = `enum _CS_GNU_LIBC_VERSION = _CS_GNU_LIBC_VERSION;`;
        static if(is(typeof({ mixin(enumMixinStr__CS_GNU_LIBC_VERSION); }))) {
            mixin(enumMixinStr__CS_GNU_LIBC_VERSION);
        }
    }




    static if(!is(typeof(_CS_POSIX_V6_WIDTH_RESTRICTED_ENVS))) {
        private enum enumMixinStr__CS_POSIX_V6_WIDTH_RESTRICTED_ENVS = `enum _CS_POSIX_V6_WIDTH_RESTRICTED_ENVS = _CS_V6_WIDTH_RESTRICTED_ENVS;`;
        static if(is(typeof({ mixin(enumMixinStr__CS_POSIX_V6_WIDTH_RESTRICTED_ENVS); }))) {
            mixin(enumMixinStr__CS_POSIX_V6_WIDTH_RESTRICTED_ENVS);
        }
    }




    static if(!is(typeof(_CS_V6_WIDTH_RESTRICTED_ENVS))) {
        private enum enumMixinStr__CS_V6_WIDTH_RESTRICTED_ENVS = `enum _CS_V6_WIDTH_RESTRICTED_ENVS = _CS_V6_WIDTH_RESTRICTED_ENVS;`;
        static if(is(typeof({ mixin(enumMixinStr__CS_V6_WIDTH_RESTRICTED_ENVS); }))) {
            mixin(enumMixinStr__CS_V6_WIDTH_RESTRICTED_ENVS);
        }
    }




    static if(!is(typeof(input_event_sec))) {
        private enum enumMixinStr_input_event_sec = `enum input_event_sec = time . tv_sec;`;
        static if(is(typeof({ mixin(enumMixinStr_input_event_sec); }))) {
            mixin(enumMixinStr_input_event_sec);
        }
    }




    static if(!is(typeof(input_event_usec))) {
        private enum enumMixinStr_input_event_usec = `enum input_event_usec = time . tv_usec;`;
        static if(is(typeof({ mixin(enumMixinStr_input_event_usec); }))) {
            mixin(enumMixinStr_input_event_usec);
        }
    }




    static if(!is(typeof(EV_VERSION))) {
        private enum enumMixinStr_EV_VERSION = `enum EV_VERSION = 0x010001;`;
        static if(is(typeof({ mixin(enumMixinStr_EV_VERSION); }))) {
            mixin(enumMixinStr_EV_VERSION);
        }
    }




    static if(!is(typeof(_CS_PATH))) {
        private enum enumMixinStr__CS_PATH = `enum _CS_PATH = _CS_PATH;`;
        static if(is(typeof({ mixin(enumMixinStr__CS_PATH); }))) {
            mixin(enumMixinStr__CS_PATH);
        }
    }




    static if(!is(typeof(_SC_THREAD_ROBUST_PRIO_PROTECT))) {
        private enum enumMixinStr__SC_THREAD_ROBUST_PRIO_PROTECT = `enum _SC_THREAD_ROBUST_PRIO_PROTECT = _SC_THREAD_ROBUST_PRIO_PROTECT;`;
        static if(is(typeof({ mixin(enumMixinStr__SC_THREAD_ROBUST_PRIO_PROTECT); }))) {
            mixin(enumMixinStr__SC_THREAD_ROBUST_PRIO_PROTECT);
        }
    }




    static if(!is(typeof(_SC_THREAD_ROBUST_PRIO_INHERIT))) {
        private enum enumMixinStr__SC_THREAD_ROBUST_PRIO_INHERIT = `enum _SC_THREAD_ROBUST_PRIO_INHERIT = _SC_THREAD_ROBUST_PRIO_INHERIT;`;
        static if(is(typeof({ mixin(enumMixinStr__SC_THREAD_ROBUST_PRIO_INHERIT); }))) {
            mixin(enumMixinStr__SC_THREAD_ROBUST_PRIO_INHERIT);
        }
    }




    static if(!is(typeof(INPUT_KEYMAP_BY_INDEX))) {
        private enum enumMixinStr_INPUT_KEYMAP_BY_INDEX = `enum INPUT_KEYMAP_BY_INDEX = ( 1 << 0 );`;
        static if(is(typeof({ mixin(enumMixinStr_INPUT_KEYMAP_BY_INDEX); }))) {
            mixin(enumMixinStr_INPUT_KEYMAP_BY_INDEX);
        }
    }




    static if(!is(typeof(_SC_XOPEN_STREAMS))) {
        private enum enumMixinStr__SC_XOPEN_STREAMS = `enum _SC_XOPEN_STREAMS = _SC_XOPEN_STREAMS;`;
        static if(is(typeof({ mixin(enumMixinStr__SC_XOPEN_STREAMS); }))) {
            mixin(enumMixinStr__SC_XOPEN_STREAMS);
        }
    }




    static if(!is(typeof(EVIOCGVERSION))) {
        private enum enumMixinStr_EVIOCGVERSION = `enum EVIOCGVERSION = _IOR ( 'E' , 0x01 , int );`;
        static if(is(typeof({ mixin(enumMixinStr_EVIOCGVERSION); }))) {
            mixin(enumMixinStr_EVIOCGVERSION);
        }
    }




    static if(!is(typeof(EVIOCGID))) {
        private enum enumMixinStr_EVIOCGID = `enum EVIOCGID = _IOR ( 'E' , 0x02 , input_id );`;
        static if(is(typeof({ mixin(enumMixinStr_EVIOCGID); }))) {
            mixin(enumMixinStr_EVIOCGID);
        }
    }




    static if(!is(typeof(EVIOCGREP))) {
        private enum enumMixinStr_EVIOCGREP = `enum EVIOCGREP = _IOR ( 'E' , 0x03 , unsigned int [ 2 ] );`;
        static if(is(typeof({ mixin(enumMixinStr_EVIOCGREP); }))) {
            mixin(enumMixinStr_EVIOCGREP);
        }
    }




    static if(!is(typeof(EVIOCSREP))) {
        private enum enumMixinStr_EVIOCSREP = `enum EVIOCSREP = _IOW ( 'E' , 0x03 , unsigned int [ 2 ] );`;
        static if(is(typeof({ mixin(enumMixinStr_EVIOCSREP); }))) {
            mixin(enumMixinStr_EVIOCSREP);
        }
    }




    static if(!is(typeof(EVIOCGKEYCODE))) {
        private enum enumMixinStr_EVIOCGKEYCODE = `enum EVIOCGKEYCODE = _IOR ( 'E' , 0x04 , unsigned int [ 2 ] );`;
        static if(is(typeof({ mixin(enumMixinStr_EVIOCGKEYCODE); }))) {
            mixin(enumMixinStr_EVIOCGKEYCODE);
        }
    }




    static if(!is(typeof(EVIOCGKEYCODE_V2))) {
        private enum enumMixinStr_EVIOCGKEYCODE_V2 = `enum EVIOCGKEYCODE_V2 = _IOR ( 'E' , 0x04 , input_keymap_entry );`;
        static if(is(typeof({ mixin(enumMixinStr_EVIOCGKEYCODE_V2); }))) {
            mixin(enumMixinStr_EVIOCGKEYCODE_V2);
        }
    }




    static if(!is(typeof(EVIOCSKEYCODE))) {
        private enum enumMixinStr_EVIOCSKEYCODE = `enum EVIOCSKEYCODE = _IOW ( 'E' , 0x04 , unsigned int [ 2 ] );`;
        static if(is(typeof({ mixin(enumMixinStr_EVIOCSKEYCODE); }))) {
            mixin(enumMixinStr_EVIOCSKEYCODE);
        }
    }




    static if(!is(typeof(EVIOCSKEYCODE_V2))) {
        private enum enumMixinStr_EVIOCSKEYCODE_V2 = `enum EVIOCSKEYCODE_V2 = _IOW ( 'E' , 0x04 , input_keymap_entry );`;
        static if(is(typeof({ mixin(enumMixinStr_EVIOCSKEYCODE_V2); }))) {
            mixin(enumMixinStr_EVIOCSKEYCODE_V2);
        }
    }
    static if(!is(typeof(EVIOCSFF))) {
        private enum enumMixinStr_EVIOCSFF = `enum EVIOCSFF = _IOW ( 'E' , 0x80 , ff_effect );`;
        static if(is(typeof({ mixin(enumMixinStr_EVIOCSFF); }))) {
            mixin(enumMixinStr_EVIOCSFF);
        }
    }




    static if(!is(typeof(EVIOCRMFF))) {
        private enum enumMixinStr_EVIOCRMFF = `enum EVIOCRMFF = _IOW ( 'E' , 0x81 , int );`;
        static if(is(typeof({ mixin(enumMixinStr_EVIOCRMFF); }))) {
            mixin(enumMixinStr_EVIOCRMFF);
        }
    }




    static if(!is(typeof(EVIOCGEFFECTS))) {
        private enum enumMixinStr_EVIOCGEFFECTS = `enum EVIOCGEFFECTS = _IOR ( 'E' , 0x84 , int );`;
        static if(is(typeof({ mixin(enumMixinStr_EVIOCGEFFECTS); }))) {
            mixin(enumMixinStr_EVIOCGEFFECTS);
        }
    }




    static if(!is(typeof(EVIOCGRAB))) {
        private enum enumMixinStr_EVIOCGRAB = `enum EVIOCGRAB = _IOW ( 'E' , 0x90 , int );`;
        static if(is(typeof({ mixin(enumMixinStr_EVIOCGRAB); }))) {
            mixin(enumMixinStr_EVIOCGRAB);
        }
    }




    static if(!is(typeof(EVIOCREVOKE))) {
        private enum enumMixinStr_EVIOCREVOKE = `enum EVIOCREVOKE = _IOW ( 'E' , 0x91 , int );`;
        static if(is(typeof({ mixin(enumMixinStr_EVIOCREVOKE); }))) {
            mixin(enumMixinStr_EVIOCREVOKE);
        }
    }




    static if(!is(typeof(EVIOCGMASK))) {
        private enum enumMixinStr_EVIOCGMASK = `enum EVIOCGMASK = _IOR ( 'E' , 0x92 , input_mask );`;
        static if(is(typeof({ mixin(enumMixinStr_EVIOCGMASK); }))) {
            mixin(enumMixinStr_EVIOCGMASK);
        }
    }




    static if(!is(typeof(EVIOCSMASK))) {
        private enum enumMixinStr_EVIOCSMASK = `enum EVIOCSMASK = _IOW ( 'E' , 0x93 , input_mask );`;
        static if(is(typeof({ mixin(enumMixinStr_EVIOCSMASK); }))) {
            mixin(enumMixinStr_EVIOCSMASK);
        }
    }




    static if(!is(typeof(EVIOCSCLOCKID))) {
        private enum enumMixinStr_EVIOCSCLOCKID = `enum EVIOCSCLOCKID = _IOW ( 'E' , 0xa0 , int );`;
        static if(is(typeof({ mixin(enumMixinStr_EVIOCSCLOCKID); }))) {
            mixin(enumMixinStr_EVIOCSCLOCKID);
        }
    }




    static if(!is(typeof(ID_BUS))) {
        private enum enumMixinStr_ID_BUS = `enum ID_BUS = 0;`;
        static if(is(typeof({ mixin(enumMixinStr_ID_BUS); }))) {
            mixin(enumMixinStr_ID_BUS);
        }
    }




    static if(!is(typeof(ID_VENDOR))) {
        private enum enumMixinStr_ID_VENDOR = `enum ID_VENDOR = 1;`;
        static if(is(typeof({ mixin(enumMixinStr_ID_VENDOR); }))) {
            mixin(enumMixinStr_ID_VENDOR);
        }
    }




    static if(!is(typeof(ID_PRODUCT))) {
        private enum enumMixinStr_ID_PRODUCT = `enum ID_PRODUCT = 2;`;
        static if(is(typeof({ mixin(enumMixinStr_ID_PRODUCT); }))) {
            mixin(enumMixinStr_ID_PRODUCT);
        }
    }




    static if(!is(typeof(ID_VERSION))) {
        private enum enumMixinStr_ID_VERSION = `enum ID_VERSION = 3;`;
        static if(is(typeof({ mixin(enumMixinStr_ID_VERSION); }))) {
            mixin(enumMixinStr_ID_VERSION);
        }
    }




    static if(!is(typeof(BUS_PCI))) {
        private enum enumMixinStr_BUS_PCI = `enum BUS_PCI = 0x01;`;
        static if(is(typeof({ mixin(enumMixinStr_BUS_PCI); }))) {
            mixin(enumMixinStr_BUS_PCI);
        }
    }




    static if(!is(typeof(BUS_ISAPNP))) {
        private enum enumMixinStr_BUS_ISAPNP = `enum BUS_ISAPNP = 0x02;`;
        static if(is(typeof({ mixin(enumMixinStr_BUS_ISAPNP); }))) {
            mixin(enumMixinStr_BUS_ISAPNP);
        }
    }




    static if(!is(typeof(BUS_USB))) {
        private enum enumMixinStr_BUS_USB = `enum BUS_USB = 0x03;`;
        static if(is(typeof({ mixin(enumMixinStr_BUS_USB); }))) {
            mixin(enumMixinStr_BUS_USB);
        }
    }




    static if(!is(typeof(BUS_HIL))) {
        private enum enumMixinStr_BUS_HIL = `enum BUS_HIL = 0x04;`;
        static if(is(typeof({ mixin(enumMixinStr_BUS_HIL); }))) {
            mixin(enumMixinStr_BUS_HIL);
        }
    }




    static if(!is(typeof(BUS_BLUETOOTH))) {
        private enum enumMixinStr_BUS_BLUETOOTH = `enum BUS_BLUETOOTH = 0x05;`;
        static if(is(typeof({ mixin(enumMixinStr_BUS_BLUETOOTH); }))) {
            mixin(enumMixinStr_BUS_BLUETOOTH);
        }
    }




    static if(!is(typeof(BUS_VIRTUAL))) {
        private enum enumMixinStr_BUS_VIRTUAL = `enum BUS_VIRTUAL = 0x06;`;
        static if(is(typeof({ mixin(enumMixinStr_BUS_VIRTUAL); }))) {
            mixin(enumMixinStr_BUS_VIRTUAL);
        }
    }




    static if(!is(typeof(BUS_ISA))) {
        private enum enumMixinStr_BUS_ISA = `enum BUS_ISA = 0x10;`;
        static if(is(typeof({ mixin(enumMixinStr_BUS_ISA); }))) {
            mixin(enumMixinStr_BUS_ISA);
        }
    }




    static if(!is(typeof(BUS_I8042))) {
        private enum enumMixinStr_BUS_I8042 = `enum BUS_I8042 = 0x11;`;
        static if(is(typeof({ mixin(enumMixinStr_BUS_I8042); }))) {
            mixin(enumMixinStr_BUS_I8042);
        }
    }




    static if(!is(typeof(BUS_XTKBD))) {
        private enum enumMixinStr_BUS_XTKBD = `enum BUS_XTKBD = 0x12;`;
        static if(is(typeof({ mixin(enumMixinStr_BUS_XTKBD); }))) {
            mixin(enumMixinStr_BUS_XTKBD);
        }
    }




    static if(!is(typeof(BUS_RS232))) {
        private enum enumMixinStr_BUS_RS232 = `enum BUS_RS232 = 0x13;`;
        static if(is(typeof({ mixin(enumMixinStr_BUS_RS232); }))) {
            mixin(enumMixinStr_BUS_RS232);
        }
    }




    static if(!is(typeof(BUS_GAMEPORT))) {
        private enum enumMixinStr_BUS_GAMEPORT = `enum BUS_GAMEPORT = 0x14;`;
        static if(is(typeof({ mixin(enumMixinStr_BUS_GAMEPORT); }))) {
            mixin(enumMixinStr_BUS_GAMEPORT);
        }
    }




    static if(!is(typeof(BUS_PARPORT))) {
        private enum enumMixinStr_BUS_PARPORT = `enum BUS_PARPORT = 0x15;`;
        static if(is(typeof({ mixin(enumMixinStr_BUS_PARPORT); }))) {
            mixin(enumMixinStr_BUS_PARPORT);
        }
    }




    static if(!is(typeof(BUS_AMIGA))) {
        private enum enumMixinStr_BUS_AMIGA = `enum BUS_AMIGA = 0x16;`;
        static if(is(typeof({ mixin(enumMixinStr_BUS_AMIGA); }))) {
            mixin(enumMixinStr_BUS_AMIGA);
        }
    }




    static if(!is(typeof(BUS_ADB))) {
        private enum enumMixinStr_BUS_ADB = `enum BUS_ADB = 0x17;`;
        static if(is(typeof({ mixin(enumMixinStr_BUS_ADB); }))) {
            mixin(enumMixinStr_BUS_ADB);
        }
    }




    static if(!is(typeof(BUS_I2C))) {
        private enum enumMixinStr_BUS_I2C = `enum BUS_I2C = 0x18;`;
        static if(is(typeof({ mixin(enumMixinStr_BUS_I2C); }))) {
            mixin(enumMixinStr_BUS_I2C);
        }
    }




    static if(!is(typeof(BUS_HOST))) {
        private enum enumMixinStr_BUS_HOST = `enum BUS_HOST = 0x19;`;
        static if(is(typeof({ mixin(enumMixinStr_BUS_HOST); }))) {
            mixin(enumMixinStr_BUS_HOST);
        }
    }




    static if(!is(typeof(BUS_GSC))) {
        private enum enumMixinStr_BUS_GSC = `enum BUS_GSC = 0x1A;`;
        static if(is(typeof({ mixin(enumMixinStr_BUS_GSC); }))) {
            mixin(enumMixinStr_BUS_GSC);
        }
    }




    static if(!is(typeof(BUS_ATARI))) {
        private enum enumMixinStr_BUS_ATARI = `enum BUS_ATARI = 0x1B;`;
        static if(is(typeof({ mixin(enumMixinStr_BUS_ATARI); }))) {
            mixin(enumMixinStr_BUS_ATARI);
        }
    }




    static if(!is(typeof(BUS_SPI))) {
        private enum enumMixinStr_BUS_SPI = `enum BUS_SPI = 0x1C;`;
        static if(is(typeof({ mixin(enumMixinStr_BUS_SPI); }))) {
            mixin(enumMixinStr_BUS_SPI);
        }
    }




    static if(!is(typeof(BUS_RMI))) {
        private enum enumMixinStr_BUS_RMI = `enum BUS_RMI = 0x1D;`;
        static if(is(typeof({ mixin(enumMixinStr_BUS_RMI); }))) {
            mixin(enumMixinStr_BUS_RMI);
        }
    }




    static if(!is(typeof(BUS_CEC))) {
        private enum enumMixinStr_BUS_CEC = `enum BUS_CEC = 0x1E;`;
        static if(is(typeof({ mixin(enumMixinStr_BUS_CEC); }))) {
            mixin(enumMixinStr_BUS_CEC);
        }
    }




    static if(!is(typeof(BUS_INTEL_ISHTP))) {
        private enum enumMixinStr_BUS_INTEL_ISHTP = `enum BUS_INTEL_ISHTP = 0x1F;`;
        static if(is(typeof({ mixin(enumMixinStr_BUS_INTEL_ISHTP); }))) {
            mixin(enumMixinStr_BUS_INTEL_ISHTP);
        }
    }




    static if(!is(typeof(MT_TOOL_FINGER))) {
        private enum enumMixinStr_MT_TOOL_FINGER = `enum MT_TOOL_FINGER = 0x00;`;
        static if(is(typeof({ mixin(enumMixinStr_MT_TOOL_FINGER); }))) {
            mixin(enumMixinStr_MT_TOOL_FINGER);
        }
    }




    static if(!is(typeof(MT_TOOL_PEN))) {
        private enum enumMixinStr_MT_TOOL_PEN = `enum MT_TOOL_PEN = 0x01;`;
        static if(is(typeof({ mixin(enumMixinStr_MT_TOOL_PEN); }))) {
            mixin(enumMixinStr_MT_TOOL_PEN);
        }
    }




    static if(!is(typeof(MT_TOOL_PALM))) {
        private enum enumMixinStr_MT_TOOL_PALM = `enum MT_TOOL_PALM = 0x02;`;
        static if(is(typeof({ mixin(enumMixinStr_MT_TOOL_PALM); }))) {
            mixin(enumMixinStr_MT_TOOL_PALM);
        }
    }




    static if(!is(typeof(MT_TOOL_DIAL))) {
        private enum enumMixinStr_MT_TOOL_DIAL = `enum MT_TOOL_DIAL = 0x0a;`;
        static if(is(typeof({ mixin(enumMixinStr_MT_TOOL_DIAL); }))) {
            mixin(enumMixinStr_MT_TOOL_DIAL);
        }
    }




    static if(!is(typeof(MT_TOOL_MAX))) {
        private enum enumMixinStr_MT_TOOL_MAX = `enum MT_TOOL_MAX = 0x0f;`;
        static if(is(typeof({ mixin(enumMixinStr_MT_TOOL_MAX); }))) {
            mixin(enumMixinStr_MT_TOOL_MAX);
        }
    }




    static if(!is(typeof(FF_STATUS_STOPPED))) {
        private enum enumMixinStr_FF_STATUS_STOPPED = `enum FF_STATUS_STOPPED = 0x00;`;
        static if(is(typeof({ mixin(enumMixinStr_FF_STATUS_STOPPED); }))) {
            mixin(enumMixinStr_FF_STATUS_STOPPED);
        }
    }




    static if(!is(typeof(FF_STATUS_PLAYING))) {
        private enum enumMixinStr_FF_STATUS_PLAYING = `enum FF_STATUS_PLAYING = 0x01;`;
        static if(is(typeof({ mixin(enumMixinStr_FF_STATUS_PLAYING); }))) {
            mixin(enumMixinStr_FF_STATUS_PLAYING);
        }
    }




    static if(!is(typeof(FF_STATUS_MAX))) {
        private enum enumMixinStr_FF_STATUS_MAX = `enum FF_STATUS_MAX = 0x01;`;
        static if(is(typeof({ mixin(enumMixinStr_FF_STATUS_MAX); }))) {
            mixin(enumMixinStr_FF_STATUS_MAX);
        }
    }




    static if(!is(typeof(_SC_TRACE_USER_EVENT_MAX))) {
        private enum enumMixinStr__SC_TRACE_USER_EVENT_MAX = `enum _SC_TRACE_USER_EVENT_MAX = _SC_TRACE_USER_EVENT_MAX;`;
        static if(is(typeof({ mixin(enumMixinStr__SC_TRACE_USER_EVENT_MAX); }))) {
            mixin(enumMixinStr__SC_TRACE_USER_EVENT_MAX);
        }
    }




    static if(!is(typeof(_SC_TRACE_SYS_MAX))) {
        private enum enumMixinStr__SC_TRACE_SYS_MAX = `enum _SC_TRACE_SYS_MAX = _SC_TRACE_SYS_MAX;`;
        static if(is(typeof({ mixin(enumMixinStr__SC_TRACE_SYS_MAX); }))) {
            mixin(enumMixinStr__SC_TRACE_SYS_MAX);
        }
    }




    static if(!is(typeof(_SC_TRACE_NAME_MAX))) {
        private enum enumMixinStr__SC_TRACE_NAME_MAX = `enum _SC_TRACE_NAME_MAX = _SC_TRACE_NAME_MAX;`;
        static if(is(typeof({ mixin(enumMixinStr__SC_TRACE_NAME_MAX); }))) {
            mixin(enumMixinStr__SC_TRACE_NAME_MAX);
        }
    }




    static if(!is(typeof(_SC_TRACE_EVENT_NAME_MAX))) {
        private enum enumMixinStr__SC_TRACE_EVENT_NAME_MAX = `enum _SC_TRACE_EVENT_NAME_MAX = _SC_TRACE_EVENT_NAME_MAX;`;
        static if(is(typeof({ mixin(enumMixinStr__SC_TRACE_EVENT_NAME_MAX); }))) {
            mixin(enumMixinStr__SC_TRACE_EVENT_NAME_MAX);
        }
    }




    static if(!is(typeof(_SC_SS_REPL_MAX))) {
        private enum enumMixinStr__SC_SS_REPL_MAX = `enum _SC_SS_REPL_MAX = _SC_SS_REPL_MAX;`;
        static if(is(typeof({ mixin(enumMixinStr__SC_SS_REPL_MAX); }))) {
            mixin(enumMixinStr__SC_SS_REPL_MAX);
        }
    }




    static if(!is(typeof(_SC_V7_LPBIG_OFFBIG))) {
        private enum enumMixinStr__SC_V7_LPBIG_OFFBIG = `enum _SC_V7_LPBIG_OFFBIG = _SC_V7_LPBIG_OFFBIG;`;
        static if(is(typeof({ mixin(enumMixinStr__SC_V7_LPBIG_OFFBIG); }))) {
            mixin(enumMixinStr__SC_V7_LPBIG_OFFBIG);
        }
    }




    static if(!is(typeof(_SC_V7_LP64_OFF64))) {
        private enum enumMixinStr__SC_V7_LP64_OFF64 = `enum _SC_V7_LP64_OFF64 = _SC_V7_LP64_OFF64;`;
        static if(is(typeof({ mixin(enumMixinStr__SC_V7_LP64_OFF64); }))) {
            mixin(enumMixinStr__SC_V7_LP64_OFF64);
        }
    }




    static if(!is(typeof(_SC_V7_ILP32_OFFBIG))) {
        private enum enumMixinStr__SC_V7_ILP32_OFFBIG = `enum _SC_V7_ILP32_OFFBIG = _SC_V7_ILP32_OFFBIG;`;
        static if(is(typeof({ mixin(enumMixinStr__SC_V7_ILP32_OFFBIG); }))) {
            mixin(enumMixinStr__SC_V7_ILP32_OFFBIG);
        }
    }




    static if(!is(typeof(_SC_V7_ILP32_OFF32))) {
        private enum enumMixinStr__SC_V7_ILP32_OFF32 = `enum _SC_V7_ILP32_OFF32 = _SC_V7_ILP32_OFF32;`;
        static if(is(typeof({ mixin(enumMixinStr__SC_V7_ILP32_OFF32); }))) {
            mixin(enumMixinStr__SC_V7_ILP32_OFF32);
        }
    }




    static if(!is(typeof(FF_RUMBLE))) {
        private enum enumMixinStr_FF_RUMBLE = `enum FF_RUMBLE = 0x50;`;
        static if(is(typeof({ mixin(enumMixinStr_FF_RUMBLE); }))) {
            mixin(enumMixinStr_FF_RUMBLE);
        }
    }




    static if(!is(typeof(FF_PERIODIC))) {
        private enum enumMixinStr_FF_PERIODIC = `enum FF_PERIODIC = 0x51;`;
        static if(is(typeof({ mixin(enumMixinStr_FF_PERIODIC); }))) {
            mixin(enumMixinStr_FF_PERIODIC);
        }
    }




    static if(!is(typeof(FF_CONSTANT))) {
        private enum enumMixinStr_FF_CONSTANT = `enum FF_CONSTANT = 0x52;`;
        static if(is(typeof({ mixin(enumMixinStr_FF_CONSTANT); }))) {
            mixin(enumMixinStr_FF_CONSTANT);
        }
    }




    static if(!is(typeof(FF_SPRING))) {
        private enum enumMixinStr_FF_SPRING = `enum FF_SPRING = 0x53;`;
        static if(is(typeof({ mixin(enumMixinStr_FF_SPRING); }))) {
            mixin(enumMixinStr_FF_SPRING);
        }
    }




    static if(!is(typeof(FF_FRICTION))) {
        private enum enumMixinStr_FF_FRICTION = `enum FF_FRICTION = 0x54;`;
        static if(is(typeof({ mixin(enumMixinStr_FF_FRICTION); }))) {
            mixin(enumMixinStr_FF_FRICTION);
        }
    }




    static if(!is(typeof(FF_DAMPER))) {
        private enum enumMixinStr_FF_DAMPER = `enum FF_DAMPER = 0x55;`;
        static if(is(typeof({ mixin(enumMixinStr_FF_DAMPER); }))) {
            mixin(enumMixinStr_FF_DAMPER);
        }
    }




    static if(!is(typeof(FF_INERTIA))) {
        private enum enumMixinStr_FF_INERTIA = `enum FF_INERTIA = 0x56;`;
        static if(is(typeof({ mixin(enumMixinStr_FF_INERTIA); }))) {
            mixin(enumMixinStr_FF_INERTIA);
        }
    }




    static if(!is(typeof(FF_RAMP))) {
        private enum enumMixinStr_FF_RAMP = `enum FF_RAMP = 0x57;`;
        static if(is(typeof({ mixin(enumMixinStr_FF_RAMP); }))) {
            mixin(enumMixinStr_FF_RAMP);
        }
    }




    static if(!is(typeof(FF_EFFECT_MIN))) {
        private enum enumMixinStr_FF_EFFECT_MIN = `enum FF_EFFECT_MIN = 0x50;`;
        static if(is(typeof({ mixin(enumMixinStr_FF_EFFECT_MIN); }))) {
            mixin(enumMixinStr_FF_EFFECT_MIN);
        }
    }




    static if(!is(typeof(FF_EFFECT_MAX))) {
        private enum enumMixinStr_FF_EFFECT_MAX = `enum FF_EFFECT_MAX = 0x57;`;
        static if(is(typeof({ mixin(enumMixinStr_FF_EFFECT_MAX); }))) {
            mixin(enumMixinStr_FF_EFFECT_MAX);
        }
    }




    static if(!is(typeof(FF_SQUARE))) {
        private enum enumMixinStr_FF_SQUARE = `enum FF_SQUARE = 0x58;`;
        static if(is(typeof({ mixin(enumMixinStr_FF_SQUARE); }))) {
            mixin(enumMixinStr_FF_SQUARE);
        }
    }




    static if(!is(typeof(FF_TRIANGLE))) {
        private enum enumMixinStr_FF_TRIANGLE = `enum FF_TRIANGLE = 0x59;`;
        static if(is(typeof({ mixin(enumMixinStr_FF_TRIANGLE); }))) {
            mixin(enumMixinStr_FF_TRIANGLE);
        }
    }




    static if(!is(typeof(FF_SINE))) {
        private enum enumMixinStr_FF_SINE = `enum FF_SINE = 0x5a;`;
        static if(is(typeof({ mixin(enumMixinStr_FF_SINE); }))) {
            mixin(enumMixinStr_FF_SINE);
        }
    }




    static if(!is(typeof(FF_SAW_UP))) {
        private enum enumMixinStr_FF_SAW_UP = `enum FF_SAW_UP = 0x5b;`;
        static if(is(typeof({ mixin(enumMixinStr_FF_SAW_UP); }))) {
            mixin(enumMixinStr_FF_SAW_UP);
        }
    }




    static if(!is(typeof(FF_SAW_DOWN))) {
        private enum enumMixinStr_FF_SAW_DOWN = `enum FF_SAW_DOWN = 0x5c;`;
        static if(is(typeof({ mixin(enumMixinStr_FF_SAW_DOWN); }))) {
            mixin(enumMixinStr_FF_SAW_DOWN);
        }
    }




    static if(!is(typeof(FF_CUSTOM))) {
        private enum enumMixinStr_FF_CUSTOM = `enum FF_CUSTOM = 0x5d;`;
        static if(is(typeof({ mixin(enumMixinStr_FF_CUSTOM); }))) {
            mixin(enumMixinStr_FF_CUSTOM);
        }
    }




    static if(!is(typeof(FF_WAVEFORM_MIN))) {
        private enum enumMixinStr_FF_WAVEFORM_MIN = `enum FF_WAVEFORM_MIN = 0x58;`;
        static if(is(typeof({ mixin(enumMixinStr_FF_WAVEFORM_MIN); }))) {
            mixin(enumMixinStr_FF_WAVEFORM_MIN);
        }
    }




    static if(!is(typeof(FF_WAVEFORM_MAX))) {
        private enum enumMixinStr_FF_WAVEFORM_MAX = `enum FF_WAVEFORM_MAX = 0x5d;`;
        static if(is(typeof({ mixin(enumMixinStr_FF_WAVEFORM_MAX); }))) {
            mixin(enumMixinStr_FF_WAVEFORM_MAX);
        }
    }




    static if(!is(typeof(FF_GAIN))) {
        private enum enumMixinStr_FF_GAIN = `enum FF_GAIN = 0x60;`;
        static if(is(typeof({ mixin(enumMixinStr_FF_GAIN); }))) {
            mixin(enumMixinStr_FF_GAIN);
        }
    }




    static if(!is(typeof(FF_AUTOCENTER))) {
        private enum enumMixinStr_FF_AUTOCENTER = `enum FF_AUTOCENTER = 0x61;`;
        static if(is(typeof({ mixin(enumMixinStr_FF_AUTOCENTER); }))) {
            mixin(enumMixinStr_FF_AUTOCENTER);
        }
    }




    static if(!is(typeof(FF_MAX_EFFECTS))) {
        private enum enumMixinStr_FF_MAX_EFFECTS = `enum FF_MAX_EFFECTS = 0x60;`;
        static if(is(typeof({ mixin(enumMixinStr_FF_MAX_EFFECTS); }))) {
            mixin(enumMixinStr_FF_MAX_EFFECTS);
        }
    }




    static if(!is(typeof(FF_MAX))) {
        private enum enumMixinStr_FF_MAX = `enum FF_MAX = 0x7f;`;
        static if(is(typeof({ mixin(enumMixinStr_FF_MAX); }))) {
            mixin(enumMixinStr_FF_MAX);
        }
    }




    static if(!is(typeof(FF_CNT))) {
        private enum enumMixinStr_FF_CNT = `enum FF_CNT = ( 0x7f + 1 );`;
        static if(is(typeof({ mixin(enumMixinStr_FF_CNT); }))) {
            mixin(enumMixinStr_FF_CNT);
        }
    }






    static if(!is(typeof(_SC_RAW_SOCKETS))) {
        private enum enumMixinStr__SC_RAW_SOCKETS = `enum _SC_RAW_SOCKETS = _SC_RAW_SOCKETS;`;
        static if(is(typeof({ mixin(enumMixinStr__SC_RAW_SOCKETS); }))) {
            mixin(enumMixinStr__SC_RAW_SOCKETS);
        }
    }






    static if(!is(typeof(_SC_IPV6))) {
        private enum enumMixinStr__SC_IPV6 = `enum _SC_IPV6 = _SC_IPV6;`;
        static if(is(typeof({ mixin(enumMixinStr__SC_IPV6); }))) {
            mixin(enumMixinStr__SC_IPV6);
        }
    }




    static if(!is(typeof(_SC_LEVEL4_CACHE_LINESIZE))) {
        private enum enumMixinStr__SC_LEVEL4_CACHE_LINESIZE = `enum _SC_LEVEL4_CACHE_LINESIZE = _SC_LEVEL4_CACHE_LINESIZE;`;
        static if(is(typeof({ mixin(enumMixinStr__SC_LEVEL4_CACHE_LINESIZE); }))) {
            mixin(enumMixinStr__SC_LEVEL4_CACHE_LINESIZE);
        }
    }




    static if(!is(typeof(_SC_LEVEL4_CACHE_ASSOC))) {
        private enum enumMixinStr__SC_LEVEL4_CACHE_ASSOC = `enum _SC_LEVEL4_CACHE_ASSOC = _SC_LEVEL4_CACHE_ASSOC;`;
        static if(is(typeof({ mixin(enumMixinStr__SC_LEVEL4_CACHE_ASSOC); }))) {
            mixin(enumMixinStr__SC_LEVEL4_CACHE_ASSOC);
        }
    }




    static if(!is(typeof(_SC_LEVEL4_CACHE_SIZE))) {
        private enum enumMixinStr__SC_LEVEL4_CACHE_SIZE = `enum _SC_LEVEL4_CACHE_SIZE = _SC_LEVEL4_CACHE_SIZE;`;
        static if(is(typeof({ mixin(enumMixinStr__SC_LEVEL4_CACHE_SIZE); }))) {
            mixin(enumMixinStr__SC_LEVEL4_CACHE_SIZE);
        }
    }




    static if(!is(typeof(_SC_LEVEL3_CACHE_LINESIZE))) {
        private enum enumMixinStr__SC_LEVEL3_CACHE_LINESIZE = `enum _SC_LEVEL3_CACHE_LINESIZE = _SC_LEVEL3_CACHE_LINESIZE;`;
        static if(is(typeof({ mixin(enumMixinStr__SC_LEVEL3_CACHE_LINESIZE); }))) {
            mixin(enumMixinStr__SC_LEVEL3_CACHE_LINESIZE);
        }
    }




    static if(!is(typeof(_SC_LEVEL3_CACHE_ASSOC))) {
        private enum enumMixinStr__SC_LEVEL3_CACHE_ASSOC = `enum _SC_LEVEL3_CACHE_ASSOC = _SC_LEVEL3_CACHE_ASSOC;`;
        static if(is(typeof({ mixin(enumMixinStr__SC_LEVEL3_CACHE_ASSOC); }))) {
            mixin(enumMixinStr__SC_LEVEL3_CACHE_ASSOC);
        }
    }




    static if(!is(typeof(_SC_LEVEL3_CACHE_SIZE))) {
        private enum enumMixinStr__SC_LEVEL3_CACHE_SIZE = `enum _SC_LEVEL3_CACHE_SIZE = _SC_LEVEL3_CACHE_SIZE;`;
        static if(is(typeof({ mixin(enumMixinStr__SC_LEVEL3_CACHE_SIZE); }))) {
            mixin(enumMixinStr__SC_LEVEL3_CACHE_SIZE);
        }
    }




    static if(!is(typeof(_SC_LEVEL2_CACHE_LINESIZE))) {
        private enum enumMixinStr__SC_LEVEL2_CACHE_LINESIZE = `enum _SC_LEVEL2_CACHE_LINESIZE = _SC_LEVEL2_CACHE_LINESIZE;`;
        static if(is(typeof({ mixin(enumMixinStr__SC_LEVEL2_CACHE_LINESIZE); }))) {
            mixin(enumMixinStr__SC_LEVEL2_CACHE_LINESIZE);
        }
    }




    static if(!is(typeof(_SC_LEVEL2_CACHE_ASSOC))) {
        private enum enumMixinStr__SC_LEVEL2_CACHE_ASSOC = `enum _SC_LEVEL2_CACHE_ASSOC = _SC_LEVEL2_CACHE_ASSOC;`;
        static if(is(typeof({ mixin(enumMixinStr__SC_LEVEL2_CACHE_ASSOC); }))) {
            mixin(enumMixinStr__SC_LEVEL2_CACHE_ASSOC);
        }
    }






    static if(!is(typeof(_SC_LEVEL2_CACHE_SIZE))) {
        private enum enumMixinStr__SC_LEVEL2_CACHE_SIZE = `enum _SC_LEVEL2_CACHE_SIZE = _SC_LEVEL2_CACHE_SIZE;`;
        static if(is(typeof({ mixin(enumMixinStr__SC_LEVEL2_CACHE_SIZE); }))) {
            mixin(enumMixinStr__SC_LEVEL2_CACHE_SIZE);
        }
    }




    static if(!is(typeof(_SC_LEVEL1_DCACHE_LINESIZE))) {
        private enum enumMixinStr__SC_LEVEL1_DCACHE_LINESIZE = `enum _SC_LEVEL1_DCACHE_LINESIZE = _SC_LEVEL1_DCACHE_LINESIZE;`;
        static if(is(typeof({ mixin(enumMixinStr__SC_LEVEL1_DCACHE_LINESIZE); }))) {
            mixin(enumMixinStr__SC_LEVEL1_DCACHE_LINESIZE);
        }
    }






    static if(!is(typeof(__bitwise))) {
        private enum enumMixinStr___bitwise = `enum __bitwise = ;`;
        static if(is(typeof({ mixin(enumMixinStr___bitwise); }))) {
            mixin(enumMixinStr___bitwise);
        }
    }




    static if(!is(typeof(_SC_LEVEL1_DCACHE_ASSOC))) {
        private enum enumMixinStr__SC_LEVEL1_DCACHE_ASSOC = `enum _SC_LEVEL1_DCACHE_ASSOC = _SC_LEVEL1_DCACHE_ASSOC;`;
        static if(is(typeof({ mixin(enumMixinStr__SC_LEVEL1_DCACHE_ASSOC); }))) {
            mixin(enumMixinStr__SC_LEVEL1_DCACHE_ASSOC);
        }
    }




    static if(!is(typeof(_SC_LEVEL1_DCACHE_SIZE))) {
        private enum enumMixinStr__SC_LEVEL1_DCACHE_SIZE = `enum _SC_LEVEL1_DCACHE_SIZE = _SC_LEVEL1_DCACHE_SIZE;`;
        static if(is(typeof({ mixin(enumMixinStr__SC_LEVEL1_DCACHE_SIZE); }))) {
            mixin(enumMixinStr__SC_LEVEL1_DCACHE_SIZE);
        }
    }




    static if(!is(typeof(_SC_LEVEL1_ICACHE_LINESIZE))) {
        private enum enumMixinStr__SC_LEVEL1_ICACHE_LINESIZE = `enum _SC_LEVEL1_ICACHE_LINESIZE = _SC_LEVEL1_ICACHE_LINESIZE;`;
        static if(is(typeof({ mixin(enumMixinStr__SC_LEVEL1_ICACHE_LINESIZE); }))) {
            mixin(enumMixinStr__SC_LEVEL1_ICACHE_LINESIZE);
        }
    }




    static if(!is(typeof(_SC_LEVEL1_ICACHE_ASSOC))) {
        private enum enumMixinStr__SC_LEVEL1_ICACHE_ASSOC = `enum _SC_LEVEL1_ICACHE_ASSOC = _SC_LEVEL1_ICACHE_ASSOC;`;
        static if(is(typeof({ mixin(enumMixinStr__SC_LEVEL1_ICACHE_ASSOC); }))) {
            mixin(enumMixinStr__SC_LEVEL1_ICACHE_ASSOC);
        }
    }




    static if(!is(typeof(_SC_LEVEL1_ICACHE_SIZE))) {
        private enum enumMixinStr__SC_LEVEL1_ICACHE_SIZE = `enum _SC_LEVEL1_ICACHE_SIZE = _SC_LEVEL1_ICACHE_SIZE;`;
        static if(is(typeof({ mixin(enumMixinStr__SC_LEVEL1_ICACHE_SIZE); }))) {
            mixin(enumMixinStr__SC_LEVEL1_ICACHE_SIZE);
        }
    }




    static if(!is(typeof(_SC_TRACE_LOG))) {
        private enum enumMixinStr__SC_TRACE_LOG = `enum _SC_TRACE_LOG = _SC_TRACE_LOG;`;
        static if(is(typeof({ mixin(enumMixinStr__SC_TRACE_LOG); }))) {
            mixin(enumMixinStr__SC_TRACE_LOG);
        }
    }




    static if(!is(typeof(_SC_TRACE_INHERIT))) {
        private enum enumMixinStr__SC_TRACE_INHERIT = `enum _SC_TRACE_INHERIT = _SC_TRACE_INHERIT;`;
        static if(is(typeof({ mixin(enumMixinStr__SC_TRACE_INHERIT); }))) {
            mixin(enumMixinStr__SC_TRACE_INHERIT);
        }
    }




    static if(!is(typeof(_SC_TRACE_EVENT_FILTER))) {
        private enum enumMixinStr__SC_TRACE_EVENT_FILTER = `enum _SC_TRACE_EVENT_FILTER = _SC_TRACE_EVENT_FILTER;`;
        static if(is(typeof({ mixin(enumMixinStr__SC_TRACE_EVENT_FILTER); }))) {
            mixin(enumMixinStr__SC_TRACE_EVENT_FILTER);
        }
    }




    static if(!is(typeof(_SC_TRACE))) {
        private enum enumMixinStr__SC_TRACE = `enum _SC_TRACE = _SC_TRACE;`;
        static if(is(typeof({ mixin(enumMixinStr__SC_TRACE); }))) {
            mixin(enumMixinStr__SC_TRACE);
        }
    }




    static if(!is(typeof(_SC_HOST_NAME_MAX))) {
        private enum enumMixinStr__SC_HOST_NAME_MAX = `enum _SC_HOST_NAME_MAX = _SC_HOST_NAME_MAX;`;
        static if(is(typeof({ mixin(enumMixinStr__SC_HOST_NAME_MAX); }))) {
            mixin(enumMixinStr__SC_HOST_NAME_MAX);
        }
    }




    static if(!is(typeof(_SC_V6_LPBIG_OFFBIG))) {
        private enum enumMixinStr__SC_V6_LPBIG_OFFBIG = `enum _SC_V6_LPBIG_OFFBIG = _SC_V6_LPBIG_OFFBIG;`;
        static if(is(typeof({ mixin(enumMixinStr__SC_V6_LPBIG_OFFBIG); }))) {
            mixin(enumMixinStr__SC_V6_LPBIG_OFFBIG);
        }
    }




    static if(!is(typeof(_SC_V6_LP64_OFF64))) {
        private enum enumMixinStr__SC_V6_LP64_OFF64 = `enum _SC_V6_LP64_OFF64 = _SC_V6_LP64_OFF64;`;
        static if(is(typeof({ mixin(enumMixinStr__SC_V6_LP64_OFF64); }))) {
            mixin(enumMixinStr__SC_V6_LP64_OFF64);
        }
    }




    static if(!is(typeof(_SC_V6_ILP32_OFFBIG))) {
        private enum enumMixinStr__SC_V6_ILP32_OFFBIG = `enum _SC_V6_ILP32_OFFBIG = _SC_V6_ILP32_OFFBIG;`;
        static if(is(typeof({ mixin(enumMixinStr__SC_V6_ILP32_OFFBIG); }))) {
            mixin(enumMixinStr__SC_V6_ILP32_OFFBIG);
        }
    }




    static if(!is(typeof(_SC_V6_ILP32_OFF32))) {
        private enum enumMixinStr__SC_V6_ILP32_OFF32 = `enum _SC_V6_ILP32_OFF32 = _SC_V6_ILP32_OFF32;`;
        static if(is(typeof({ mixin(enumMixinStr__SC_V6_ILP32_OFF32); }))) {
            mixin(enumMixinStr__SC_V6_ILP32_OFF32);
        }
    }




    static if(!is(typeof(_SC_2_PBS_CHECKPOINT))) {
        private enum enumMixinStr__SC_2_PBS_CHECKPOINT = `enum _SC_2_PBS_CHECKPOINT = _SC_2_PBS_CHECKPOINT;`;
        static if(is(typeof({ mixin(enumMixinStr__SC_2_PBS_CHECKPOINT); }))) {
            mixin(enumMixinStr__SC_2_PBS_CHECKPOINT);
        }
    }




    static if(!is(typeof(_SC_STREAMS))) {
        private enum enumMixinStr__SC_STREAMS = `enum _SC_STREAMS = _SC_STREAMS;`;
        static if(is(typeof({ mixin(enumMixinStr__SC_STREAMS); }))) {
            mixin(enumMixinStr__SC_STREAMS);
        }
    }




    static if(!is(typeof(__aligned_u64))) {
        private enum enumMixinStr___aligned_u64 = `enum __aligned_u64 = __u64 __attribute__ ( ( aligned ( 8 ) ) );`;
        static if(is(typeof({ mixin(enumMixinStr___aligned_u64); }))) {
            mixin(enumMixinStr___aligned_u64);
        }
    }




    static if(!is(typeof(__aligned_be64))) {
        private enum enumMixinStr___aligned_be64 = `enum __aligned_be64 = __be64 __attribute__ ( ( aligned ( 8 ) ) );`;
        static if(is(typeof({ mixin(enumMixinStr___aligned_be64); }))) {
            mixin(enumMixinStr___aligned_be64);
        }
    }




    static if(!is(typeof(__aligned_le64))) {
        private enum enumMixinStr___aligned_le64 = `enum __aligned_le64 = __le64 __attribute__ ( ( aligned ( 8 ) ) );`;
        static if(is(typeof({ mixin(enumMixinStr___aligned_le64); }))) {
            mixin(enumMixinStr___aligned_le64);
        }
    }




    static if(!is(typeof(_SC_SYMLOOP_MAX))) {
        private enum enumMixinStr__SC_SYMLOOP_MAX = `enum _SC_SYMLOOP_MAX = _SC_SYMLOOP_MAX;`;
        static if(is(typeof({ mixin(enumMixinStr__SC_SYMLOOP_MAX); }))) {
            mixin(enumMixinStr__SC_SYMLOOP_MAX);
        }
    }




    static if(!is(typeof(_SC_2_PBS_TRACK))) {
        private enum enumMixinStr__SC_2_PBS_TRACK = `enum _SC_2_PBS_TRACK = _SC_2_PBS_TRACK;`;
        static if(is(typeof({ mixin(enumMixinStr__SC_2_PBS_TRACK); }))) {
            mixin(enumMixinStr__SC_2_PBS_TRACK);
        }
    }




    static if(!is(typeof(_STDC_PREDEF_H))) {
        private enum enumMixinStr__STDC_PREDEF_H = `enum _STDC_PREDEF_H = 1;`;
        static if(is(typeof({ mixin(enumMixinStr__STDC_PREDEF_H); }))) {
            mixin(enumMixinStr__STDC_PREDEF_H);
        }
    }




    static if(!is(typeof(_STRING_H))) {
        private enum enumMixinStr__STRING_H = `enum _STRING_H = 1;`;
        static if(is(typeof({ mixin(enumMixinStr__STRING_H); }))) {
            mixin(enumMixinStr__STRING_H);
        }
    }






    static if(!is(typeof(_SC_2_PBS_MESSAGE))) {
        private enum enumMixinStr__SC_2_PBS_MESSAGE = `enum _SC_2_PBS_MESSAGE = _SC_2_PBS_MESSAGE;`;
        static if(is(typeof({ mixin(enumMixinStr__SC_2_PBS_MESSAGE); }))) {
            mixin(enumMixinStr__SC_2_PBS_MESSAGE);
        }
    }




    static if(!is(typeof(_SC_2_PBS_LOCATE))) {
        private enum enumMixinStr__SC_2_PBS_LOCATE = `enum _SC_2_PBS_LOCATE = _SC_2_PBS_LOCATE;`;
        static if(is(typeof({ mixin(enumMixinStr__SC_2_PBS_LOCATE); }))) {
            mixin(enumMixinStr__SC_2_PBS_LOCATE);
        }
    }
    static if(!is(typeof(_SC_2_PBS_ACCOUNTING))) {
        private enum enumMixinStr__SC_2_PBS_ACCOUNTING = `enum _SC_2_PBS_ACCOUNTING = _SC_2_PBS_ACCOUNTING;`;
        static if(is(typeof({ mixin(enumMixinStr__SC_2_PBS_ACCOUNTING); }))) {
            mixin(enumMixinStr__SC_2_PBS_ACCOUNTING);
        }
    }




    static if(!is(typeof(_SC_2_PBS))) {
        private enum enumMixinStr__SC_2_PBS = `enum _SC_2_PBS = _SC_2_PBS;`;
        static if(is(typeof({ mixin(enumMixinStr__SC_2_PBS); }))) {
            mixin(enumMixinStr__SC_2_PBS);
        }
    }




    static if(!is(typeof(_SC_USER_GROUPS_R))) {
        private enum enumMixinStr__SC_USER_GROUPS_R = `enum _SC_USER_GROUPS_R = _SC_USER_GROUPS_R;`;
        static if(is(typeof({ mixin(enumMixinStr__SC_USER_GROUPS_R); }))) {
            mixin(enumMixinStr__SC_USER_GROUPS_R);
        }
    }




    static if(!is(typeof(_SC_USER_GROUPS))) {
        private enum enumMixinStr__SC_USER_GROUPS = `enum _SC_USER_GROUPS = _SC_USER_GROUPS;`;
        static if(is(typeof({ mixin(enumMixinStr__SC_USER_GROUPS); }))) {
            mixin(enumMixinStr__SC_USER_GROUPS);
        }
    }




    static if(!is(typeof(_SC_TYPED_MEMORY_OBJECTS))) {
        private enum enumMixinStr__SC_TYPED_MEMORY_OBJECTS = `enum _SC_TYPED_MEMORY_OBJECTS = _SC_TYPED_MEMORY_OBJECTS;`;
        static if(is(typeof({ mixin(enumMixinStr__SC_TYPED_MEMORY_OBJECTS); }))) {
            mixin(enumMixinStr__SC_TYPED_MEMORY_OBJECTS);
        }
    }




    static if(!is(typeof(_SC_TIMEOUTS))) {
        private enum enumMixinStr__SC_TIMEOUTS = `enum _SC_TIMEOUTS = _SC_TIMEOUTS;`;
        static if(is(typeof({ mixin(enumMixinStr__SC_TIMEOUTS); }))) {
            mixin(enumMixinStr__SC_TIMEOUTS);
        }
    }




    static if(!is(typeof(_SC_SYSTEM_DATABASE_R))) {
        private enum enumMixinStr__SC_SYSTEM_DATABASE_R = `enum _SC_SYSTEM_DATABASE_R = _SC_SYSTEM_DATABASE_R;`;
        static if(is(typeof({ mixin(enumMixinStr__SC_SYSTEM_DATABASE_R); }))) {
            mixin(enumMixinStr__SC_SYSTEM_DATABASE_R);
        }
    }




    static if(!is(typeof(_SC_SYSTEM_DATABASE))) {
        private enum enumMixinStr__SC_SYSTEM_DATABASE = `enum _SC_SYSTEM_DATABASE = _SC_SYSTEM_DATABASE;`;
        static if(is(typeof({ mixin(enumMixinStr__SC_SYSTEM_DATABASE); }))) {
            mixin(enumMixinStr__SC_SYSTEM_DATABASE);
        }
    }




    static if(!is(typeof(_SC_THREAD_SPORADIC_SERVER))) {
        private enum enumMixinStr__SC_THREAD_SPORADIC_SERVER = `enum _SC_THREAD_SPORADIC_SERVER = _SC_THREAD_SPORADIC_SERVER;`;
        static if(is(typeof({ mixin(enumMixinStr__SC_THREAD_SPORADIC_SERVER); }))) {
            mixin(enumMixinStr__SC_THREAD_SPORADIC_SERVER);
        }
    }




    static if(!is(typeof(_SC_SPORADIC_SERVER))) {
        private enum enumMixinStr__SC_SPORADIC_SERVER = `enum _SC_SPORADIC_SERVER = _SC_SPORADIC_SERVER;`;
        static if(is(typeof({ mixin(enumMixinStr__SC_SPORADIC_SERVER); }))) {
            mixin(enumMixinStr__SC_SPORADIC_SERVER);
        }
    }




    static if(!is(typeof(_SC_SPAWN))) {
        private enum enumMixinStr__SC_SPAWN = `enum _SC_SPAWN = _SC_SPAWN;`;
        static if(is(typeof({ mixin(enumMixinStr__SC_SPAWN); }))) {
            mixin(enumMixinStr__SC_SPAWN);
        }
    }




    static if(!is(typeof(_SC_SIGNALS))) {
        private enum enumMixinStr__SC_SIGNALS = `enum _SC_SIGNALS = _SC_SIGNALS;`;
        static if(is(typeof({ mixin(enumMixinStr__SC_SIGNALS); }))) {
            mixin(enumMixinStr__SC_SIGNALS);
        }
    }




    static if(!is(typeof(_SC_SHELL))) {
        private enum enumMixinStr__SC_SHELL = `enum _SC_SHELL = _SC_SHELL;`;
        static if(is(typeof({ mixin(enumMixinStr__SC_SHELL); }))) {
            mixin(enumMixinStr__SC_SHELL);
        }
    }




    static if(!is(typeof(_SC_REGEX_VERSION))) {
        private enum enumMixinStr__SC_REGEX_VERSION = `enum _SC_REGEX_VERSION = _SC_REGEX_VERSION;`;
        static if(is(typeof({ mixin(enumMixinStr__SC_REGEX_VERSION); }))) {
            mixin(enumMixinStr__SC_REGEX_VERSION);
        }
    }




    static if(!is(typeof(_SC_REGEXP))) {
        private enum enumMixinStr__SC_REGEXP = `enum _SC_REGEXP = _SC_REGEXP;`;
        static if(is(typeof({ mixin(enumMixinStr__SC_REGEXP); }))) {
            mixin(enumMixinStr__SC_REGEXP);
        }
    }




    static if(!is(typeof(_SC_SPIN_LOCKS))) {
        private enum enumMixinStr__SC_SPIN_LOCKS = `enum _SC_SPIN_LOCKS = _SC_SPIN_LOCKS;`;
        static if(is(typeof({ mixin(enumMixinStr__SC_SPIN_LOCKS); }))) {
            mixin(enumMixinStr__SC_SPIN_LOCKS);
        }
    }




    static if(!is(typeof(_SC_READER_WRITER_LOCKS))) {
        private enum enumMixinStr__SC_READER_WRITER_LOCKS = `enum _SC_READER_WRITER_LOCKS = _SC_READER_WRITER_LOCKS;`;
        static if(is(typeof({ mixin(enumMixinStr__SC_READER_WRITER_LOCKS); }))) {
            mixin(enumMixinStr__SC_READER_WRITER_LOCKS);
        }
    }




    static if(!is(typeof(_SC_NETWORKING))) {
        private enum enumMixinStr__SC_NETWORKING = `enum _SC_NETWORKING = _SC_NETWORKING;`;
        static if(is(typeof({ mixin(enumMixinStr__SC_NETWORKING); }))) {
            mixin(enumMixinStr__SC_NETWORKING);
        }
    }




    static if(!is(typeof(_SC_SINGLE_PROCESS))) {
        private enum enumMixinStr__SC_SINGLE_PROCESS = `enum _SC_SINGLE_PROCESS = _SC_SINGLE_PROCESS;`;
        static if(is(typeof({ mixin(enumMixinStr__SC_SINGLE_PROCESS); }))) {
            mixin(enumMixinStr__SC_SINGLE_PROCESS);
        }
    }




    static if(!is(typeof(_SC_MULTI_PROCESS))) {
        private enum enumMixinStr__SC_MULTI_PROCESS = `enum _SC_MULTI_PROCESS = _SC_MULTI_PROCESS;`;
        static if(is(typeof({ mixin(enumMixinStr__SC_MULTI_PROCESS); }))) {
            mixin(enumMixinStr__SC_MULTI_PROCESS);
        }
    }




    static if(!is(typeof(_SC_MONOTONIC_CLOCK))) {
        private enum enumMixinStr__SC_MONOTONIC_CLOCK = `enum _SC_MONOTONIC_CLOCK = _SC_MONOTONIC_CLOCK;`;
        static if(is(typeof({ mixin(enumMixinStr__SC_MONOTONIC_CLOCK); }))) {
            mixin(enumMixinStr__SC_MONOTONIC_CLOCK);
        }
    }




    static if(!is(typeof(_SC_FILE_SYSTEM))) {
        private enum enumMixinStr__SC_FILE_SYSTEM = `enum _SC_FILE_SYSTEM = _SC_FILE_SYSTEM;`;
        static if(is(typeof({ mixin(enumMixinStr__SC_FILE_SYSTEM); }))) {
            mixin(enumMixinStr__SC_FILE_SYSTEM);
        }
    }




    static if(!is(typeof(_SC_FILE_LOCKING))) {
        private enum enumMixinStr__SC_FILE_LOCKING = `enum _SC_FILE_LOCKING = _SC_FILE_LOCKING;`;
        static if(is(typeof({ mixin(enumMixinStr__SC_FILE_LOCKING); }))) {
            mixin(enumMixinStr__SC_FILE_LOCKING);
        }
    }




    static if(!is(typeof(_SC_FILE_ATTRIBUTES))) {
        private enum enumMixinStr__SC_FILE_ATTRIBUTES = `enum _SC_FILE_ATTRIBUTES = _SC_FILE_ATTRIBUTES;`;
        static if(is(typeof({ mixin(enumMixinStr__SC_FILE_ATTRIBUTES); }))) {
            mixin(enumMixinStr__SC_FILE_ATTRIBUTES);
        }
    }




    static if(!is(typeof(_SC_PIPE))) {
        private enum enumMixinStr__SC_PIPE = `enum _SC_PIPE = _SC_PIPE;`;
        static if(is(typeof({ mixin(enumMixinStr__SC_PIPE); }))) {
            mixin(enumMixinStr__SC_PIPE);
        }
    }




    static if(!is(typeof(_SC_FIFO))) {
        private enum enumMixinStr__SC_FIFO = `enum _SC_FIFO = _SC_FIFO;`;
        static if(is(typeof({ mixin(enumMixinStr__SC_FIFO); }))) {
            mixin(enumMixinStr__SC_FIFO);
        }
    }




    static if(!is(typeof(_SC_FD_MGMT))) {
        private enum enumMixinStr__SC_FD_MGMT = `enum _SC_FD_MGMT = _SC_FD_MGMT;`;
        static if(is(typeof({ mixin(enumMixinStr__SC_FD_MGMT); }))) {
            mixin(enumMixinStr__SC_FD_MGMT);
        }
    }




    static if(!is(typeof(_SC_DEVICE_SPECIFIC_R))) {
        private enum enumMixinStr__SC_DEVICE_SPECIFIC_R = `enum _SC_DEVICE_SPECIFIC_R = _SC_DEVICE_SPECIFIC_R;`;
        static if(is(typeof({ mixin(enumMixinStr__SC_DEVICE_SPECIFIC_R); }))) {
            mixin(enumMixinStr__SC_DEVICE_SPECIFIC_R);
        }
    }




    static if(!is(typeof(_SC_DEVICE_SPECIFIC))) {
        private enum enumMixinStr__SC_DEVICE_SPECIFIC = `enum _SC_DEVICE_SPECIFIC = _SC_DEVICE_SPECIFIC;`;
        static if(is(typeof({ mixin(enumMixinStr__SC_DEVICE_SPECIFIC); }))) {
            mixin(enumMixinStr__SC_DEVICE_SPECIFIC);
        }
    }




    static if(!is(typeof(_SC_DEVICE_IO))) {
        private enum enumMixinStr__SC_DEVICE_IO = `enum _SC_DEVICE_IO = _SC_DEVICE_IO;`;
        static if(is(typeof({ mixin(enumMixinStr__SC_DEVICE_IO); }))) {
            mixin(enumMixinStr__SC_DEVICE_IO);
        }
    }




    static if(!is(typeof(_SC_THREAD_CPUTIME))) {
        private enum enumMixinStr__SC_THREAD_CPUTIME = `enum _SC_THREAD_CPUTIME = _SC_THREAD_CPUTIME;`;
        static if(is(typeof({ mixin(enumMixinStr__SC_THREAD_CPUTIME); }))) {
            mixin(enumMixinStr__SC_THREAD_CPUTIME);
        }
    }




    static if(!is(typeof(_SC_CPUTIME))) {
        private enum enumMixinStr__SC_CPUTIME = `enum _SC_CPUTIME = _SC_CPUTIME;`;
        static if(is(typeof({ mixin(enumMixinStr__SC_CPUTIME); }))) {
            mixin(enumMixinStr__SC_CPUTIME);
        }
    }




    static if(!is(typeof(_SC_CLOCK_SELECTION))) {
        private enum enumMixinStr__SC_CLOCK_SELECTION = `enum _SC_CLOCK_SELECTION = _SC_CLOCK_SELECTION;`;
        static if(is(typeof({ mixin(enumMixinStr__SC_CLOCK_SELECTION); }))) {
            mixin(enumMixinStr__SC_CLOCK_SELECTION);
        }
    }




    static if(!is(typeof(_SC_C_LANG_SUPPORT_R))) {
        private enum enumMixinStr__SC_C_LANG_SUPPORT_R = `enum _SC_C_LANG_SUPPORT_R = _SC_C_LANG_SUPPORT_R;`;
        static if(is(typeof({ mixin(enumMixinStr__SC_C_LANG_SUPPORT_R); }))) {
            mixin(enumMixinStr__SC_C_LANG_SUPPORT_R);
        }
    }




    static if(!is(typeof(_SC_C_LANG_SUPPORT))) {
        private enum enumMixinStr__SC_C_LANG_SUPPORT = `enum _SC_C_LANG_SUPPORT = _SC_C_LANG_SUPPORT;`;
        static if(is(typeof({ mixin(enumMixinStr__SC_C_LANG_SUPPORT); }))) {
            mixin(enumMixinStr__SC_C_LANG_SUPPORT);
        }
    }




    static if(!is(typeof(_SC_BASE))) {
        private enum enumMixinStr__SC_BASE = `enum _SC_BASE = _SC_BASE;`;
        static if(is(typeof({ mixin(enumMixinStr__SC_BASE); }))) {
            mixin(enumMixinStr__SC_BASE);
        }
    }




    static if(!is(typeof(_SC_BARRIERS))) {
        private enum enumMixinStr__SC_BARRIERS = `enum _SC_BARRIERS = _SC_BARRIERS;`;
        static if(is(typeof({ mixin(enumMixinStr__SC_BARRIERS); }))) {
            mixin(enumMixinStr__SC_BARRIERS);
        }
    }




    static if(!is(typeof(_SC_ADVISORY_INFO))) {
        private enum enumMixinStr__SC_ADVISORY_INFO = `enum _SC_ADVISORY_INFO = _SC_ADVISORY_INFO;`;
        static if(is(typeof({ mixin(enumMixinStr__SC_ADVISORY_INFO); }))) {
            mixin(enumMixinStr__SC_ADVISORY_INFO);
        }
    }




    static if(!is(typeof(_SC_XOPEN_REALTIME_THREADS))) {
        private enum enumMixinStr__SC_XOPEN_REALTIME_THREADS = `enum _SC_XOPEN_REALTIME_THREADS = _SC_XOPEN_REALTIME_THREADS;`;
        static if(is(typeof({ mixin(enumMixinStr__SC_XOPEN_REALTIME_THREADS); }))) {
            mixin(enumMixinStr__SC_XOPEN_REALTIME_THREADS);
        }
    }




    static if(!is(typeof(_SC_XOPEN_REALTIME))) {
        private enum enumMixinStr__SC_XOPEN_REALTIME = `enum _SC_XOPEN_REALTIME = _SC_XOPEN_REALTIME;`;
        static if(is(typeof({ mixin(enumMixinStr__SC_XOPEN_REALTIME); }))) {
            mixin(enumMixinStr__SC_XOPEN_REALTIME);
        }
    }




    static if(!is(typeof(_SC_XOPEN_LEGACY))) {
        private enum enumMixinStr__SC_XOPEN_LEGACY = `enum _SC_XOPEN_LEGACY = _SC_XOPEN_LEGACY;`;
        static if(is(typeof({ mixin(enumMixinStr__SC_XOPEN_LEGACY); }))) {
            mixin(enumMixinStr__SC_XOPEN_LEGACY);
        }
    }




    static if(!is(typeof(_SC_XBS5_LPBIG_OFFBIG))) {
        private enum enumMixinStr__SC_XBS5_LPBIG_OFFBIG = `enum _SC_XBS5_LPBIG_OFFBIG = _SC_XBS5_LPBIG_OFFBIG;`;
        static if(is(typeof({ mixin(enumMixinStr__SC_XBS5_LPBIG_OFFBIG); }))) {
            mixin(enumMixinStr__SC_XBS5_LPBIG_OFFBIG);
        }
    }




    static if(!is(typeof(_SC_XBS5_LP64_OFF64))) {
        private enum enumMixinStr__SC_XBS5_LP64_OFF64 = `enum _SC_XBS5_LP64_OFF64 = _SC_XBS5_LP64_OFF64;`;
        static if(is(typeof({ mixin(enumMixinStr__SC_XBS5_LP64_OFF64); }))) {
            mixin(enumMixinStr__SC_XBS5_LP64_OFF64);
        }
    }




    static if(!is(typeof(_SC_XBS5_ILP32_OFFBIG))) {
        private enum enumMixinStr__SC_XBS5_ILP32_OFFBIG = `enum _SC_XBS5_ILP32_OFFBIG = _SC_XBS5_ILP32_OFFBIG;`;
        static if(is(typeof({ mixin(enumMixinStr__SC_XBS5_ILP32_OFFBIG); }))) {
            mixin(enumMixinStr__SC_XBS5_ILP32_OFFBIG);
        }
    }




    static if(!is(typeof(_SC_XBS5_ILP32_OFF32))) {
        private enum enumMixinStr__SC_XBS5_ILP32_OFF32 = `enum _SC_XBS5_ILP32_OFF32 = _SC_XBS5_ILP32_OFF32;`;
        static if(is(typeof({ mixin(enumMixinStr__SC_XBS5_ILP32_OFF32); }))) {
            mixin(enumMixinStr__SC_XBS5_ILP32_OFF32);
        }
    }




    static if(!is(typeof(_SC_NL_TEXTMAX))) {
        private enum enumMixinStr__SC_NL_TEXTMAX = `enum _SC_NL_TEXTMAX = _SC_NL_TEXTMAX;`;
        static if(is(typeof({ mixin(enumMixinStr__SC_NL_TEXTMAX); }))) {
            mixin(enumMixinStr__SC_NL_TEXTMAX);
        }
    }




    static if(!is(typeof(_SC_NL_SETMAX))) {
        private enum enumMixinStr__SC_NL_SETMAX = `enum _SC_NL_SETMAX = _SC_NL_SETMAX;`;
        static if(is(typeof({ mixin(enumMixinStr__SC_NL_SETMAX); }))) {
            mixin(enumMixinStr__SC_NL_SETMAX);
        }
    }




    static if(!is(typeof(_SC_NL_NMAX))) {
        private enum enumMixinStr__SC_NL_NMAX = `enum _SC_NL_NMAX = _SC_NL_NMAX;`;
        static if(is(typeof({ mixin(enumMixinStr__SC_NL_NMAX); }))) {
            mixin(enumMixinStr__SC_NL_NMAX);
        }
    }




    static if(!is(typeof(_SC_NL_MSGMAX))) {
        private enum enumMixinStr__SC_NL_MSGMAX = `enum _SC_NL_MSGMAX = _SC_NL_MSGMAX;`;
        static if(is(typeof({ mixin(enumMixinStr__SC_NL_MSGMAX); }))) {
            mixin(enumMixinStr__SC_NL_MSGMAX);
        }
    }




    static if(!is(typeof(_SC_NL_LANGMAX))) {
        private enum enumMixinStr__SC_NL_LANGMAX = `enum _SC_NL_LANGMAX = _SC_NL_LANGMAX;`;
        static if(is(typeof({ mixin(enumMixinStr__SC_NL_LANGMAX); }))) {
            mixin(enumMixinStr__SC_NL_LANGMAX);
        }
    }




    static if(!is(typeof(_SC_NL_ARGMAX))) {
        private enum enumMixinStr__SC_NL_ARGMAX = `enum _SC_NL_ARGMAX = _SC_NL_ARGMAX;`;
        static if(is(typeof({ mixin(enumMixinStr__SC_NL_ARGMAX); }))) {
            mixin(enumMixinStr__SC_NL_ARGMAX);
        }
    }




    static if(!is(typeof(_SC_USHRT_MAX))) {
        private enum enumMixinStr__SC_USHRT_MAX = `enum _SC_USHRT_MAX = _SC_USHRT_MAX;`;
        static if(is(typeof({ mixin(enumMixinStr__SC_USHRT_MAX); }))) {
            mixin(enumMixinStr__SC_USHRT_MAX);
        }
    }




    static if(!is(typeof(_SC_ULONG_MAX))) {
        private enum enumMixinStr__SC_ULONG_MAX = `enum _SC_ULONG_MAX = _SC_ULONG_MAX;`;
        static if(is(typeof({ mixin(enumMixinStr__SC_ULONG_MAX); }))) {
            mixin(enumMixinStr__SC_ULONG_MAX);
        }
    }




    static if(!is(typeof(_SC_UINT_MAX))) {
        private enum enumMixinStr__SC_UINT_MAX = `enum _SC_UINT_MAX = _SC_UINT_MAX;`;
        static if(is(typeof({ mixin(enumMixinStr__SC_UINT_MAX); }))) {
            mixin(enumMixinStr__SC_UINT_MAX);
        }
    }




    static if(!is(typeof(_SC_UCHAR_MAX))) {
        private enum enumMixinStr__SC_UCHAR_MAX = `enum _SC_UCHAR_MAX = _SC_UCHAR_MAX;`;
        static if(is(typeof({ mixin(enumMixinStr__SC_UCHAR_MAX); }))) {
            mixin(enumMixinStr__SC_UCHAR_MAX);
        }
    }




    static if(!is(typeof(_SC_SHRT_MIN))) {
        private enum enumMixinStr__SC_SHRT_MIN = `enum _SC_SHRT_MIN = _SC_SHRT_MIN;`;
        static if(is(typeof({ mixin(enumMixinStr__SC_SHRT_MIN); }))) {
            mixin(enumMixinStr__SC_SHRT_MIN);
        }
    }




    static if(!is(typeof(_SC_SHRT_MAX))) {
        private enum enumMixinStr__SC_SHRT_MAX = `enum _SC_SHRT_MAX = _SC_SHRT_MAX;`;
        static if(is(typeof({ mixin(enumMixinStr__SC_SHRT_MAX); }))) {
            mixin(enumMixinStr__SC_SHRT_MAX);
        }
    }




    static if(!is(typeof(_SC_SCHAR_MIN))) {
        private enum enumMixinStr__SC_SCHAR_MIN = `enum _SC_SCHAR_MIN = _SC_SCHAR_MIN;`;
        static if(is(typeof({ mixin(enumMixinStr__SC_SCHAR_MIN); }))) {
            mixin(enumMixinStr__SC_SCHAR_MIN);
        }
    }




    static if(!is(typeof(_SC_SCHAR_MAX))) {
        private enum enumMixinStr__SC_SCHAR_MAX = `enum _SC_SCHAR_MAX = _SC_SCHAR_MAX;`;
        static if(is(typeof({ mixin(enumMixinStr__SC_SCHAR_MAX); }))) {
            mixin(enumMixinStr__SC_SCHAR_MAX);
        }
    }




    static if(!is(typeof(_SC_SSIZE_MAX))) {
        private enum enumMixinStr__SC_SSIZE_MAX = `enum _SC_SSIZE_MAX = _SC_SSIZE_MAX;`;
        static if(is(typeof({ mixin(enumMixinStr__SC_SSIZE_MAX); }))) {
            mixin(enumMixinStr__SC_SSIZE_MAX);
        }
    }




    static if(!is(typeof(_SC_NZERO))) {
        private enum enumMixinStr__SC_NZERO = `enum _SC_NZERO = _SC_NZERO;`;
        static if(is(typeof({ mixin(enumMixinStr__SC_NZERO); }))) {
            mixin(enumMixinStr__SC_NZERO);
        }
    }




    static if(!is(typeof(_SC_MB_LEN_MAX))) {
        private enum enumMixinStr__SC_MB_LEN_MAX = `enum _SC_MB_LEN_MAX = _SC_MB_LEN_MAX;`;
        static if(is(typeof({ mixin(enumMixinStr__SC_MB_LEN_MAX); }))) {
            mixin(enumMixinStr__SC_MB_LEN_MAX);
        }
    }




    static if(!is(typeof(_SC_WORD_BIT))) {
        private enum enumMixinStr__SC_WORD_BIT = `enum _SC_WORD_BIT = _SC_WORD_BIT;`;
        static if(is(typeof({ mixin(enumMixinStr__SC_WORD_BIT); }))) {
            mixin(enumMixinStr__SC_WORD_BIT);
        }
    }




    static if(!is(typeof(_SC_LONG_BIT))) {
        private enum enumMixinStr__SC_LONG_BIT = `enum _SC_LONG_BIT = _SC_LONG_BIT;`;
        static if(is(typeof({ mixin(enumMixinStr__SC_LONG_BIT); }))) {
            mixin(enumMixinStr__SC_LONG_BIT);
        }
    }




    static if(!is(typeof(_SC_INT_MIN))) {
        private enum enumMixinStr__SC_INT_MIN = `enum _SC_INT_MIN = _SC_INT_MIN;`;
        static if(is(typeof({ mixin(enumMixinStr__SC_INT_MIN); }))) {
            mixin(enumMixinStr__SC_INT_MIN);
        }
    }




    static if(!is(typeof(_SC_INT_MAX))) {
        private enum enumMixinStr__SC_INT_MAX = `enum _SC_INT_MAX = _SC_INT_MAX;`;
        static if(is(typeof({ mixin(enumMixinStr__SC_INT_MAX); }))) {
            mixin(enumMixinStr__SC_INT_MAX);
        }
    }




    static if(!is(typeof(_SC_CHAR_MIN))) {
        private enum enumMixinStr__SC_CHAR_MIN = `enum _SC_CHAR_MIN = _SC_CHAR_MIN;`;
        static if(is(typeof({ mixin(enumMixinStr__SC_CHAR_MIN); }))) {
            mixin(enumMixinStr__SC_CHAR_MIN);
        }
    }




    static if(!is(typeof(_SC_CHAR_MAX))) {
        private enum enumMixinStr__SC_CHAR_MAX = `enum _SC_CHAR_MAX = _SC_CHAR_MAX;`;
        static if(is(typeof({ mixin(enumMixinStr__SC_CHAR_MAX); }))) {
            mixin(enumMixinStr__SC_CHAR_MAX);
        }
    }




    static if(!is(typeof(_SC_CHAR_BIT))) {
        private enum enumMixinStr__SC_CHAR_BIT = `enum _SC_CHAR_BIT = _SC_CHAR_BIT;`;
        static if(is(typeof({ mixin(enumMixinStr__SC_CHAR_BIT); }))) {
            mixin(enumMixinStr__SC_CHAR_BIT);
        }
    }




    static if(!is(typeof(_SC_XOPEN_XPG4))) {
        private enum enumMixinStr__SC_XOPEN_XPG4 = `enum _SC_XOPEN_XPG4 = _SC_XOPEN_XPG4;`;
        static if(is(typeof({ mixin(enumMixinStr__SC_XOPEN_XPG4); }))) {
            mixin(enumMixinStr__SC_XOPEN_XPG4);
        }
    }




    static if(!is(typeof(_SC_XOPEN_XPG3))) {
        private enum enumMixinStr__SC_XOPEN_XPG3 = `enum _SC_XOPEN_XPG3 = _SC_XOPEN_XPG3;`;
        static if(is(typeof({ mixin(enumMixinStr__SC_XOPEN_XPG3); }))) {
            mixin(enumMixinStr__SC_XOPEN_XPG3);
        }
    }




    static if(!is(typeof(_SC_XOPEN_XPG2))) {
        private enum enumMixinStr__SC_XOPEN_XPG2 = `enum _SC_XOPEN_XPG2 = _SC_XOPEN_XPG2;`;
        static if(is(typeof({ mixin(enumMixinStr__SC_XOPEN_XPG2); }))) {
            mixin(enumMixinStr__SC_XOPEN_XPG2);
        }
    }




    static if(!is(typeof(_SC_2_UPE))) {
        private enum enumMixinStr__SC_2_UPE = `enum _SC_2_UPE = _SC_2_UPE;`;
        static if(is(typeof({ mixin(enumMixinStr__SC_2_UPE); }))) {
            mixin(enumMixinStr__SC_2_UPE);
        }
    }




    static if(!is(typeof(_SC_2_C_VERSION))) {
        private enum enumMixinStr__SC_2_C_VERSION = `enum _SC_2_C_VERSION = _SC_2_C_VERSION;`;
        static if(is(typeof({ mixin(enumMixinStr__SC_2_C_VERSION); }))) {
            mixin(enumMixinStr__SC_2_C_VERSION);
        }
    }




    static if(!is(typeof(_SC_2_CHAR_TERM))) {
        private enum enumMixinStr__SC_2_CHAR_TERM = `enum _SC_2_CHAR_TERM = _SC_2_CHAR_TERM;`;
        static if(is(typeof({ mixin(enumMixinStr__SC_2_CHAR_TERM); }))) {
            mixin(enumMixinStr__SC_2_CHAR_TERM);
        }
    }




    static if(!is(typeof(_SC_XOPEN_SHM))) {
        private enum enumMixinStr__SC_XOPEN_SHM = `enum _SC_XOPEN_SHM = _SC_XOPEN_SHM;`;
        static if(is(typeof({ mixin(enumMixinStr__SC_XOPEN_SHM); }))) {
            mixin(enumMixinStr__SC_XOPEN_SHM);
        }
    }




    static if(!is(typeof(_SC_XOPEN_ENH_I18N))) {
        private enum enumMixinStr__SC_XOPEN_ENH_I18N = `enum _SC_XOPEN_ENH_I18N = _SC_XOPEN_ENH_I18N;`;
        static if(is(typeof({ mixin(enumMixinStr__SC_XOPEN_ENH_I18N); }))) {
            mixin(enumMixinStr__SC_XOPEN_ENH_I18N);
        }
    }




    static if(!is(typeof(_SC_XOPEN_CRYPT))) {
        private enum enumMixinStr__SC_XOPEN_CRYPT = `enum _SC_XOPEN_CRYPT = _SC_XOPEN_CRYPT;`;
        static if(is(typeof({ mixin(enumMixinStr__SC_XOPEN_CRYPT); }))) {
            mixin(enumMixinStr__SC_XOPEN_CRYPT);
        }
    }




    static if(!is(typeof(_SC_XOPEN_UNIX))) {
        private enum enumMixinStr__SC_XOPEN_UNIX = `enum _SC_XOPEN_UNIX = _SC_XOPEN_UNIX;`;
        static if(is(typeof({ mixin(enumMixinStr__SC_XOPEN_UNIX); }))) {
            mixin(enumMixinStr__SC_XOPEN_UNIX);
        }
    }




    static if(!is(typeof(_SC_XOPEN_XCU_VERSION))) {
        private enum enumMixinStr__SC_XOPEN_XCU_VERSION = `enum _SC_XOPEN_XCU_VERSION = _SC_XOPEN_XCU_VERSION;`;
        static if(is(typeof({ mixin(enumMixinStr__SC_XOPEN_XCU_VERSION); }))) {
            mixin(enumMixinStr__SC_XOPEN_XCU_VERSION);
        }
    }




    static if(!is(typeof(_SC_XOPEN_VERSION))) {
        private enum enumMixinStr__SC_XOPEN_VERSION = `enum _SC_XOPEN_VERSION = _SC_XOPEN_VERSION;`;
        static if(is(typeof({ mixin(enumMixinStr__SC_XOPEN_VERSION); }))) {
            mixin(enumMixinStr__SC_XOPEN_VERSION);
        }
    }




    static if(!is(typeof(_SC_PASS_MAX))) {
        private enum enumMixinStr__SC_PASS_MAX = `enum _SC_PASS_MAX = _SC_PASS_MAX;`;
        static if(is(typeof({ mixin(enumMixinStr__SC_PASS_MAX); }))) {
            mixin(enumMixinStr__SC_PASS_MAX);
        }
    }




    static if(!is(typeof(_SC_ATEXIT_MAX))) {
        private enum enumMixinStr__SC_ATEXIT_MAX = `enum _SC_ATEXIT_MAX = _SC_ATEXIT_MAX;`;
        static if(is(typeof({ mixin(enumMixinStr__SC_ATEXIT_MAX); }))) {
            mixin(enumMixinStr__SC_ATEXIT_MAX);
        }
    }




    static if(!is(typeof(_SC_AVPHYS_PAGES))) {
        private enum enumMixinStr__SC_AVPHYS_PAGES = `enum _SC_AVPHYS_PAGES = _SC_AVPHYS_PAGES;`;
        static if(is(typeof({ mixin(enumMixinStr__SC_AVPHYS_PAGES); }))) {
            mixin(enumMixinStr__SC_AVPHYS_PAGES);
        }
    }




    static if(!is(typeof(_SC_PHYS_PAGES))) {
        private enum enumMixinStr__SC_PHYS_PAGES = `enum _SC_PHYS_PAGES = _SC_PHYS_PAGES;`;
        static if(is(typeof({ mixin(enumMixinStr__SC_PHYS_PAGES); }))) {
            mixin(enumMixinStr__SC_PHYS_PAGES);
        }
    }




    static if(!is(typeof(_SC_NPROCESSORS_ONLN))) {
        private enum enumMixinStr__SC_NPROCESSORS_ONLN = `enum _SC_NPROCESSORS_ONLN = _SC_NPROCESSORS_ONLN;`;
        static if(is(typeof({ mixin(enumMixinStr__SC_NPROCESSORS_ONLN); }))) {
            mixin(enumMixinStr__SC_NPROCESSORS_ONLN);
        }
    }




    static if(!is(typeof(_SC_NPROCESSORS_CONF))) {
        private enum enumMixinStr__SC_NPROCESSORS_CONF = `enum _SC_NPROCESSORS_CONF = _SC_NPROCESSORS_CONF;`;
        static if(is(typeof({ mixin(enumMixinStr__SC_NPROCESSORS_CONF); }))) {
            mixin(enumMixinStr__SC_NPROCESSORS_CONF);
        }
    }




    static if(!is(typeof(_SC_THREAD_PROCESS_SHARED))) {
        private enum enumMixinStr__SC_THREAD_PROCESS_SHARED = `enum _SC_THREAD_PROCESS_SHARED = _SC_THREAD_PROCESS_SHARED;`;
        static if(is(typeof({ mixin(enumMixinStr__SC_THREAD_PROCESS_SHARED); }))) {
            mixin(enumMixinStr__SC_THREAD_PROCESS_SHARED);
        }
    }




    static if(!is(typeof(_SC_THREAD_PRIO_PROTECT))) {
        private enum enumMixinStr__SC_THREAD_PRIO_PROTECT = `enum _SC_THREAD_PRIO_PROTECT = _SC_THREAD_PRIO_PROTECT;`;
        static if(is(typeof({ mixin(enumMixinStr__SC_THREAD_PRIO_PROTECT); }))) {
            mixin(enumMixinStr__SC_THREAD_PRIO_PROTECT);
        }
    }




    static if(!is(typeof(_SC_THREAD_PRIO_INHERIT))) {
        private enum enumMixinStr__SC_THREAD_PRIO_INHERIT = `enum _SC_THREAD_PRIO_INHERIT = _SC_THREAD_PRIO_INHERIT;`;
        static if(is(typeof({ mixin(enumMixinStr__SC_THREAD_PRIO_INHERIT); }))) {
            mixin(enumMixinStr__SC_THREAD_PRIO_INHERIT);
        }
    }




    static if(!is(typeof(_SC_THREAD_PRIORITY_SCHEDULING))) {
        private enum enumMixinStr__SC_THREAD_PRIORITY_SCHEDULING = `enum _SC_THREAD_PRIORITY_SCHEDULING = _SC_THREAD_PRIORITY_SCHEDULING;`;
        static if(is(typeof({ mixin(enumMixinStr__SC_THREAD_PRIORITY_SCHEDULING); }))) {
            mixin(enumMixinStr__SC_THREAD_PRIORITY_SCHEDULING);
        }
    }




    static if(!is(typeof(_SC_THREAD_ATTR_STACKSIZE))) {
        private enum enumMixinStr__SC_THREAD_ATTR_STACKSIZE = `enum _SC_THREAD_ATTR_STACKSIZE = _SC_THREAD_ATTR_STACKSIZE;`;
        static if(is(typeof({ mixin(enumMixinStr__SC_THREAD_ATTR_STACKSIZE); }))) {
            mixin(enumMixinStr__SC_THREAD_ATTR_STACKSIZE);
        }
    }




    static if(!is(typeof(_SC_THREAD_ATTR_STACKADDR))) {
        private enum enumMixinStr__SC_THREAD_ATTR_STACKADDR = `enum _SC_THREAD_ATTR_STACKADDR = _SC_THREAD_ATTR_STACKADDR;`;
        static if(is(typeof({ mixin(enumMixinStr__SC_THREAD_ATTR_STACKADDR); }))) {
            mixin(enumMixinStr__SC_THREAD_ATTR_STACKADDR);
        }
    }




    static if(!is(typeof(_SC_THREAD_THREADS_MAX))) {
        private enum enumMixinStr__SC_THREAD_THREADS_MAX = `enum _SC_THREAD_THREADS_MAX = _SC_THREAD_THREADS_MAX;`;
        static if(is(typeof({ mixin(enumMixinStr__SC_THREAD_THREADS_MAX); }))) {
            mixin(enumMixinStr__SC_THREAD_THREADS_MAX);
        }
    }




    static if(!is(typeof(_SC_THREAD_STACK_MIN))) {
        private enum enumMixinStr__SC_THREAD_STACK_MIN = `enum _SC_THREAD_STACK_MIN = _SC_THREAD_STACK_MIN;`;
        static if(is(typeof({ mixin(enumMixinStr__SC_THREAD_STACK_MIN); }))) {
            mixin(enumMixinStr__SC_THREAD_STACK_MIN);
        }
    }




    static if(!is(typeof(_SC_THREAD_KEYS_MAX))) {
        private enum enumMixinStr__SC_THREAD_KEYS_MAX = `enum _SC_THREAD_KEYS_MAX = _SC_THREAD_KEYS_MAX;`;
        static if(is(typeof({ mixin(enumMixinStr__SC_THREAD_KEYS_MAX); }))) {
            mixin(enumMixinStr__SC_THREAD_KEYS_MAX);
        }
    }




    static if(!is(typeof(_SC_THREAD_DESTRUCTOR_ITERATIONS))) {
        private enum enumMixinStr__SC_THREAD_DESTRUCTOR_ITERATIONS = `enum _SC_THREAD_DESTRUCTOR_ITERATIONS = _SC_THREAD_DESTRUCTOR_ITERATIONS;`;
        static if(is(typeof({ mixin(enumMixinStr__SC_THREAD_DESTRUCTOR_ITERATIONS); }))) {
            mixin(enumMixinStr__SC_THREAD_DESTRUCTOR_ITERATIONS);
        }
    }




    static if(!is(typeof(_SC_TTY_NAME_MAX))) {
        private enum enumMixinStr__SC_TTY_NAME_MAX = `enum _SC_TTY_NAME_MAX = _SC_TTY_NAME_MAX;`;
        static if(is(typeof({ mixin(enumMixinStr__SC_TTY_NAME_MAX); }))) {
            mixin(enumMixinStr__SC_TTY_NAME_MAX);
        }
    }




    static if(!is(typeof(_SC_LOGIN_NAME_MAX))) {
        private enum enumMixinStr__SC_LOGIN_NAME_MAX = `enum _SC_LOGIN_NAME_MAX = _SC_LOGIN_NAME_MAX;`;
        static if(is(typeof({ mixin(enumMixinStr__SC_LOGIN_NAME_MAX); }))) {
            mixin(enumMixinStr__SC_LOGIN_NAME_MAX);
        }
    }




    static if(!is(typeof(_SC_GETPW_R_SIZE_MAX))) {
        private enum enumMixinStr__SC_GETPW_R_SIZE_MAX = `enum _SC_GETPW_R_SIZE_MAX = _SC_GETPW_R_SIZE_MAX;`;
        static if(is(typeof({ mixin(enumMixinStr__SC_GETPW_R_SIZE_MAX); }))) {
            mixin(enumMixinStr__SC_GETPW_R_SIZE_MAX);
        }
    }




    static if(!is(typeof(_SC_GETGR_R_SIZE_MAX))) {
        private enum enumMixinStr__SC_GETGR_R_SIZE_MAX = `enum _SC_GETGR_R_SIZE_MAX = _SC_GETGR_R_SIZE_MAX;`;
        static if(is(typeof({ mixin(enumMixinStr__SC_GETGR_R_SIZE_MAX); }))) {
            mixin(enumMixinStr__SC_GETGR_R_SIZE_MAX);
        }
    }




    static if(!is(typeof(_SC_THREAD_SAFE_FUNCTIONS))) {
        private enum enumMixinStr__SC_THREAD_SAFE_FUNCTIONS = `enum _SC_THREAD_SAFE_FUNCTIONS = _SC_THREAD_SAFE_FUNCTIONS;`;
        static if(is(typeof({ mixin(enumMixinStr__SC_THREAD_SAFE_FUNCTIONS); }))) {
            mixin(enumMixinStr__SC_THREAD_SAFE_FUNCTIONS);
        }
    }




    static if(!is(typeof(_SC_THREADS))) {
        private enum enumMixinStr__SC_THREADS = `enum _SC_THREADS = _SC_THREADS;`;
        static if(is(typeof({ mixin(enumMixinStr__SC_THREADS); }))) {
            mixin(enumMixinStr__SC_THREADS);
        }
    }




    static if(!is(typeof(_SC_T_IOV_MAX))) {
        private enum enumMixinStr__SC_T_IOV_MAX = `enum _SC_T_IOV_MAX = _SC_T_IOV_MAX;`;
        static if(is(typeof({ mixin(enumMixinStr__SC_T_IOV_MAX); }))) {
            mixin(enumMixinStr__SC_T_IOV_MAX);
        }
    }




    static if(!is(typeof(_SC_PII_OSI_M))) {
        private enum enumMixinStr__SC_PII_OSI_M = `enum _SC_PII_OSI_M = _SC_PII_OSI_M;`;
        static if(is(typeof({ mixin(enumMixinStr__SC_PII_OSI_M); }))) {
            mixin(enumMixinStr__SC_PII_OSI_M);
        }
    }




    static if(!is(typeof(_SC_PII_OSI_CLTS))) {
        private enum enumMixinStr__SC_PII_OSI_CLTS = `enum _SC_PII_OSI_CLTS = _SC_PII_OSI_CLTS;`;
        static if(is(typeof({ mixin(enumMixinStr__SC_PII_OSI_CLTS); }))) {
            mixin(enumMixinStr__SC_PII_OSI_CLTS);
        }
    }




    static if(!is(typeof(_SC_PII_OSI_COTS))) {
        private enum enumMixinStr__SC_PII_OSI_COTS = `enum _SC_PII_OSI_COTS = _SC_PII_OSI_COTS;`;
        static if(is(typeof({ mixin(enumMixinStr__SC_PII_OSI_COTS); }))) {
            mixin(enumMixinStr__SC_PII_OSI_COTS);
        }
    }




    static if(!is(typeof(_SC_PII_INTERNET_DGRAM))) {
        private enum enumMixinStr__SC_PII_INTERNET_DGRAM = `enum _SC_PII_INTERNET_DGRAM = _SC_PII_INTERNET_DGRAM;`;
        static if(is(typeof({ mixin(enumMixinStr__SC_PII_INTERNET_DGRAM); }))) {
            mixin(enumMixinStr__SC_PII_INTERNET_DGRAM);
        }
    }




    static if(!is(typeof(_SC_PII_INTERNET_STREAM))) {
        private enum enumMixinStr__SC_PII_INTERNET_STREAM = `enum _SC_PII_INTERNET_STREAM = _SC_PII_INTERNET_STREAM;`;
        static if(is(typeof({ mixin(enumMixinStr__SC_PII_INTERNET_STREAM); }))) {
            mixin(enumMixinStr__SC_PII_INTERNET_STREAM);
        }
    }




    static if(!is(typeof(_SC_IOV_MAX))) {
        private enum enumMixinStr__SC_IOV_MAX = `enum _SC_IOV_MAX = _SC_IOV_MAX;`;
        static if(is(typeof({ mixin(enumMixinStr__SC_IOV_MAX); }))) {
            mixin(enumMixinStr__SC_IOV_MAX);
        }
    }




    static if(!is(typeof(_SC_UIO_MAXIOV))) {
        private enum enumMixinStr__SC_UIO_MAXIOV = `enum _SC_UIO_MAXIOV = _SC_UIO_MAXIOV;`;
        static if(is(typeof({ mixin(enumMixinStr__SC_UIO_MAXIOV); }))) {
            mixin(enumMixinStr__SC_UIO_MAXIOV);
        }
    }




    static if(!is(typeof(_SC_SELECT))) {
        private enum enumMixinStr__SC_SELECT = `enum _SC_SELECT = _SC_SELECT;`;
        static if(is(typeof({ mixin(enumMixinStr__SC_SELECT); }))) {
            mixin(enumMixinStr__SC_SELECT);
        }
    }




    static if(!is(typeof(_SC_POLL))) {
        private enum enumMixinStr__SC_POLL = `enum _SC_POLL = _SC_POLL;`;
        static if(is(typeof({ mixin(enumMixinStr__SC_POLL); }))) {
            mixin(enumMixinStr__SC_POLL);
        }
    }




    static if(!is(typeof(_SC_PII_OSI))) {
        private enum enumMixinStr__SC_PII_OSI = `enum _SC_PII_OSI = _SC_PII_OSI;`;
        static if(is(typeof({ mixin(enumMixinStr__SC_PII_OSI); }))) {
            mixin(enumMixinStr__SC_PII_OSI);
        }
    }




    static if(!is(typeof(_SC_PII_INTERNET))) {
        private enum enumMixinStr__SC_PII_INTERNET = `enum _SC_PII_INTERNET = _SC_PII_INTERNET;`;
        static if(is(typeof({ mixin(enumMixinStr__SC_PII_INTERNET); }))) {
            mixin(enumMixinStr__SC_PII_INTERNET);
        }
    }




    static if(!is(typeof(_SC_PII_SOCKET))) {
        private enum enumMixinStr__SC_PII_SOCKET = `enum _SC_PII_SOCKET = _SC_PII_SOCKET;`;
        static if(is(typeof({ mixin(enumMixinStr__SC_PII_SOCKET); }))) {
            mixin(enumMixinStr__SC_PII_SOCKET);
        }
    }




    static if(!is(typeof(_SC_PII_XTI))) {
        private enum enumMixinStr__SC_PII_XTI = `enum _SC_PII_XTI = _SC_PII_XTI;`;
        static if(is(typeof({ mixin(enumMixinStr__SC_PII_XTI); }))) {
            mixin(enumMixinStr__SC_PII_XTI);
        }
    }




    static if(!is(typeof(_SC_PII))) {
        private enum enumMixinStr__SC_PII = `enum _SC_PII = _SC_PII;`;
        static if(is(typeof({ mixin(enumMixinStr__SC_PII); }))) {
            mixin(enumMixinStr__SC_PII);
        }
    }




    static if(!is(typeof(_SC_2_LOCALEDEF))) {
        private enum enumMixinStr__SC_2_LOCALEDEF = `enum _SC_2_LOCALEDEF = _SC_2_LOCALEDEF;`;
        static if(is(typeof({ mixin(enumMixinStr__SC_2_LOCALEDEF); }))) {
            mixin(enumMixinStr__SC_2_LOCALEDEF);
        }
    }




    static if(!is(typeof(_SC_2_SW_DEV))) {
        private enum enumMixinStr__SC_2_SW_DEV = `enum _SC_2_SW_DEV = _SC_2_SW_DEV;`;
        static if(is(typeof({ mixin(enumMixinStr__SC_2_SW_DEV); }))) {
            mixin(enumMixinStr__SC_2_SW_DEV);
        }
    }




    static if(!is(typeof(_SC_2_FORT_RUN))) {
        private enum enumMixinStr__SC_2_FORT_RUN = `enum _SC_2_FORT_RUN = _SC_2_FORT_RUN;`;
        static if(is(typeof({ mixin(enumMixinStr__SC_2_FORT_RUN); }))) {
            mixin(enumMixinStr__SC_2_FORT_RUN);
        }
    }




    static if(!is(typeof(_SC_2_FORT_DEV))) {
        private enum enumMixinStr__SC_2_FORT_DEV = `enum _SC_2_FORT_DEV = _SC_2_FORT_DEV;`;
        static if(is(typeof({ mixin(enumMixinStr__SC_2_FORT_DEV); }))) {
            mixin(enumMixinStr__SC_2_FORT_DEV);
        }
    }




    static if(!is(typeof(_SC_2_C_DEV))) {
        private enum enumMixinStr__SC_2_C_DEV = `enum _SC_2_C_DEV = _SC_2_C_DEV;`;
        static if(is(typeof({ mixin(enumMixinStr__SC_2_C_DEV); }))) {
            mixin(enumMixinStr__SC_2_C_DEV);
        }
    }




    static if(!is(typeof(_SC_2_C_BIND))) {
        private enum enumMixinStr__SC_2_C_BIND = `enum _SC_2_C_BIND = _SC_2_C_BIND;`;
        static if(is(typeof({ mixin(enumMixinStr__SC_2_C_BIND); }))) {
            mixin(enumMixinStr__SC_2_C_BIND);
        }
    }




    static if(!is(typeof(_SC_2_VERSION))) {
        private enum enumMixinStr__SC_2_VERSION = `enum _SC_2_VERSION = _SC_2_VERSION;`;
        static if(is(typeof({ mixin(enumMixinStr__SC_2_VERSION); }))) {
            mixin(enumMixinStr__SC_2_VERSION);
        }
    }




    static if(!is(typeof(_SC_CHARCLASS_NAME_MAX))) {
        private enum enumMixinStr__SC_CHARCLASS_NAME_MAX = `enum _SC_CHARCLASS_NAME_MAX = _SC_CHARCLASS_NAME_MAX;`;
        static if(is(typeof({ mixin(enumMixinStr__SC_CHARCLASS_NAME_MAX); }))) {
            mixin(enumMixinStr__SC_CHARCLASS_NAME_MAX);
        }
    }




    static if(!is(typeof(_SC_RE_DUP_MAX))) {
        private enum enumMixinStr__SC_RE_DUP_MAX = `enum _SC_RE_DUP_MAX = _SC_RE_DUP_MAX;`;
        static if(is(typeof({ mixin(enumMixinStr__SC_RE_DUP_MAX); }))) {
            mixin(enumMixinStr__SC_RE_DUP_MAX);
        }
    }




    static if(!is(typeof(_SC_LINE_MAX))) {
        private enum enumMixinStr__SC_LINE_MAX = `enum _SC_LINE_MAX = _SC_LINE_MAX;`;
        static if(is(typeof({ mixin(enumMixinStr__SC_LINE_MAX); }))) {
            mixin(enumMixinStr__SC_LINE_MAX);
        }
    }




    static if(!is(typeof(_SC_EXPR_NEST_MAX))) {
        private enum enumMixinStr__SC_EXPR_NEST_MAX = `enum _SC_EXPR_NEST_MAX = _SC_EXPR_NEST_MAX;`;
        static if(is(typeof({ mixin(enumMixinStr__SC_EXPR_NEST_MAX); }))) {
            mixin(enumMixinStr__SC_EXPR_NEST_MAX);
        }
    }




    static if(!is(typeof(_SC_EQUIV_CLASS_MAX))) {
        private enum enumMixinStr__SC_EQUIV_CLASS_MAX = `enum _SC_EQUIV_CLASS_MAX = _SC_EQUIV_CLASS_MAX;`;
        static if(is(typeof({ mixin(enumMixinStr__SC_EQUIV_CLASS_MAX); }))) {
            mixin(enumMixinStr__SC_EQUIV_CLASS_MAX);
        }
    }




    static if(!is(typeof(_SC_COLL_WEIGHTS_MAX))) {
        private enum enumMixinStr__SC_COLL_WEIGHTS_MAX = `enum _SC_COLL_WEIGHTS_MAX = _SC_COLL_WEIGHTS_MAX;`;
        static if(is(typeof({ mixin(enumMixinStr__SC_COLL_WEIGHTS_MAX); }))) {
            mixin(enumMixinStr__SC_COLL_WEIGHTS_MAX);
        }
    }




    static if(!is(typeof(_SC_BC_STRING_MAX))) {
        private enum enumMixinStr__SC_BC_STRING_MAX = `enum _SC_BC_STRING_MAX = _SC_BC_STRING_MAX;`;
        static if(is(typeof({ mixin(enumMixinStr__SC_BC_STRING_MAX); }))) {
            mixin(enumMixinStr__SC_BC_STRING_MAX);
        }
    }




    static if(!is(typeof(_SC_BC_SCALE_MAX))) {
        private enum enumMixinStr__SC_BC_SCALE_MAX = `enum _SC_BC_SCALE_MAX = _SC_BC_SCALE_MAX;`;
        static if(is(typeof({ mixin(enumMixinStr__SC_BC_SCALE_MAX); }))) {
            mixin(enumMixinStr__SC_BC_SCALE_MAX);
        }
    }




    static if(!is(typeof(_SC_BC_DIM_MAX))) {
        private enum enumMixinStr__SC_BC_DIM_MAX = `enum _SC_BC_DIM_MAX = _SC_BC_DIM_MAX;`;
        static if(is(typeof({ mixin(enumMixinStr__SC_BC_DIM_MAX); }))) {
            mixin(enumMixinStr__SC_BC_DIM_MAX);
        }
    }




    static if(!is(typeof(_SC_BC_BASE_MAX))) {
        private enum enumMixinStr__SC_BC_BASE_MAX = `enum _SC_BC_BASE_MAX = _SC_BC_BASE_MAX;`;
        static if(is(typeof({ mixin(enumMixinStr__SC_BC_BASE_MAX); }))) {
            mixin(enumMixinStr__SC_BC_BASE_MAX);
        }
    }




    static if(!is(typeof(_SC_TIMER_MAX))) {
        private enum enumMixinStr__SC_TIMER_MAX = `enum _SC_TIMER_MAX = _SC_TIMER_MAX;`;
        static if(is(typeof({ mixin(enumMixinStr__SC_TIMER_MAX); }))) {
            mixin(enumMixinStr__SC_TIMER_MAX);
        }
    }




    static if(!is(typeof(_SC_SIGQUEUE_MAX))) {
        private enum enumMixinStr__SC_SIGQUEUE_MAX = `enum _SC_SIGQUEUE_MAX = _SC_SIGQUEUE_MAX;`;
        static if(is(typeof({ mixin(enumMixinStr__SC_SIGQUEUE_MAX); }))) {
            mixin(enumMixinStr__SC_SIGQUEUE_MAX);
        }
    }




    static if(!is(typeof(_SC_SEM_VALUE_MAX))) {
        private enum enumMixinStr__SC_SEM_VALUE_MAX = `enum _SC_SEM_VALUE_MAX = _SC_SEM_VALUE_MAX;`;
        static if(is(typeof({ mixin(enumMixinStr__SC_SEM_VALUE_MAX); }))) {
            mixin(enumMixinStr__SC_SEM_VALUE_MAX);
        }
    }




    static if(!is(typeof(_SC_SEM_NSEMS_MAX))) {
        private enum enumMixinStr__SC_SEM_NSEMS_MAX = `enum _SC_SEM_NSEMS_MAX = _SC_SEM_NSEMS_MAX;`;
        static if(is(typeof({ mixin(enumMixinStr__SC_SEM_NSEMS_MAX); }))) {
            mixin(enumMixinStr__SC_SEM_NSEMS_MAX);
        }
    }




    static if(!is(typeof(_SC_RTSIG_MAX))) {
        private enum enumMixinStr__SC_RTSIG_MAX = `enum _SC_RTSIG_MAX = _SC_RTSIG_MAX;`;
        static if(is(typeof({ mixin(enumMixinStr__SC_RTSIG_MAX); }))) {
            mixin(enumMixinStr__SC_RTSIG_MAX);
        }
    }




    static if(!is(typeof(_SC_PAGE_SIZE))) {
        private enum enumMixinStr__SC_PAGE_SIZE = `enum _SC_PAGE_SIZE = _SC_PAGESIZE;`;
        static if(is(typeof({ mixin(enumMixinStr__SC_PAGE_SIZE); }))) {
            mixin(enumMixinStr__SC_PAGE_SIZE);
        }
    }




    static if(!is(typeof(_SC_PAGESIZE))) {
        private enum enumMixinStr__SC_PAGESIZE = `enum _SC_PAGESIZE = _SC_PAGESIZE;`;
        static if(is(typeof({ mixin(enumMixinStr__SC_PAGESIZE); }))) {
            mixin(enumMixinStr__SC_PAGESIZE);
        }
    }




    static if(!is(typeof(_SC_VERSION))) {
        private enum enumMixinStr__SC_VERSION = `enum _SC_VERSION = _SC_VERSION;`;
        static if(is(typeof({ mixin(enumMixinStr__SC_VERSION); }))) {
            mixin(enumMixinStr__SC_VERSION);
        }
    }




    static if(!is(typeof(_SC_MQ_PRIO_MAX))) {
        private enum enumMixinStr__SC_MQ_PRIO_MAX = `enum _SC_MQ_PRIO_MAX = _SC_MQ_PRIO_MAX;`;
        static if(is(typeof({ mixin(enumMixinStr__SC_MQ_PRIO_MAX); }))) {
            mixin(enumMixinStr__SC_MQ_PRIO_MAX);
        }
    }




    static if(!is(typeof(_SC_MQ_OPEN_MAX))) {
        private enum enumMixinStr__SC_MQ_OPEN_MAX = `enum _SC_MQ_OPEN_MAX = _SC_MQ_OPEN_MAX;`;
        static if(is(typeof({ mixin(enumMixinStr__SC_MQ_OPEN_MAX); }))) {
            mixin(enumMixinStr__SC_MQ_OPEN_MAX);
        }
    }




    static if(!is(typeof(_SC_DELAYTIMER_MAX))) {
        private enum enumMixinStr__SC_DELAYTIMER_MAX = `enum _SC_DELAYTIMER_MAX = _SC_DELAYTIMER_MAX;`;
        static if(is(typeof({ mixin(enumMixinStr__SC_DELAYTIMER_MAX); }))) {
            mixin(enumMixinStr__SC_DELAYTIMER_MAX);
        }
    }




    static if(!is(typeof(_SC_AIO_PRIO_DELTA_MAX))) {
        private enum enumMixinStr__SC_AIO_PRIO_DELTA_MAX = `enum _SC_AIO_PRIO_DELTA_MAX = _SC_AIO_PRIO_DELTA_MAX;`;
        static if(is(typeof({ mixin(enumMixinStr__SC_AIO_PRIO_DELTA_MAX); }))) {
            mixin(enumMixinStr__SC_AIO_PRIO_DELTA_MAX);
        }
    }




    static if(!is(typeof(_SC_AIO_MAX))) {
        private enum enumMixinStr__SC_AIO_MAX = `enum _SC_AIO_MAX = _SC_AIO_MAX;`;
        static if(is(typeof({ mixin(enumMixinStr__SC_AIO_MAX); }))) {
            mixin(enumMixinStr__SC_AIO_MAX);
        }
    }




    static if(!is(typeof(_SC_AIO_LISTIO_MAX))) {
        private enum enumMixinStr__SC_AIO_LISTIO_MAX = `enum _SC_AIO_LISTIO_MAX = _SC_AIO_LISTIO_MAX;`;
        static if(is(typeof({ mixin(enumMixinStr__SC_AIO_LISTIO_MAX); }))) {
            mixin(enumMixinStr__SC_AIO_LISTIO_MAX);
        }
    }




    static if(!is(typeof(_SC_SHARED_MEMORY_OBJECTS))) {
        private enum enumMixinStr__SC_SHARED_MEMORY_OBJECTS = `enum _SC_SHARED_MEMORY_OBJECTS = _SC_SHARED_MEMORY_OBJECTS;`;
        static if(is(typeof({ mixin(enumMixinStr__SC_SHARED_MEMORY_OBJECTS); }))) {
            mixin(enumMixinStr__SC_SHARED_MEMORY_OBJECTS);
        }
    }




    static if(!is(typeof(_SC_SEMAPHORES))) {
        private enum enumMixinStr__SC_SEMAPHORES = `enum _SC_SEMAPHORES = _SC_SEMAPHORES;`;
        static if(is(typeof({ mixin(enumMixinStr__SC_SEMAPHORES); }))) {
            mixin(enumMixinStr__SC_SEMAPHORES);
        }
    }




    static if(!is(typeof(_SC_MESSAGE_PASSING))) {
        private enum enumMixinStr__SC_MESSAGE_PASSING = `enum _SC_MESSAGE_PASSING = _SC_MESSAGE_PASSING;`;
        static if(is(typeof({ mixin(enumMixinStr__SC_MESSAGE_PASSING); }))) {
            mixin(enumMixinStr__SC_MESSAGE_PASSING);
        }
    }




    static if(!is(typeof(_SC_MEMORY_PROTECTION))) {
        private enum enumMixinStr__SC_MEMORY_PROTECTION = `enum _SC_MEMORY_PROTECTION = _SC_MEMORY_PROTECTION;`;
        static if(is(typeof({ mixin(enumMixinStr__SC_MEMORY_PROTECTION); }))) {
            mixin(enumMixinStr__SC_MEMORY_PROTECTION);
        }
    }




    static if(!is(typeof(_SC_MEMLOCK_RANGE))) {
        private enum enumMixinStr__SC_MEMLOCK_RANGE = `enum _SC_MEMLOCK_RANGE = _SC_MEMLOCK_RANGE;`;
        static if(is(typeof({ mixin(enumMixinStr__SC_MEMLOCK_RANGE); }))) {
            mixin(enumMixinStr__SC_MEMLOCK_RANGE);
        }
    }




    static if(!is(typeof(_SC_MEMLOCK))) {
        private enum enumMixinStr__SC_MEMLOCK = `enum _SC_MEMLOCK = _SC_MEMLOCK;`;
        static if(is(typeof({ mixin(enumMixinStr__SC_MEMLOCK); }))) {
            mixin(enumMixinStr__SC_MEMLOCK);
        }
    }




    static if(!is(typeof(_SC_MAPPED_FILES))) {
        private enum enumMixinStr__SC_MAPPED_FILES = `enum _SC_MAPPED_FILES = _SC_MAPPED_FILES;`;
        static if(is(typeof({ mixin(enumMixinStr__SC_MAPPED_FILES); }))) {
            mixin(enumMixinStr__SC_MAPPED_FILES);
        }
    }




    static if(!is(typeof(_SC_FSYNC))) {
        private enum enumMixinStr__SC_FSYNC = `enum _SC_FSYNC = _SC_FSYNC;`;
        static if(is(typeof({ mixin(enumMixinStr__SC_FSYNC); }))) {
            mixin(enumMixinStr__SC_FSYNC);
        }
    }




    static if(!is(typeof(_SC_SYNCHRONIZED_IO))) {
        private enum enumMixinStr__SC_SYNCHRONIZED_IO = `enum _SC_SYNCHRONIZED_IO = _SC_SYNCHRONIZED_IO;`;
        static if(is(typeof({ mixin(enumMixinStr__SC_SYNCHRONIZED_IO); }))) {
            mixin(enumMixinStr__SC_SYNCHRONIZED_IO);
        }
    }




    static if(!is(typeof(_SC_PRIORITIZED_IO))) {
        private enum enumMixinStr__SC_PRIORITIZED_IO = `enum _SC_PRIORITIZED_IO = _SC_PRIORITIZED_IO;`;
        static if(is(typeof({ mixin(enumMixinStr__SC_PRIORITIZED_IO); }))) {
            mixin(enumMixinStr__SC_PRIORITIZED_IO);
        }
    }




    static if(!is(typeof(_SC_ASYNCHRONOUS_IO))) {
        private enum enumMixinStr__SC_ASYNCHRONOUS_IO = `enum _SC_ASYNCHRONOUS_IO = _SC_ASYNCHRONOUS_IO;`;
        static if(is(typeof({ mixin(enumMixinStr__SC_ASYNCHRONOUS_IO); }))) {
            mixin(enumMixinStr__SC_ASYNCHRONOUS_IO);
        }
    }




    static if(!is(typeof(_SC_TIMERS))) {
        private enum enumMixinStr__SC_TIMERS = `enum _SC_TIMERS = _SC_TIMERS;`;
        static if(is(typeof({ mixin(enumMixinStr__SC_TIMERS); }))) {
            mixin(enumMixinStr__SC_TIMERS);
        }
    }




    static if(!is(typeof(_SC_PRIORITY_SCHEDULING))) {
        private enum enumMixinStr__SC_PRIORITY_SCHEDULING = `enum _SC_PRIORITY_SCHEDULING = _SC_PRIORITY_SCHEDULING;`;
        static if(is(typeof({ mixin(enumMixinStr__SC_PRIORITY_SCHEDULING); }))) {
            mixin(enumMixinStr__SC_PRIORITY_SCHEDULING);
        }
    }




    static if(!is(typeof(_STRINGS_H))) {
        private enum enumMixinStr__STRINGS_H = `enum _STRINGS_H = 1;`;
        static if(is(typeof({ mixin(enumMixinStr__STRINGS_H); }))) {
            mixin(enumMixinStr__STRINGS_H);
        }
    }




    static if(!is(typeof(_SC_REALTIME_SIGNALS))) {
        private enum enumMixinStr__SC_REALTIME_SIGNALS = `enum _SC_REALTIME_SIGNALS = _SC_REALTIME_SIGNALS;`;
        static if(is(typeof({ mixin(enumMixinStr__SC_REALTIME_SIGNALS); }))) {
            mixin(enumMixinStr__SC_REALTIME_SIGNALS);
        }
    }




    static if(!is(typeof(_SC_SAVED_IDS))) {
        private enum enumMixinStr__SC_SAVED_IDS = `enum _SC_SAVED_IDS = _SC_SAVED_IDS;`;
        static if(is(typeof({ mixin(enumMixinStr__SC_SAVED_IDS); }))) {
            mixin(enumMixinStr__SC_SAVED_IDS);
        }
    }




    static if(!is(typeof(_SC_JOB_CONTROL))) {
        private enum enumMixinStr__SC_JOB_CONTROL = `enum _SC_JOB_CONTROL = _SC_JOB_CONTROL;`;
        static if(is(typeof({ mixin(enumMixinStr__SC_JOB_CONTROL); }))) {
            mixin(enumMixinStr__SC_JOB_CONTROL);
        }
    }




    static if(!is(typeof(_SC_TZNAME_MAX))) {
        private enum enumMixinStr__SC_TZNAME_MAX = `enum _SC_TZNAME_MAX = _SC_TZNAME_MAX;`;
        static if(is(typeof({ mixin(enumMixinStr__SC_TZNAME_MAX); }))) {
            mixin(enumMixinStr__SC_TZNAME_MAX);
        }
    }




    static if(!is(typeof(_SC_STREAM_MAX))) {
        private enum enumMixinStr__SC_STREAM_MAX = `enum _SC_STREAM_MAX = _SC_STREAM_MAX;`;
        static if(is(typeof({ mixin(enumMixinStr__SC_STREAM_MAX); }))) {
            mixin(enumMixinStr__SC_STREAM_MAX);
        }
    }




    static if(!is(typeof(_SC_OPEN_MAX))) {
        private enum enumMixinStr__SC_OPEN_MAX = `enum _SC_OPEN_MAX = _SC_OPEN_MAX;`;
        static if(is(typeof({ mixin(enumMixinStr__SC_OPEN_MAX); }))) {
            mixin(enumMixinStr__SC_OPEN_MAX);
        }
    }




    static if(!is(typeof(_SC_NGROUPS_MAX))) {
        private enum enumMixinStr__SC_NGROUPS_MAX = `enum _SC_NGROUPS_MAX = _SC_NGROUPS_MAX;`;
        static if(is(typeof({ mixin(enumMixinStr__SC_NGROUPS_MAX); }))) {
            mixin(enumMixinStr__SC_NGROUPS_MAX);
        }
    }




    static if(!is(typeof(_SC_CLK_TCK))) {
        private enum enumMixinStr__SC_CLK_TCK = `enum _SC_CLK_TCK = _SC_CLK_TCK;`;
        static if(is(typeof({ mixin(enumMixinStr__SC_CLK_TCK); }))) {
            mixin(enumMixinStr__SC_CLK_TCK);
        }
    }




    static if(!is(typeof(_SC_CHILD_MAX))) {
        private enum enumMixinStr__SC_CHILD_MAX = `enum _SC_CHILD_MAX = _SC_CHILD_MAX;`;
        static if(is(typeof({ mixin(enumMixinStr__SC_CHILD_MAX); }))) {
            mixin(enumMixinStr__SC_CHILD_MAX);
        }
    }




    static if(!is(typeof(_SC_ARG_MAX))) {
        private enum enumMixinStr__SC_ARG_MAX = `enum _SC_ARG_MAX = _SC_ARG_MAX;`;
        static if(is(typeof({ mixin(enumMixinStr__SC_ARG_MAX); }))) {
            mixin(enumMixinStr__SC_ARG_MAX);
        }
    }




    static if(!is(typeof(_PC_2_SYMLINKS))) {
        private enum enumMixinStr__PC_2_SYMLINKS = `enum _PC_2_SYMLINKS = _PC_2_SYMLINKS;`;
        static if(is(typeof({ mixin(enumMixinStr__PC_2_SYMLINKS); }))) {
            mixin(enumMixinStr__PC_2_SYMLINKS);
        }
    }




    static if(!is(typeof(_PC_SYMLINK_MAX))) {
        private enum enumMixinStr__PC_SYMLINK_MAX = `enum _PC_SYMLINK_MAX = _PC_SYMLINK_MAX;`;
        static if(is(typeof({ mixin(enumMixinStr__PC_SYMLINK_MAX); }))) {
            mixin(enumMixinStr__PC_SYMLINK_MAX);
        }
    }




    static if(!is(typeof(_PC_ALLOC_SIZE_MIN))) {
        private enum enumMixinStr__PC_ALLOC_SIZE_MIN = `enum _PC_ALLOC_SIZE_MIN = _PC_ALLOC_SIZE_MIN;`;
        static if(is(typeof({ mixin(enumMixinStr__PC_ALLOC_SIZE_MIN); }))) {
            mixin(enumMixinStr__PC_ALLOC_SIZE_MIN);
        }
    }




    static if(!is(typeof(_PC_REC_XFER_ALIGN))) {
        private enum enumMixinStr__PC_REC_XFER_ALIGN = `enum _PC_REC_XFER_ALIGN = _PC_REC_XFER_ALIGN;`;
        static if(is(typeof({ mixin(enumMixinStr__PC_REC_XFER_ALIGN); }))) {
            mixin(enumMixinStr__PC_REC_XFER_ALIGN);
        }
    }




    static if(!is(typeof(_PC_REC_MIN_XFER_SIZE))) {
        private enum enumMixinStr__PC_REC_MIN_XFER_SIZE = `enum _PC_REC_MIN_XFER_SIZE = _PC_REC_MIN_XFER_SIZE;`;
        static if(is(typeof({ mixin(enumMixinStr__PC_REC_MIN_XFER_SIZE); }))) {
            mixin(enumMixinStr__PC_REC_MIN_XFER_SIZE);
        }
    }




    static if(!is(typeof(_PC_REC_MAX_XFER_SIZE))) {
        private enum enumMixinStr__PC_REC_MAX_XFER_SIZE = `enum _PC_REC_MAX_XFER_SIZE = _PC_REC_MAX_XFER_SIZE;`;
        static if(is(typeof({ mixin(enumMixinStr__PC_REC_MAX_XFER_SIZE); }))) {
            mixin(enumMixinStr__PC_REC_MAX_XFER_SIZE);
        }
    }




    static if(!is(typeof(_PC_REC_INCR_XFER_SIZE))) {
        private enum enumMixinStr__PC_REC_INCR_XFER_SIZE = `enum _PC_REC_INCR_XFER_SIZE = _PC_REC_INCR_XFER_SIZE;`;
        static if(is(typeof({ mixin(enumMixinStr__PC_REC_INCR_XFER_SIZE); }))) {
            mixin(enumMixinStr__PC_REC_INCR_XFER_SIZE);
        }
    }




    static if(!is(typeof(_PC_FILESIZEBITS))) {
        private enum enumMixinStr__PC_FILESIZEBITS = `enum _PC_FILESIZEBITS = _PC_FILESIZEBITS;`;
        static if(is(typeof({ mixin(enumMixinStr__PC_FILESIZEBITS); }))) {
            mixin(enumMixinStr__PC_FILESIZEBITS);
        }
    }




    static if(!is(typeof(_PC_SOCK_MAXBUF))) {
        private enum enumMixinStr__PC_SOCK_MAXBUF = `enum _PC_SOCK_MAXBUF = _PC_SOCK_MAXBUF;`;
        static if(is(typeof({ mixin(enumMixinStr__PC_SOCK_MAXBUF); }))) {
            mixin(enumMixinStr__PC_SOCK_MAXBUF);
        }
    }




    static if(!is(typeof(_PC_PRIO_IO))) {
        private enum enumMixinStr__PC_PRIO_IO = `enum _PC_PRIO_IO = _PC_PRIO_IO;`;
        static if(is(typeof({ mixin(enumMixinStr__PC_PRIO_IO); }))) {
            mixin(enumMixinStr__PC_PRIO_IO);
        }
    }




    static if(!is(typeof(_PC_ASYNC_IO))) {
        private enum enumMixinStr__PC_ASYNC_IO = `enum _PC_ASYNC_IO = _PC_ASYNC_IO;`;
        static if(is(typeof({ mixin(enumMixinStr__PC_ASYNC_IO); }))) {
            mixin(enumMixinStr__PC_ASYNC_IO);
        }
    }




    static if(!is(typeof(_PC_SYNC_IO))) {
        private enum enumMixinStr__PC_SYNC_IO = `enum _PC_SYNC_IO = _PC_SYNC_IO;`;
        static if(is(typeof({ mixin(enumMixinStr__PC_SYNC_IO); }))) {
            mixin(enumMixinStr__PC_SYNC_IO);
        }
    }




    static if(!is(typeof(_PC_VDISABLE))) {
        private enum enumMixinStr__PC_VDISABLE = `enum _PC_VDISABLE = _PC_VDISABLE;`;
        static if(is(typeof({ mixin(enumMixinStr__PC_VDISABLE); }))) {
            mixin(enumMixinStr__PC_VDISABLE);
        }
    }




    static if(!is(typeof(_PC_NO_TRUNC))) {
        private enum enumMixinStr__PC_NO_TRUNC = `enum _PC_NO_TRUNC = _PC_NO_TRUNC;`;
        static if(is(typeof({ mixin(enumMixinStr__PC_NO_TRUNC); }))) {
            mixin(enumMixinStr__PC_NO_TRUNC);
        }
    }




    static if(!is(typeof(_PC_CHOWN_RESTRICTED))) {
        private enum enumMixinStr__PC_CHOWN_RESTRICTED = `enum _PC_CHOWN_RESTRICTED = _PC_CHOWN_RESTRICTED;`;
        static if(is(typeof({ mixin(enumMixinStr__PC_CHOWN_RESTRICTED); }))) {
            mixin(enumMixinStr__PC_CHOWN_RESTRICTED);
        }
    }




    static if(!is(typeof(_PC_PIPE_BUF))) {
        private enum enumMixinStr__PC_PIPE_BUF = `enum _PC_PIPE_BUF = _PC_PIPE_BUF;`;
        static if(is(typeof({ mixin(enumMixinStr__PC_PIPE_BUF); }))) {
            mixin(enumMixinStr__PC_PIPE_BUF);
        }
    }




    static if(!is(typeof(_PC_PATH_MAX))) {
        private enum enumMixinStr__PC_PATH_MAX = `enum _PC_PATH_MAX = _PC_PATH_MAX;`;
        static if(is(typeof({ mixin(enumMixinStr__PC_PATH_MAX); }))) {
            mixin(enumMixinStr__PC_PATH_MAX);
        }
    }




    static if(!is(typeof(_PC_NAME_MAX))) {
        private enum enumMixinStr__PC_NAME_MAX = `enum _PC_NAME_MAX = _PC_NAME_MAX;`;
        static if(is(typeof({ mixin(enumMixinStr__PC_NAME_MAX); }))) {
            mixin(enumMixinStr__PC_NAME_MAX);
        }
    }




    static if(!is(typeof(_PC_MAX_INPUT))) {
        private enum enumMixinStr__PC_MAX_INPUT = `enum _PC_MAX_INPUT = _PC_MAX_INPUT;`;
        static if(is(typeof({ mixin(enumMixinStr__PC_MAX_INPUT); }))) {
            mixin(enumMixinStr__PC_MAX_INPUT);
        }
    }




    static if(!is(typeof(_PC_MAX_CANON))) {
        private enum enumMixinStr__PC_MAX_CANON = `enum _PC_MAX_CANON = _PC_MAX_CANON;`;
        static if(is(typeof({ mixin(enumMixinStr__PC_MAX_CANON); }))) {
            mixin(enumMixinStr__PC_MAX_CANON);
        }
    }




    static if(!is(typeof(_PC_LINK_MAX))) {
        private enum enumMixinStr__PC_LINK_MAX = `enum _PC_LINK_MAX = _PC_LINK_MAX;`;
        static if(is(typeof({ mixin(enumMixinStr__PC_LINK_MAX); }))) {
            mixin(enumMixinStr__PC_LINK_MAX);
        }
    }
    static if(!is(typeof(_BITS_BYTESWAP_H))) {
        private enum enumMixinStr__BITS_BYTESWAP_H = `enum _BITS_BYTESWAP_H = 1;`;
        static if(is(typeof({ mixin(enumMixinStr__BITS_BYTESWAP_H); }))) {
            mixin(enumMixinStr__BITS_BYTESWAP_H);
        }
    }




    static if(!is(typeof(__kernel_old_dev_t))) {
        private enum enumMixinStr___kernel_old_dev_t = `enum __kernel_old_dev_t = __kernel_old_dev_t;`;
        static if(is(typeof({ mixin(enumMixinStr___kernel_old_dev_t); }))) {
            mixin(enumMixinStr___kernel_old_dev_t);
        }
    }




    static if(!is(typeof(__kernel_old_uid_t))) {
        private enum enumMixinStr___kernel_old_uid_t = `enum __kernel_old_uid_t = __kernel_old_uid_t;`;
        static if(is(typeof({ mixin(enumMixinStr___kernel_old_uid_t); }))) {
            mixin(enumMixinStr___kernel_old_uid_t);
        }
    }






    static if(!is(typeof(__BITS_PER_LONG))) {
        private enum enumMixinStr___BITS_PER_LONG = `enum __BITS_PER_LONG = 64;`;
        static if(is(typeof({ mixin(enumMixinStr___BITS_PER_LONG); }))) {
            mixin(enumMixinStr___BITS_PER_LONG);
        }
    }
    static if(!is(typeof(TIOCSER_TEMT))) {
        private enum enumMixinStr_TIOCSER_TEMT = `enum TIOCSER_TEMT = 0x01;`;
        static if(is(typeof({ mixin(enumMixinStr_TIOCSER_TEMT); }))) {
            mixin(enumMixinStr_TIOCSER_TEMT);
        }
    }




    static if(!is(typeof(TIOCPKT_IOCTL))) {
        private enum enumMixinStr_TIOCPKT_IOCTL = `enum TIOCPKT_IOCTL = 64;`;
        static if(is(typeof({ mixin(enumMixinStr_TIOCPKT_IOCTL); }))) {
            mixin(enumMixinStr_TIOCPKT_IOCTL);
        }
    }




    static if(!is(typeof(TIOCPKT_DOSTOP))) {
        private enum enumMixinStr_TIOCPKT_DOSTOP = `enum TIOCPKT_DOSTOP = 32;`;
        static if(is(typeof({ mixin(enumMixinStr_TIOCPKT_DOSTOP); }))) {
            mixin(enumMixinStr_TIOCPKT_DOSTOP);
        }
    }




    static if(!is(typeof(TIOCPKT_NOSTOP))) {
        private enum enumMixinStr_TIOCPKT_NOSTOP = `enum TIOCPKT_NOSTOP = 16;`;
        static if(is(typeof({ mixin(enumMixinStr_TIOCPKT_NOSTOP); }))) {
            mixin(enumMixinStr_TIOCPKT_NOSTOP);
        }
    }




    static if(!is(typeof(TIOCPKT_START))) {
        private enum enumMixinStr_TIOCPKT_START = `enum TIOCPKT_START = 8;`;
        static if(is(typeof({ mixin(enumMixinStr_TIOCPKT_START); }))) {
            mixin(enumMixinStr_TIOCPKT_START);
        }
    }




    static if(!is(typeof(TIOCPKT_STOP))) {
        private enum enumMixinStr_TIOCPKT_STOP = `enum TIOCPKT_STOP = 4;`;
        static if(is(typeof({ mixin(enumMixinStr_TIOCPKT_STOP); }))) {
            mixin(enumMixinStr_TIOCPKT_STOP);
        }
    }




    static if(!is(typeof(TIOCPKT_FLUSHWRITE))) {
        private enum enumMixinStr_TIOCPKT_FLUSHWRITE = `enum TIOCPKT_FLUSHWRITE = 2;`;
        static if(is(typeof({ mixin(enumMixinStr_TIOCPKT_FLUSHWRITE); }))) {
            mixin(enumMixinStr_TIOCPKT_FLUSHWRITE);
        }
    }




    static if(!is(typeof(TIOCPKT_FLUSHREAD))) {
        private enum enumMixinStr_TIOCPKT_FLUSHREAD = `enum TIOCPKT_FLUSHREAD = 1;`;
        static if(is(typeof({ mixin(enumMixinStr_TIOCPKT_FLUSHREAD); }))) {
            mixin(enumMixinStr_TIOCPKT_FLUSHREAD);
        }
    }




    static if(!is(typeof(TIOCPKT_DATA))) {
        private enum enumMixinStr_TIOCPKT_DATA = `enum TIOCPKT_DATA = 0;`;
        static if(is(typeof({ mixin(enumMixinStr_TIOCPKT_DATA); }))) {
            mixin(enumMixinStr_TIOCPKT_DATA);
        }
    }




    static if(!is(typeof(FIOQSIZE))) {
        private enum enumMixinStr_FIOQSIZE = `enum FIOQSIZE = 0x5460;`;
        static if(is(typeof({ mixin(enumMixinStr_FIOQSIZE); }))) {
            mixin(enumMixinStr_FIOQSIZE);
        }
    }




    static if(!is(typeof(TIOCGICOUNT))) {
        private enum enumMixinStr_TIOCGICOUNT = `enum TIOCGICOUNT = 0x545D;`;
        static if(is(typeof({ mixin(enumMixinStr_TIOCGICOUNT); }))) {
            mixin(enumMixinStr_TIOCGICOUNT);
        }
    }




    static if(!is(typeof(TIOCMIWAIT))) {
        private enum enumMixinStr_TIOCMIWAIT = `enum TIOCMIWAIT = 0x545C;`;
        static if(is(typeof({ mixin(enumMixinStr_TIOCMIWAIT); }))) {
            mixin(enumMixinStr_TIOCMIWAIT);
        }
    }




    static if(!is(typeof(TIOCSERSETMULTI))) {
        private enum enumMixinStr_TIOCSERSETMULTI = `enum TIOCSERSETMULTI = 0x545B;`;
        static if(is(typeof({ mixin(enumMixinStr_TIOCSERSETMULTI); }))) {
            mixin(enumMixinStr_TIOCSERSETMULTI);
        }
    }




    static if(!is(typeof(TIOCSERGETMULTI))) {
        private enum enumMixinStr_TIOCSERGETMULTI = `enum TIOCSERGETMULTI = 0x545A;`;
        static if(is(typeof({ mixin(enumMixinStr_TIOCSERGETMULTI); }))) {
            mixin(enumMixinStr_TIOCSERGETMULTI);
        }
    }




    static if(!is(typeof(TIOCSERGETLSR))) {
        private enum enumMixinStr_TIOCSERGETLSR = `enum TIOCSERGETLSR = 0x5459;`;
        static if(is(typeof({ mixin(enumMixinStr_TIOCSERGETLSR); }))) {
            mixin(enumMixinStr_TIOCSERGETLSR);
        }
    }




    static if(!is(typeof(TIOCSERGSTRUCT))) {
        private enum enumMixinStr_TIOCSERGSTRUCT = `enum TIOCSERGSTRUCT = 0x5458;`;
        static if(is(typeof({ mixin(enumMixinStr_TIOCSERGSTRUCT); }))) {
            mixin(enumMixinStr_TIOCSERGSTRUCT);
        }
    }




    static if(!is(typeof(_SYS_CDEFS_H))) {
        private enum enumMixinStr__SYS_CDEFS_H = `enum _SYS_CDEFS_H = 1;`;
        static if(is(typeof({ mixin(enumMixinStr__SYS_CDEFS_H); }))) {
            mixin(enumMixinStr__SYS_CDEFS_H);
        }
    }




    static if(!is(typeof(TIOCSLCKTRMIOS))) {
        private enum enumMixinStr_TIOCSLCKTRMIOS = `enum TIOCSLCKTRMIOS = 0x5457;`;
        static if(is(typeof({ mixin(enumMixinStr_TIOCSLCKTRMIOS); }))) {
            mixin(enumMixinStr_TIOCSLCKTRMIOS);
        }
    }




    static if(!is(typeof(TIOCGLCKTRMIOS))) {
        private enum enumMixinStr_TIOCGLCKTRMIOS = `enum TIOCGLCKTRMIOS = 0x5456;`;
        static if(is(typeof({ mixin(enumMixinStr_TIOCGLCKTRMIOS); }))) {
            mixin(enumMixinStr_TIOCGLCKTRMIOS);
        }
    }




    static if(!is(typeof(TIOCSERSWILD))) {
        private enum enumMixinStr_TIOCSERSWILD = `enum TIOCSERSWILD = 0x5455;`;
        static if(is(typeof({ mixin(enumMixinStr_TIOCSERSWILD); }))) {
            mixin(enumMixinStr_TIOCSERSWILD);
        }
    }




    static if(!is(typeof(TIOCSERGWILD))) {
        private enum enumMixinStr_TIOCSERGWILD = `enum TIOCSERGWILD = 0x5454;`;
        static if(is(typeof({ mixin(enumMixinStr_TIOCSERGWILD); }))) {
            mixin(enumMixinStr_TIOCSERGWILD);
        }
    }




    static if(!is(typeof(TIOCSERCONFIG))) {
        private enum enumMixinStr_TIOCSERCONFIG = `enum TIOCSERCONFIG = 0x5453;`;
        static if(is(typeof({ mixin(enumMixinStr_TIOCSERCONFIG); }))) {
            mixin(enumMixinStr_TIOCSERCONFIG);
        }
    }
    static if(!is(typeof(FIOASYNC))) {
        private enum enumMixinStr_FIOASYNC = `enum FIOASYNC = 0x5452;`;
        static if(is(typeof({ mixin(enumMixinStr_FIOASYNC); }))) {
            mixin(enumMixinStr_FIOASYNC);
        }
    }




    static if(!is(typeof(__THROW))) {
        private enum enumMixinStr___THROW = `enum __THROW = __attribute__ ( ( __nothrow__ ) );`;
        static if(is(typeof({ mixin(enumMixinStr___THROW); }))) {
            mixin(enumMixinStr___THROW);
        }
    }




    static if(!is(typeof(__THROWNL))) {
        private enum enumMixinStr___THROWNL = `enum __THROWNL = __attribute__ ( ( __nothrow__ ) );`;
        static if(is(typeof({ mixin(enumMixinStr___THROWNL); }))) {
            mixin(enumMixinStr___THROWNL);
        }
    }
    static if(!is(typeof(FIOCLEX))) {
        private enum enumMixinStr_FIOCLEX = `enum FIOCLEX = 0x5451;`;
        static if(is(typeof({ mixin(enumMixinStr_FIOCLEX); }))) {
            mixin(enumMixinStr_FIOCLEX);
        }
    }




    static if(!is(typeof(FIONCLEX))) {
        private enum enumMixinStr_FIONCLEX = `enum FIONCLEX = 0x5450;`;
        static if(is(typeof({ mixin(enumMixinStr_FIONCLEX); }))) {
            mixin(enumMixinStr_FIONCLEX);
        }
    }
    static if(!is(typeof(__ptr_t))) {
        private enum enumMixinStr___ptr_t = `enum __ptr_t = void *;`;
        static if(is(typeof({ mixin(enumMixinStr___ptr_t); }))) {
            mixin(enumMixinStr___ptr_t);
        }
    }
    static if(!is(typeof(TIOCSISO7816))) {
        private enum enumMixinStr_TIOCSISO7816 = `enum TIOCSISO7816 = _IOWR ( 'T' , 0x43 , serial_iso7816 );`;
        static if(is(typeof({ mixin(enumMixinStr_TIOCSISO7816); }))) {
            mixin(enumMixinStr_TIOCSISO7816);
        }
    }
    static if(!is(typeof(TIOCGISO7816))) {
        private enum enumMixinStr_TIOCGISO7816 = `enum TIOCGISO7816 = _IOR ( 'T' , 0x42 , serial_iso7816 );`;
        static if(is(typeof({ mixin(enumMixinStr_TIOCGISO7816); }))) {
            mixin(enumMixinStr_TIOCGISO7816);
        }
    }




    static if(!is(typeof(TIOCGPTPEER))) {
        private enum enumMixinStr_TIOCGPTPEER = `enum TIOCGPTPEER = _IO ( 'T' , 0x41 );`;
        static if(is(typeof({ mixin(enumMixinStr_TIOCGPTPEER); }))) {
            mixin(enumMixinStr_TIOCGPTPEER);
        }
    }




    static if(!is(typeof(__flexarr))) {
        private enum enumMixinStr___flexarr = `enum __flexarr = [ ];`;
        static if(is(typeof({ mixin(enumMixinStr___flexarr); }))) {
            mixin(enumMixinStr___flexarr);
        }
    }




    static if(!is(typeof(__glibc_c99_flexarr_available))) {
        private enum enumMixinStr___glibc_c99_flexarr_available = `enum __glibc_c99_flexarr_available = 1;`;
        static if(is(typeof({ mixin(enumMixinStr___glibc_c99_flexarr_available); }))) {
            mixin(enumMixinStr___glibc_c99_flexarr_available);
        }
    }




    static if(!is(typeof(TIOCGEXCL))) {
        private enum enumMixinStr_TIOCGEXCL = `enum TIOCGEXCL = _IOR ( 'T' , 0x40 , int );`;
        static if(is(typeof({ mixin(enumMixinStr_TIOCGEXCL); }))) {
            mixin(enumMixinStr_TIOCGEXCL);
        }
    }




    static if(!is(typeof(TIOCGPTLCK))) {
        private enum enumMixinStr_TIOCGPTLCK = `enum TIOCGPTLCK = _IOR ( 'T' , 0x39 , int );`;
        static if(is(typeof({ mixin(enumMixinStr_TIOCGPTLCK); }))) {
            mixin(enumMixinStr_TIOCGPTLCK);
        }
    }
    static if(!is(typeof(TIOCGPKT))) {
        private enum enumMixinStr_TIOCGPKT = `enum TIOCGPKT = _IOR ( 'T' , 0x38 , int );`;
        static if(is(typeof({ mixin(enumMixinStr_TIOCGPKT); }))) {
            mixin(enumMixinStr_TIOCGPKT);
        }
    }




    static if(!is(typeof(TIOCVHANGUP))) {
        private enum enumMixinStr_TIOCVHANGUP = `enum TIOCVHANGUP = 0x5437;`;
        static if(is(typeof({ mixin(enumMixinStr_TIOCVHANGUP); }))) {
            mixin(enumMixinStr_TIOCVHANGUP);
        }
    }




    static if(!is(typeof(TIOCSIG))) {
        private enum enumMixinStr_TIOCSIG = `enum TIOCSIG = _IOW ( 'T' , 0x36 , int );`;
        static if(is(typeof({ mixin(enumMixinStr_TIOCSIG); }))) {
            mixin(enumMixinStr_TIOCSIG);
        }
    }




    static if(!is(typeof(__attribute_malloc__))) {
        private enum enumMixinStr___attribute_malloc__ = `enum __attribute_malloc__ = __attribute__ ( ( __malloc__ ) );`;
        static if(is(typeof({ mixin(enumMixinStr___attribute_malloc__); }))) {
            mixin(enumMixinStr___attribute_malloc__);
        }
    }




    static if(!is(typeof(TCSETXW))) {
        private enum enumMixinStr_TCSETXW = `enum TCSETXW = 0x5435;`;
        static if(is(typeof({ mixin(enumMixinStr_TCSETXW); }))) {
            mixin(enumMixinStr_TCSETXW);
        }
    }






    static if(!is(typeof(TCSETXF))) {
        private enum enumMixinStr_TCSETXF = `enum TCSETXF = 0x5434;`;
        static if(is(typeof({ mixin(enumMixinStr_TCSETXF); }))) {
            mixin(enumMixinStr_TCSETXF);
        }
    }




    static if(!is(typeof(__attribute_pure__))) {
        private enum enumMixinStr___attribute_pure__ = `enum __attribute_pure__ = __attribute__ ( ( __pure__ ) );`;
        static if(is(typeof({ mixin(enumMixinStr___attribute_pure__); }))) {
            mixin(enumMixinStr___attribute_pure__);
        }
    }




    static if(!is(typeof(TCSETX))) {
        private enum enumMixinStr_TCSETX = `enum TCSETX = 0x5433;`;
        static if(is(typeof({ mixin(enumMixinStr_TCSETX); }))) {
            mixin(enumMixinStr_TCSETX);
        }
    }




    static if(!is(typeof(__attribute_const__))) {
        private enum enumMixinStr___attribute_const__ = `enum __attribute_const__ = __attribute__ ( cast( __const__ ) );`;
        static if(is(typeof({ mixin(enumMixinStr___attribute_const__); }))) {
            mixin(enumMixinStr___attribute_const__);
        }
    }




    static if(!is(typeof(TCGETX))) {
        private enum enumMixinStr_TCGETX = `enum TCGETX = 0x5432;`;
        static if(is(typeof({ mixin(enumMixinStr_TCGETX); }))) {
            mixin(enumMixinStr_TCGETX);
        }
    }




    static if(!is(typeof(__attribute_used__))) {
        private enum enumMixinStr___attribute_used__ = `enum __attribute_used__ = __attribute__ ( ( __used__ ) );`;
        static if(is(typeof({ mixin(enumMixinStr___attribute_used__); }))) {
            mixin(enumMixinStr___attribute_used__);
        }
    }




    static if(!is(typeof(__attribute_noinline__))) {
        private enum enumMixinStr___attribute_noinline__ = `enum __attribute_noinline__ = __attribute__ ( ( __noinline__ ) );`;
        static if(is(typeof({ mixin(enumMixinStr___attribute_noinline__); }))) {
            mixin(enumMixinStr___attribute_noinline__);
        }
    }




    static if(!is(typeof(TIOCGDEV))) {
        private enum enumMixinStr_TIOCGDEV = `enum TIOCGDEV = _IOR ( 'T' , 0x32 , unsigned int );`;
        static if(is(typeof({ mixin(enumMixinStr_TIOCGDEV); }))) {
            mixin(enumMixinStr_TIOCGDEV);
        }
    }




    static if(!is(typeof(__attribute_deprecated__))) {
        private enum enumMixinStr___attribute_deprecated__ = `enum __attribute_deprecated__ = __attribute__ ( ( __deprecated__ ) );`;
        static if(is(typeof({ mixin(enumMixinStr___attribute_deprecated__); }))) {
            mixin(enumMixinStr___attribute_deprecated__);
        }
    }




    static if(!is(typeof(TIOCSPTLCK))) {
        private enum enumMixinStr_TIOCSPTLCK = `enum TIOCSPTLCK = _IOW ( 'T' , 0x31 , int );`;
        static if(is(typeof({ mixin(enumMixinStr_TIOCSPTLCK); }))) {
            mixin(enumMixinStr_TIOCSPTLCK);
        }
    }




    static if(!is(typeof(TIOCGPTN))) {
        private enum enumMixinStr_TIOCGPTN = `enum TIOCGPTN = _IOR ( 'T' , 0x30 , unsigned int );`;
        static if(is(typeof({ mixin(enumMixinStr_TIOCGPTN); }))) {
            mixin(enumMixinStr_TIOCGPTN);
        }
    }






    static if(!is(typeof(TIOCSRS485))) {
        private enum enumMixinStr_TIOCSRS485 = `enum TIOCSRS485 = 0x542F;`;
        static if(is(typeof({ mixin(enumMixinStr_TIOCSRS485); }))) {
            mixin(enumMixinStr_TIOCSRS485);
        }
    }






    static if(!is(typeof(TIOCGRS485))) {
        private enum enumMixinStr_TIOCGRS485 = `enum TIOCGRS485 = 0x542E;`;
        static if(is(typeof({ mixin(enumMixinStr_TIOCGRS485); }))) {
            mixin(enumMixinStr_TIOCGRS485);
        }
    }






    static if(!is(typeof(TCSETSF2))) {
        private enum enumMixinStr_TCSETSF2 = `enum TCSETSF2 = _IOW ( 'T' , 0x2D , termios2 );`;
        static if(is(typeof({ mixin(enumMixinStr_TCSETSF2); }))) {
            mixin(enumMixinStr_TCSETSF2);
        }
    }






    static if(!is(typeof(TCSETSW2))) {
        private enum enumMixinStr_TCSETSW2 = `enum TCSETSW2 = _IOW ( 'T' , 0x2C , termios2 );`;
        static if(is(typeof({ mixin(enumMixinStr_TCSETSW2); }))) {
            mixin(enumMixinStr_TCSETSW2);
        }
    }




    static if(!is(typeof(__attribute_warn_unused_result__))) {
        private enum enumMixinStr___attribute_warn_unused_result__ = `enum __attribute_warn_unused_result__ = __attribute__ ( ( __warn_unused_result__ ) );`;
        static if(is(typeof({ mixin(enumMixinStr___attribute_warn_unused_result__); }))) {
            mixin(enumMixinStr___attribute_warn_unused_result__);
        }
    }




    static if(!is(typeof(TCSETS2))) {
        private enum enumMixinStr_TCSETS2 = `enum TCSETS2 = _IOW ( 'T' , 0x2B , termios2 );`;
        static if(is(typeof({ mixin(enumMixinStr_TCSETS2); }))) {
            mixin(enumMixinStr_TCSETS2);
        }
    }






    static if(!is(typeof(TCGETS2))) {
        private enum enumMixinStr_TCGETS2 = `enum TCGETS2 = _IOR ( 'T' , 0x2A , termios2 );`;
        static if(is(typeof({ mixin(enumMixinStr_TCGETS2); }))) {
            mixin(enumMixinStr_TCGETS2);
        }
    }




    static if(!is(typeof(__always_inline))) {
        private enum enumMixinStr___always_inline = `enum __always_inline = __inline __attribute__ ( ( __always_inline__ ) );`;
        static if(is(typeof({ mixin(enumMixinStr___always_inline); }))) {
            mixin(enumMixinStr___always_inline);
        }
    }




    static if(!is(typeof(TIOCGSID))) {
        private enum enumMixinStr_TIOCGSID = `enum TIOCGSID = 0x5429;`;
        static if(is(typeof({ mixin(enumMixinStr_TIOCGSID); }))) {
            mixin(enumMixinStr_TIOCGSID);
        }
    }






    static if(!is(typeof(TIOCCBRK))) {
        private enum enumMixinStr_TIOCCBRK = `enum TIOCCBRK = 0x5428;`;
        static if(is(typeof({ mixin(enumMixinStr_TIOCCBRK); }))) {
            mixin(enumMixinStr_TIOCCBRK);
        }
    }




    static if(!is(typeof(TIOCSBRK))) {
        private enum enumMixinStr_TIOCSBRK = `enum TIOCSBRK = 0x5427;`;
        static if(is(typeof({ mixin(enumMixinStr_TIOCSBRK); }))) {
            mixin(enumMixinStr_TIOCSBRK);
        }
    }




    static if(!is(typeof(TCSBRKP))) {
        private enum enumMixinStr_TCSBRKP = `enum TCSBRKP = 0x5425;`;
        static if(is(typeof({ mixin(enumMixinStr_TCSBRKP); }))) {
            mixin(enumMixinStr_TCSBRKP);
        }
    }




    static if(!is(typeof(TIOCGETD))) {
        private enum enumMixinStr_TIOCGETD = `enum TIOCGETD = 0x5424;`;
        static if(is(typeof({ mixin(enumMixinStr_TIOCGETD); }))) {
            mixin(enumMixinStr_TIOCGETD);
        }
    }




    static if(!is(typeof(__extern_inline))) {
        private enum enumMixinStr___extern_inline = `enum __extern_inline = extern __inline __attribute__ ( ( __gnu_inline__ ) );`;
        static if(is(typeof({ mixin(enumMixinStr___extern_inline); }))) {
            mixin(enumMixinStr___extern_inline);
        }
    }




    static if(!is(typeof(__extern_always_inline))) {
        private enum enumMixinStr___extern_always_inline = `enum __extern_always_inline = extern __inline __attribute__ ( ( __always_inline__ ) ) __attribute__ ( ( __gnu_inline__ ) );`;
        static if(is(typeof({ mixin(enumMixinStr___extern_always_inline); }))) {
            mixin(enumMixinStr___extern_always_inline);
        }
    }




    static if(!is(typeof(TIOCSETD))) {
        private enum enumMixinStr_TIOCSETD = `enum TIOCSETD = 0x5423;`;
        static if(is(typeof({ mixin(enumMixinStr_TIOCSETD); }))) {
            mixin(enumMixinStr_TIOCSETD);
        }
    }




    static if(!is(typeof(__fortify_function))) {
        private enum enumMixinStr___fortify_function = `enum __fortify_function = extern __inline __attribute__ ( ( __always_inline__ ) ) __attribute__ ( ( __gnu_inline__ ) ) ;`;
        static if(is(typeof({ mixin(enumMixinStr___fortify_function); }))) {
            mixin(enumMixinStr___fortify_function);
        }
    }




    static if(!is(typeof(TIOCNOTTY))) {
        private enum enumMixinStr_TIOCNOTTY = `enum TIOCNOTTY = 0x5422;`;
        static if(is(typeof({ mixin(enumMixinStr_TIOCNOTTY); }))) {
            mixin(enumMixinStr_TIOCNOTTY);
        }
    }




    static if(!is(typeof(FIONBIO))) {
        private enum enumMixinStr_FIONBIO = `enum FIONBIO = 0x5421;`;
        static if(is(typeof({ mixin(enumMixinStr_FIONBIO); }))) {
            mixin(enumMixinStr_FIONBIO);
        }
    }




    static if(!is(typeof(TIOCPKT))) {
        private enum enumMixinStr_TIOCPKT = `enum TIOCPKT = 0x5420;`;
        static if(is(typeof({ mixin(enumMixinStr_TIOCPKT); }))) {
            mixin(enumMixinStr_TIOCPKT);
        }
    }




    static if(!is(typeof(TIOCSSERIAL))) {
        private enum enumMixinStr_TIOCSSERIAL = `enum TIOCSSERIAL = 0x541F;`;
        static if(is(typeof({ mixin(enumMixinStr_TIOCSSERIAL); }))) {
            mixin(enumMixinStr_TIOCSSERIAL);
        }
    }




    static if(!is(typeof(__restrict_arr))) {
        private enum enumMixinStr___restrict_arr = `enum __restrict_arr = __restrict;`;
        static if(is(typeof({ mixin(enumMixinStr___restrict_arr); }))) {
            mixin(enumMixinStr___restrict_arr);
        }
    }




    static if(!is(typeof(TIOCGSERIAL))) {
        private enum enumMixinStr_TIOCGSERIAL = `enum TIOCGSERIAL = 0x541E;`;
        static if(is(typeof({ mixin(enumMixinStr_TIOCGSERIAL); }))) {
            mixin(enumMixinStr_TIOCGSERIAL);
        }
    }
    static if(!is(typeof(TIOCCONS))) {
        private enum enumMixinStr_TIOCCONS = `enum TIOCCONS = 0x541D;`;
        static if(is(typeof({ mixin(enumMixinStr_TIOCCONS); }))) {
            mixin(enumMixinStr_TIOCCONS);
        }
    }






    static if(!is(typeof(TIOCLINUX))) {
        private enum enumMixinStr_TIOCLINUX = `enum TIOCLINUX = 0x541C;`;
        static if(is(typeof({ mixin(enumMixinStr_TIOCLINUX); }))) {
            mixin(enumMixinStr_TIOCLINUX);
        }
    }




    static if(!is(typeof(TIOCINQ))) {
        private enum enumMixinStr_TIOCINQ = `enum TIOCINQ = FIONREAD;`;
        static if(is(typeof({ mixin(enumMixinStr_TIOCINQ); }))) {
            mixin(enumMixinStr_TIOCINQ);
        }
    }




    static if(!is(typeof(FIONREAD))) {
        private enum enumMixinStr_FIONREAD = `enum FIONREAD = 0x541B;`;
        static if(is(typeof({ mixin(enumMixinStr_FIONREAD); }))) {
            mixin(enumMixinStr_FIONREAD);
        }
    }




    static if(!is(typeof(TIOCSSOFTCAR))) {
        private enum enumMixinStr_TIOCSSOFTCAR = `enum TIOCSSOFTCAR = 0x541A;`;
        static if(is(typeof({ mixin(enumMixinStr_TIOCSSOFTCAR); }))) {
            mixin(enumMixinStr_TIOCSSOFTCAR);
        }
    }






    static if(!is(typeof(TIOCGSOFTCAR))) {
        private enum enumMixinStr_TIOCGSOFTCAR = `enum TIOCGSOFTCAR = 0x5419;`;
        static if(is(typeof({ mixin(enumMixinStr_TIOCGSOFTCAR); }))) {
            mixin(enumMixinStr_TIOCGSOFTCAR);
        }
    }






    static if(!is(typeof(TIOCMSET))) {
        private enum enumMixinStr_TIOCMSET = `enum TIOCMSET = 0x5418;`;
        static if(is(typeof({ mixin(enumMixinStr_TIOCMSET); }))) {
            mixin(enumMixinStr_TIOCMSET);
        }
    }




    static if(!is(typeof(TIOCMBIC))) {
        private enum enumMixinStr_TIOCMBIC = `enum TIOCMBIC = 0x5417;`;
        static if(is(typeof({ mixin(enumMixinStr_TIOCMBIC); }))) {
            mixin(enumMixinStr_TIOCMBIC);
        }
    }




    static if(!is(typeof(TIOCMBIS))) {
        private enum enumMixinStr_TIOCMBIS = `enum TIOCMBIS = 0x5416;`;
        static if(is(typeof({ mixin(enumMixinStr_TIOCMBIS); }))) {
            mixin(enumMixinStr_TIOCMBIS);
        }
    }




    static if(!is(typeof(TIOCMGET))) {
        private enum enumMixinStr_TIOCMGET = `enum TIOCMGET = 0x5415;`;
        static if(is(typeof({ mixin(enumMixinStr_TIOCMGET); }))) {
            mixin(enumMixinStr_TIOCMGET);
        }
    }




    static if(!is(typeof(TIOCSWINSZ))) {
        private enum enumMixinStr_TIOCSWINSZ = `enum TIOCSWINSZ = 0x5414;`;
        static if(is(typeof({ mixin(enumMixinStr_TIOCSWINSZ); }))) {
            mixin(enumMixinStr_TIOCSWINSZ);
        }
    }




    static if(!is(typeof(TIOCGWINSZ))) {
        private enum enumMixinStr_TIOCGWINSZ = `enum TIOCGWINSZ = 0x5413;`;
        static if(is(typeof({ mixin(enumMixinStr_TIOCGWINSZ); }))) {
            mixin(enumMixinStr_TIOCGWINSZ);
        }
    }




    static if(!is(typeof(TIOCSTI))) {
        private enum enumMixinStr_TIOCSTI = `enum TIOCSTI = 0x5412;`;
        static if(is(typeof({ mixin(enumMixinStr_TIOCSTI); }))) {
            mixin(enumMixinStr_TIOCSTI);
        }
    }




    static if(!is(typeof(TIOCOUTQ))) {
        private enum enumMixinStr_TIOCOUTQ = `enum TIOCOUTQ = 0x5411;`;
        static if(is(typeof({ mixin(enumMixinStr_TIOCOUTQ); }))) {
            mixin(enumMixinStr_TIOCOUTQ);
        }
    }
    static if(!is(typeof(TIOCSPGRP))) {
        private enum enumMixinStr_TIOCSPGRP = `enum TIOCSPGRP = 0x5410;`;
        static if(is(typeof({ mixin(enumMixinStr_TIOCSPGRP); }))) {
            mixin(enumMixinStr_TIOCSPGRP);
        }
    }
    static if(!is(typeof(TIOCGPGRP))) {
        private enum enumMixinStr_TIOCGPGRP = `enum TIOCGPGRP = 0x540F;`;
        static if(is(typeof({ mixin(enumMixinStr_TIOCGPGRP); }))) {
            mixin(enumMixinStr_TIOCGPGRP);
        }
    }




    static if(!is(typeof(TIOCSCTTY))) {
        private enum enumMixinStr_TIOCSCTTY = `enum TIOCSCTTY = 0x540E;`;
        static if(is(typeof({ mixin(enumMixinStr_TIOCSCTTY); }))) {
            mixin(enumMixinStr_TIOCSCTTY);
        }
    }
    static if(!is(typeof(TIOCNXCL))) {
        private enum enumMixinStr_TIOCNXCL = `enum TIOCNXCL = 0x540D;`;
        static if(is(typeof({ mixin(enumMixinStr_TIOCNXCL); }))) {
            mixin(enumMixinStr_TIOCNXCL);
        }
    }




    static if(!is(typeof(TIOCEXCL))) {
        private enum enumMixinStr_TIOCEXCL = `enum TIOCEXCL = 0x540C;`;
        static if(is(typeof({ mixin(enumMixinStr_TIOCEXCL); }))) {
            mixin(enumMixinStr_TIOCEXCL);
        }
    }




    static if(!is(typeof(TCFLSH))) {
        private enum enumMixinStr_TCFLSH = `enum TCFLSH = 0x540B;`;
        static if(is(typeof({ mixin(enumMixinStr_TCFLSH); }))) {
            mixin(enumMixinStr_TCFLSH);
        }
    }




    static if(!is(typeof(TCXONC))) {
        private enum enumMixinStr_TCXONC = `enum TCXONC = 0x540A;`;
        static if(is(typeof({ mixin(enumMixinStr_TCXONC); }))) {
            mixin(enumMixinStr_TCXONC);
        }
    }




    static if(!is(typeof(TCSBRK))) {
        private enum enumMixinStr_TCSBRK = `enum TCSBRK = 0x5409;`;
        static if(is(typeof({ mixin(enumMixinStr_TCSBRK); }))) {
            mixin(enumMixinStr_TCSBRK);
        }
    }




    static if(!is(typeof(__HAVE_GENERIC_SELECTION))) {
        private enum enumMixinStr___HAVE_GENERIC_SELECTION = `enum __HAVE_GENERIC_SELECTION = 1;`;
        static if(is(typeof({ mixin(enumMixinStr___HAVE_GENERIC_SELECTION); }))) {
            mixin(enumMixinStr___HAVE_GENERIC_SELECTION);
        }
    }




    static if(!is(typeof(TCSETAF))) {
        private enum enumMixinStr_TCSETAF = `enum TCSETAF = 0x5408;`;
        static if(is(typeof({ mixin(enumMixinStr_TCSETAF); }))) {
            mixin(enumMixinStr_TCSETAF);
        }
    }






    static if(!is(typeof(_SYS_IOCTL_H))) {
        private enum enumMixinStr__SYS_IOCTL_H = `enum _SYS_IOCTL_H = 1;`;
        static if(is(typeof({ mixin(enumMixinStr__SYS_IOCTL_H); }))) {
            mixin(enumMixinStr__SYS_IOCTL_H);
        }
    }




    static if(!is(typeof(TCSETAW))) {
        private enum enumMixinStr_TCSETAW = `enum TCSETAW = 0x5407;`;
        static if(is(typeof({ mixin(enumMixinStr_TCSETAW); }))) {
            mixin(enumMixinStr_TCSETAW);
        }
    }




    static if(!is(typeof(TCSETA))) {
        private enum enumMixinStr_TCSETA = `enum TCSETA = 0x5406;`;
        static if(is(typeof({ mixin(enumMixinStr_TCSETA); }))) {
            mixin(enumMixinStr_TCSETA);
        }
    }




    static if(!is(typeof(TCGETA))) {
        private enum enumMixinStr_TCGETA = `enum TCGETA = 0x5405;`;
        static if(is(typeof({ mixin(enumMixinStr_TCGETA); }))) {
            mixin(enumMixinStr_TCGETA);
        }
    }




    static if(!is(typeof(TCSETSF))) {
        private enum enumMixinStr_TCSETSF = `enum TCSETSF = 0x5404;`;
        static if(is(typeof({ mixin(enumMixinStr_TCSETSF); }))) {
            mixin(enumMixinStr_TCSETSF);
        }
    }




    static if(!is(typeof(TCSETSW))) {
        private enum enumMixinStr_TCSETSW = `enum TCSETSW = 0x5403;`;
        static if(is(typeof({ mixin(enumMixinStr_TCSETSW); }))) {
            mixin(enumMixinStr_TCSETSW);
        }
    }




    static if(!is(typeof(TCSETS))) {
        private enum enumMixinStr_TCSETS = `enum TCSETS = 0x5402;`;
        static if(is(typeof({ mixin(enumMixinStr_TCSETS); }))) {
            mixin(enumMixinStr_TCSETS);
        }
    }




    static if(!is(typeof(TCGETS))) {
        private enum enumMixinStr_TCGETS = `enum TCGETS = 0x5401;`;
        static if(is(typeof({ mixin(enumMixinStr_TCGETS); }))) {
            mixin(enumMixinStr_TCGETS);
        }
    }






    static if(!is(typeof(_SYS_SELECT_H))) {
        private enum enumMixinStr__SYS_SELECT_H = `enum _SYS_SELECT_H = 1;`;
        static if(is(typeof({ mixin(enumMixinStr__SYS_SELECT_H); }))) {
            mixin(enumMixinStr__SYS_SELECT_H);
        }
    }




    static if(!is(typeof(IOCSIZE_SHIFT))) {
        private enum enumMixinStr_IOCSIZE_SHIFT = `enum IOCSIZE_SHIFT = ( _IOC_SIZESHIFT );`;
        static if(is(typeof({ mixin(enumMixinStr_IOCSIZE_SHIFT); }))) {
            mixin(enumMixinStr_IOCSIZE_SHIFT);
        }
    }




    static if(!is(typeof(IOCSIZE_MASK))) {
        private enum enumMixinStr_IOCSIZE_MASK = `enum IOCSIZE_MASK = ( _IOC_SIZEMASK << _IOC_SIZESHIFT );`;
        static if(is(typeof({ mixin(enumMixinStr_IOCSIZE_MASK); }))) {
            mixin(enumMixinStr_IOCSIZE_MASK);
        }
    }




    static if(!is(typeof(IOC_INOUT))) {
        private enum enumMixinStr_IOC_INOUT = `enum IOC_INOUT = ( ( _IOC_WRITE | _IOC_READ ) << _IOC_DIRSHIFT );`;
        static if(is(typeof({ mixin(enumMixinStr_IOC_INOUT); }))) {
            mixin(enumMixinStr_IOC_INOUT);
        }
    }




    static if(!is(typeof(IOC_OUT))) {
        private enum enumMixinStr_IOC_OUT = `enum IOC_OUT = ( _IOC_READ << _IOC_DIRSHIFT );`;
        static if(is(typeof({ mixin(enumMixinStr_IOC_OUT); }))) {
            mixin(enumMixinStr_IOC_OUT);
        }
    }




    static if(!is(typeof(IOC_IN))) {
        private enum enumMixinStr_IOC_IN = `enum IOC_IN = ( _IOC_WRITE << _IOC_DIRSHIFT );`;
        static if(is(typeof({ mixin(enumMixinStr_IOC_IN); }))) {
            mixin(enumMixinStr_IOC_IN);
        }
    }
    static if(!is(typeof(__NFDBITS))) {
        private enum enumMixinStr___NFDBITS = `enum __NFDBITS = ( 8 * cast( int ) ( __fd_mask ) .sizeof );`;
        static if(is(typeof({ mixin(enumMixinStr___NFDBITS); }))) {
            mixin(enumMixinStr___NFDBITS);
        }
    }
    static if(!is(typeof(FD_SETSIZE))) {
        private enum enumMixinStr_FD_SETSIZE = `enum FD_SETSIZE = 1024;`;
        static if(is(typeof({ mixin(enumMixinStr_FD_SETSIZE); }))) {
            mixin(enumMixinStr_FD_SETSIZE);
        }
    }
    static if(!is(typeof(NFDBITS))) {
        private enum enumMixinStr_NFDBITS = `enum NFDBITS = ( 8 * cast( int ) ( __fd_mask ) .sizeof );`;
        static if(is(typeof({ mixin(enumMixinStr_NFDBITS); }))) {
            mixin(enumMixinStr_NFDBITS);
        }
    }
    static if(!is(typeof(_IOC_READ))) {
        private enum enumMixinStr__IOC_READ = `enum _IOC_READ = 2U;`;
        static if(is(typeof({ mixin(enumMixinStr__IOC_READ); }))) {
            mixin(enumMixinStr__IOC_READ);
        }
    }




    static if(!is(typeof(_IOC_WRITE))) {
        private enum enumMixinStr__IOC_WRITE = `enum _IOC_WRITE = 1U;`;
        static if(is(typeof({ mixin(enumMixinStr__IOC_WRITE); }))) {
            mixin(enumMixinStr__IOC_WRITE);
        }
    }




    static if(!is(typeof(_IOC_NONE))) {
        private enum enumMixinStr__IOC_NONE = `enum _IOC_NONE = 0U;`;
        static if(is(typeof({ mixin(enumMixinStr__IOC_NONE); }))) {
            mixin(enumMixinStr__IOC_NONE);
        }
    }




    static if(!is(typeof(_IOC_DIRSHIFT))) {
        private enum enumMixinStr__IOC_DIRSHIFT = `enum _IOC_DIRSHIFT = ( _IOC_SIZESHIFT + _IOC_SIZEBITS );`;
        static if(is(typeof({ mixin(enumMixinStr__IOC_DIRSHIFT); }))) {
            mixin(enumMixinStr__IOC_DIRSHIFT);
        }
    }




    static if(!is(typeof(_IOC_SIZESHIFT))) {
        private enum enumMixinStr__IOC_SIZESHIFT = `enum _IOC_SIZESHIFT = ( _IOC_TYPESHIFT + _IOC_TYPEBITS );`;
        static if(is(typeof({ mixin(enumMixinStr__IOC_SIZESHIFT); }))) {
            mixin(enumMixinStr__IOC_SIZESHIFT);
        }
    }




    static if(!is(typeof(_SYS_TIME_H))) {
        private enum enumMixinStr__SYS_TIME_H = `enum _SYS_TIME_H = 1;`;
        static if(is(typeof({ mixin(enumMixinStr__SYS_TIME_H); }))) {
            mixin(enumMixinStr__SYS_TIME_H);
        }
    }




    static if(!is(typeof(_IOC_TYPESHIFT))) {
        private enum enumMixinStr__IOC_TYPESHIFT = `enum _IOC_TYPESHIFT = ( _IOC_NRSHIFT + _IOC_NRBITS );`;
        static if(is(typeof({ mixin(enumMixinStr__IOC_TYPESHIFT); }))) {
            mixin(enumMixinStr__IOC_TYPESHIFT);
        }
    }




    static if(!is(typeof(_IOC_NRSHIFT))) {
        private enum enumMixinStr__IOC_NRSHIFT = `enum _IOC_NRSHIFT = 0;`;
        static if(is(typeof({ mixin(enumMixinStr__IOC_NRSHIFT); }))) {
            mixin(enumMixinStr__IOC_NRSHIFT);
        }
    }




    static if(!is(typeof(_IOC_DIRMASK))) {
        private enum enumMixinStr__IOC_DIRMASK = `enum _IOC_DIRMASK = ( ( 1 << _IOC_DIRBITS ) - 1 );`;
        static if(is(typeof({ mixin(enumMixinStr__IOC_DIRMASK); }))) {
            mixin(enumMixinStr__IOC_DIRMASK);
        }
    }




    static if(!is(typeof(_IOC_SIZEMASK))) {
        private enum enumMixinStr__IOC_SIZEMASK = `enum _IOC_SIZEMASK = ( ( 1 << _IOC_SIZEBITS ) - 1 );`;
        static if(is(typeof({ mixin(enumMixinStr__IOC_SIZEMASK); }))) {
            mixin(enumMixinStr__IOC_SIZEMASK);
        }
    }




    static if(!is(typeof(_IOC_TYPEMASK))) {
        private enum enumMixinStr__IOC_TYPEMASK = `enum _IOC_TYPEMASK = ( ( 1 << _IOC_TYPEBITS ) - 1 );`;
        static if(is(typeof({ mixin(enumMixinStr__IOC_TYPEMASK); }))) {
            mixin(enumMixinStr__IOC_TYPEMASK);
        }
    }






    static if(!is(typeof(_IOC_NRMASK))) {
        private enum enumMixinStr__IOC_NRMASK = `enum _IOC_NRMASK = ( ( 1 << _IOC_NRBITS ) - 1 );`;
        static if(is(typeof({ mixin(enumMixinStr__IOC_NRMASK); }))) {
            mixin(enumMixinStr__IOC_NRMASK);
        }
    }




    static if(!is(typeof(_IOC_DIRBITS))) {
        private enum enumMixinStr__IOC_DIRBITS = `enum _IOC_DIRBITS = 2;`;
        static if(is(typeof({ mixin(enumMixinStr__IOC_DIRBITS); }))) {
            mixin(enumMixinStr__IOC_DIRBITS);
        }
    }




    static if(!is(typeof(_IOC_SIZEBITS))) {
        private enum enumMixinStr__IOC_SIZEBITS = `enum _IOC_SIZEBITS = 14;`;
        static if(is(typeof({ mixin(enumMixinStr__IOC_SIZEBITS); }))) {
            mixin(enumMixinStr__IOC_SIZEBITS);
        }
    }




    static if(!is(typeof(_IOC_TYPEBITS))) {
        private enum enumMixinStr__IOC_TYPEBITS = `enum _IOC_TYPEBITS = 8;`;
        static if(is(typeof({ mixin(enumMixinStr__IOC_TYPEBITS); }))) {
            mixin(enumMixinStr__IOC_TYPEBITS);
        }
    }




    static if(!is(typeof(_IOC_NRBITS))) {
        private enum enumMixinStr__IOC_NRBITS = `enum _IOC_NRBITS = 8;`;
        static if(is(typeof({ mixin(enumMixinStr__IOC_NRBITS); }))) {
            mixin(enumMixinStr__IOC_NRBITS);
        }
    }
    static if(!is(typeof(EHWPOISON))) {
        private enum enumMixinStr_EHWPOISON = `enum EHWPOISON = 133;`;
        static if(is(typeof({ mixin(enumMixinStr_EHWPOISON); }))) {
            mixin(enumMixinStr_EHWPOISON);
        }
    }




    static if(!is(typeof(ERFKILL))) {
        private enum enumMixinStr_ERFKILL = `enum ERFKILL = 132;`;
        static if(is(typeof({ mixin(enumMixinStr_ERFKILL); }))) {
            mixin(enumMixinStr_ERFKILL);
        }
    }




    static if(!is(typeof(ENOTRECOVERABLE))) {
        private enum enumMixinStr_ENOTRECOVERABLE = `enum ENOTRECOVERABLE = 131;`;
        static if(is(typeof({ mixin(enumMixinStr_ENOTRECOVERABLE); }))) {
            mixin(enumMixinStr_ENOTRECOVERABLE);
        }
    }




    static if(!is(typeof(EOWNERDEAD))) {
        private enum enumMixinStr_EOWNERDEAD = `enum EOWNERDEAD = 130;`;
        static if(is(typeof({ mixin(enumMixinStr_EOWNERDEAD); }))) {
            mixin(enumMixinStr_EOWNERDEAD);
        }
    }




    static if(!is(typeof(EKEYREJECTED))) {
        private enum enumMixinStr_EKEYREJECTED = `enum EKEYREJECTED = 129;`;
        static if(is(typeof({ mixin(enumMixinStr_EKEYREJECTED); }))) {
            mixin(enumMixinStr_EKEYREJECTED);
        }
    }




    static if(!is(typeof(EKEYREVOKED))) {
        private enum enumMixinStr_EKEYREVOKED = `enum EKEYREVOKED = 128;`;
        static if(is(typeof({ mixin(enumMixinStr_EKEYREVOKED); }))) {
            mixin(enumMixinStr_EKEYREVOKED);
        }
    }




    static if(!is(typeof(ITIMER_REAL))) {
        private enum enumMixinStr_ITIMER_REAL = `enum ITIMER_REAL = ITIMER_REAL;`;
        static if(is(typeof({ mixin(enumMixinStr_ITIMER_REAL); }))) {
            mixin(enumMixinStr_ITIMER_REAL);
        }
    }




    static if(!is(typeof(ITIMER_VIRTUAL))) {
        private enum enumMixinStr_ITIMER_VIRTUAL = `enum ITIMER_VIRTUAL = ITIMER_VIRTUAL;`;
        static if(is(typeof({ mixin(enumMixinStr_ITIMER_VIRTUAL); }))) {
            mixin(enumMixinStr_ITIMER_VIRTUAL);
        }
    }




    static if(!is(typeof(ITIMER_PROF))) {
        private enum enumMixinStr_ITIMER_PROF = `enum ITIMER_PROF = ITIMER_PROF;`;
        static if(is(typeof({ mixin(enumMixinStr_ITIMER_PROF); }))) {
            mixin(enumMixinStr_ITIMER_PROF);
        }
    }




    static if(!is(typeof(EKEYEXPIRED))) {
        private enum enumMixinStr_EKEYEXPIRED = `enum EKEYEXPIRED = 127;`;
        static if(is(typeof({ mixin(enumMixinStr_EKEYEXPIRED); }))) {
            mixin(enumMixinStr_EKEYEXPIRED);
        }
    }




    static if(!is(typeof(ENOKEY))) {
        private enum enumMixinStr_ENOKEY = `enum ENOKEY = 126;`;
        static if(is(typeof({ mixin(enumMixinStr_ENOKEY); }))) {
            mixin(enumMixinStr_ENOKEY);
        }
    }




    static if(!is(typeof(ECANCELED))) {
        private enum enumMixinStr_ECANCELED = `enum ECANCELED = 125;`;
        static if(is(typeof({ mixin(enumMixinStr_ECANCELED); }))) {
            mixin(enumMixinStr_ECANCELED);
        }
    }




    static if(!is(typeof(EMEDIUMTYPE))) {
        private enum enumMixinStr_EMEDIUMTYPE = `enum EMEDIUMTYPE = 124;`;
        static if(is(typeof({ mixin(enumMixinStr_EMEDIUMTYPE); }))) {
            mixin(enumMixinStr_EMEDIUMTYPE);
        }
    }




    static if(!is(typeof(ENOMEDIUM))) {
        private enum enumMixinStr_ENOMEDIUM = `enum ENOMEDIUM = 123;`;
        static if(is(typeof({ mixin(enumMixinStr_ENOMEDIUM); }))) {
            mixin(enumMixinStr_ENOMEDIUM);
        }
    }




    static if(!is(typeof(EDQUOT))) {
        private enum enumMixinStr_EDQUOT = `enum EDQUOT = 122;`;
        static if(is(typeof({ mixin(enumMixinStr_EDQUOT); }))) {
            mixin(enumMixinStr_EDQUOT);
        }
    }




    static if(!is(typeof(EREMOTEIO))) {
        private enum enumMixinStr_EREMOTEIO = `enum EREMOTEIO = 121;`;
        static if(is(typeof({ mixin(enumMixinStr_EREMOTEIO); }))) {
            mixin(enumMixinStr_EREMOTEIO);
        }
    }




    static if(!is(typeof(EISNAM))) {
        private enum enumMixinStr_EISNAM = `enum EISNAM = 120;`;
        static if(is(typeof({ mixin(enumMixinStr_EISNAM); }))) {
            mixin(enumMixinStr_EISNAM);
        }
    }




    static if(!is(typeof(ENAVAIL))) {
        private enum enumMixinStr_ENAVAIL = `enum ENAVAIL = 119;`;
        static if(is(typeof({ mixin(enumMixinStr_ENAVAIL); }))) {
            mixin(enumMixinStr_ENAVAIL);
        }
    }




    static if(!is(typeof(ENOTNAM))) {
        private enum enumMixinStr_ENOTNAM = `enum ENOTNAM = 118;`;
        static if(is(typeof({ mixin(enumMixinStr_ENOTNAM); }))) {
            mixin(enumMixinStr_ENOTNAM);
        }
    }




    static if(!is(typeof(EUCLEAN))) {
        private enum enumMixinStr_EUCLEAN = `enum EUCLEAN = 117;`;
        static if(is(typeof({ mixin(enumMixinStr_EUCLEAN); }))) {
            mixin(enumMixinStr_EUCLEAN);
        }
    }




    static if(!is(typeof(ESTALE))) {
        private enum enumMixinStr_ESTALE = `enum ESTALE = 116;`;
        static if(is(typeof({ mixin(enumMixinStr_ESTALE); }))) {
            mixin(enumMixinStr_ESTALE);
        }
    }




    static if(!is(typeof(EINPROGRESS))) {
        private enum enumMixinStr_EINPROGRESS = `enum EINPROGRESS = 115;`;
        static if(is(typeof({ mixin(enumMixinStr_EINPROGRESS); }))) {
            mixin(enumMixinStr_EINPROGRESS);
        }
    }




    static if(!is(typeof(EALREADY))) {
        private enum enumMixinStr_EALREADY = `enum EALREADY = 114;`;
        static if(is(typeof({ mixin(enumMixinStr_EALREADY); }))) {
            mixin(enumMixinStr_EALREADY);
        }
    }




    static if(!is(typeof(EHOSTUNREACH))) {
        private enum enumMixinStr_EHOSTUNREACH = `enum EHOSTUNREACH = 113;`;
        static if(is(typeof({ mixin(enumMixinStr_EHOSTUNREACH); }))) {
            mixin(enumMixinStr_EHOSTUNREACH);
        }
    }




    static if(!is(typeof(EHOSTDOWN))) {
        private enum enumMixinStr_EHOSTDOWN = `enum EHOSTDOWN = 112;`;
        static if(is(typeof({ mixin(enumMixinStr_EHOSTDOWN); }))) {
            mixin(enumMixinStr_EHOSTDOWN);
        }
    }
    static if(!is(typeof(ECONNREFUSED))) {
        private enum enumMixinStr_ECONNREFUSED = `enum ECONNREFUSED = 111;`;
        static if(is(typeof({ mixin(enumMixinStr_ECONNREFUSED); }))) {
            mixin(enumMixinStr_ECONNREFUSED);
        }
    }






    static if(!is(typeof(TTYDEF_IFLAG))) {
        private enum enumMixinStr_TTYDEF_IFLAG = `enum TTYDEF_IFLAG = ( BRKINT | ISTRIP | ICRNL | IMAXBEL | IXON | IXANY );`;
        static if(is(typeof({ mixin(enumMixinStr_TTYDEF_IFLAG); }))) {
            mixin(enumMixinStr_TTYDEF_IFLAG);
        }
    }




    static if(!is(typeof(TTYDEF_OFLAG))) {
        private enum enumMixinStr_TTYDEF_OFLAG = `enum TTYDEF_OFLAG = ( OPOST | ONLCR | XTABS );`;
        static if(is(typeof({ mixin(enumMixinStr_TTYDEF_OFLAG); }))) {
            mixin(enumMixinStr_TTYDEF_OFLAG);
        }
    }




    static if(!is(typeof(TTYDEF_LFLAG))) {
        private enum enumMixinStr_TTYDEF_LFLAG = `enum TTYDEF_LFLAG = ( ECHO | ICANON | ISIG | IEXTEN | ECHOE | ECHOKE | ECHOCTL );`;
        static if(is(typeof({ mixin(enumMixinStr_TTYDEF_LFLAG); }))) {
            mixin(enumMixinStr_TTYDEF_LFLAG);
        }
    }




    static if(!is(typeof(TTYDEF_CFLAG))) {
        private enum enumMixinStr_TTYDEF_CFLAG = `enum TTYDEF_CFLAG = ( CREAD | CS7 | PARENB | HUPCL );`;
        static if(is(typeof({ mixin(enumMixinStr_TTYDEF_CFLAG); }))) {
            mixin(enumMixinStr_TTYDEF_CFLAG);
        }
    }




    static if(!is(typeof(TTYDEF_SPEED))) {
        private enum enumMixinStr_TTYDEF_SPEED = `enum TTYDEF_SPEED = ( B9600 );`;
        static if(is(typeof({ mixin(enumMixinStr_TTYDEF_SPEED); }))) {
            mixin(enumMixinStr_TTYDEF_SPEED);
        }
    }






    static if(!is(typeof(CEOF))) {
        private enum enumMixinStr_CEOF = `enum CEOF = ( 'd' & 037 );`;
        static if(is(typeof({ mixin(enumMixinStr_CEOF); }))) {
            mixin(enumMixinStr_CEOF);
        }
    }




    static if(!is(typeof(ETIMEDOUT))) {
        private enum enumMixinStr_ETIMEDOUT = `enum ETIMEDOUT = 110;`;
        static if(is(typeof({ mixin(enumMixinStr_ETIMEDOUT); }))) {
            mixin(enumMixinStr_ETIMEDOUT);
        }
    }




    static if(!is(typeof(CEOL))) {
        private enum enumMixinStr_CEOL = `enum CEOL = '\0';`;
        static if(is(typeof({ mixin(enumMixinStr_CEOL); }))) {
            mixin(enumMixinStr_CEOL);
        }
    }




    static if(!is(typeof(CERASE))) {
        private enum enumMixinStr_CERASE = `enum CERASE = std.conv.octal!177;`;
        static if(is(typeof({ mixin(enumMixinStr_CERASE); }))) {
            mixin(enumMixinStr_CERASE);
        }
    }




    static if(!is(typeof(CINTR))) {
        private enum enumMixinStr_CINTR = `enum CINTR = ( 'c' & 037 );`;
        static if(is(typeof({ mixin(enumMixinStr_CINTR); }))) {
            mixin(enumMixinStr_CINTR);
        }
    }




    static if(!is(typeof(ETOOMANYREFS))) {
        private enum enumMixinStr_ETOOMANYREFS = `enum ETOOMANYREFS = 109;`;
        static if(is(typeof({ mixin(enumMixinStr_ETOOMANYREFS); }))) {
            mixin(enumMixinStr_ETOOMANYREFS);
        }
    }




    static if(!is(typeof(CSTATUS))) {
        private enum enumMixinStr_CSTATUS = `enum CSTATUS = '\0';`;
        static if(is(typeof({ mixin(enumMixinStr_CSTATUS); }))) {
            mixin(enumMixinStr_CSTATUS);
        }
    }




    static if(!is(typeof(CKILL))) {
        private enum enumMixinStr_CKILL = `enum CKILL = ( 'u' & 037 );`;
        static if(is(typeof({ mixin(enumMixinStr_CKILL); }))) {
            mixin(enumMixinStr_CKILL);
        }
    }




    static if(!is(typeof(CMIN))) {
        private enum enumMixinStr_CMIN = `enum CMIN = 1;`;
        static if(is(typeof({ mixin(enumMixinStr_CMIN); }))) {
            mixin(enumMixinStr_CMIN);
        }
    }




    static if(!is(typeof(CQUIT))) {
        private enum enumMixinStr_CQUIT = `enum CQUIT = std.conv.octal!34;`;
        static if(is(typeof({ mixin(enumMixinStr_CQUIT); }))) {
            mixin(enumMixinStr_CQUIT);
        }
    }




    static if(!is(typeof(CSUSP))) {
        private enum enumMixinStr_CSUSP = `enum CSUSP = ( 'z' & 037 );`;
        static if(is(typeof({ mixin(enumMixinStr_CSUSP); }))) {
            mixin(enumMixinStr_CSUSP);
        }
    }




    static if(!is(typeof(CTIME))) {
        private enum enumMixinStr_CTIME = `enum CTIME = 0;`;
        static if(is(typeof({ mixin(enumMixinStr_CTIME); }))) {
            mixin(enumMixinStr_CTIME);
        }
    }




    static if(!is(typeof(CDSUSP))) {
        private enum enumMixinStr_CDSUSP = `enum CDSUSP = ( 'y' & 037 );`;
        static if(is(typeof({ mixin(enumMixinStr_CDSUSP); }))) {
            mixin(enumMixinStr_CDSUSP);
        }
    }




    static if(!is(typeof(CSTART))) {
        private enum enumMixinStr_CSTART = `enum CSTART = ( 'q' & 037 );`;
        static if(is(typeof({ mixin(enumMixinStr_CSTART); }))) {
            mixin(enumMixinStr_CSTART);
        }
    }




    static if(!is(typeof(CSTOP))) {
        private enum enumMixinStr_CSTOP = `enum CSTOP = ( 's' & 037 );`;
        static if(is(typeof({ mixin(enumMixinStr_CSTOP); }))) {
            mixin(enumMixinStr_CSTOP);
        }
    }




    static if(!is(typeof(CLNEXT))) {
        private enum enumMixinStr_CLNEXT = `enum CLNEXT = ( 'v' & 037 );`;
        static if(is(typeof({ mixin(enumMixinStr_CLNEXT); }))) {
            mixin(enumMixinStr_CLNEXT);
        }
    }




    static if(!is(typeof(CDISCARD))) {
        private enum enumMixinStr_CDISCARD = `enum CDISCARD = ( 'o' & 037 );`;
        static if(is(typeof({ mixin(enumMixinStr_CDISCARD); }))) {
            mixin(enumMixinStr_CDISCARD);
        }
    }




    static if(!is(typeof(CWERASE))) {
        private enum enumMixinStr_CWERASE = `enum CWERASE = ( 'w' & 037 );`;
        static if(is(typeof({ mixin(enumMixinStr_CWERASE); }))) {
            mixin(enumMixinStr_CWERASE);
        }
    }




    static if(!is(typeof(CREPRINT))) {
        private enum enumMixinStr_CREPRINT = `enum CREPRINT = ( 'r' & 037 );`;
        static if(is(typeof({ mixin(enumMixinStr_CREPRINT); }))) {
            mixin(enumMixinStr_CREPRINT);
        }
    }




    static if(!is(typeof(CEOT))) {
        private enum enumMixinStr_CEOT = `enum CEOT = ( 'd' & 037 );`;
        static if(is(typeof({ mixin(enumMixinStr_CEOT); }))) {
            mixin(enumMixinStr_CEOT);
        }
    }




    static if(!is(typeof(CBRK))) {
        private enum enumMixinStr_CBRK = `enum CBRK = '\0';`;
        static if(is(typeof({ mixin(enumMixinStr_CBRK); }))) {
            mixin(enumMixinStr_CBRK);
        }
    }




    static if(!is(typeof(CRPRNT))) {
        private enum enumMixinStr_CRPRNT = `enum CRPRNT = ( 'r' & 037 );`;
        static if(is(typeof({ mixin(enumMixinStr_CRPRNT); }))) {
            mixin(enumMixinStr_CRPRNT);
        }
    }




    static if(!is(typeof(CFLUSH))) {
        private enum enumMixinStr_CFLUSH = `enum CFLUSH = ( 'o' & 037 );`;
        static if(is(typeof({ mixin(enumMixinStr_CFLUSH); }))) {
            mixin(enumMixinStr_CFLUSH);
        }
    }




    static if(!is(typeof(_SYS_TYPES_H))) {
        private enum enumMixinStr__SYS_TYPES_H = `enum _SYS_TYPES_H = 1;`;
        static if(is(typeof({ mixin(enumMixinStr__SYS_TYPES_H); }))) {
            mixin(enumMixinStr__SYS_TYPES_H);
        }
    }




    static if(!is(typeof(ESHUTDOWN))) {
        private enum enumMixinStr_ESHUTDOWN = `enum ESHUTDOWN = 108;`;
        static if(is(typeof({ mixin(enumMixinStr_ESHUTDOWN); }))) {
            mixin(enumMixinStr_ESHUTDOWN);
        }
    }




    static if(!is(typeof(ENOTCONN))) {
        private enum enumMixinStr_ENOTCONN = `enum ENOTCONN = 107;`;
        static if(is(typeof({ mixin(enumMixinStr_ENOTCONN); }))) {
            mixin(enumMixinStr_ENOTCONN);
        }
    }




    static if(!is(typeof(EISCONN))) {
        private enum enumMixinStr_EISCONN = `enum EISCONN = 106;`;
        static if(is(typeof({ mixin(enumMixinStr_EISCONN); }))) {
            mixin(enumMixinStr_EISCONN);
        }
    }




    static if(!is(typeof(ENOBUFS))) {
        private enum enumMixinStr_ENOBUFS = `enum ENOBUFS = 105;`;
        static if(is(typeof({ mixin(enumMixinStr_ENOBUFS); }))) {
            mixin(enumMixinStr_ENOBUFS);
        }
    }




    static if(!is(typeof(ECONNRESET))) {
        private enum enumMixinStr_ECONNRESET = `enum ECONNRESET = 104;`;
        static if(is(typeof({ mixin(enumMixinStr_ECONNRESET); }))) {
            mixin(enumMixinStr_ECONNRESET);
        }
    }




    static if(!is(typeof(ECONNABORTED))) {
        private enum enumMixinStr_ECONNABORTED = `enum ECONNABORTED = 103;`;
        static if(is(typeof({ mixin(enumMixinStr_ECONNABORTED); }))) {
            mixin(enumMixinStr_ECONNABORTED);
        }
    }




    static if(!is(typeof(ENETRESET))) {
        private enum enumMixinStr_ENETRESET = `enum ENETRESET = 102;`;
        static if(is(typeof({ mixin(enumMixinStr_ENETRESET); }))) {
            mixin(enumMixinStr_ENETRESET);
        }
    }




    static if(!is(typeof(ENETUNREACH))) {
        private enum enumMixinStr_ENETUNREACH = `enum ENETUNREACH = 101;`;
        static if(is(typeof({ mixin(enumMixinStr_ENETUNREACH); }))) {
            mixin(enumMixinStr_ENETUNREACH);
        }
    }




    static if(!is(typeof(ENETDOWN))) {
        private enum enumMixinStr_ENETDOWN = `enum ENETDOWN = 100;`;
        static if(is(typeof({ mixin(enumMixinStr_ENETDOWN); }))) {
            mixin(enumMixinStr_ENETDOWN);
        }
    }




    static if(!is(typeof(EADDRNOTAVAIL))) {
        private enum enumMixinStr_EADDRNOTAVAIL = `enum EADDRNOTAVAIL = 99;`;
        static if(is(typeof({ mixin(enumMixinStr_EADDRNOTAVAIL); }))) {
            mixin(enumMixinStr_EADDRNOTAVAIL);
        }
    }




    static if(!is(typeof(EADDRINUSE))) {
        private enum enumMixinStr_EADDRINUSE = `enum EADDRINUSE = 98;`;
        static if(is(typeof({ mixin(enumMixinStr_EADDRINUSE); }))) {
            mixin(enumMixinStr_EADDRINUSE);
        }
    }






    static if(!is(typeof(EAFNOSUPPORT))) {
        private enum enumMixinStr_EAFNOSUPPORT = `enum EAFNOSUPPORT = 97;`;
        static if(is(typeof({ mixin(enumMixinStr_EAFNOSUPPORT); }))) {
            mixin(enumMixinStr_EAFNOSUPPORT);
        }
    }




    static if(!is(typeof(EPFNOSUPPORT))) {
        private enum enumMixinStr_EPFNOSUPPORT = `enum EPFNOSUPPORT = 96;`;
        static if(is(typeof({ mixin(enumMixinStr_EPFNOSUPPORT); }))) {
            mixin(enumMixinStr_EPFNOSUPPORT);
        }
    }






    static if(!is(typeof(EOPNOTSUPP))) {
        private enum enumMixinStr_EOPNOTSUPP = `enum EOPNOTSUPP = 95;`;
        static if(is(typeof({ mixin(enumMixinStr_EOPNOTSUPP); }))) {
            mixin(enumMixinStr_EOPNOTSUPP);
        }
    }






    static if(!is(typeof(ESOCKTNOSUPPORT))) {
        private enum enumMixinStr_ESOCKTNOSUPPORT = `enum ESOCKTNOSUPPORT = 94;`;
        static if(is(typeof({ mixin(enumMixinStr_ESOCKTNOSUPPORT); }))) {
            mixin(enumMixinStr_ESOCKTNOSUPPORT);
        }
    }




    static if(!is(typeof(EPROTONOSUPPORT))) {
        private enum enumMixinStr_EPROTONOSUPPORT = `enum EPROTONOSUPPORT = 93;`;
        static if(is(typeof({ mixin(enumMixinStr_EPROTONOSUPPORT); }))) {
            mixin(enumMixinStr_EPROTONOSUPPORT);
        }
    }




    static if(!is(typeof(ENOPROTOOPT))) {
        private enum enumMixinStr_ENOPROTOOPT = `enum ENOPROTOOPT = 92;`;
        static if(is(typeof({ mixin(enumMixinStr_ENOPROTOOPT); }))) {
            mixin(enumMixinStr_ENOPROTOOPT);
        }
    }






    static if(!is(typeof(EPROTOTYPE))) {
        private enum enumMixinStr_EPROTOTYPE = `enum EPROTOTYPE = 91;`;
        static if(is(typeof({ mixin(enumMixinStr_EPROTOTYPE); }))) {
            mixin(enumMixinStr_EPROTOTYPE);
        }
    }




    static if(!is(typeof(EMSGSIZE))) {
        private enum enumMixinStr_EMSGSIZE = `enum EMSGSIZE = 90;`;
        static if(is(typeof({ mixin(enumMixinStr_EMSGSIZE); }))) {
            mixin(enumMixinStr_EMSGSIZE);
        }
    }




    static if(!is(typeof(EDESTADDRREQ))) {
        private enum enumMixinStr_EDESTADDRREQ = `enum EDESTADDRREQ = 89;`;
        static if(is(typeof({ mixin(enumMixinStr_EDESTADDRREQ); }))) {
            mixin(enumMixinStr_EDESTADDRREQ);
        }
    }




    static if(!is(typeof(ENOTSOCK))) {
        private enum enumMixinStr_ENOTSOCK = `enum ENOTSOCK = 88;`;
        static if(is(typeof({ mixin(enumMixinStr_ENOTSOCK); }))) {
            mixin(enumMixinStr_ENOTSOCK);
        }
    }




    static if(!is(typeof(EUSERS))) {
        private enum enumMixinStr_EUSERS = `enum EUSERS = 87;`;
        static if(is(typeof({ mixin(enumMixinStr_EUSERS); }))) {
            mixin(enumMixinStr_EUSERS);
        }
    }






    static if(!is(typeof(ESTRPIPE))) {
        private enum enumMixinStr_ESTRPIPE = `enum ESTRPIPE = 86;`;
        static if(is(typeof({ mixin(enumMixinStr_ESTRPIPE); }))) {
            mixin(enumMixinStr_ESTRPIPE);
        }
    }




    static if(!is(typeof(ERESTART))) {
        private enum enumMixinStr_ERESTART = `enum ERESTART = 85;`;
        static if(is(typeof({ mixin(enumMixinStr_ERESTART); }))) {
            mixin(enumMixinStr_ERESTART);
        }
    }




    static if(!is(typeof(EILSEQ))) {
        private enum enumMixinStr_EILSEQ = `enum EILSEQ = 84;`;
        static if(is(typeof({ mixin(enumMixinStr_EILSEQ); }))) {
            mixin(enumMixinStr_EILSEQ);
        }
    }




    static if(!is(typeof(ELIBEXEC))) {
        private enum enumMixinStr_ELIBEXEC = `enum ELIBEXEC = 83;`;
        static if(is(typeof({ mixin(enumMixinStr_ELIBEXEC); }))) {
            mixin(enumMixinStr_ELIBEXEC);
        }
    }






    static if(!is(typeof(ELIBMAX))) {
        private enum enumMixinStr_ELIBMAX = `enum ELIBMAX = 82;`;
        static if(is(typeof({ mixin(enumMixinStr_ELIBMAX); }))) {
            mixin(enumMixinStr_ELIBMAX);
        }
    }




    static if(!is(typeof(ELIBSCN))) {
        private enum enumMixinStr_ELIBSCN = `enum ELIBSCN = 81;`;
        static if(is(typeof({ mixin(enumMixinStr_ELIBSCN); }))) {
            mixin(enumMixinStr_ELIBSCN);
        }
    }






    static if(!is(typeof(ELIBBAD))) {
        private enum enumMixinStr_ELIBBAD = `enum ELIBBAD = 80;`;
        static if(is(typeof({ mixin(enumMixinStr_ELIBBAD); }))) {
            mixin(enumMixinStr_ELIBBAD);
        }
    }




    static if(!is(typeof(ELIBACC))) {
        private enum enumMixinStr_ELIBACC = `enum ELIBACC = 79;`;
        static if(is(typeof({ mixin(enumMixinStr_ELIBACC); }))) {
            mixin(enumMixinStr_ELIBACC);
        }
    }




    static if(!is(typeof(EREMCHG))) {
        private enum enumMixinStr_EREMCHG = `enum EREMCHG = 78;`;
        static if(is(typeof({ mixin(enumMixinStr_EREMCHG); }))) {
            mixin(enumMixinStr_EREMCHG);
        }
    }




    static if(!is(typeof(EBADFD))) {
        private enum enumMixinStr_EBADFD = `enum EBADFD = 77;`;
        static if(is(typeof({ mixin(enumMixinStr_EBADFD); }))) {
            mixin(enumMixinStr_EBADFD);
        }
    }




    static if(!is(typeof(ENOTUNIQ))) {
        private enum enumMixinStr_ENOTUNIQ = `enum ENOTUNIQ = 76;`;
        static if(is(typeof({ mixin(enumMixinStr_ENOTUNIQ); }))) {
            mixin(enumMixinStr_ENOTUNIQ);
        }
    }




    static if(!is(typeof(EOVERFLOW))) {
        private enum enumMixinStr_EOVERFLOW = `enum EOVERFLOW = 75;`;
        static if(is(typeof({ mixin(enumMixinStr_EOVERFLOW); }))) {
            mixin(enumMixinStr_EOVERFLOW);
        }
    }




    static if(!is(typeof(EBADMSG))) {
        private enum enumMixinStr_EBADMSG = `enum EBADMSG = 74;`;
        static if(is(typeof({ mixin(enumMixinStr_EBADMSG); }))) {
            mixin(enumMixinStr_EBADMSG);
        }
    }




    static if(!is(typeof(EDOTDOT))) {
        private enum enumMixinStr_EDOTDOT = `enum EDOTDOT = 73;`;
        static if(is(typeof({ mixin(enumMixinStr_EDOTDOT); }))) {
            mixin(enumMixinStr_EDOTDOT);
        }
    }




    static if(!is(typeof(EMULTIHOP))) {
        private enum enumMixinStr_EMULTIHOP = `enum EMULTIHOP = 72;`;
        static if(is(typeof({ mixin(enumMixinStr_EMULTIHOP); }))) {
            mixin(enumMixinStr_EMULTIHOP);
        }
    }




    static if(!is(typeof(EPROTO))) {
        private enum enumMixinStr_EPROTO = `enum EPROTO = 71;`;
        static if(is(typeof({ mixin(enumMixinStr_EPROTO); }))) {
            mixin(enumMixinStr_EPROTO);
        }
    }




    static if(!is(typeof(ECOMM))) {
        private enum enumMixinStr_ECOMM = `enum ECOMM = 70;`;
        static if(is(typeof({ mixin(enumMixinStr_ECOMM); }))) {
            mixin(enumMixinStr_ECOMM);
        }
    }




    static if(!is(typeof(ESRMNT))) {
        private enum enumMixinStr_ESRMNT = `enum ESRMNT = 69;`;
        static if(is(typeof({ mixin(enumMixinStr_ESRMNT); }))) {
            mixin(enumMixinStr_ESRMNT);
        }
    }




    static if(!is(typeof(EADV))) {
        private enum enumMixinStr_EADV = `enum EADV = 68;`;
        static if(is(typeof({ mixin(enumMixinStr_EADV); }))) {
            mixin(enumMixinStr_EADV);
        }
    }




    static if(!is(typeof(ENOLINK))) {
        private enum enumMixinStr_ENOLINK = `enum ENOLINK = 67;`;
        static if(is(typeof({ mixin(enumMixinStr_ENOLINK); }))) {
            mixin(enumMixinStr_ENOLINK);
        }
    }




    static if(!is(typeof(EREMOTE))) {
        private enum enumMixinStr_EREMOTE = `enum EREMOTE = 66;`;
        static if(is(typeof({ mixin(enumMixinStr_EREMOTE); }))) {
            mixin(enumMixinStr_EREMOTE);
        }
    }




    static if(!is(typeof(ENOPKG))) {
        private enum enumMixinStr_ENOPKG = `enum ENOPKG = 65;`;
        static if(is(typeof({ mixin(enumMixinStr_ENOPKG); }))) {
            mixin(enumMixinStr_ENOPKG);
        }
    }




    static if(!is(typeof(ENONET))) {
        private enum enumMixinStr_ENONET = `enum ENONET = 64;`;
        static if(is(typeof({ mixin(enumMixinStr_ENONET); }))) {
            mixin(enumMixinStr_ENONET);
        }
    }




    static if(!is(typeof(__BIT_TYPES_DEFINED__))) {
        private enum enumMixinStr___BIT_TYPES_DEFINED__ = `enum __BIT_TYPES_DEFINED__ = 1;`;
        static if(is(typeof({ mixin(enumMixinStr___BIT_TYPES_DEFINED__); }))) {
            mixin(enumMixinStr___BIT_TYPES_DEFINED__);
        }
    }




    static if(!is(typeof(ENOSR))) {
        private enum enumMixinStr_ENOSR = `enum ENOSR = 63;`;
        static if(is(typeof({ mixin(enumMixinStr_ENOSR); }))) {
            mixin(enumMixinStr_ENOSR);
        }
    }




    static if(!is(typeof(ETIME))) {
        private enum enumMixinStr_ETIME = `enum ETIME = 62;`;
        static if(is(typeof({ mixin(enumMixinStr_ETIME); }))) {
            mixin(enumMixinStr_ETIME);
        }
    }




    static if(!is(typeof(ENODATA))) {
        private enum enumMixinStr_ENODATA = `enum ENODATA = 61;`;
        static if(is(typeof({ mixin(enumMixinStr_ENODATA); }))) {
            mixin(enumMixinStr_ENODATA);
        }
    }




    static if(!is(typeof(ENOSTR))) {
        private enum enumMixinStr_ENOSTR = `enum ENOSTR = 60;`;
        static if(is(typeof({ mixin(enumMixinStr_ENOSTR); }))) {
            mixin(enumMixinStr_ENOSTR);
        }
    }




    static if(!is(typeof(EBFONT))) {
        private enum enumMixinStr_EBFONT = `enum EBFONT = 59;`;
        static if(is(typeof({ mixin(enumMixinStr_EBFONT); }))) {
            mixin(enumMixinStr_EBFONT);
        }
    }






    static if(!is(typeof(EDEADLOCK))) {
        private enum enumMixinStr_EDEADLOCK = `enum EDEADLOCK = EDEADLK;`;
        static if(is(typeof({ mixin(enumMixinStr_EDEADLOCK); }))) {
            mixin(enumMixinStr_EDEADLOCK);
        }
    }






    static if(!is(typeof(EBADSLT))) {
        private enum enumMixinStr_EBADSLT = `enum EBADSLT = 57;`;
        static if(is(typeof({ mixin(enumMixinStr_EBADSLT); }))) {
            mixin(enumMixinStr_EBADSLT);
        }
    }






    static if(!is(typeof(EBADRQC))) {
        private enum enumMixinStr_EBADRQC = `enum EBADRQC = 56;`;
        static if(is(typeof({ mixin(enumMixinStr_EBADRQC); }))) {
            mixin(enumMixinStr_EBADRQC);
        }
    }






    static if(!is(typeof(ENOANO))) {
        private enum enumMixinStr_ENOANO = `enum ENOANO = 55;`;
        static if(is(typeof({ mixin(enumMixinStr_ENOANO); }))) {
            mixin(enumMixinStr_ENOANO);
        }
    }




    static if(!is(typeof(EXFULL))) {
        private enum enumMixinStr_EXFULL = `enum EXFULL = 54;`;
        static if(is(typeof({ mixin(enumMixinStr_EXFULL); }))) {
            mixin(enumMixinStr_EXFULL);
        }
    }




    static if(!is(typeof(EBADR))) {
        private enum enumMixinStr_EBADR = `enum EBADR = 53;`;
        static if(is(typeof({ mixin(enumMixinStr_EBADR); }))) {
            mixin(enumMixinStr_EBADR);
        }
    }




    static if(!is(typeof(_UNISTD_H))) {
        private enum enumMixinStr__UNISTD_H = `enum _UNISTD_H = 1;`;
        static if(is(typeof({ mixin(enumMixinStr__UNISTD_H); }))) {
            mixin(enumMixinStr__UNISTD_H);
        }
    }




    static if(!is(typeof(EBADE))) {
        private enum enumMixinStr_EBADE = `enum EBADE = 52;`;
        static if(is(typeof({ mixin(enumMixinStr_EBADE); }))) {
            mixin(enumMixinStr_EBADE);
        }
    }




    static if(!is(typeof(EL2HLT))) {
        private enum enumMixinStr_EL2HLT = `enum EL2HLT = 51;`;
        static if(is(typeof({ mixin(enumMixinStr_EL2HLT); }))) {
            mixin(enumMixinStr_EL2HLT);
        }
    }




    static if(!is(typeof(ENOCSI))) {
        private enum enumMixinStr_ENOCSI = `enum ENOCSI = 50;`;
        static if(is(typeof({ mixin(enumMixinStr_ENOCSI); }))) {
            mixin(enumMixinStr_ENOCSI);
        }
    }




    static if(!is(typeof(_POSIX_VERSION))) {
        private enum enumMixinStr__POSIX_VERSION = `enum _POSIX_VERSION = 200809L;`;
        static if(is(typeof({ mixin(enumMixinStr__POSIX_VERSION); }))) {
            mixin(enumMixinStr__POSIX_VERSION);
        }
    }




    static if(!is(typeof(EUNATCH))) {
        private enum enumMixinStr_EUNATCH = `enum EUNATCH = 49;`;
        static if(is(typeof({ mixin(enumMixinStr_EUNATCH); }))) {
            mixin(enumMixinStr_EUNATCH);
        }
    }




    static if(!is(typeof(__POSIX2_THIS_VERSION))) {
        private enum enumMixinStr___POSIX2_THIS_VERSION = `enum __POSIX2_THIS_VERSION = 200809L;`;
        static if(is(typeof({ mixin(enumMixinStr___POSIX2_THIS_VERSION); }))) {
            mixin(enumMixinStr___POSIX2_THIS_VERSION);
        }
    }




    static if(!is(typeof(_POSIX2_VERSION))) {
        private enum enumMixinStr__POSIX2_VERSION = `enum _POSIX2_VERSION = 200809L;`;
        static if(is(typeof({ mixin(enumMixinStr__POSIX2_VERSION); }))) {
            mixin(enumMixinStr__POSIX2_VERSION);
        }
    }




    static if(!is(typeof(_POSIX2_C_VERSION))) {
        private enum enumMixinStr__POSIX2_C_VERSION = `enum _POSIX2_C_VERSION = 200809L;`;
        static if(is(typeof({ mixin(enumMixinStr__POSIX2_C_VERSION); }))) {
            mixin(enumMixinStr__POSIX2_C_VERSION);
        }
    }




    static if(!is(typeof(_POSIX2_C_BIND))) {
        private enum enumMixinStr__POSIX2_C_BIND = `enum _POSIX2_C_BIND = 200809L;`;
        static if(is(typeof({ mixin(enumMixinStr__POSIX2_C_BIND); }))) {
            mixin(enumMixinStr__POSIX2_C_BIND);
        }
    }




    static if(!is(typeof(_POSIX2_C_DEV))) {
        private enum enumMixinStr__POSIX2_C_DEV = `enum _POSIX2_C_DEV = 200809L;`;
        static if(is(typeof({ mixin(enumMixinStr__POSIX2_C_DEV); }))) {
            mixin(enumMixinStr__POSIX2_C_DEV);
        }
    }




    static if(!is(typeof(_POSIX2_SW_DEV))) {
        private enum enumMixinStr__POSIX2_SW_DEV = `enum _POSIX2_SW_DEV = 200809L;`;
        static if(is(typeof({ mixin(enumMixinStr__POSIX2_SW_DEV); }))) {
            mixin(enumMixinStr__POSIX2_SW_DEV);
        }
    }




    static if(!is(typeof(_POSIX2_LOCALEDEF))) {
        private enum enumMixinStr__POSIX2_LOCALEDEF = `enum _POSIX2_LOCALEDEF = 200809L;`;
        static if(is(typeof({ mixin(enumMixinStr__POSIX2_LOCALEDEF); }))) {
            mixin(enumMixinStr__POSIX2_LOCALEDEF);
        }
    }




    static if(!is(typeof(ELNRNG))) {
        private enum enumMixinStr_ELNRNG = `enum ELNRNG = 48;`;
        static if(is(typeof({ mixin(enumMixinStr_ELNRNG); }))) {
            mixin(enumMixinStr_ELNRNG);
        }
    }




    static if(!is(typeof(_XOPEN_VERSION))) {
        private enum enumMixinStr__XOPEN_VERSION = `enum _XOPEN_VERSION = 700;`;
        static if(is(typeof({ mixin(enumMixinStr__XOPEN_VERSION); }))) {
            mixin(enumMixinStr__XOPEN_VERSION);
        }
    }




    static if(!is(typeof(_XOPEN_XCU_VERSION))) {
        private enum enumMixinStr__XOPEN_XCU_VERSION = `enum _XOPEN_XCU_VERSION = 4;`;
        static if(is(typeof({ mixin(enumMixinStr__XOPEN_XCU_VERSION); }))) {
            mixin(enumMixinStr__XOPEN_XCU_VERSION);
        }
    }




    static if(!is(typeof(_XOPEN_XPG2))) {
        private enum enumMixinStr__XOPEN_XPG2 = `enum _XOPEN_XPG2 = 1;`;
        static if(is(typeof({ mixin(enumMixinStr__XOPEN_XPG2); }))) {
            mixin(enumMixinStr__XOPEN_XPG2);
        }
    }




    static if(!is(typeof(_XOPEN_XPG3))) {
        private enum enumMixinStr__XOPEN_XPG3 = `enum _XOPEN_XPG3 = 1;`;
        static if(is(typeof({ mixin(enumMixinStr__XOPEN_XPG3); }))) {
            mixin(enumMixinStr__XOPEN_XPG3);
        }
    }




    static if(!is(typeof(_XOPEN_XPG4))) {
        private enum enumMixinStr__XOPEN_XPG4 = `enum _XOPEN_XPG4 = 1;`;
        static if(is(typeof({ mixin(enumMixinStr__XOPEN_XPG4); }))) {
            mixin(enumMixinStr__XOPEN_XPG4);
        }
    }




    static if(!is(typeof(_XOPEN_UNIX))) {
        private enum enumMixinStr__XOPEN_UNIX = `enum _XOPEN_UNIX = 1;`;
        static if(is(typeof({ mixin(enumMixinStr__XOPEN_UNIX); }))) {
            mixin(enumMixinStr__XOPEN_UNIX);
        }
    }




    static if(!is(typeof(_XOPEN_ENH_I18N))) {
        private enum enumMixinStr__XOPEN_ENH_I18N = `enum _XOPEN_ENH_I18N = 1;`;
        static if(is(typeof({ mixin(enumMixinStr__XOPEN_ENH_I18N); }))) {
            mixin(enumMixinStr__XOPEN_ENH_I18N);
        }
    }




    static if(!is(typeof(_XOPEN_LEGACY))) {
        private enum enumMixinStr__XOPEN_LEGACY = `enum _XOPEN_LEGACY = 1;`;
        static if(is(typeof({ mixin(enumMixinStr__XOPEN_LEGACY); }))) {
            mixin(enumMixinStr__XOPEN_LEGACY);
        }
    }




    static if(!is(typeof(EL3RST))) {
        private enum enumMixinStr_EL3RST = `enum EL3RST = 47;`;
        static if(is(typeof({ mixin(enumMixinStr_EL3RST); }))) {
            mixin(enumMixinStr_EL3RST);
        }
    }




    static if(!is(typeof(EL3HLT))) {
        private enum enumMixinStr_EL3HLT = `enum EL3HLT = 46;`;
        static if(is(typeof({ mixin(enumMixinStr_EL3HLT); }))) {
            mixin(enumMixinStr_EL3HLT);
        }
    }




    static if(!is(typeof(EL2NSYNC))) {
        private enum enumMixinStr_EL2NSYNC = `enum EL2NSYNC = 45;`;
        static if(is(typeof({ mixin(enumMixinStr_EL2NSYNC); }))) {
            mixin(enumMixinStr_EL2NSYNC);
        }
    }




    static if(!is(typeof(STDIN_FILENO))) {
        private enum enumMixinStr_STDIN_FILENO = `enum STDIN_FILENO = 0;`;
        static if(is(typeof({ mixin(enumMixinStr_STDIN_FILENO); }))) {
            mixin(enumMixinStr_STDIN_FILENO);
        }
    }




    static if(!is(typeof(STDOUT_FILENO))) {
        private enum enumMixinStr_STDOUT_FILENO = `enum STDOUT_FILENO = 1;`;
        static if(is(typeof({ mixin(enumMixinStr_STDOUT_FILENO); }))) {
            mixin(enumMixinStr_STDOUT_FILENO);
        }
    }




    static if(!is(typeof(STDERR_FILENO))) {
        private enum enumMixinStr_STDERR_FILENO = `enum STDERR_FILENO = 2;`;
        static if(is(typeof({ mixin(enumMixinStr_STDERR_FILENO); }))) {
            mixin(enumMixinStr_STDERR_FILENO);
        }
    }




    static if(!is(typeof(ECHRNG))) {
        private enum enumMixinStr_ECHRNG = `enum ECHRNG = 44;`;
        static if(is(typeof({ mixin(enumMixinStr_ECHRNG); }))) {
            mixin(enumMixinStr_ECHRNG);
        }
    }




    static if(!is(typeof(EIDRM))) {
        private enum enumMixinStr_EIDRM = `enum EIDRM = 43;`;
        static if(is(typeof({ mixin(enumMixinStr_EIDRM); }))) {
            mixin(enumMixinStr_EIDRM);
        }
    }






    static if(!is(typeof(ENOMSG))) {
        private enum enumMixinStr_ENOMSG = `enum ENOMSG = 42;`;
        static if(is(typeof({ mixin(enumMixinStr_ENOMSG); }))) {
            mixin(enumMixinStr_ENOMSG);
        }
    }




    static if(!is(typeof(EWOULDBLOCK))) {
        private enum enumMixinStr_EWOULDBLOCK = `enum EWOULDBLOCK = EAGAIN;`;
        static if(is(typeof({ mixin(enumMixinStr_EWOULDBLOCK); }))) {
            mixin(enumMixinStr_EWOULDBLOCK);
        }
    }




    static if(!is(typeof(ELOOP))) {
        private enum enumMixinStr_ELOOP = `enum ELOOP = 40;`;
        static if(is(typeof({ mixin(enumMixinStr_ELOOP); }))) {
            mixin(enumMixinStr_ELOOP);
        }
    }






    static if(!is(typeof(ENOTEMPTY))) {
        private enum enumMixinStr_ENOTEMPTY = `enum ENOTEMPTY = 39;`;
        static if(is(typeof({ mixin(enumMixinStr_ENOTEMPTY); }))) {
            mixin(enumMixinStr_ENOTEMPTY);
        }
    }






    static if(!is(typeof(ENOSYS))) {
        private enum enumMixinStr_ENOSYS = `enum ENOSYS = 38;`;
        static if(is(typeof({ mixin(enumMixinStr_ENOSYS); }))) {
            mixin(enumMixinStr_ENOSYS);
        }
    }






    static if(!is(typeof(ENOLCK))) {
        private enum enumMixinStr_ENOLCK = `enum ENOLCK = 37;`;
        static if(is(typeof({ mixin(enumMixinStr_ENOLCK); }))) {
            mixin(enumMixinStr_ENOLCK);
        }
    }






    static if(!is(typeof(ENAMETOOLONG))) {
        private enum enumMixinStr_ENAMETOOLONG = `enum ENAMETOOLONG = 36;`;
        static if(is(typeof({ mixin(enumMixinStr_ENAMETOOLONG); }))) {
            mixin(enumMixinStr_ENAMETOOLONG);
        }
    }






    static if(!is(typeof(EDEADLK))) {
        private enum enumMixinStr_EDEADLK = `enum EDEADLK = 35;`;
        static if(is(typeof({ mixin(enumMixinStr_EDEADLK); }))) {
            mixin(enumMixinStr_EDEADLK);
        }
    }
    static if(!is(typeof(ERANGE))) {
        private enum enumMixinStr_ERANGE = `enum ERANGE = 34;`;
        static if(is(typeof({ mixin(enumMixinStr_ERANGE); }))) {
            mixin(enumMixinStr_ERANGE);
        }
    }




    static if(!is(typeof(EDOM))) {
        private enum enumMixinStr_EDOM = `enum EDOM = 33;`;
        static if(is(typeof({ mixin(enumMixinStr_EDOM); }))) {
            mixin(enumMixinStr_EDOM);
        }
    }






    static if(!is(typeof(R_OK))) {
        private enum enumMixinStr_R_OK = `enum R_OK = 4;`;
        static if(is(typeof({ mixin(enumMixinStr_R_OK); }))) {
            mixin(enumMixinStr_R_OK);
        }
    }




    static if(!is(typeof(W_OK))) {
        private enum enumMixinStr_W_OK = `enum W_OK = 2;`;
        static if(is(typeof({ mixin(enumMixinStr_W_OK); }))) {
            mixin(enumMixinStr_W_OK);
        }
    }




    static if(!is(typeof(X_OK))) {
        private enum enumMixinStr_X_OK = `enum X_OK = 1;`;
        static if(is(typeof({ mixin(enumMixinStr_X_OK); }))) {
            mixin(enumMixinStr_X_OK);
        }
    }




    static if(!is(typeof(F_OK))) {
        private enum enumMixinStr_F_OK = `enum F_OK = 0;`;
        static if(is(typeof({ mixin(enumMixinStr_F_OK); }))) {
            mixin(enumMixinStr_F_OK);
        }
    }




    static if(!is(typeof(EPIPE))) {
        private enum enumMixinStr_EPIPE = `enum EPIPE = 32;`;
        static if(is(typeof({ mixin(enumMixinStr_EPIPE); }))) {
            mixin(enumMixinStr_EPIPE);
        }
    }




    static if(!is(typeof(EMLINK))) {
        private enum enumMixinStr_EMLINK = `enum EMLINK = 31;`;
        static if(is(typeof({ mixin(enumMixinStr_EMLINK); }))) {
            mixin(enumMixinStr_EMLINK);
        }
    }




    static if(!is(typeof(EROFS))) {
        private enum enumMixinStr_EROFS = `enum EROFS = 30;`;
        static if(is(typeof({ mixin(enumMixinStr_EROFS); }))) {
            mixin(enumMixinStr_EROFS);
        }
    }




    static if(!is(typeof(ESPIPE))) {
        private enum enumMixinStr_ESPIPE = `enum ESPIPE = 29;`;
        static if(is(typeof({ mixin(enumMixinStr_ESPIPE); }))) {
            mixin(enumMixinStr_ESPIPE);
        }
    }




    static if(!is(typeof(ENOSPC))) {
        private enum enumMixinStr_ENOSPC = `enum ENOSPC = 28;`;
        static if(is(typeof({ mixin(enumMixinStr_ENOSPC); }))) {
            mixin(enumMixinStr_ENOSPC);
        }
    }




    static if(!is(typeof(EFBIG))) {
        private enum enumMixinStr_EFBIG = `enum EFBIG = 27;`;
        static if(is(typeof({ mixin(enumMixinStr_EFBIG); }))) {
            mixin(enumMixinStr_EFBIG);
        }
    }




    static if(!is(typeof(ETXTBSY))) {
        private enum enumMixinStr_ETXTBSY = `enum ETXTBSY = 26;`;
        static if(is(typeof({ mixin(enumMixinStr_ETXTBSY); }))) {
            mixin(enumMixinStr_ETXTBSY);
        }
    }




    static if(!is(typeof(ENOTTY))) {
        private enum enumMixinStr_ENOTTY = `enum ENOTTY = 25;`;
        static if(is(typeof({ mixin(enumMixinStr_ENOTTY); }))) {
            mixin(enumMixinStr_ENOTTY);
        }
    }




    static if(!is(typeof(EMFILE))) {
        private enum enumMixinStr_EMFILE = `enum EMFILE = 24;`;
        static if(is(typeof({ mixin(enumMixinStr_EMFILE); }))) {
            mixin(enumMixinStr_EMFILE);
        }
    }




    static if(!is(typeof(L_SET))) {
        private enum enumMixinStr_L_SET = `enum L_SET = 0;`;
        static if(is(typeof({ mixin(enumMixinStr_L_SET); }))) {
            mixin(enumMixinStr_L_SET);
        }
    }




    static if(!is(typeof(L_INCR))) {
        private enum enumMixinStr_L_INCR = `enum L_INCR = 1;`;
        static if(is(typeof({ mixin(enumMixinStr_L_INCR); }))) {
            mixin(enumMixinStr_L_INCR);
        }
    }




    static if(!is(typeof(L_XTND))) {
        private enum enumMixinStr_L_XTND = `enum L_XTND = 2;`;
        static if(is(typeof({ mixin(enumMixinStr_L_XTND); }))) {
            mixin(enumMixinStr_L_XTND);
        }
    }




    static if(!is(typeof(ENFILE))) {
        private enum enumMixinStr_ENFILE = `enum ENFILE = 23;`;
        static if(is(typeof({ mixin(enumMixinStr_ENFILE); }))) {
            mixin(enumMixinStr_ENFILE);
        }
    }




    static if(!is(typeof(EINVAL))) {
        private enum enumMixinStr_EINVAL = `enum EINVAL = 22;`;
        static if(is(typeof({ mixin(enumMixinStr_EINVAL); }))) {
            mixin(enumMixinStr_EINVAL);
        }
    }




    static if(!is(typeof(EISDIR))) {
        private enum enumMixinStr_EISDIR = `enum EISDIR = 21;`;
        static if(is(typeof({ mixin(enumMixinStr_EISDIR); }))) {
            mixin(enumMixinStr_EISDIR);
        }
    }




    static if(!is(typeof(ENOTDIR))) {
        private enum enumMixinStr_ENOTDIR = `enum ENOTDIR = 20;`;
        static if(is(typeof({ mixin(enumMixinStr_ENOTDIR); }))) {
            mixin(enumMixinStr_ENOTDIR);
        }
    }




    static if(!is(typeof(ENODEV))) {
        private enum enumMixinStr_ENODEV = `enum ENODEV = 19;`;
        static if(is(typeof({ mixin(enumMixinStr_ENODEV); }))) {
            mixin(enumMixinStr_ENODEV);
        }
    }




    static if(!is(typeof(EXDEV))) {
        private enum enumMixinStr_EXDEV = `enum EXDEV = 18;`;
        static if(is(typeof({ mixin(enumMixinStr_EXDEV); }))) {
            mixin(enumMixinStr_EXDEV);
        }
    }




    static if(!is(typeof(EEXIST))) {
        private enum enumMixinStr_EEXIST = `enum EEXIST = 17;`;
        static if(is(typeof({ mixin(enumMixinStr_EEXIST); }))) {
            mixin(enumMixinStr_EEXIST);
        }
    }




    static if(!is(typeof(EBUSY))) {
        private enum enumMixinStr_EBUSY = `enum EBUSY = 16;`;
        static if(is(typeof({ mixin(enumMixinStr_EBUSY); }))) {
            mixin(enumMixinStr_EBUSY);
        }
    }




    static if(!is(typeof(ENOTBLK))) {
        private enum enumMixinStr_ENOTBLK = `enum ENOTBLK = 15;`;
        static if(is(typeof({ mixin(enumMixinStr_ENOTBLK); }))) {
            mixin(enumMixinStr_ENOTBLK);
        }
    }




    static if(!is(typeof(EFAULT))) {
        private enum enumMixinStr_EFAULT = `enum EFAULT = 14;`;
        static if(is(typeof({ mixin(enumMixinStr_EFAULT); }))) {
            mixin(enumMixinStr_EFAULT);
        }
    }




    static if(!is(typeof(EACCES))) {
        private enum enumMixinStr_EACCES = `enum EACCES = 13;`;
        static if(is(typeof({ mixin(enumMixinStr_EACCES); }))) {
            mixin(enumMixinStr_EACCES);
        }
    }




    static if(!is(typeof(ENOMEM))) {
        private enum enumMixinStr_ENOMEM = `enum ENOMEM = 12;`;
        static if(is(typeof({ mixin(enumMixinStr_ENOMEM); }))) {
            mixin(enumMixinStr_ENOMEM);
        }
    }




    static if(!is(typeof(EAGAIN))) {
        private enum enumMixinStr_EAGAIN = `enum EAGAIN = 11;`;
        static if(is(typeof({ mixin(enumMixinStr_EAGAIN); }))) {
            mixin(enumMixinStr_EAGAIN);
        }
    }




    static if(!is(typeof(ECHILD))) {
        private enum enumMixinStr_ECHILD = `enum ECHILD = 10;`;
        static if(is(typeof({ mixin(enumMixinStr_ECHILD); }))) {
            mixin(enumMixinStr_ECHILD);
        }
    }




    static if(!is(typeof(EBADF))) {
        private enum enumMixinStr_EBADF = `enum EBADF = 9;`;
        static if(is(typeof({ mixin(enumMixinStr_EBADF); }))) {
            mixin(enumMixinStr_EBADF);
        }
    }




    static if(!is(typeof(ENOEXEC))) {
        private enum enumMixinStr_ENOEXEC = `enum ENOEXEC = 8;`;
        static if(is(typeof({ mixin(enumMixinStr_ENOEXEC); }))) {
            mixin(enumMixinStr_ENOEXEC);
        }
    }




    static if(!is(typeof(E2BIG))) {
        private enum enumMixinStr_E2BIG = `enum E2BIG = 7;`;
        static if(is(typeof({ mixin(enumMixinStr_E2BIG); }))) {
            mixin(enumMixinStr_E2BIG);
        }
    }




    static if(!is(typeof(ENXIO))) {
        private enum enumMixinStr_ENXIO = `enum ENXIO = 6;`;
        static if(is(typeof({ mixin(enumMixinStr_ENXIO); }))) {
            mixin(enumMixinStr_ENXIO);
        }
    }




    static if(!is(typeof(EIO))) {
        private enum enumMixinStr_EIO = `enum EIO = 5;`;
        static if(is(typeof({ mixin(enumMixinStr_EIO); }))) {
            mixin(enumMixinStr_EIO);
        }
    }




    static if(!is(typeof(EINTR))) {
        private enum enumMixinStr_EINTR = `enum EINTR = 4;`;
        static if(is(typeof({ mixin(enumMixinStr_EINTR); }))) {
            mixin(enumMixinStr_EINTR);
        }
    }




    static if(!is(typeof(ESRCH))) {
        private enum enumMixinStr_ESRCH = `enum ESRCH = 3;`;
        static if(is(typeof({ mixin(enumMixinStr_ESRCH); }))) {
            mixin(enumMixinStr_ESRCH);
        }
    }




    static if(!is(typeof(ENOENT))) {
        private enum enumMixinStr_ENOENT = `enum ENOENT = 2;`;
        static if(is(typeof({ mixin(enumMixinStr_ENOENT); }))) {
            mixin(enumMixinStr_ENOENT);
        }
    }




    static if(!is(typeof(EPERM))) {
        private enum enumMixinStr_EPERM = `enum EPERM = 1;`;
        static if(is(typeof({ mixin(enumMixinStr_EPERM); }))) {
            mixin(enumMixinStr_EPERM);
        }
    }
    static if(!is(typeof(F_ULOCK))) {
        private enum enumMixinStr_F_ULOCK = `enum F_ULOCK = 0;`;
        static if(is(typeof({ mixin(enumMixinStr_F_ULOCK); }))) {
            mixin(enumMixinStr_F_ULOCK);
        }
    }




    static if(!is(typeof(F_LOCK))) {
        private enum enumMixinStr_F_LOCK = `enum F_LOCK = 1;`;
        static if(is(typeof({ mixin(enumMixinStr_F_LOCK); }))) {
            mixin(enumMixinStr_F_LOCK);
        }
    }




    static if(!is(typeof(F_TLOCK))) {
        private enum enumMixinStr_F_TLOCK = `enum F_TLOCK = 2;`;
        static if(is(typeof({ mixin(enumMixinStr_F_TLOCK); }))) {
            mixin(enumMixinStr_F_TLOCK);
        }
    }




    static if(!is(typeof(F_TEST))) {
        private enum enumMixinStr_F_TEST = `enum F_TEST = 3;`;
        static if(is(typeof({ mixin(enumMixinStr_F_TEST); }))) {
            mixin(enumMixinStr_F_TEST);
        }
    }
    static if(!is(typeof(__GNUC_VA_LIST))) {
        private enum enumMixinStr___GNUC_VA_LIST = `enum __GNUC_VA_LIST = 1;`;
        static if(is(typeof({ mixin(enumMixinStr___GNUC_VA_LIST); }))) {
            mixin(enumMixinStr___GNUC_VA_LIST);
        }
    }






    static if(!is(typeof(NULL))) {
        private enum enumMixinStr_NULL = `enum NULL = ( cast( void * ) 0 );`;
        static if(is(typeof({ mixin(enumMixinStr_NULL); }))) {
            mixin(enumMixinStr_NULL);
        }
    }

}
