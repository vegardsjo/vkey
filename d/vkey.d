import std.stdio;
import std.conv : to;
import core.stdc.stdlib;
import core.time;
import core.thread;
import vkey_c;

const DEBUG = false;
const VIRTUAL_KEYBOARD_NAME = "vkey keyboard";
const FILENAME = "/dev/input/by-id/usb-Apple_Inc._Magic_Keyboard_F0T5407008DGD6LA4-if01-event-kbd";
const SLEEP_TIME = dur!"usecs"(300);

bool caps_pressed = false;
bool ralt_pressed = false;
bool ctrl_key_sent = false;
bool custom_keydown_sent = false;

const int ralt_key = KEY_RIGHTMETA;

libevdev_uinput *create_device(libevdev *orig_dev) {
  int uifd = open("/dev/uinput", O_RDWR);

  if (uifd < 0) {
    stderr.writefln("Could not open uinput device for reading+writing\n");
    return null;
  }

  libevdev *dev = cast(libevdev*)libevdev_new();
  libevdev_set_name(dev, VIRTUAL_KEYBOARD_NAME.ptr);

  writefln("Created device: \"%s\"\n", libevdev_get_name(dev).to!string);
  writefln("Device ID: bus %#x vendor %#x product %#x\n",
           libevdev_get_id_bustype(dev),
           libevdev_get_id_vendor(dev),
           libevdev_get_id_product(dev));

  for (uint code = 0; code < KEY_MAX; code++) {
    if (libevdev_enable_event_code(dev, EV_KEY, code, NULL)) {
      stderr.writefln("Could not enable key %s\n", libevdev_event_code_get_name(EV_KEY, code));
    }
  }

  for (uint code = 0; code < SW_MAX; code++) {
    if (code == SW_RFKILL_ALL) continue;

    if (libevdev_enable_event_code(dev, EV_SW, code, NULL)) {
      stderr.writefln("Could not enable key %s\n", libevdev_event_code_get_name(EV_SW, code));
    }
  }

  for (uint code = 0; code < REL_MAX; code++) {
    if (libevdev_enable_event_code(dev, EV_REL, code, NULL)) {
      stderr.writefln("Could not enable key %s\n", libevdev_event_code_get_name(EV_REL, code));
    }
  }

  libevdev_uinput *uidev;
  if (libevdev_uinput_create_from_device(dev, uifd, &uidev) != 0) {
    stderr.writefln("Could not create new device");
    return null;
  }

  return uidev;
}

void send_custom_key(libevdev_uinput *uidev, int code, int value) {
  libevdev_uinput_write_event(uidev, EV_KEY, code, value);
  libevdev_uinput_write_event(uidev, EV_SYN, SYN_REPORT, 0);
  custom_keydown_sent = value > 0;

}

void send_key(libevdev_uinput *uidev, int code, int value) {
  libevdev_uinput_write_event(uidev, EV_KEY, code, value);
  libevdev_uinput_write_event(uidev, EV_SYN, SYN_REPORT, 0);
}

void main() {
  writeln("VKEY v0.2 (D)");

  libevdev *dev = null;

  int fd = open(FILENAME.ptr, O_RDONLY | O_NONBLOCK);
  int rc = libevdev_new_from_fd(fd, &dev);

  if (rc < 0) {
    stderr.writefln("Failed to init libevdev (%s)\n", strerror(-rc).to!string);
    exit(EXIT_FAILURE);
  }

  writefln("Input device name: \"%s\"\n", libevdev_get_name(dev).to!string);
  writefln("Input device ID: bus %#x vendor %#x product %#x\n",
           libevdev_get_id_bustype(dev),
           libevdev_get_id_vendor(dev),
           libevdev_get_id_product(dev));

  if (libevdev_grab(dev, LIBEVDEV_GRAB)) {
    stderr.writefln("Could not grab device %s\n", FILENAME);
    exit(EXIT_FAILURE);
  }

  libevdev_uinput *uidev = create_device(dev);
  if (!uidev) {
    exit(EXIT_FAILURE);
  }

  do {
    input_event ev;
    rc = libevdev_next_event(dev, LIBEVDEV_READ_FLAG_NORMAL, &ev);
    if (rc == 0) {
      int type = ev.type;
      int code = ev.code;
      int value = ev.value;

      if (type == EV_KEY) {
        if (code == KEY_CAPSLOCK) {
          if (value > 0) {
            caps_pressed = true;
            continue;
          } else {
            caps_pressed = false;

            if (ctrl_key_sent) {
              ctrl_key_sent = false;
              libevdev_uinput_write_event(uidev, type, KEY_LEFTCTRL, 0);
              libevdev_uinput_write_event(uidev, EV_SYN, SYN_REPORT, 0);
            } else {
              libevdev_uinput_write_event(uidev, type, KEY_ESC, 1);
              libevdev_uinput_write_event(uidev, EV_SYN, SYN_REPORT, 0);
              libevdev_uinput_write_event(uidev, type, KEY_ESC, 0);
              libevdev_uinput_write_event(uidev, EV_SYN, SYN_REPORT, 0);
            }

            continue;
          }
        } else {
          if (caps_pressed) {
            if (value == 1) {
              ctrl_key_sent = true;
              libevdev_uinput_write_event(uidev, type, KEY_LEFTCTRL, 1);
              libevdev_uinput_write_event(uidev, EV_SYN, SYN_REPORT, 0);
            }
          }
        }

        if (code == ralt_key) {
          ralt_pressed = !!value;
          continue;
        }
    
        if (code == KEY_ESC) {
          send_key(uidev, KEY_LEFTCTRL, ev.value);
          continue;
        }

        if (code == KEY_LEFTCTRL) {
          send_key(uidev, KEY_LEFTMETA, ev.value);
          continue;
        }

        if (code == KEY_LEFTALT) {
          send_key(uidev, KEY_SYSRQ, ev.value);
          continue;
        }

        if (code == KEY_LEFTMETA) {
          send_key(uidev, KEY_LEFTALT, ev.value);
          continue;
        }

        if (code == KEY_LEFTCTRL) {
          send_key(uidev, KEY_SYSRQ, ev.value);
          continue;
        }

        // Apple keyboard: section to grave/tilde
        /* if (code == KEY_102ND) { */
        /*   send_key(uidev, KEY_GRAVE, ev.value); */
        /*   continue; */
        /* } */
        
        
        // Apple keyboard: less/greater to sysrq (hyper)
        //if (code == KEY_GRAVE) {
        if (code == KEY_102ND) {
          send_key(uidev, KEY_SYSRQ, ev.value);
          continue;
        }

        if (ralt_pressed || custom_keydown_sent) {
          // æ
          if (code == KEY_SEMICOLON) {
            send_custom_key(uidev, KEY_KP0, ev.value);
            continue;
          }

          // ø
          if (code == KEY_O) {
            send_custom_key(uidev, KEY_KP1, ev.value);
            continue;
          }

          // å
          if (code == KEY_A) {
            send_custom_key(uidev, KEY_KP2, ev.value);
            continue;
          }

          // é
          if (code == KEY_E) {
            send_custom_key(uidev, KEY_KP3, ev.value);
            continue;
          }

          // è
          if (code == KEY_W) {
            send_custom_key(uidev, KEY_KP4, ev.value);
            continue;
          }

          // ó
          if (code == KEY_I) {
            send_custom_key(uidev, KEY_KP5, ev.value);
            continue;
          }

          // ô
          if (code == KEY_P) {
            send_custom_key(uidev, KEY_KP6, ev.value);
            continue;
          }

          // ò
          if (code == KEY_U) {
            send_custom_key(uidev, KEY_KP7, ev.value);
            continue;
          }

          // ö
          if (code == KEY_K) {
            send_custom_key(uidev, KEY_KP8, ev.value);
            continue;
          }

          //  😖
          if (code == KEY_DELETE) {
            send_custom_key(uidev, KEY_KP9, ev.value);
            continue;
          }

          if (code == KEY_F10) {
            send_custom_key(uidev, KEY_MUTE, ev.value);
            continue;
          }

          if (code == KEY_F11) {
            send_custom_key(uidev, KEY_VOLUMEDOWN, ev.value);
            continue;
          }

          if (code == KEY_F12) {
            send_custom_key(uidev, KEY_VOLUMEUP, ev.value);
            continue;
          }

          if (code == KEY_Q) {
            exit(EXIT_SUCCESS);
          }
        }
      }

      // Ignore Caps LED
      if (type == EV_LED && code == LED_CAPSL) {
        continue;
      }

      // if (type == EV_KEY) {
      //   debug("%s %s %d\n",
      //         libevdev_event_type_get_name(type),
      //         libevdev_event_code_get_name(type, code),
      //         value);
      // }

      libevdev_uinput_write_event(uidev, type, code, value);
    }

    Thread.sleep(SLEEP_TIME);

  } while(rc == 1 || rc == 0 || rc == -EAGAIN);

  libevdev_uinput_destroy(uidev);
  libevdev_free(dev);

  exit(0);
}
